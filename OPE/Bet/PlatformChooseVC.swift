//
//  PlatformChooseVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/11/29.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class PlatformOptionCell: UICollectionViewCell {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var imgvIcon: UIImageView!
    @IBOutlet weak var lineSeperater: UIView!
    var entry: HYPlayStation.Entry!{
        didSet{
            lbTitle.text = entry.title
            imgvIcon.image = entry.icon
        }
    }
}

class PlatformChooseVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var platform: String!
    var category: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let user = HYAuth.auth.currentUser else {
            categories = []
            self.platforms = [:]
            collectionView.reloadData()
            return
        }
        
        
        var platforms = user.availableCategoryPlatforms
        
        // 平台加上AG電子遊戲
        if let ary = platforms[HYPlayStation.Category.Real], ary.contains(HYPlayStation.Platform.AG){
            if nil == platforms[HYPlayStation.Category.Slot]{
                platforms[HYPlayStation.Category.Slot] = []
            }
            platforms[HYPlayStation.Category.Slot]!.append(HYPlayStation.Platform.AG)
        }
        self.platforms = platforms
        
        categories = user.favorCategoryOrder.filter{(platforms[$0] ?? []).count > 0}
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        cellWidth = CGFloat(Int(collectionView.frame.size.width) / 3)
        numOfRows = cellWidth > 124 ? 3 : 2
        if numOfRows != 3{
            cellWidth = CGFloat(Int(collectionView.frame.size.width) / numOfRows)
        }
        collectionView.reloadData()
    }
    
    // MARK: - Action
    
    var didSelectPlatformHandler: ((Category, Platform) -> ())!
    
    // MARK: - CollectionView
    
    var categories: [Category] = []
    var platforms: [Category: [Platform]] = [:]
    var cellWidth: CGFloat = 0.0
    var numOfRows: Int = 3
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return platforms[categories[section]]!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlatformOptionCell", for: indexPath) as! PlatformOptionCell
        let category = categories[indexPath.section]
        let platform = platforms[category]![indexPath.row]
        cell.entry = HYPlayStation.entries[category]![platform]!
        cell.lineSeperater.isHidden = indexPath.row + 1 % numOfRows == 0 || indexPath.row == platforms[category]!.count - 1
        cell.isSelected = platform == self.platform
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let i = numOfRows - 1
        if indexPath.row % numOfRows == i{
            return CGSize(width: collectionView.frame.size.width - CGFloat(i) * cellWidth, height: 60)
        }else{
            return CGSize(width: cellWidth, height: 60)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let category = categories[indexPath.section]
        let platform = platforms[category]![indexPath.row]
        didSelectPlatformHandler(category, platform)
    }
    
}
