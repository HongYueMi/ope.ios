//
//  BetRecordVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/29.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class BetRecordCell: UITableViewCell{
    
    // MARK: Data
    
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var lbNo: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbAmount: UILabel!
    @IBOutlet weak var lbCashUnit: UILabel!
    @IBOutlet weak var lbStatus: UILabel!
    @IBOutlet weak var viewStatus: UIView!

    @IBOutlet weak var viewLabel: UIView!
    @IBOutlet weak var imgvLabel: UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        viewStatus.layer.borderWidth = 1
    }

    var record: HYBetRecord.Item!{
        didSet{
            
            lbTime.text = record.creatDate?.toString(format: "yyyy-MM-dd HH:mm:ss")
            lbNo.text = record.transSN
            lbTitle.text = record.info
            
            if record.winLoseAmount > 0.0{
                lbAmount.text = NSString(format: "%.2f", record.winLoseAmount) as String
                lbAmount.textColor = #colorLiteral(red: 0.0431372549, green: 0.5607843137, blue: 0.003921568627, alpha: 1)
                lbCashUnit.text = "元"
            }else{
                lbAmount.text = "未中奖"
                lbAmount.textColor = .red
                lbCashUnit.text = nil
            }
            
            lbStatus.text = record.status
            let isCompleted = record.status == "已结算"
            viewStatus.layer.borderColor = isCompleted ? #colorLiteral(red: 0, green: 0.7960784314, blue: 0.7529411765, alpha: 1) : #colorLiteral(red: 0.9058823529, green: 0.1137254902, blue: 0.6039215686, alpha: 1)
            viewLabel.backgroundColor = isCompleted ? #colorLiteral(red: 0, green: 0.7960784314, blue: 0.7529411765, alpha: 1) : #colorLiteral(red: 0.9058823529, green: 0.1137254902, blue: 0.6039215686, alpha: 1)
            lbStatus.textColor = isCompleted ? #colorLiteral(red: 0, green: 0.7960784314, blue: 0.7529411765, alpha: 1) : #colorLiteral(red: 0.9058823529, green: 0.1137254902, blue: 0.6039215686, alpha: 1)
            imgvLabel.image = isCompleted ? #imageLiteral(resourceName: "Record_Indicator_Success") : #imageLiteral(resourceName: "Record_Indicator_Fail")
            
        }
    }
}

class BetRecordVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let DateFormat = "yyyy/MM/dd"
    
    var category: String = HYPlayStation.Category.Sport
    var platform: String = HYPlayStation.Platform.BETCONSTRUCT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem?.title = HYPlayStation.entries[category]?[platform]?.title ?? "平台"
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: HYBetRecord.NotificationName, object: HYBetRecord.center)
        HYBetRecord.center.syncRecords(category: category, platform: platform)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: HYBetRecord.NotificationName, object: HYBetRecord.center)
    }
    
    // MARK: - Action
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PlatformChooseVC{
            vc.platform = platform
            vc.category = category
            vc.didSelectPlatformHandler = { [weak self] category, platform in
                self?.category = category
                self?.platform = platform
                HYBetRecord.center.syncRecords(category: category, platform: platform)
                self?.navigationItem.rightBarButtonItem?.title = HYPlayStation.entries[category]?[platform]?.title ?? "平台"
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    // MARK: - Data
    
    @objc func updateData(){
        self.view.isUserInteractionEnabled = true
        tableData = HYBetRecord.center.records[category]?[platform] ?? []
    }
    
    // MARK: - Table
    
    var tableData: [HYBetRecord.Item] = []{
        didSet{
            tableView.reloadData()
            viewNoData.isHidden = tableData.count > 0
        }
    }
    
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BetRecordCell") as! BetRecordCell
        cell.record = tableData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
}
