//
//  HYLanguage.swift
//  OPE
//
//  Created by rwt113 on 2017/7/13.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import Foundation

//Main.storyboard**********
//登入頁
public class LanLoading {
    
    static let showDialog_CantOpenURL = NSLocalizedString("showDialog_CantOpenURL", tableName: "LanLoading", value: "無法開啟URL", comment: "無法開啟URL");
    
    static let showDialog_IPAddressNo = NSLocalizedString("showDialog_IPAddressNo", tableName: "LanLoading", value: "IP位置不允許", comment: "IP位置不允許");
    static let showDialog_upgradeVersion = NSLocalizedString("LanLoading_showDialog_upgradeVersion", value: "有新版本，是否升級？", comment: "有新版本，是否升級？");
    static let InfoItem_PleaseReDownloadApp = NSLocalizedString("LanLoading_InfoItem_PleaseReDownloadApp", value: "APP版本過舊，請重新下載", comment: "APP版本過舊，請重新下載");
    static let SelectVersionVC_continueBut = NSLocalizedString("LanLoading_SelectVersionVC_continueBut", value: "繼續使用", comment: "繼續使用");
    static let SelectVersionVC_upgradeBut = NSLocalizedString("LanLoading_SelectVersionVC_upgradeBut", value: "升級版本", comment: "升級版本");
    
    static let upgradeVersionVC_upgradeBut = NSLocalizedString("LanLoading_upgradeVersionVC_upgradeBut", value: "升級版本", comment: "升級版本");
}

//主要頁面
public class LanMain {
    
    static let genSignInBtn_Login = NSLocalizedString("genSignInBtn_Login", value: "登入", comment: "登入");
    static let genSignUpBtn_Register = NSLocalizedString("genSignUpBtn_Register", value: "註冊", comment: "註冊");
    
    static let showDialog_PleaseLogin = NSLocalizedString("showDialog_PleaseLogin", value: "請先登入", comment: "請先登入");
    
    static let LowerButton_SportsLive = NSLocalizedString("LowerButton_SportsLive", value: "贊助專題", comment: "贊助專題");
    static let LowerButton_FundManagement = NSLocalizedString("LowerButton_FundManagement", value: "資金管理", comment: "資金管理");
    static let LowerButton_OPE = NSLocalizedString("LowerButton_OPE", value: "OPE", comment: "OPE");
    static let LowerButton_Promotions = NSLocalizedString("LowerButton_Promotions", value: "優惠活動", comment: "優惠活動");
    static let LowerButton_PersonalCenter = NSLocalizedString("LowerButton_PersonalCenter", value: "個人中心", comment: "個人中心");
}

//Promotion.storyboard**********
//PromotionVC.swift
public class LanPromotion {
    
    static let lbDate_ActivityDate = NSLocalizedString("lbDate_ActivityDate", value: "活動日期：", comment: "活動日期：");
    static let lbCount_PeopleParticipate = NSLocalizedString("lbCount_PeopleParticipate", value: "人參與", comment: "人參與");
    static let btnJoin_ParticipateActivities = NSLocalizedString("btnJoin_ParticipateActivities", value: "參加活動", comment: "參加活動");
}

//PromotionListVC.swift
public class LanPromotionCell {
    
    static let lbDate_ActivityDate = NSLocalizedString("LanPromotionCell_lbDate_ActivityDate", value: "截止日期：", comment: "截止日期：");
    static let lbCount_PeopleParticipate = NSLocalizedString("LanPromotionCell_lbCount_PeopleParticipate", value: "人參與", comment: "人參與");
    static let showDialog_ParticipateOk = NSLocalizedString("LanPromotionCell_showDialog_ParticipateOk", value: "參加成功", comment: "參加成功");
    
    static let cell_btnJoinA = NSLocalizedString("LanPromotionCell_cell_btnJoinA", value: "審核中  ", comment: "審核中  ");
    static let cell_btnJoinB = NSLocalizedString("LanPromotionCell_cell_btnJoinB", value: "參加活動   ", comment: "參加活動   ");
    
    static let cell_btnJoinC = NSLocalizedString("LanPromotionCell_cell_btnJoinC", value: "申請成功   ", comment: "申請成功   ");
    static let cell_btnJoinD = NSLocalizedString("LanPromotionCell_cell_btnJoinD", value: "申請失敗   ", comment: "申請失敗   ");
}

//PromotionPageVC.swift
public class LanPromotionPage {
 
    static let ctrlAll_AllActivities = NSLocalizedString("LanPromotionPage_ctrlAll_AllActivities", value: "全部活動", comment: "全部活動");
    static let ctrlSpecial_SpecialPromotion = NSLocalizedString("LanPromotionPage_ctrlSpecial_SpecialPromotion", value: "特別優惠", comment: "特別優惠");
}

//Message.storyboard
//MessageVC.swift
public class LanMessage {
    static let btnRead_NotRead = NSLocalizedString("LanMessage_btnRead_NotRead", value: "未讀取", comment: "未讀取");
    static let btnUnread_HasBeenRead = NSLocalizedString("LanMessage_btnUnread_HasBeenRead", value: "已讀取", comment: "已讀取");
}

//MessageDialogVC.swift
public class LanMessageDialog {
    static let btnRead_NotRead = NSLocalizedString("LanMessage_btnRead_NotRead", value: "未讀取", comment: "未讀取");
}

//Member.storyboard**********
//電話驗證 MemberPhoneEditVC.swift
public class LangMemberPhoneEdit {
    
    static let lbTitle_lan = NSLocalizedString("LangMemberPhoneEdit_lbTitle_lan", value: "請輸入您的手機號碼", comment: "請輸入您的手機號碼");
    static let showDialog_SendVerificationCodeError = NSLocalizedString("LangMemberPhoneEdit_showDialog_SendVerificationCodeError", value: "發送驗證碼失敗！", comment: "發送驗證碼失敗！");
    static let showDialog_PhoneNumberError = NSLocalizedString("LangMemberPhoneEdit_showDialog_PhoneNumberError", value: "手機號碼格式錯誤(僅支持中國大陸手機號碼)", comment: "手機號碼格式錯誤(僅支持中國大陸手機號碼)");
    static let showDialog_CreatPhoneNumberOk = NSLocalizedString("LangMemberPhoneEdit_showDialog_CreatPhoneNumberOk", value: "新手機號碼新增完成，並驗證成功!", comment: "新手機號碼新增完成，並驗證成功!");
    static let showDialog_OldPhoneNumberOk = NSLocalizedString("LangMemberPhoneEdit_showDialog_OldPhoneNumberOk", value: "原手機號碼確認成功，請點擊「確認」驗證新手機號碼", comment: "原手機號碼確認成功，請點擊「確認」驗證新手機號碼");
    static let showDialog_VerificationOk = NSLocalizedString("LangMemberPhoneEdit_showDialog_VerificationOk", value: "恭喜您，驗證成功", comment: "恭喜您，驗證成功");
    static let showDialog_PleaseEnterPhone = NSLocalizedString("LangMemberPhoneEdit_showDialog_PleaseEnterPhone", value: "請輸入手機號碼", comment: "請輸入手機號碼");
    static let showDialog_PleaseEnterVerificationNumber = NSLocalizedString("LangMemberPhoneEdit_showDialog_PleaseEnterVerificationNumber", value: "請輸入驗證碼", comment: "請輸入驗證碼");
    
}

//個人資料 MemberInfoVC.swift
public class LanMemberInfo {
    
    static let showDialog_Error = NSLocalizedString("LanMemberInfo_showDialog_Error", value: "Error", comment: "Error");
    static let showDialog_QQFormatError = NSLocalizedString("LanMemberInfo_showDialog_QQFormatError",  value: "QQ格式不符。", comment: "QQ格式不符。");
    
    static let InfoItem_GameID = NSLocalizedString("LanMemberInfo_InfoItem_GameID", value: "遊戲账號", comment: "遊戲账號");
    static let InfoItem_Name = NSLocalizedString("LanMemberInfo_InfoItem_Name", value: "姓名", comment: "姓名");
    static let InfoItem_Gender = NSLocalizedString("LanMemberInfo_InfoItem_Gender", value: "性別", comment: "性別");
    static let InfoItem_BornDate = NSLocalizedString("LanMemberInfo_InfoItem_BornDate", value: "出生日期", comment: "出生日期");
    static let InfoItem_QQNumber = NSLocalizedString("LanMemberInfo_InfoItem_QQNumber", value: "ＱＱ號碼", comment: "ＱＱ號碼");
    static let InfoItem_Province = NSLocalizedString("LanMemberInfo_InfoItem_Province", value: "所在省份", comment: "所在省份");
    static let InfoItem_Address = NSLocalizedString("LanMemberInfo_InfoItem_Address", value: "詳細地址", comment: "詳細地址");
    
}

//安全設置 SafeSettingVC.swift
public class LanSafeSetting {
    
    static let showDialog_Error = NSLocalizedString("LanSafeSetting_showDialog_Error", value: "Error", comment: "Error");
    
    static let InfoItem_Email = NSLocalizedString("LanSafeSetting_InfoItem_Email", value: "電子郵件", comment: "電子郵件");
    static let InfoItem_PhoneNumber = NSLocalizedString("LanSafeSetting_InfoItem_PhoneNumber", value: "手機號碼", comment: "手機號碼");
    
    static let showDialog_PleaseLoginEmail = NSLocalizedString("LanSafeSetting_showDialog_PleaseLoginEmail", value: "請登入您的電子郵件進行驗證", comment: "請登入您的電子郵件進行驗證");
    
}

//電子郵件驗證 EmailVerifyVC.swift
public class LanEmailVerify {
    
    static let showDialog_PleaseEnterEmail = NSLocalizedString("LanEmailVerify_showDialog_PleaseEnterEmail", value: "請輸入電子郵箱", comment: "請輸入電子郵箱");
    static let showDialog_EmailError = NSLocalizedString("LanEmailVerify_showDialog_EmailError", value: "電子郵箱格式有誤", comment: "電子郵箱格式有誤");
    static let showDialog_PleaseLoginEmail = NSLocalizedString("LanEmailVerify_showDialog_PleaseLoginEmail", value: "請登入您的電子信箱進行驗證", comment: "請登入您的電子信箱進行驗證");
    static let showDialog_EmailChangeOk = NSLocalizedString("LanEmailVerify_showDialog_EmailChangeOk", value: "電子信箱修改成功", comment: "電子信箱修改成功");
}

//修改账號密碼 PasswordEditVC .swift
public class LanPasswordEdit {
    
    static let showDialog_NoNull = NSLocalizedString("LanPasswordEdit_showDialog_NoNull", value: "不能為空！(6-12位必須含有字母和數字的組合)", comment: "不能為空！(6-12位必須含有字母和數字的組合)");
    static let showDialog_FormatError = NSLocalizedString("LanPasswordEdit_showDialog_FormatError", value: "格式有誤！(6-12位必須含有字母和數字的組合)", comment: "格式有誤！(6-12位必須含有字母和數字的組合)");
    static let showDialog_ChangeOk = NSLocalizedString("LanPasswordEdit_showDialog_ChangeOk", value: "修改成功", comment: "修改成功");
    static let showDialog_CheckPasswordNotSame = NSLocalizedString("LanPasswordEdit_showDialog_CheckPasswordNotSame", value: "兩次輸入密碼不一致", comment: "兩次輸入密碼不一致");
    
}

//遊戲平台餘額 WalletBalance .swift
public class LanWalletBalance {
    
    static let btn_Sorting = NSLocalizedString("LanWalletBalance_btn_Sorting", value: "排序", comment: "排序");
    static let lbTitle_Sorting = NSLocalizedString("LanWalletBalance_lbTitle_Sorting", value: "錢包", comment: "錢包");
}


//Game.storyboard**********
//GameVC .swift
public class LanGame {
    
    static let showDialog_PleaseLogin = NSLocalizedString("LanGame_showDialog_PleaseLogin", value: "請先登入", comment: "請先登入");
    static let showDialog_SystemMaintenance = NSLocalizedString("LanGame_showDialog_SystemMaintenance", value: "系統維護中，當前無法進入遊戲，請關閉本窗口。", comment: "系統維護中，當前無法進入遊戲，請關閉本窗口。");
    
    static let InfoItem_QuickTransfer = NSLocalizedString("LanGame_InfoItem_QuickTransfer", value: "快速轉账", comment: "快速轉账");
    
    static let showDialog_PleaseEnterTransferAmount = NSLocalizedString("LanGame_showDialog_PleaseEnterTransferAmount", value: "請輸入轉账金額", comment: "請輸入轉账金額");
    static let showDialog_Error = NSLocalizedString("LanGame_showDialog_Error", value: "Error", comment: "Error");

}

public struct Language{
    struct Game {
        struct Category {
            static let All = NSLocalizedString("Category_All", tableName: "Localiable Game", value: "全部遊戲", comment: "全部遊戲")
            static let Progress = NSLocalizedString("Category_Progress", tableName: "Localiable Game", value: "彩金遊戲", comment: "彩金遊戲")
            static let TableGame = NSLocalizedString("Category_TableGame", tableName: "Localiable Game", value: "賭桌遊戲", comment: "賭桌遊戲")
            static let TableGame_PT = NSLocalizedString("Category_TableGame_PT", tableName: "Localiable Game", value: "牌桌＆牌", comment: "牌桌＆牌")
            static let Porker = NSLocalizedString("Category_Porker", tableName: "Localiable Game", value: "電子撲克", comment: "電子撲克")
            static let CardGame = NSLocalizedString("Category_CardGame", tableName: "Localiable Game", value: "紙牌遊戲", comment: "紙牌遊戲")

            static let Arcades = NSLocalizedString("Category_Arcades", tableName: "Localiable Game", value: "街機遊戲", comment: "彩金遊戲")
            static let Slot = NSLocalizedString("Category_Slot", tableName: "Localiable Game", value: "老虎機", comment: "老虎機")
            static let Slot_PT = NSLocalizedString("Category_Slot_PT", tableName: "Localiable Game", value: "吃角子老虎機", comment: "吃角子老虎機")
            
            static let Slot_HOT = NSLocalizedString("Category_Slot_HOT", tableName: "Localiable Game", value: "熱門拉霸機", comment: "熱門拉霸機")
            static let Slot_Classic = NSLocalizedString("Category_Slot_Classic", tableName: "Localiable Game", value: "經典電子遊戲", comment: "經典電子遊戲")
            static let Scratch = NSLocalizedString("Category_Scratch", tableName: "Localiable Game", value: "刮刮樂遊戲", comment: "刮刮樂遊戲")
            static let Scratch_PT = NSLocalizedString("Category_Scratch_PT", tableName: "Localiable Game", value: "刮開", comment: "刮開")
            static let Other = NSLocalizedString("Category_Other", tableName: "Localiable Game", value: "其他遊戲", comment: "其他遊戲")
        }
    }
}

//Record.storyboard
//RecordVC .swift
public class LanRecord {
    static let btnDeposit_Recharge = NSLocalizedString("LanRecord_btnDeposit_Recharge", value: "充值", comment: "充值");
    static let btnWithdrawal_Withdraw = NSLocalizedString("LanRecord_btnWithdrawal_Withdraw", value: "提現", comment: "提現");
    static let btnTransfer_Transfer = NSLocalizedString("LanRecord_btnTransfer_Transfer", value: "轉账", comment: "轉账");
    static let btnPromotion_preferential = NSLocalizedString("LanRecord_btnPromotion_preferential", value: "優惠", comment: "優惠");
}

//Func.storyboard
//DepositVC .swift
public class LanDeposit {
    
    static let onlinePayment = NSLocalizedString("LanDeposit_onlinePayment", value: "在線充值", comment: "在線充值");
    
    static let manual = NSLocalizedString("LanDeposit_manual", value: "轉账", comment: "轉账");
    
    static let aliPayment = NSLocalizedString("LanDeposit_aliPayment", value: "支付寶", comment: "支付寶");
    
    static let weChatPayment = NSLocalizedString("LanDeposit_weChatPayment", value: "微信支付", comment: "微信支付");
    
    static let showDialog_DepositAgain = NSLocalizedString("LanDeposit_showDialog_DepositAgain", value: "本次存款申請狀態已經提交客服審核，您可以再次存款！", comment: "本次存款申請狀態已經提交客服審核，您可以再次存款！");
    
}

// OnlineDepositVC .swift
public class LanOnlineDeposit {
    
    static let showDialog_cashNoNull = NSLocalizedString("LanOnlineDeposit_showDialog_cashNoNull", value: "充值金額不可為空，只能輸入整數", comment: "充值金額不可為空，只能輸入整數");
//    static let showDialog_SingleUpperLimit = NSLocalizedString("LanOnlineDeposit_showDialog_SingleUpperLimit", value: "單筆提現金額最低100元，上限30,000元!", comment: "單筆提現金額最低100元，上限30,000元!");
    static let showDialog_InstallApp = NSLocalizedString("LanOnlineDeposit_showDialog_InstallApp", value: "請先安裝微信APP", comment: "請先安裝微信APP");
    
    static let lbSingle_A = NSLocalizedString("LanOnlineDeposit_lbSingle_A", value: "單筆存款最低", comment: "單筆存款最低");
    static let lbSingle_B = NSLocalizedString("LanOnlineDeposit_lbSingle_B", value: "元，上限", comment: "元，上限");
    static let lbSingle_C = NSLocalizedString("LanOnlineDeposit_lbSingle_C", value: "元", comment: "元");
    
}

// DepositWebBankVC .swift
public class LanDepositWebBank {
    static let showDialog_selectBank = NSLocalizedString("LanDepositWebBank_showDialog_selectBank", value: "請選擇銀行", comment: "請選擇銀行");
    static let lbSingle_A = NSLocalizedString("LanDepositWebBank_lbSingle_A", value: "單筆存款最低", comment: "單筆存款最低");
    static let lbSingle_B = NSLocalizedString("LanDepositWebBank_lbSingle_B", value: "元，上限", comment: "元，上限");
    static let lbSingle_C = NSLocalizedString("LanDepositWebBank_lbSingle_C", value: "元", comment: "元");
    
}

// BankCardAddSecurityCheckVC .swift
public class LanBankCardAddSecurityCheck {
    static let showDialog_passwdError = NSLocalizedString("LanBankCardAddSecurityCheck_showDialog_passwdError", value: "輸入密碼有錯誤！", comment: "輸入密碼有錯誤！");
}

// TransferVC .swift
public class LanTransfer {
    static let showDialog_AlertA = NSLocalizedString("LanTransfer_showDialog_AlertA", value: "請選擇轉出平台！", comment: "請選擇轉出平台！");
    static let showDialog_AlertB = NSLocalizedString("LanTransfer_showDialog_AlertB", value: "請選擇轉入平台！", comment: "請選擇轉入平台！");
    static let showDialog_AlertC = NSLocalizedString("LanTransfer_showDialog_AlertC", value: "不能對同一個账戶進行操作！", comment: "不能對同一個账戶進行操作！");
    static let showDialog_AlertD = NSLocalizedString("LanTransfer_showDialog_AlertD", value: "無法取得轉出平台餘額！", comment: "無法取得轉出平台餘額！");
    static let showDialog_AlertE = NSLocalizedString("LanTransfer_showDialog_AlertE", value: "無法取得轉入平台餘額！", comment: "無法取得轉入平台餘額！");
    static let showDialog_AlertF = NSLocalizedString("LanTransfer_showDialog_AlertF", value: "金額不能為空！", comment: "金額不能為空！");
    static let showDialog_AlertG = NSLocalizedString("LanTransfer_showDialog_AlertG", value: "金額格式有誤", comment: "金額格式有誤");
    static let showDialog_AlertH = NSLocalizedString("LanTransfer_showDialog_AlertH", value: "金額不能小於0！", comment: "金額不能小於0！");
    static let showDialog_AlertI = NSLocalizedString("LanTransfer_showDialog_AlertI", value: "轉账金額不能大於平台餘額！", comment: "轉账金額不能大於平台餘額！");
    
}

// WithdrawalBankCardAddVC .swift
public class LanWithdrawalBankCardAdd {
    static let InfoItem_PleaseSelect = NSLocalizedString("LanWithdrawalBankCardAdd_InfoItem_PleaseSelect", value: "選擇", comment: "選擇");
    
    static let showDialog_PleaseSelect = NSLocalizedString("LanWithdrawalBankCardAdd_showDialog_PleaseSelect", value: "請選擇", comment: "請選擇");
    static let showDialog_NoNull = NSLocalizedString("LanWithdrawalBankCardAdd_showDialog_NoNull", value: "不能為空！", comment: "不能為空！");
    static let showDialog_FormatError = NSLocalizedString("LanWithdrawalBankCardAdd_showDialog_FormatError", value: "格式有誤！", comment: "格式有誤！");
}

// static let xxxx = NSLocalizedString("xxxx", value: "xxxx", comment: "xxxx");

// WithdrawalBankPasswordVC .swift
public class LanWithdrawalBankPassword {
    static let showDialog_SuccessfullyModified  = NSLocalizedString("LanWithdrawalBankPassword_showDialog_SuccessfullyModified", value: "修改成功", comment: "修改成功");
}

// WithdrawalVC .swift
public class LanWithdrawal {
    static let showDialog_AlertA = NSLocalizedString("LanWithdrawal_showDialog_AlertA", value: "資金密碼不能為空！(6-12位必須含有字母和數字的組合)", comment: "資金密碼不能為空！(6-12位必須含有字母和數字的組合)");
    static let showDialog_AlertB = NSLocalizedString("LanWithdrawal_showDialog_AlertB", value: "提現金額不能為空!", comment: "提現金額不能為空!");
    static let showDialog_AlertC = NSLocalizedString("LanWithdrawal_showDialog_AlertC", value: "單筆提現金額最低100元，上限50,000元!", comment: "單筆提現金額最低100元，上限50,000元!");
}

// BetRecordVC .swift
public class LanBetRecord {
    static let InfoItem_SelectStartDate = NSLocalizedString("LanBetRecord_InfoItem_selectStartDate", value: "請選擇開始日期", comment: "請選擇開始日期");
    static let InfoItem_SelectEndDate = NSLocalizedString("LanBetRecord_InfoItem_SelectEndDate", value: "請選擇結束日期", comment: "請選擇結束日期");
    static let InfoItem_SelectPlatform = NSLocalizedString("LanBetRecord_InfoItem_SelectPlatform", value: "請選擇平台", comment: "請選擇平台");
}

//RecordListVC .swift
public class LanRecordList {
    static let lbTitle_Bank = NSLocalizedString("LanRecordList_lbTitle_Bank", value: "銀行卡號：", comment: "銀行卡號：");
}

//DepositManualPayVC .swift
public class LanDepositManualPay {
//    static let tvPrecautions = NSLocalizedString("LanDepositManualPay_tvPrecautions", value: "注意事項：\n1. 單筆存款最低100元，上限30000元。 \n2. 在匯款的“附言”或“用途”等處填寫附言編碼即可秒速到賬。 \n3. 該收款賬戶僅支持銀行網銀轉賬，禁止使用支付寶轉入，若使用支付寶轉入而導致金額出現問題均由個人承擔。 \n4. 如充值後未到賬，請聯繫在線客服。", comment: "注意事項：\n1. 單筆存款最低100元，上限30000元。 \n2. 在匯款的“附言”或“用途”等處填寫附言編碼即可秒速到賬。 \n3. 該收款賬戶僅支持銀行網銀轉賬，禁止使用支付寶轉入，若使用支付寶轉入而導致金額出現問題均由個人承擔。 \n4. 如充值後未到賬，請聯繫在線客服。");
    
    static let navigationItem_title = NSLocalizedString("LanDepositManualPay_navigationItem_title", value: "轉账", comment: "轉账")
    
    static let tvPrecautionsA = NSLocalizedString("LanDepositManualPay_tvPrecautionsA", value: "注意事項：\n1. 單筆存款最低", comment: "注意事項：\n1. 單筆存款最低")
    static let tvPrecautionsB = NSLocalizedString("LanDepositManualPay_tvPrecautionsB", value: "元，上限", comment: "元，上限")
    static let tvPrecautionsC = NSLocalizedString("LanDepositManualPay_tvPrecautionsC", value: "元。 \n2. 在匯款的“附言”或“用途”等處填寫附言編碼即可秒速到賬。 \n3. 該收款賬戶僅支持銀行網銀轉賬，禁止使用支付寶轉入，若使用支付寶轉入而導致金額出現問題均由個人承擔。 \n4. 如充值後未到賬，請聯繫在線客服。", comment: "元。 \n2. 在匯款的“附言”或“用途”等處填寫附言編碼即可秒速到賬。 \n3. 該收款賬戶僅支持銀行網銀轉賬，禁止使用支付寶轉入，若使用支付寶轉入而導致金額出現問題均由個人承擔。 \n4. 如充值後未到賬，請聯繫在線客服。")
}

//DepositTransferPayVC .swift
public class LanDepositTransferPay {
    static let tvRemarks = NSLocalizedString("LanDepositTransferPay_tvRemarks", value: "如使用支付寶，請填寫支付寶账號", comment: "如使用支付寶，請填寫支付寶账號");
    static let showDialog_cashNoNull = NSLocalizedString("LanDepositTransferPay_showDialog_cashNoNull", value: "支付金額不可為空，只能輸入整數", comment: "支付金額不可為空，只能輸入整數");
    static let showDialog_SingleUpperLimit = NSLocalizedString("LanDepositTransferPay_showDialog_SingleUpperLimit", value: "單筆支付金額最低100元，上限30,000元!", comment: "單筆支付金額最低100元，上限30,000元!");
    
    static let showDialog_tfAccountA = NSLocalizedString("LanDepositTransferPay_showDialog_tfAccountA", value: "支付账號不可為空", comment: "支付账號不可為空")
    static let showDialog_tfAccountB = NSLocalizedString("LanDepositTransferPay_showDialog_tfAccountB", value: "支付账號格式錯誤", comment: "支付账號格式錯誤")
    static let showDialog_tfName = NSLocalizedString("LanDepositTransferPay_showDialog_tfName", value: "支付人姓名不可為空", comment: "支付人姓名不可為空")
    static let showDialog_lbPayBank = NSLocalizedString("LanDepositTransferPay_showDialog_lbPayBank", value: "請選擇銀行", comment: "請選擇銀行")
    
    static let tvPrecautionsA = NSLocalizedString("LanDepositTransferPay_tvPrecautionsA", value: "注意事項：\n1. 單筆存款最低", comment: "注意事項：\n1. 單筆存款最低")
    static let tvPrecautionsB = NSLocalizedString("LanDepositTransferPay_tvPrecautionsB", value: "元，上限", comment: "元，上限")
    static let tvPrecautionsC = NSLocalizedString("LanDepositTransferPay_tvPrecautionsC", value: "元。 \n2. 在匯款的“附言”或“用途”等處填寫附言編碼即可秒速到賬。 \n3. 該收款賬戶僅支持銀行網銀轉賬，禁止使用支付寶轉入，若使用支付寶轉入而導致金額出現問題均由個人承擔。 \n4. 如充值後未到賬，請聯繫在線客服。", comment: "元。 \n2. 在匯款的“附言”或“用途”等處填寫附言編碼即可秒速到賬。 \n3. 該收款賬戶僅支持銀行網銀轉賬，禁止使用支付寶轉入，若使用支付寶轉入而導致金額出現問題均由個人承擔。 \n4. 如充值後未到賬，請聯繫在線客服。")
    
}

























