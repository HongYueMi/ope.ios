//
//  NewUserDetailedStreetVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/30.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class NewUserDetailedStreetVC: UIEditVC {

    @IBOutlet weak var btNext: UIBarButtonItem!
    
    @IBOutlet weak var tvUserDetailedStreet: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if HYAuth.auth.currentUser!.address != nil {
            self.navigationItem.title = "修改详细街道"
            tvUserDetailedStreet.text = HYAuth.auth.currentUser!.address;
        }else{
            tvUserDetailedStreet.text = "请输入您的街道地址"
            tvUserDetailedStreet.textColor = UIColor.lightGray
        }
        tvUserDetailedStreet.becomeFirstResponder()
        
        tvUserDetailedStreet.selectedTextRange = tvUserDetailedStreet.textRange(from: tvUserDetailedStreet.beginningOfDocument, to: tvUserDetailedStreet.beginningOfDocument)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //右上方OK按鈕
    @IBAction func didBtNextClick(_ sender: UIBarButtonItem) {
        checkTextInput();
    }
    
    //檢查文字輸入
    func checkTextInput(){
        if let chk = tvUserDetailedStreet.text , chk == "" || chk == "请输入您的街道地址" {
            showGeneralAlert(.alert_info,lbTextA:"详细街道格式有误"){ value in }
            return
        }
        
        newUserNameFunc(["Address":tvUserDetailedStreet.text!]);
    }

    //更新UserData
    func newUserNameFunc(_ info: [String: Any]){
        self.newLoadingScreenView.showActivityIndicator(uiView: self.view);
        HYAuth.auth.currentUser!.editInfo(withInfos: info) {
            [weak self] error in
            self?.newLoadingScreenView.hideActivityIndicator(uiView: self!.view);
            
            if nil != error {
                let outMsg = error!.localizedDescription;
                self?.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
            }
            else{
                HYAuth.auth.currentUser!.update { error in
                    if let error = error{
                        let outMsg = error.localizedDescription;
                        self?.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
                    } else{
                        self?.showGeneralAlert(.alert_ok,lbTextA:"设置成功"){ value in
                            if value == 1 {
                                _ = self?.navigationController?.popViewController(animated: true);
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText = textView.text as NSString?
        let updatedText = currentText?.replacingCharacters(in: range, with: text)
        
        if (updatedText?.isEmpty)! {
            
            textView.text = "请输入您的街道地址"
            textView.textColor = UIColor.lightGray
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            
            return false
        } else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
        return true
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if self.view.window != nil {
            if textView.textColor == UIColor.lightGray {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
    }
    
    
    
}
































