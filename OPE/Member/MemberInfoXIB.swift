//
//  MemberInfoXIB.swift
//  OPE
//
//  Created by rwt113 on 2017/11/29.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//  定義XIB用的Class

import UIKit

//資料選擇用
class SelectPickerDataXIB: EditUIView {
    
    @IBOutlet weak var pickerView: UIView!
    
    @IBOutlet weak var btDone: UIButton!            //確定按鈕
    @IBOutlet weak var btClose: UIButton!           //關閉按鈕
    
    @IBOutlet weak var lbTitle: UILabel!            //標題
    
    @IBOutlet weak var lbContentTitle: UILabel!     //選擇標題
    
    @IBOutlet weak var pvSelect: UIPickerView!      //選擇Picker
    
    override func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self));
        let nib = UINib(nibName: "SelectPickerDataXIB", bundle: bundle);
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView;
        view.frame = bounds;
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight];
        self.addSubview(view);
    }
    
    
}


//時間日期選擇用
class SelectDatePickerXIB: EditUIView {
    
    @IBOutlet weak var btDone: UIButton!            //確定按鈕
    @IBOutlet weak var btClose: UIButton!           //關閉按鈕
    
    @IBOutlet weak var pdSelect: UIDatePicker!
    
    override func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self));
        let nib = UINib(nibName: "SelectDatePickerXIB", bundle: bundle);
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView;
        view.frame = bounds;
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight];
        self.addSubview(view);
    }
    
    
}

//繼承UIDatePicker 修改字顏色
class ColoredDatePicker: UIDatePicker {
    var changed = false
    override func addSubview(_ view: UIView) {
        if !changed {
            changed = true
            self.setValue(UIColor(red: 0/255, green: 203/255, blue: 192/255, alpha: 1), forKey: "textColor")
        }
        super.addSubview(view)
    }
}






