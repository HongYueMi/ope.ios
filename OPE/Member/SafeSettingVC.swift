//
//  SafeSettingVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/13.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class SafeSettingVC: UIViewController {

    @IBOutlet weak var lbEmail: UILabel!
    @IBOutlet weak var lbPhone: UILabel!
    @IBOutlet weak var btnEmailVerify: UIButton!
    @IBOutlet weak var btnPhoneEdit: UIButton!
    @IBOutlet weak var btnPhoneVerify: UIButton!

    @IBOutlet weak var btnBankPasswordAdd: UIButton!
    @IBOutlet weak var btnBankPasswordEdit: UIButton!
    @IBOutlet weak var btnPasswordEdit: UIButton!
    
    @IBOutlet weak var btnGuestureAdd: UIButton!
    @IBOutlet weak var btnGuestureEdit: UIButton!
    @IBOutlet weak var switchGesture: UISwitch!
    
    @IBOutlet weak var lbCash: UILabel!; //資金密碼
    @IBOutlet weak var lbPassword: UILabel!; //账號密碼
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HYAuth.auth.currentUser?.update{[weak self] error in
            if nil != error{
                self?.showGeneralAlert(.alert_error, lbTextA:error?.localizedDescription ?? "Error"){ value in }
            }else if let user = HYAuth.auth.currentUser{
                self?.updateData(withUser: user)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateData(withUser: HYAuth.auth.currentUser!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddGesturePassword"{
            let vc = segue.destination as! GesturePasswordVC
            vc.useCase = .add
        }
        else if segue.identifier == "EditGesturePassword"{
            let vc = segue.destination as! GesturePasswordVC
            vc.useCase = .modify
        }
        else if segue.identifier == "OnGesturePassword"{
            let swtch = sender as! UISwitch
            let vc = segue.destination as! GesturePasswordVC
            vc.useCase = swtch.isOn ? .add : .remove
        }
    }
    
    // 電子郵件
    
    func didBtnEmailEditClick(btn: UIButton){
//        if user.isEmailVerified{
//            let info = InfoItem(title: LanSafeSetting.InfoItem_Email, info: user.email, type: .email, key: "Email")
//            showEditMemberDataDialog(withInfo: info)
//        }
//        else{
//            showGeneralAlert(.alert_info, lbTextA:"\(user.name)，请登入您的电子邮件进行验证"){ value in }
//        }
    }
    
    // 手機號碼
    
    func didBtnPhoneEditClick(btn: UIButton){
//        if user.isPhoneVerified{
//            let info = InfoItem(title: LanSafeSetting.InfoItem_PhoneNumber, info: user.phone, type: .phone, key: "CellPhone")
//            showEditMemberDataDialog(withInfo: info)
//        }
//        else{
//            showGeneralAlert(.alert_info, lbTextA:"\(user.name)，請登入您的電子郵件進行驗證"){ value in }
//        }
    }
        
    // MARK: - Data
    
    var user: HYUser!
    
    let Word_UnVerify = "未驗證"
    let Word_Edit = "修改"
    func updateData(withUser user: HYUser){
        self.user = user
        lbEmail.text = user.secretEmail
        lbPhone.text = user.secretPhone
        
        btnEmailVerify.isHidden = user.isEmailVerified
        
        btnPhoneEdit.isHidden = !user.isPhoneVerified
        btnPhoneVerify.isHidden = !btnPhoneEdit.isHidden
        
        btnBankPasswordAdd.isHidden = user.isBankPasswordSet
        btnBankPasswordEdit.isHidden = !btnBankPasswordAdd.isHidden
        
        switchGesture.isOn = user.isGestureOn
        btnGuestureAdd.isHidden = user.isGestureOn
        btnGuestureEdit.isHidden = !user.isGestureOn
        
        if user.isBankPasswordSet == true {
            lbCash.text! = "******";
        }else{
            lbCash.text! = "";
        }
        
    }
    
    func showEditMemberDataDialog(withInfo info: InfoItem){
//        let dialog = DialogVC.dialog(withInfo: info){ [weak self] in
//            self?.updateUserData([info.key!: info.info ?? ""])
//        }
//        present(dialog, animated: true){
//            self.view.isUserInteractionEnabled = true
//        }
    }
    
    func updateUserData(_ info: [String: Any]){
        
        HYMember.shared.editMember(withInfos: info){
            [weak self] user, error in
            if nil != error {
                self?.dismiss(animated: true){
                    self?.showGeneralAlert(.alert_error, lbTextA:error?.localizedDescription ?? "Error"){ value in }
                }
            }
            else{
                self?.updateData(withUser: HYAuth.auth.currentUser!)
                self?.dismiss(animated: true)
            }
        }
    }
    
}
