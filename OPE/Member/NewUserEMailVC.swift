//
//  NewUserEMailVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/29.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class NewUserEMailVC: UIEditVC {

    @IBOutlet weak var btNext: UIButton!
    
    @IBOutlet weak var tfUserEMail: UITextField!
    
    @IBOutlet weak var btDelete: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btNext.layer.borderColor = UIColor(red: 0/255, green: 203/255, blue: 192/255, alpha: 1).cgColor; //邊框顏色1
        btNext.layer.borderWidth = 1.0; //邊框大小
        btNext.layer.masksToBounds = true;
        btNext.layer.cornerRadius = 25.0;
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //下一步OK按鈕
    @IBAction func didBtNextClick(_ sender: UIButton) {
        checkTextInput();
    }
    
    //聽說這個叫刪除
    @IBAction func didBtDeleteClick(_ sender: UIButton) {
        tfUserEMail.text = nil;
    }
    
    //檢查文字輸入
    func checkTextInput(){
        guard
            let value = tfUserEMail.text?.notEmptyValue
            , HYRegex.Email.match(input: value)
            else
        {
            showGeneralAlert(.alert_info,lbTextA:"电子邮件格式有误"){ value in }
            return
        }
        
        updateUserData(tfUserEMail.text!)
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        if textField == tfUserEMail{
            guard
                let value = tfUserEMail.text?.notEmptyValue
                , HYRegex.Email.match(input: value)
                else
            {
                showGeneralAlert(.alert_info,lbTextA:"电子邮件格式有误"){ value in }
                return
            }
        }
        
    }
    
    //更新UserData
    func updateUserData(_ info: String){
        var outURL = "";    //https://www.ope88.com/
        for (k,v) in HYDatas.mailList {
            let words = info.components(separatedBy: "@");
            if words[1] == k {
                outURL = v;
            }
        }
        
        HYAuth.auth.currentUser!.editEmail(email: info) {
            [weak self] error in
            if nil != error {
                let outMsg = error!.localizedDescription;
                self?.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
            }
            else{
                self?.showTwoSelectAlert(.alert_mail,lbTextA:"新增邮箱成功，是否对电子邮箱进行验证？"){ okValue, noValue in
                    if okValue == 1 , noValue == 0 {
                        if let url = URL(string: outURL), UIApplication.shared.canOpenURL(url){
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil);
                            } else {
                                UIApplication.shared.openURL(url);
                            }
                        }else{
//                            let actionA = UIAlertAction(title: "关闭",style: .default,handler: { action in
//                                _ = self?.navigationController?.popViewController(animated: true);
//                            });
//                            self?.alertActionView(message:"无法开启网址", loopAction:[actionA]);
                        }
                    }else{
                        _ = self?.navigationController?.popViewController(animated: true);
                    }
                }

            }
        }
        
    }
    
}



























