//
//  MemberPhoneEditVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/13.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class MemberPhoneEditVC: UIEditVC {

    var isNew: Bool = false
    
    @IBOutlet weak var lbTitle: UILabel?
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfVerify: UITextField!
    @IBOutlet weak var btnGetVerifyCode: UIButton!
    @IBOutlet weak var btnNext: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if HYAuth.auth.currentUser!.isPhoneVerified{ //修改手機 isNew = 1
            btnNext?.isHidden = false
        }
        else{   //第一次驗證手機 isNew = 0
            lbTitle?.text = LangMemberPhoneEdit.lbTitle_lan;
            btnNext?.isHidden = true
        }
        // 元手機號碼確認成功，請點擊「確認」驗證新手機號碼
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Count
    
    @IBOutlet weak var lbCount: UILabel!
    weak var timer: Timer?
    var count: Int = 60
    func startCounting(){
        timer?.invalidate()
        btnGetVerifyCode.isEnabled = false
        count = 60
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCount), userInfo: nil, repeats: true)
    }
    
    @objc func updateCount(){
        lbCount.text = "\(count)"
        if 0 == count{
            timer?.invalidate()
            btnGetVerifyCode.isEnabled = true
        }else{
            count -= 1
        }
    }
    
    // MARK: - Action
    //獲取驗證碼
    @IBAction func didBtnGetVerifyCodeClick(btn: UIButton){
        hideKeyboard()
        guard checkPhoneInput() else {
            return
        }
        view.isUserInteractionEnabled = false
        HYAuth.auth.currentUser!.sendVerifyCode(toPhone: tfPhone.text!){ [weak self] error in
            if let _ = error{
                self?.showGeneralAlert(.alert_error, lbTextA:"发送验证码失败！"){ value in
                    self?.view.isUserInteractionEnabled = true
                }
            }
            else{
                self?.view.isUserInteractionEnabled = true
                self?.startCounting()
            }
        }
    }

    @IBAction func didBtnEnterClick(btn: UIButton?){ // 驗證
        
        guard checkPhoneInput(), checkVerifyInput() else {
            return
        }
        view.isUserInteractionEnabled = false

        let isFirstTimeVerify = HYAuth.auth.currentUser!.isPhoneVerified
        HYAuth.auth.currentUser!.verifyPhone(phone: tfPhone.text!, verifyCode: tfVerify.text!){ [weak self] error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
        if value == 1 {
            self?.view.isUserInteractionEnabled = true
        }
     }
            }
            else{
                if isFirstTimeVerify{
                    if self!.isNew{ // 新增手機號碼
                        self?.showGeneralAlert(.alert_info, lbTextA:"新手机号码新增完成，并验证成功！"){ value in
                            let vc = self!.navigationController!.viewControllers[1]
                            self?.navigationController?.popToViewController(vc, animated: true)
                        }
                    }else{ // 驗證完跳頁
                        self?.showGeneralAlert(.alert_info, lbTextA:"原手机号码确认成功，请点击「确认」验证新手机号码"){ value in
                            self?.performSegue(withIdentifier: "EnterNewPhone", sender: nil)
                        }
                    }
                }
                else{ //第一次驗證手機
                    self?.showGeneralAlert(.alert_info, lbTextA:"恭喜您，验证成功"){ value in
                        let vc = self!.navigationController!.viewControllers[1]
                        self?.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EnterNewPhone"{
            let vc = segue.destination as! MemberPhoneEditVC
            vc.isNew = true
        }
    }
    
    // MARK: - Input
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfVerify{
            didBtnEnterClick(btn: nil)
        }
        return true
    }
    
    func checkPhoneInput() -> Bool{
        guard let phone = tfPhone.text?.notEmptyValue else {
            showGeneralAlert(.alert_info, lbTextA:"请输入手机号码"){ value in }
            return false
        }
        guard HYRegex.Phone.match(input: phone)
            else
        {
            showGeneralAlert(.alert_info, lbTextA:"手机号码格式错误（仅支持中国大陆手机号码）"){ value in }
            return false
        }
        return true
    }
    
    func checkVerifyInput() -> Bool{
        guard let _ = tfVerify.text?.notEmptyValue else {
            showGeneralAlert(.alert_info, lbTextA:"请输入验证码"){ value in }
            return false
        }
        return true
    }

}
