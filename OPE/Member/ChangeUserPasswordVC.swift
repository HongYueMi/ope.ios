//
//  ChangeUserPasswordVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/27.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//  修改账號密碼

import UIKit

class ChangeUserPasswordVC: UIEditVC {

    @IBOutlet weak var tfPasswordA: UITextField!    //原账號密碼
    @IBOutlet weak var tfPasswordB: UITextField!    //新密碼
    @IBOutlet weak var tfPasswordC: UITextField!    //新密碼*2
    
    @IBOutlet weak var btnEyePasswordA: UIButton!   //原密碼顯示或隱藏
    @IBOutlet weak var btnEyePasswordB: UIButton!   //顯示或隱藏
    @IBOutlet weak var btnEyePasswordC: UIButton!   //顯示或隱藏
    
    @IBOutlet weak var BarBack: UIBarButtonItem!    //完成按鈕
    
    @IBOutlet weak var lbAlertMsg: UILabel!         //
    
    var item: PasswordResetItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didBtnEyePasswordAClick(_ sender: UIButton) {
        btnEyePasswordA.isSelected = !btnEyePasswordA.isSelected
        tfPasswordA.isSecureTextEntry = !btnEyePasswordA.isSelected
    }
    
    @IBAction func didBtnEyePasswordBClick(_ sender: UIButton) {
        btnEyePasswordB.isSelected = !btnEyePasswordB.isSelected
        tfPasswordB.isSecureTextEntry = !btnEyePasswordB.isSelected
    }
    
    @IBAction func didBtnEyePasswordCClick(_ sender: UIButton) {
        btnEyePasswordC.isSelected = !btnEyePasswordC.isSelected
        tfPasswordC.isSecureTextEntry = !btnEyePasswordC.isSelected
    }
    
    @IBAction func didBarBackClick(_ sender: UIBarButtonItem) {
        hideKeyboard()
        
        for tf in [tfPasswordB, tfPasswordC]{
            textFieldDidEndEditing(tf!)
        }
        
        editPassword()
    }
    
    func editPassword(){
        
        guard
            let password = tfPasswordB.text?.notEmptyValue
            , HYRegex.Password.match(input: password)
            else
        {
            lbAlertMsg.text = "密码格式错误（6-12位必须含有字母和数字的组合）";
            return
        }
        
        guard let passwordConfirm = tfPasswordC.text?.notEmptyValue, passwordConfirm == tfPasswordB.text! else {
            lbAlertMsg.text = "两次输入的密码必须一致";
            return
        }
        
        self.newLoadingScreenView.showActivityIndicator(uiView: self.view); //開啟Londing畫面
        var item = PasswordEditItem()
        item.old = tfPasswordA.text!
        item.new = tfPasswordB.text!
        HYMember.shared.editPassword(withItem: item){ [weak self] error in
            self?.newLoadingScreenView.hideActivityIndicator(uiView: (self?.view)!); //關閉Londing畫面
            if let error = error{
                let outMsg = error.localizedDescription;
                self?.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
            }
            else{
                self?.showGeneralAlert(.alert_info,lbTextA:"修改成功"){ value in
                    if value == 1 {
                        self?.navigationController?.popViewController(animated: true);
                    }
                }

            }
        }
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        if textField == tfPasswordB {
            guard
                let password = tfPasswordB.text?.notEmptyValue
                , HYRegex.Password.match(input: password)
                else
            {
                lbAlertMsg.text = "密码格式错误（6-12位必须含有字母和数字的组合）";
                return
            }
            lbAlertMsg.text = ""
        }
        else if textField == tfPasswordC {
            guard let passwordConfirm = tfPasswordC.text?.notEmptyValue, passwordConfirm == tfPasswordB.text! else {
                lbAlertMsg.text = "两次输入的密码必须一致";
                return
            }
            lbAlertMsg.text = ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true;
        }
        
        if textField == tfPasswordA , let txt = tfPasswordA.text , txt.count >= 12 {
            return false
        }
        
        if textField == tfPasswordB , let txt = tfPasswordB.text , txt.count >= 12 {
            return false
        }
        
        if textField == tfPasswordC , let txt = tfPasswordC.text , txt.count >= 12 {
            return false
        }
        return true
    }
    
}








































