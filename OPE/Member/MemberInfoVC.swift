//
//  MemberInfoVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/23.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension UIView  {
    //檢查是否滾動狀態 true:滾動中 , false:滾動結束
    func isScrolling () -> Bool {
        if let scrollView = self as? UIScrollView {
            if (scrollView.isDragging || scrollView.isDecelerating) {
                return true
            }
        }
        
        for subview in self.subviews {
            if ( subview.isScrolling() ) {
                return true
            }
        }
        return false
    }
    
    //滾動結束返回
    func waitTillDoneScrolling (completion: @escaping () -> Void) {
        var isMoving = true
        DispatchQueue.global(qos: .background).async {
            while isMoving == true {
                isMoving = self.isScrolling()
                
            }
            DispatchQueue.main.async {
                completion()}
            
        }
    }
    
}

class MemberInfoVC: UIEditVC, UIPickerViewDataSource, UIPickerViewDelegate{
    
    var checkUser:HYUser?;
    
    let screenSize = getDeviceSystemInfo.SingletonDSI().screenSize;
    
    var selPickerView:selSpecies?;  //放置使用項目
    enum selSpecies {
        case Gender
        case Area
        case Birthday
    }
    var blackView = UIView();       //底黑
    var newSelectPickerDataXIB = SelectPickerDataXIB();         //旋轉～ 跳躍～ Picker畫面
    var newSelectDatePickerXIB = SelectDatePickerXIB();         //旋轉～ 跳躍～ Picker Date畫面
    let genderData = ["男","女"];
    
    let provinceData = HYDatas.provinces; //省份資料
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViewController();   //初始化 XIB
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.newLoadingScreenView.showActivityIndicator(uiView: self.view);
        HYAuth.auth.currentUser?.update{[weak self] error in
            self?.newLoadingScreenView.hideActivityIndicator(uiView: self!.view);
            if nil != error{
                let outMsg = error?.localizedDescription;
                self?.showGeneralAlert(.alert_error,lbTextA:outMsg ?? "Error"){ value in }
            }
            else if let user = HYAuth.auth.currentUser{
                self?.checkUser = user;
                self?.updateViewData(withUser: user)
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - View
    
    @IBOutlet weak var controlGender: UIControl!
    
    @IBOutlet weak var lbUID: UILabel!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbEmail: UILabel!
    @IBOutlet weak var lbPhone: UILabel!
    @IBOutlet weak var lbGender: UILabel!
    @IBOutlet weak var lbBirthday: UILabel!
    @IBOutlet weak var lbQQ: UILabel!
    @IBOutlet weak var lbDistribute: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    
    func updateViewData(withUser user: HYUser){
        lbUID.text = user.uid
        if user.name != ""{
            lbName.text = user.name
        }
        
        if user.email != "" {
            if user.isEmailVerified == false {
                lbEmail.text = "尚未验证电子邮箱\n\(user.email)"
            }else{
                lbEmail.text = user.email
            }
        }
        
        if user.phone != "" {
            lbPhone.text = user.phone
        }
        
        if user.sex?.description() != nil {
            lbGender.text = user.sex?.description()
        }
        
        if user.strBirthdate != nil {
            if let dateBD = user.strBirthdate {
                if (dateBD.components(separatedBy: "T"))[0] != "" {
                    lbBirthday.text = (dateBD.components(separatedBy: "T"))[0].replacingOccurrences(of: "-", with: "/");
                }else{
                    lbBirthday.text = user.strBirthdate
                }
            }else{
                lbBirthday.text = user.strBirthdate
            }
        }
        
        if user.qq != nil {
            lbQQ.text = user.qq
        }
        
        if user.province?.name != nil {
            lbDistribute.text = user.province?.name
        }
        
        if user.address != nil {
            lbAddress.text = user.address
        }

    }
    
    func initViewController(){
        blackView.frame = CGRect(x: screenSize.width+20 , y: 0, width: screenSize.width , height: screenSize.height);
        blackView.backgroundColor = UIColor.black;
        blackView.alpha = 0.5;
        blackView.tag = 100;
        self.view.addSubview(self.blackView);
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(studySelectPickerView)));
        
        self.newSelectPickerDataXIB.frame = CGRect(x: 0 , y: screenSize.height, width: screenSize.width , height: screenSize.height/3);
        self.newSelectPickerDataXIB.tag = 200;
        self.view.addSubview(self.newSelectPickerDataXIB);
        self.newSelectPickerDataXIB.pvSelect.delegate = self;
        self.newSelectPickerDataXIB.pvSelect.dataSource = self;
        self.newSelectPickerDataXIB.btClose.addTarget(self, action: #selector(studySelectPickerView), for: .touchUpInside); //點擊放開時觸發
        self.newSelectPickerDataXIB.btDone.addTarget(self, action: #selector(donePickerData), for: .touchUpInside);
        
        self.newSelectDatePickerXIB.frame = CGRect(x: 0 , y: screenSize.height, width: screenSize.width , height: screenSize.height/3);
        self.newSelectDatePickerXIB.tag = 300;
        self.view.addSubview(self.newSelectDatePickerXIB);
        self.newSelectDatePickerXIB.btClose.addTarget(self, action: #selector(studySelectPickerView), for: .touchUpInside); //點擊放開時觸發
        self.newSelectDatePickerXIB.pdSelect.date = NSDate() as Date;
        self.newSelectDatePickerXIB.pdSelect.maximumDate = NSDate() as Date;
        let formatter = DateFormatter();
        formatter.dateFormat = "yyyy-MM-dd";
        self.newSelectDatePickerXIB.pdSelect.minimumDate = formatter.date(from: "1888/01/01");
        self.newSelectDatePickerXIB.pdSelect.setValue(UIColor(red: 0/255, green: 203/255, blue: 192/255, alpha: 1), forKeyPath: "textColor");
        self.newSelectDatePickerXIB.pdSelect.addTarget(self, action: #selector(dateaction(_:)), for: UIControlEvents.valueChanged)
        self.newSelectDatePickerXIB.btDone.addTarget(self, action: #selector(donePickerData), for: .touchUpInside);
    }
    
    //用户名 - 不動作
    @IBAction func didUserAccountClick(_ sender: UIControl) {
//        self.performSegue(withIdentifier: "sendTest", sender: nil)
    }
    
    //姓名
    @IBAction func didUserNameClick(_ sender: UIControl) {
        if checkUser?.name != "" {
            showGeneralAlert(.alert_info,lbTextA:"用户姓名一旦设置，就不能更改"){ value in }
        }else{
            self.performSegue(withIdentifier: "sendNewUserNameVC", sender: nil);
        }
    }
    
    //电子邮箱
    @IBAction func didEMailClick(_ sender: UIControl) {
        var outURL = "";    //https://www.ope88.com/
        
        if checkUser?.email == "" {
            self.performSegue(withIdentifier: "sendNewUserEMailVC", sender: nil);
            
        }else if checkUser?.email != "" , checkUser?.isEmailVerified != false {
            showGeneralAlert(.alert_info,lbTextA:"电子邮箱一旦设置，就不能更改"){ value in }
            
        }else if checkUser?.isEmailVerified == false {
            for (k,v) in HYDatas.mailList {
                let words = checkUser!.email.components(separatedBy: "@");
                if words[1] == k {
                    outURL = v;
                }
            }
            showTwoSelectAlert(.alert_mail,lbTextA:"是否对电子邮箱进行验证？"){ okValue, noValue in
                if okValue == 1 , noValue == 0 {
                    if let url = URL(string: outURL), UIApplication.shared.canOpenURL(url){
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil);
                        } else {
                            UIApplication.shared.openURL(url);
                        }
                    }else{
//                        let actionA = UIAlertAction(title: "关闭",style: .default,handler: { action in
//
//                        });
//                        self.alertActionView(message:"无法开启网址", loopAction:[actionA]);
                    }
                }
            }
            
        }
    }
    
    //手機號碼
    @IBAction func didUserPhoneClick(_ sender: UIControl) {
        if checkUser?.phone == "" {
            self.performSegue(withIdentifier: "sendNewUserPhoneVC", sender: nil);
            
        }else if checkUser?.phone != "" , checkUser?.isPhoneVerified != false {
            showGeneralAlert(.alert_info,lbTextA:"手机号码一旦设置，就不能更改"){ value in }
            
        }else if checkUser?.isPhoneVerified == false {
            showTwoSelectAlert(.alert_info,lbTextA:"您尚未对手机号码验证，是否进行验证？"){ okValue, noValue in
                if okValue == 1 , noValue == 0 {
                    //要轉跳到驗證手機
                }
            }
        }
    }
    
    //性別
    @IBAction func didUserGenderClick(_ sender: UIControl) {
        selPickerView = .Gender
        openSelectPickerView();
    }
    
    //出生日期
    @IBAction func didBirthdayClick(_ sender: UIControl) {
        if checkUser?.strBirthdate == "" || checkUser?.strBirthdate == nil {
            selPickerView = .Birthday
            openSelectPickerView();
        }else{
            showGeneralAlert(.alert_info,lbTextA:"出生日期一旦设置，就不能更改"){ value in }
        }
    }
    
    //QQ號碼
    @IBAction func didQQNumberClick(_ sender: UIControl) {
        self.performSegue(withIdentifier: "sendNewUserQQVC", sender: nil);
    }
    
    //收貨地址
    @IBAction func didAddressClick(_ sender: UIControl) {
        selPickerView = .Area
        openSelectPickerView();
    }
    
    //詳細街道
    @IBAction func didDetailedStreetClick(_ sender: UIControl) {
        self.performSegue(withIdentifier: "sendNewUserDetailedStreetVC", sender: nil);
    }
    
    //使用Picker
    func openSelectPickerView() {
        blackView.frame = CGRect(x: 0 , y: 0, width: screenSize.width , height: screenSize.height);
        
        let user = HYAuth.auth.currentUser!
        let usingSWD:CGFloat = 0.7; //彈簧效果
        let withDTime = 0.5;        //動畫時間
        
        switch selPickerView {
        case .Gender?:   //選性別 0女 1男
            self.newSelectPickerDataXIB.lbTitle.text = "请选择性别";
            self.newSelectPickerDataXIB.lbContentTitle.text = "";
            var sex = 0;
            if let val = user.sex ,  val.rawValue == 0 { //val = 0女
                sex = 1;
            }
            
            UIView.animate(withDuration: withDTime, delay: 0.0, usingSpringWithDamping:usingSWD, initialSpringVelocity: 1, options: [], animations: {
                () -> Void in
                self.newSelectPickerDataXIB.frame = CGRect(x: 0 , y: (self.screenSize.height/3)*2, width: self.screenSize.width , height: self.screenSize.height/3);
            }, completion: nil);
            self.newSelectPickerDataXIB.pvSelect.reloadAllComponents();
            self.newSelectPickerDataXIB.pvSelect.selectRow(sex, inComponent: 0, animated: true);
            
        case .Area?:     //選地區立委
            self.newSelectPickerDataXIB.lbTitle.text = "请选择地区";
            self.newSelectPickerDataXIB.lbContentTitle.text = "省份";

            var provinceId = 0;
            if let val = user.provinceId , val > 0{
                provinceId = val-1
            }
            
            UIView.animate(withDuration: withDTime, delay: 0.0, usingSpringWithDamping:usingSWD, initialSpringVelocity: 1, options: [], animations: {
                () -> Void in
                self.newSelectPickerDataXIB.frame = CGRect(x: 0 , y: (self.screenSize.height/3)*2, width: self.screenSize.width , height: self.screenSize.height/3);
            }, completion: nil);
            self.newSelectPickerDataXIB.pvSelect.reloadAllComponents();
            self.newSelectPickerDataXIB.pvSelect.selectRow(provinceId, inComponent: 0, animated: true);
            
        case .Birthday?: //選總統時間拉
            UIView.animate(withDuration: withDTime, delay: 0.0, usingSpringWithDamping:usingSWD, initialSpringVelocity: 1, options: [], animations: {
                () -> Void in
                self.newSelectDatePickerXIB.frame = CGRect(x: 0 , y: (self.screenSize.height/3)*2, width: self.screenSize.width , height: self.screenSize.height/3);
            }, completion: nil);
            
        case .none:
            return
        }
    }
    
    //回到預備狀態
    @objc func studySelectPickerView(){
        self.blackView.frame = CGRect(x: screenSize.width+20 , y: 0, width: screenSize.width , height: screenSize.height);
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: [], animations: { () -> Void in
            self.newSelectPickerDataXIB.frame = CGRect(x: 0 , y: self.screenSize.height, width: self.screenSize.width , height: self.screenSize.height/3);
        }, completion: nil);
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: [], animations: { () -> Void in
            self.newSelectDatePickerXIB.frame = CGRect(x: 0 , y: self.screenSize.height, width: self.screenSize.width , height: self.screenSize.height/3);
        }, completion: nil);
        
    }
    
    //在選擇器視圖組件的設置數量
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    //在組件設置的行數
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch selPickerView {
        case .Gender?:   //選性別
            return genderData.count;
            
        case .Area?:     //選地區立委
            return provinceData.count;
            
        case .Birthday?: //選總統時間拉
            return genderData.count;
        case .none:
            return 0;
        }
    }
    
    //每一行設置內容
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch selPickerView {
        case .Gender?:   //選性別
            return genderData[row];
            
        case .Area?:     //選地區立委
            return provinceData[row].name;
            
        case .Birthday?: //選總統時間拉
            return genderData[row];
        case .none:
            return "";
        }
        
    }
    
    //高度
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40.0
    }
    
    //客製化
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel = view as! UILabel!
        if view == nil {
            pickerLabel = UILabel()
            pickerLabel?.backgroundColor = .white
        }
        pickerLabel!.textColor = UIColor(red: 0/255, green: 203/255, blue: 192/255, alpha: 1)
        if(selPickerView == .Gender){
            pickerLabel!.text = genderData[row];
        }else if(selPickerView == .Area){
            pickerLabel!.text = provinceData[row].name;
        }
        pickerLabel!.textAlignment = .center
        return pickerLabel!
        
    }
    
    var selectID:Int = 0;       //選中的
    var selectDate:String = ""; //選中的日期
    //選擇行時更新文本框的文本
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch selPickerView {
        case .Gender?:   //選性別
            selectID = row
            
        case .Area?:     //選地區立委
            selectID = provinceData[row].id;
            
        case .Birthday?: //選總統時間拉
            selectID = row
        case .none:
            selectID = 0;
        }
    }
    
    //日期選擇
    @objc func dateaction(_ datea:UIDatePicker){
        let formatter = DateFormatter();
        formatter.dateFormat = "yyyy/MM/dd";
        selectDate = formatter.string(from: datea.date);
    }
    
    //按下確定按鈕
    @objc func donePickerData(){
        //先判斷是否還在滾動中
        if self.newSelectPickerDataXIB.pvSelect.isScrolling() == false , self.newSelectDatePickerXIB.pdSelect.isScrolling() == false {
            studySelectPickerView()
            
            switch selPickerView {
            case .Gender?:   //選性別
                var sex = 0;
                if selectID == 0 {
                    sex = 1;
                }
                updateUserData(["Sex":sex]);
                
            case .Area?:     //選地區立委
                
                updateUserData(["ProvinceID":selectID]);
                
            case .Birthday?: //選總統時間拉
                if selectDate == "" {
                    let formatter = DateFormatter();
                    formatter.dateFormat = "yyyy/MM/dd";
                    selectDate = formatter.string(from: NSDate() as Date);
                }
                updateUserData(["BirthDay":selectDate]);
                
            case .none:
                selectID = 0;
            }
        }
    }
    
    //更新UserData
    func updateUserData(_ info: [String: Any]){
        self.newLoadingScreenView.showActivityIndicator(uiView: self.view);
        HYAuth.auth.currentUser!.editInfo(withInfos: info) {
            [weak self] error in
            self?.newLoadingScreenView.hideActivityIndicator(uiView: self!.view);
            
            if nil != error {
                let outMsg = error!.localizedDescription;
                self?.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
            }
            else{
                self?.showGeneralAlert(.alert_ok,lbTextA:"设置成功"){ value in
                    if value == 1 {
                        self?.updateViewData(withUser: HYAuth.auth.currentUser!);
                    }
                }
                
            }
        }
        
    }
    
    
    
}































