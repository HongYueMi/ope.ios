//
//  MemberVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/18.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension UIStoryboard{
    static let UserCenter : UIStoryboard = UIStoryboard(name: "UserCenter", bundle: nil)
}

class MemberCenterVC: UIViewController{

    @IBOutlet weak var lbName: UILabel!;
    @IBOutlet weak var lbDateCount: UILabel!;
    @IBOutlet weak var lbVIPLevel: UILabel!;
    @IBOutlet weak var lbFundBalance: UILabel!
    @IBOutlet weak var lbUnreadMessage: UILabel!
    @IBOutlet weak var swchNightTheme: UISwitch!
    @IBOutlet weak var lbBankPasswordSetting: UILabel!
    @IBOutlet weak var btWithdrawal: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbVIPLevel.layer.cornerRadius = 4
        lbVIPLevel.layer.masksToBounds = true
        _ = HYAuth.auth.addAuthStateDidChangeListener{
            [weak self] auth, user in
            self?.lbName.text = user?.uid
            var dateCount = 0
            if let dateStart = user?.registerDate{
                dateCount = Int(Date().timeIntervalSince(dateStart) / (60.0 * 60.0 * 24.0))
            }
            self?.lbDateCount.text = "已加入大家庭\(dateCount)天"
            self?.lbVIPLevel.text = " VIP \(user?.vipLevel ?? 0) "
        }
        NotificationCenter.default.addObserver(self, selector: #selector(updateBalance), name: HYFund.NotificationName, object: HYFund.fund)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUnreadCount), name: HYMessage.NotificationName, object: HYMessage.shared)
        updateUnreadCount()
        updateBalance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "个人中心"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func updateBalance(){
        lbFundBalance.text = HYFund.fund.strBalance
    }
    
    @objc func updateUnreadCount(){
        lbUnreadMessage.text = HYMessage.shared.unreadCount
    }
   
    @IBAction func didBtnSignOutClick(){
        showTwoSelectAlert(.alert_info, lbTextA:"您确定要退出登录？"){ okValue, noValue in
            if okValue == 1 , noValue == 0 {
                HYAuth.auth.signOut()
                HYAuth.auth.forgotUser();
                HYAuth.auth.askForUserAuthorization();
                self.dismiss(animated: true)
                
            }
        }
    }
    
    //提款
    @IBAction func didBtWithdrawalClick(_ sender: UIButton) {
        if !HYAuth.auth.currentUser!.isBankPasswordSet {    //判斷用戶是否有設定資金密碼
            defer {
                self.performSegue(withIdentifier: "sendSetFundPWVC", sender: 0)    //前往設置資金密碼
            }
        }else if HYWithdrawal.withdrawal.withdrawalBankAccounts.count <= 0 {    //抓用戶是否有註冊銀行卡
            defer {
                self.performSegue(withIdentifier: "sendBankCardSetListVC", sender: nil) //前往信用卡設置
            }
        }else{
            defer {
                self.performSegue(withIdentifier: "sendCashWithdrawalVC", sender: nil) //前往０８５７領錢  sendCheckWithdrawalVC原本的
            }
        }
    }
    
    //銀行卡設置
    @IBAction func didBankCardSetClick(_ sender: UIButton) {
        if !HYAuth.auth.currentUser!.isBankPasswordSet {    //判斷用戶是否有設定資金密碼
            defer {
                self.performSegue(withIdentifier: "sendSetFundPWVC", sender: 1)    //前往設置資金密碼
            }
        }else{
            defer {
                self.performSegue(withIdentifier: "sendBankCardSetListVC", sender: nil) //前往銀行卡設置
            }
        }
    }
    
    //資金密碼
    @IBAction func didFundingPasswordClick(_ sender: UIButton) {
        if !HYAuth.auth.currentUser!.isBankPasswordSet{ //true有設置
            defer {
                performSegue(withIdentifier: "sendSetFundPWVC", sender: 3)
            }
        } else {
            defer {
                performSegue(withIdentifier: "sendChangeFundPWVC", sender: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendSetFundPWVC" {  //前往設定資金密碼
            if let vc = segue.destination as? SetFundPWVC , let flag = sender as? Int {
                if flag == 0 {          //0:Withdrawal//提款
                    vc.routeLink = "Withdrawal"
                }else if flag == 1 {    //1:BankCardSet//銀行卡設置
                    vc.routeLink = "BankCardSet"
                }else if flag == 3 {    //3:純資金密碼
                    vc.routeLink = ""
                }
            }
        }
    }
    
    // MARK: - Navigation
    
//    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//        if identifier == "sendWithdrawal" {
//            if !HYAuth.auth.currentUser!.isBankPasswordSet{
//                defer {
//                    performSegue(withIdentifier: "SetBankPassword", sender: nil)
//                }
//            }
//            else if HYWithdrawal.withdrawal.withdrawalBankAccounts.count <= 0{
//                defer {
//                    performSegue(withIdentifier: "SetBankCard", sender: nil)
//                }
//            }
//        }
//        return true
//    }
    
    
    
    
    
    
    
    
    
}


































