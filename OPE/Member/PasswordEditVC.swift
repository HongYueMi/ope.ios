//
//  PasswordEditVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/16.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class PasswordEditVC: UIEditVC {
    
    @IBOutlet weak var lbPassword1: UILabel!
    @IBOutlet weak var tfPassword1: UITextField!
    @IBOutlet weak var btnEyePassword1: UIButton!

    @IBOutlet weak var lbPassword2: UILabel!
    @IBOutlet weak var tfPassword2: UITextField!
    @IBOutlet weak var btnEyePassword2: UIButton!
    
    @IBOutlet weak var lbCheckPassword: UILabel?
    @IBOutlet weak var tfCheckPassword: UITextField?
    @IBOutlet weak var btnEyeCheckPassword: UIButton?
    
    @IBOutlet weak var btnEnter: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tfPassword1.delegate = self;
        tfPassword2.delegate = self;
        tfCheckPassword?.delegate = self;
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didBtnPassword1EyeClick(btn: UIButton){
        btnEyePassword1.isSelected = !btnEyePassword1.isSelected
        tfPassword1.isSecureTextEntry = !btnEyePassword1.isSelected
    }
    
    @IBAction func didBtnPassword2EyeClick(btn: UIButton){
        btnEyePassword2.isSelected = !btnEyePassword2.isSelected
        tfPassword2.isSecureTextEntry = !btnEyePassword2.isSelected
    }
    
    @IBAction func didBtnCheckPasswordEyeClick(btn: UIButton){
        btnEyeCheckPassword!.isSelected = !btnEyeCheckPassword!.isSelected
        tfCheckPassword!.isSecureTextEntry = !btnEyeCheckPassword!.isSelected
    }

    @IBAction func didBtnEnterClick(btn: UIButton?){
        guard checkInput() else {
            return
        }
        editPassword()
    }
    
    func checkInput() -> Bool{
        guard let password1 = tfPassword1.text?.notEmptyValue else {
            showGeneralAlert(.alert_info, lbTextA:"\(lbPassword1.text!) 不能为空！（6-12位必须含有字母和数字的组合）"){ value in }
            return false
        }
        guard
            HYRegex.Password.match(input: password1)
            else
        {
            showGeneralAlert(.alert_info, lbTextA:"\(lbPassword1.text!) 格式有误！（6-12位必须含有字母和数字的组合）"){ value in }
            return false
        }
        guard let password2 = tfPassword2.text?.notEmptyValue else {
            showGeneralAlert(.alert_info, lbTextA:"\(lbPassword2.text!) 不能为空！（6-12位必须含有字母和数字的组合）"){ value in }
            return false
        }
        guard
            HYRegex.Password.match(input: password2)
            else
        {
            showGeneralAlert(.alert_info, lbTextA:"\(lbPassword2.text!) 格式有误！（6-12位必须含有字母和数字的组合）"){ value in }
            return false
        }
        
        if nil == tfCheckPassword{
            return true
        }
        
        guard let passwordCheck = tfCheckPassword?.text?.notEmptyValue else {
            showGeneralAlert(.alert_info, lbTextA:"\(lbCheckPassword!.text!) 不能为空！（6-12位必须含有字母和数字的组合）"){ value in }
            return false
        }
        guard
            passwordCheck == password2
            else
        {
            showGeneralAlert(.alert_info, lbTextA:"两次输入密码不一致"){ value in }
            return false
        }
        
        return true
    }
    
    func editPassword(){
        view.isUserInteractionEnabled = false
        self.newLoadingScreenView.showActivityIndicator(uiView: self.view); //開啟Londing畫面
        var item = PasswordEditItem()
        item.old = tfPassword1.text!
        item.new = tfPassword2.text!
        HYMember.shared.editPassword(withItem: item){ [weak self] error in
            self?.newLoadingScreenView.hideActivityIndicator(uiView: (self?.view)!); //關閉Londing畫面
            if let error = error{
                self?.showGeneralAlert(.alert_error, lbTextA:error.localizedDescription){ value in
                    self?.view.isUserInteractionEnabled = true
                }
            }
            else{
                self?.showGeneralAlert(.alert_info, lbTextA:"修改成功"){ value in
                    if value == 1 {
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfPassword1{
            tfPassword2.becomeFirstResponder()
        }
        else if textField == tfPassword2{
            tfPassword2.resignFirstResponder()
            didBtnEnterClick(btn: btnEnter)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    
}

class BankPasswordEditVC: PasswordEditVC {
    
    override func editPassword() {
        view.isUserInteractionEnabled = false
        var item = PasswordEditItem()
        item.old = tfPassword1.text!
        item.new = tfPassword2.text!
        HYFund.fund.editPassword(withItem: item){ [weak self] error in
            if let error = error{
                self?.showGeneralAlert(.alert_error, lbTextA:error.localizedDescription){ value in
                    self?.view.isUserInteractionEnabled = true
                }
            }
            else{
                self?.showGeneralAlert(.alert_info, lbTextA:"修改成功"){ value in
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
}

class BankPasswordAddVC: PasswordEditVC {
    
    override func editPassword() {
        view.isUserInteractionEnabled = false
        var item = PasswordEditItem()
        item.old = tfPassword1.text!
        item.new = tfPassword2.text!
        HYFund.fund.addPassword(withItem: item){ [weak self] error in
            if let error = error{
                self?.showGeneralAlert(.alert_error, lbTextA:error.localizedDescription){ value in
                    self?.view.isUserInteractionEnabled = true
                }
            }
            else{
                self?.didModifySuccess()
            }
        }
    }
    
    func didModifySuccess(){
        self.showGeneralAlert(.alert_info, lbTextA:"修改成功"){ value in
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
