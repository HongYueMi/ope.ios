//
//  NewUserQQVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/30.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class NewUserQQVC: UIEditVC {

    @IBOutlet weak var btNext: UIBarButtonItem!
    
    @IBOutlet weak var tfUserQQ: UITextField!
    
    @IBOutlet weak var btDelete: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if HYAuth.auth.currentUser!.qq != nil {
            self.navigationItem.title = "修改QQ"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //右上方OK按鈕
    @IBAction func didBtNextClick(_ sender: UIBarButtonItem) {
        checkTextInput();
    }
    
    //聽說這個叫刪除
    @IBAction func didBtDeleteClick(_ sender: UIButton) {
        tfUserQQ.text = nil;
    }
    
    //檢查文字輸入
    func checkTextInput(){
        guard
            let value = tfUserQQ.text?.notEmptyValue
            , HYRegex.QQ.match(input: value)
            else
        {
            showGeneralAlert(.alert_info,lbTextA:"QQ格式有误"){ value in }
            return
        }
        
        newUserNameFunc(["QQ":tfUserQQ.text!]);
    }
    
    //更新UserData
    func newUserNameFunc(_ info: [String: Any]){
        self.newLoadingScreenView.showActivityIndicator(uiView: self.view);
        HYAuth.auth.currentUser!.editInfo(withInfos: info) {
            [weak self] error in
            self?.newLoadingScreenView.hideActivityIndicator(uiView: self!.view);
            
            if nil != error {
                let outMsg = error!.localizedDescription;
                self?.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
            }
            else{
                HYAuth.auth.currentUser!.update { error in
                    if let error = error{
                        let outMsg = error.localizedDescription;
                        self?.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
                    } else{
                        self?.showGeneralAlert(.alert_ok,lbTextA:"设置成功"){ value in
                            if value == 1 {
                                _ = self?.navigationController?.popViewController(animated: true);
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        if textField == tfUserQQ{
            guard
                let value = tfUserQQ.text?.notEmptyValue
                , HYRegex.QQ.match(input: value)
                else
            {
                showGeneralAlert(.alert_info,lbTextA:"QQ格式有误"){ value in }
                return
            }
        }
        
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            return true;
        }
        
        if textField == tfUserQQ , let txt = tfUserQQ.text , txt.count >= 13 {
            return false
        }
        
        return true;
    }
    
}








































