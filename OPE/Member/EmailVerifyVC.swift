//
//  EmailVerifyVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/5.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class EmailVerifyVC: UIEditVC {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnEnter: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action

    @IBAction func didBtnEnterClick(btn: UIButton){
        guard checkInput() else {
            return
        }
        sendVerifyEmail()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfEmail{
            didBtnEnterClick(btn: btnEnter)
        }
        return true
    }
    
    func checkInput() -> Bool{
        guard let email = tfEmail.text?.notEmptyValue else {
            showGeneralAlert(.alert_info, lbTextA:"请输入电子邮箱"){ value in }
            return false
        }
        guard HYRegex.Email.match(input: email)
            else
        {
            showGeneralAlert(.alert_info, lbTextA:"电子邮箱格式有误"){ value in }
            return false
        }
        return true
    }
    
    // MARK: - Verify

    func sendVerifyEmail(){
        hideKeyboard()
        view.isUserInteractionEnabled = false
        HYAuth.auth.currentUser!.verifyEmail{ [weak self] error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
        if value == 1 {
            self?.view.isUserInteractionEnabled = true
        }
     }
            }
            else{
                self?.showGeneralAlert(.alert_info, lbTextA:"請登入您的電子信箱進行驗證"){ value in
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func editEmail(){
        HYAuth.auth.currentUser!.editEmail(email: tfEmail.text!){ [weak self] error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in }
            }
            else{
                self?.showGeneralAlert(.alert_info, lbTextA:"电子信箱修改成功"){ value in }
            }
        }
    }

}
