//
//  NewUserNameVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/29.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class NewUserNameVC: UIEditVC {

    @IBOutlet weak var btNext: UIBarButtonItem!
    
    @IBOutlet weak var tfUserName: UITextField!
    
    @IBOutlet weak var btDelete: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //右上方OK按鈕
    @IBAction func didBtNextClick(_ sender: UIBarButtonItem) {
        checkTextInput();
    }
    
    //聽說這個叫刪除
    @IBAction func didBtDeleteClick(_ sender: UIButton) {
        tfUserName.text = nil;
    }
    
    //檢查文字輸入
    func checkTextInput(){
        if(tfUserName.text == ""){
            showGeneralAlert(.alert_info,lbTextA:"请输入姓名"){ value in }
            return
        }
        
        guard
            let uid = tfUserName.text?.notEmptyValue?.trimmingCharacters(in: .whitespacesAndNewlines)
            , HYRegex.UserName.match(input: uid)
            else
        {
            showGeneralAlert(.alert_info,lbTextA:"姓名格式错误"){ value in }
            return
        }
        
        newUserNameFunc();
    }
    
    func newUserNameFunc(){
        //新增用戶名
        HYWithdrawal.withdrawal.newNameAsync(tfUserName.text!){ error in
            if let error = error{
                let outMsg = error.localizedDescription;
                self.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
            }
            else{
                self.updateUserData(["Name": self.tfUserName.text!])    //刷新重抓用戶資料
            }
        }
    }
    
    //更新本機用戶資訊
    func updateUserData(_ info: [String: Any]){
        HYAuth.auth.currentUser!.update { error in
            if let error = error{
                let outMsg = error.localizedDescription;
                self.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
            } else {
                self.showGeneralAlert(.alert_ok,lbTextA:"设置成功"){ value in
                    if value == 1 {
                        _ = self.navigationController?.popViewController(animated: true);
                    }
                }
                
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}








































