//
//  NewUserPhoneVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/29.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class NewUserPhoneVC: UIEditVC {

    @IBOutlet weak var lbInfoMsg: UILabel!
    @IBOutlet weak var btNext: UIBarButtonItem!
    
    @IBOutlet weak var tfUserPhone: UITextField!
    @IBOutlet weak var tfChcekCode: UITextField!
    
    @IBOutlet weak var btAgainPush: UIButton!   //獲取驗證碼
    
    @IBOutlet weak var btDelete: UIButton!
    
    @IBOutlet weak var lbCount: UILabel!
    
    weak var timer: Timer?
    var count: Int = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //右上方OK按鈕
    @IBAction func didBtNextClick(_ sender: UIBarButtonItem) {
        guard let _ = tfChcekCode.text?.notEmptyValue else {
            lbInfoMsg.text = "请输入验证码";
//            showGeneralAlert(.alert_info,lbTextA:"请输入验证码"){ value in }
            return
        }
        
        guard
            let value = tfUserPhone.text?.notEmptyValue
            , HYRegex.Phone.match(input: value)
            else
        {
            lbInfoMsg.text = "手机号码格式有误";
//            showGeneralAlert(.alert_info,lbTextA:"手机号码格式有误"){ value in }
            return
        }
        
        setVerifyPhoneCode(phone: tfUserPhone.text! ,verifyCode: tfChcekCode.text! ) { [weak self] error in
            if error != nil{
//                let outMsg = err.localizedDescription;
                self?.showGeneralAlert(.alert_error,lbTextA:"验证码错误"){ value in }
            }else{
                self?.updateUserData();
            }
        }
    }
    
    //聽說這個叫刪除
    @IBAction func didBtDeleteClick(_ sender: UIButton) {
        tfUserPhone.text = nil;
    }
    
    //發送電話認證
    @IBAction func didBtAgainPushClick(_ sender: UIButton) {
        guard let _ = tfUserPhone.text?.notEmptyValue else {
            showGeneralAlert(.alert_info,lbTextA:"请输入手机号码"){ value in }
            return
        }
        
        guard
            let value = tfUserPhone.text?.notEmptyValue
            , HYRegex.Phone.match(input: value)
            else
        {
            showGeneralAlert(.alert_info,lbTextA:"手机号码格式有误"){ value in }
            return
        }
        
        getVerifyCode();
    }

    //發送驗證碼
    func getVerifyCode(){
        HYSession.member.authRequest(URL_HOST + "/API/MemberSecurity/SendCellPhoneVerifyCodeAsync",  parameters: ["CellPhone": tfUserPhone.text!], encoding: JSONEncode.default).validate().responseHYJson { response in
            if let _ = response.error{
                self.showGeneralAlert(.alert_info,lbTextA:"发送验证码失败！"){ value in }
            } else{
                self.showGeneralAlert(.alert_info,lbTextA:"验证码已寄出！"){ value in
                    self.startCounting();   //開始倒數
                }
            }
        }
    }
    
    //下一步驗證手機與驗證碼 並且新增
    func setVerifyPhoneCode(phone: String,verifyCode code: String, completion: @escaping (Error?)->Void){
        let parameters: [String: Any] = [
            "IsNew": 0
            , "CellPhone": phone
            , "VerificationCode": code
        ]
        HYSession.app.authRequest(URL_HOST + "/API/MemberSecurity/VerifyCellPhoneAsync", parameters: parameters).validate().responseHYJson { response in
            if let err = response.error{
                completion(err)
            }else{
                completion(nil);
            }
        }
    }
    
    //更新本機用戶資訊
    func updateUserData(){
        HYAuth.auth.currentUser!.update { error in
            if let error = error{
                let outMsg = error.localizedDescription;
                self.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
            } else{
                self.showGeneralAlert(.alert_ok,lbTextA:"设置成功"){ value in
                    if value == 1 {
                        _ = self.navigationController?.popViewController(animated: true);
                    }
                }
            }
        }
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        if textField == tfUserPhone{
            guard
                let value = tfUserPhone.text?.notEmptyValue
                , HYRegex.Phone.match(input: value)
                else
            {
                lbInfoMsg.text = "手机号码格式有误";
//                showGeneralAlert(.alert_info,lbTextA:"手机号码格式有误"){ value in }
                return
            }
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        lbInfoMsg.text = "";
        
        if string == "" {
            return true;
        }
        
        if textField == tfUserPhone , let txt = tfUserPhone.text , txt.count >= 11 {
            return false
        }
        
        if textField == tfChcekCode , let txt = tfChcekCode.text , txt.count >= 6 {
            return false
        }
        
        return true;
    }
    
    func startCounting(){
        timer?.invalidate()
        btAgainPush.isEnabled = false
        count = 60
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCount), userInfo: nil, repeats: true)
    }
    
    @objc func updateCount(){
        lbCount.text = "(\(count)s)";
        if 0 == count{
            timer?.invalidate()
            btAgainPush.isEnabled = true
        }else{
            count -= 1
        }
    }
    
}











































