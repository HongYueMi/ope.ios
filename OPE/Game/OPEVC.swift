//
//  OPEVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/23.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class OPEVC: GameVC {

    @IBOutlet weak var BtnOPEClick: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //判斷iPad 或是iPhone
        let device = HYApp.app.CheckDevice();
        if device.DeviceNumber == 1 {
            let imageTi = UIImage(named: "OPE_BtnCate_Sports_iPad.png");
            self.BtnOPEClick.setBackgroundImage(imageTi, for: UIControlState());
        }
        
    }
    
    @IBAction func didBtnOPEClick(btn: UIButton){
        let sport = HYGames.list(withCategory: HYGames.Category.SPORT).first!
        openGame(sport)
    }
    
    @IBAction func didBtnGameCategoryClick(btn: UIButton){
        navToGameCategoryVC(withCategory: HYGames.Category.list[btn.tag])
    }
    
    func navToGameCategoryVC(withCategory category: HYGames.Category){
        let vc = UIStoryboard.Game.identifier("GameCategoryVC") as! GameCateVC
        vc.category = category
        navigationController?.pushViewController(vc, animated: true)
    }
    

}
