//
//  PlatformEntryCategoryVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/22.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class PlatformCell: UITableViewCell {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubTitle: UILabel?
    @IBOutlet weak var btnMore: UIButton?
    @IBOutlet weak var imgvBackground: UIImageView!
    @IBOutlet weak var lbNumberOfPlayers: UILabel?

    @IBOutlet weak var imgBlack: UIImageView!
    @IBOutlet weak var imgComingSoon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        btnMore?.layer.borderWidth = 1
        btnMore?.layer.borderColor = UIColor.white.cgColor
        btnMore?.layer.cornerRadius = btnMore!.bounds.size.height / 2
        btnMore?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
    }
    
    var entry: HYPlayStation.Entry!{
        didSet{
            lbTitle.text = entry.title
            lbSubTitle?.text = entry.subTitle
            imgvBackground.image = entry.image
        }
    }
}


class PlatformListVC: PlayStationVC, UITableViewDataSource, UITableViewDelegate{

    var category : String!{
        didSet{
            let user = HYAuth.auth.currentUser ?? HYUser.anonymous
            tableData = (user.availableCategoryPlatforms[category] ?? []).flatMap{HYPlayStation.entries[category]?[$0]}
        }
    }
    
    override func viewDidLayoutSubviews() {
        rowHight = tableView.frame.size.width * 392 / 1080
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var tableView : UITableView!
    
    var tableData : [HYPlayStation.Entry] = [] {
        didSet{
            tableView?.reloadData()
        }
    }
    
    var rowHight : CGFloat = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlatformCell") as! PlatformCell
        let entry = tableData[indexPath.row]
        cell.entry = entry
//        let isUnabled = entry == HYPlayStation.Entry.SB
        cell.imgComingSoon.isHidden = true
        cell.imgBlack.isHidden = true
        cell.isUserInteractionEnabled = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openGame(tableData[indexPath.row])
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
}

class SlotPlatformListVC: PlatformListVC{
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if times.count == 0{
            startAnimating()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkTimes()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopAnimating()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GameCategoryTableVC, let entry = sender as? HYPlayStation.Entry {
            vc.entryItem = entry
        }
        else if let vc = segue.destination as? GameCategoryCollectionVC, let entry = sender as? HYPlayStation.Entry {
            vc.entryItem = entry
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let entry = tableData[indexPath.row]
        if entry.platform == HYPlayStation.Platform.OPE{
            performSegue(withIdentifier: "CollectionStyle", sender: entry)
        }else{
            performSegue(withIdentifier: "ListStyle", sender: entry)
        }
        // Do Nothing -> Segue
    }
    
    // MARK: - 累計人數
    
    var times : [String: Int] = [:]
    var labels: [String: UILabel] = [:]
    
    func checkTimes(){
        HYPlayStation.timesPlayed{
            [weak self] times, _ in
            self?.times = times
            for (k,v) in times{
                self?.labels[k]?.text = "累计人数： \(v)人"
            }
            self?.stopAnimating()
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath) as! PlatformCell
        if let time = times[cell.entry.platform]{
            cell.lbNumberOfPlayers!.text = "累计人数： \(time)人"
        }else{
            cell.lbNumberOfPlayers!.text = "累计人数： 0人" //抓失敗獲取不到也顯示0
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let entry = tableData[indexPath.row]
        labels[entry.platform] = (cell as! PlatformCell).lbNumberOfPlayers
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let platform = tableData[indexPath.row].platform
        labels[platform] = nil
    }

}

class PlayStationCategoryVC: UIViewController {
    
    var hasMore: Bool{
        return false
    }
    
    var category: String!{
        didSet{
            navigationItem.title = titles[category]
            listVC?.category = category
        }
    }
    
    var titles: [String: String] = [
        HYPlayStation.Category.Sport: "体育游戏"
        , HYPlayStation.Category.Electronic: "电子竞技"
        , HYPlayStation.Category.Real: "现场荷官"
        , HYPlayStation.Category.Slot: "游戏厅"
        , HYPlayStation.Category.Lottery: "彩票游戏"
    ]
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ListVC"{
            listVC = segue.destination as! PlatformListVC
            listVC.category = category
        }
    }
    
    var listVC: PlatformListVC!
}

class PlayStationSlotCategoryVC: PlayStationCategoryVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateJackpod()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startRunJackpod()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timer?.invalidate()
    }
    
    // MARK: - 總獎金
    @IBOutlet var aryLbJackpodAmount: [UILabel]!
    
    func updateJackpod(){
        HYPlayStation.jackpodinfo{ [weak self] amount in
            self?.actureAmount = amount
            self?.amountNow = max(0, amount - 100.00)
            self?.startRunJackpod()
        }
    }
    
    var timer: Timer?
    var actureAmount: Double = 0
    var amountNow: Double = 0
    func startRunJackpod(){
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(nextAmount), userInfo: nil, repeats: true)
    }
    
    @objc func nextAmount(){
        if amountNow >= actureAmount{
            timer?.invalidate()
            showJackpot(amount: actureAmount)
        }
        showJackpot(amount: amountNow)
        amountNow += 0.01
    }
    
    func showJackpot(amount: Double){
        let word = String(format: "%.2f", amount)
        let chars: [Character] = word.reversed()
        
        for (idx, lb) in aryLbJackpodAmount.enumerated(){
            if idx < chars.count{
                lb.text = String(chars[idx])
            }else{
                break
            }
        }
    }
    
}

class PlayStationSportCategoryVC: PlayStationCategoryVC {
    
    @IBOutlet weak var bar: UIView!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbTeamHome: UILabel!
    @IBOutlet weak var lbTeamAway: UILabel!
    @IBOutlet weak var imgvTeamHome: UIImageView!
    @IBOutlet weak var imgvTeamAway: UIImageView!

    @IBOutlet weak var imgvNext: UIImageView!
    @IBOutlet weak var imgvPrevious: UIImageView!
    var idxNow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateSportMessage()
    }
    
    var messages: [HYPlayStation.SportMessage] = []{
        didSet{
            if messages.count > 0 {
                updateTopBarNews(message: messages[0])
            }
        }
    }
    
    func updateSportMessage(){
        HYPlayStation.fetchSportInfo{ [weak self] messages in
            self?.messages = messages
        }
    }
    
    @IBAction func didBtnNextClick(btn: UIButton){
        let idx = idxNow + 1
        if idx < messages.count{
            changeToIndex(idx: idx)
        }
    }
    
    @IBAction func didBtnPreviousClick(btn: UIButton){
        let idx = idxNow - 1
        if idx >= 0{
            changeToIndex(idx: idx)
        }
    }
    
    func changeToIndex(idx: Int){
        updateTopBarNews(message: messages[idx])
        idxNow = idx
        imgvNext.isHidden = idxNow == messages.count - 1
        imgvPrevious.isHidden = idxNow == 0
    }
    
    func updateTopBarNews(message: HYPlayStation.SportMessage?){
        lbDate.text = message?.date?.toString(format: "yyyy/MM/dd HH:mm:ss")
        lbTeamHome.text = message?.teamHomeTitle
        lbTeamAway.text = message?.teamAwayTitle
        imgvTeamHome.sd_setImage(with: message?.urlImageTeamHome, placeholderImage: nil)
        imgvTeamAway.sd_setImage(with: message?.urlImageTeamAway, placeholderImage: nil)
    }
    
}
