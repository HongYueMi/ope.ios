//
//  GameCategoryCollectionVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/6.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class GameCategoryCollectionCell: UICollectionViewCell {
    
    @IBInspectable var colorSelected: UIColor = .gray
    @IBInspectable var colorDefault: UIColor = .white
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    
    override func awakeFromNib() {
        self.backgroundColor = colorDefault
    }
    
    override var isHighlighted: Bool{
        didSet{
            self.backgroundColor = isHighlighted ? colorSelected : isSelected ? colorSelected : colorDefault
        }
    }
    
    override var isSelected: Bool{
        didSet{
            self.backgroundColor = isSelected ? colorSelected : colorDefault
        }
    }
    
}

class GameCategoryCollectionVC: GameCategoryVC, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        windowWidth = UIScreen.main.bounds.width
    }
    
    func didBtnPlayClick(btn: UIButton){
        openGame(collectionData[btn.tag])
    }
    
    override var filteredData: [HYPlayStation.Game]{
        set{
            collectionData = newValue
        }
        get{
            return collectionData
        }
    }
    //MARK: Collection
    
    @IBOutlet weak var collectionView: UICollectionView!
    var collectionData: [HYPlayStation.Game] = []{
        didSet{
            collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GameCategoryCollectionCell", for: indexPath) as! GameCategoryCollectionCell
        let game = collectionData[indexPath.row]
        cell.lbTitle.text = game.title
        cell.imageView.image = game.imageName == nil ? nil : UIImage(named: game.imageName!)
        if cell.imageView.image == nil{
            print("標題：\(game.title), game code：\(game.code)")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        openGame(collectionData[indexPath.row])
    }
    
    // Layout
    
    var windowWidth: CGFloat = 414
    let space: CGFloat = 12.00
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let l: CGFloat = min((windowWidth - 3.0 * space) / 2, 207)
        return CGSize(width: l, height: l)
    }

}
