//
//  PlayWebVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/5.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import WebKit

class PlayWebVC: UIViewController, WKUIDelegate{
    
    var navDelegate: GameWKNavigationDelegate?
    var game: HYPlayable!
    
    weak var webVC : WebVC!
    
    var webView: WKWebView{
        return webVC.webView
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EmbedWebView"{
            self.webVC = segue.destination as! WebVC
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = game.title
        navigationItem.leftBarButtonItem = genDismissBarButtonItem()
        webView.uiDelegate = self
        webView.navigationDelegate = navDelegate
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let url = game.playURL{
            webView.load(URLRequest(url: url))
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        webView.stopLoading()
    }
    
    // MARK: - Navigation
    
    func genDismissBarButtonItem() -> UIBarButtonItem {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btn.setImage(#imageLiteral(resourceName: "Main_Nav_BackArror"), for: .normal)
        btn.addTarget(self, action: #selector(checkExit), for: .touchUpInside)
        return UIBarButtonItem(customView: btn)
    }
    
    @objc func checkExit(){
        let dialog = UIStoryboard.Game.identifier("CheckExitGameDialog")
        present(dialog, animated: true)
    }
    
    //MARK: - Action
    
    @IBAction func refresh(){
       webView.reload()
    }
    
    // MARK: - UI
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let panel = UIStoryboard.Game.identifier("WebViewJavaScriptPanelVCAlert") as! WebViewJavaScriptPanelVC
        panel.message = message
        panel.answerHandler = { _,_  in
            self.dismiss(animated: true){
                completionHandler()
            }
        }
        showVC(vc: panel)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        let panel = UIStoryboard.Game.identifier("WebViewJavaScriptPanelVCConfirm") as! WebViewJavaScriptPanelVC
        panel.message = message
        panel.answerHandler = { answer, _ in
            self.dismiss(animated: true){
                completionHandler(answer)
            }
        }
        showVC(vc: panel)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        let panel = UIStoryboard.Game.identifier("WebViewJavaScriptPanelVCTextInput") as! WebViewJavaScriptPanelVC
        panel.message = prompt
        panel.placeholder = defaultText
        
        panel.answerHandler = { _, panel in
            let answer = panel.tfTextField?.text
            self.dismiss(animated: true){
                completionHandler(answer)
            }
        }
        showVC(vc: panel)
    }
    
}

class WebViewJavaScriptPanelVC: UIEditVC{
    
    @IBOutlet weak var viewDialog: UIView!
    @IBOutlet weak var lbMessage: UILabel!
    @IBOutlet weak var tfTextField: UITextField?
    
    var message: String?
    var placeholder: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDialog.layer.cornerRadius = 16
        lbMessage.text = message
        tfTextField?.placeholder = placeholder
    }
    
    @IBAction func didBtnCancelClick(){
        answerHandler(false, self)
    }
    
    @IBAction func didBtnConfirmClick(){
        answerHandler(true, self)
    }
    
    var answerHandler: ((Bool, WebViewJavaScriptPanelVC) -> Void)!
}

class GameWKNavigationDelegateAG: GameWKNavigationDelegate{
    
    // Permitting
    
    var limitedPaths: [String] = [
        "www.ope88.com/" // Online 環境 Disable AG首頁返回鍵
        , "rd.www.room88.net/" // RD Demo環境 Disable AG首頁返回鍵
    ]
    
    override func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if
            let url = navigationAction.request.url
        {
            let components = url.absoluteString.components(separatedBy: "://")
            if components.count > 1{
                let path = components[1]
                // 限制網站
                for limitedPath in limitedPaths{
                    if path == limitedPath{
                        decisionHandler(.cancel)
                        vc.refresh()
                        return
                    }
                }
            }
        }
        decisionHandler(.allow)
    }
}

class GameWKNavigationDelegateBBIN: GameWKNavigationDelegate{
    
    func handel(schema: String, url: URL){
        if UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }else{
            vc.showGeneralAlert(.alert_info, lbTextA:"无适合APP可执行您的请求"){ value in }
        }
    }
    
    override func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print("\n[Web Nav Start]\n--> Now at\n \(webView.url?.absoluteString ?? "nil") \ndecidePolicyFor navigationAction to \n\(navigationAction.request.url?.absoluteString ?? "nil") \ntime: \(Date())")

        if let url = navigationAction.request.url, let scheme = url.scheme
            , scheme == "com.bb.aio3.appstore" 
        {
            decisionHandler(.cancel)
            handel(schema: scheme, url: url)
        }
        decisionHandler(.allow)
    }
}

class GameWKNavigationDelegate: NSObject, WKNavigationDelegate{
    
    static let BBIN = GameWKNavigationDelegateBBIN()
    static let AG = GameWKNavigationDelegateAG()
    static let Develope = GameWKNavigationDelegate()

    weak var vc: PlayWebVC!
    
    // MARK: - Navigation
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("--> didStartProvisionalNavigation, \nurl = \(webView.url?.absoluteString ?? "nil") \ntime: \(Date())")
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
       print("--> didReceiveServerRedirectForProvisionalNavigation, \nurl = \(webView.url?.absoluteString ?? "nil") \ntime: \(Date())")
    }
    
    // 取回html，開始解析
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print("--> didCommit, \nurl = \(webView.url?.absoluteString ?? "nil") \ntime: \(Date())")
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("--> didFail [失敗！！], \nurl = \(webView.url?.absoluteString ?? "nil") \ntime: \(Date())")
        print(error)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("--> didFinish, \nurl = \(webView.url?.absoluteString ?? "nil") \ntime: \(Date())")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print("\n[Web Nav Start]\n--> Now at\n \(webView.url?.absoluteString ?? "nil") \ndecidePolicyFor navigationAction to \n\(navigationAction.request.url?.absoluteString ?? "nil") \ntime: \(Date())")
        
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        print("--> decidePolicyFor navigationResponse, \nurl = \(webView.url?.absoluteString ?? "nil") \ntime: \(Date())")
        decisionHandler(.allow)
    }
}

class PlayOnUIWebViewVC: UIViewController, UIWebViewDelegate {
    
    var game: HYPlayable!
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HTTPCookieStorage.shared.cookieAcceptPolicy = .always
        navigationItem.title = game.title
        navigationItem.leftBarButtonItem = genDismissBarButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let url = game.playURL{
            webView.loadRequest(URLRequest(url: url))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK: - Navigation
    
    func genDismissBarButtonItem() -> UIBarButtonItem {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btn.setImage(#imageLiteral(resourceName: "Main_Nav_BackArror"), for: .normal)
        btn.addTarget(self, action: #selector(checkExit), for: .touchUpInside)
        return UIBarButtonItem(customView: btn)
    }
    
    @objc func checkExit(){
        let dialog = UIStoryboard.Game.identifier("CheckExitGameDialog")
        present(dialog, animated: true)
    }
    
}

class CheckExitGameDialog: UIViewController {
    
    @IBOutlet weak var viewDialog: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDialog.layer.cornerRadius = 16
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didBtnCancelClick(btn: UIButton){
        presentingViewController?.dismiss(animated: true)
    }
    
    @IBAction func didBtnEnterClick(btn: UIButton){
        presentingViewController?.presentingViewController?.dismiss(animated: true)
    }
    
    func dismissDialog(completion: (() -> Void)?){
        
    }
    
}
