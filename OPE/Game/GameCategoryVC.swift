//
//  GameCategoryVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/27.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class GameCategoryVC: PlayStationVC, UISearchBarDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let tf = searchBar.value(forKey: "searchField") as? UITextField{
            tf.backgroundColor = #colorLiteral(red: 0.5176470588, green: 0.5843137255, blue: 0.6549019608, alpha: 1)
        }
        if let all = categories.first{
            selectCategory(all)
        }
        registerKeyBoardListener()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
        isSearchActive = false
        searchBar.setShowsCancelButton(false, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Data
    
    var entryItem: HYPlayStation.Entry!{
        didSet{
            navigationItem.title = entryItem.title
            HYPlayStation.shared.getGameList(playform: entryItem.platform){[weak self] list, error in
                self?.gameList = list ?? []
            }
        }
    }
    
    var gameList: [HYPlayStation.Game] = []{
        didSet{
            resetCategory()
        }
    }
    
    //MARK: - Category Open Buttom
    
    @IBOutlet weak var lbCategory: UILabel!
    @IBOutlet weak var ctrlCategory: UIControl!
    
    typealias TypeName = String
    
    class Category: HYOption{
        var id: Int
        var key: String
        var name: String
        
        init(withID id: Int, key: String, name: String) {
            self.id = id
            self.key = key
            self.name = name
        }
    }
    
    var categories: [Category] = []
    var mapCategoryAndGames: [TypeName: [HYPlayStation.Game]] = [:]
    
    let keyPROGRESSIVE = "PROGRESSIVE"
    let keyALLGAME = "ALLGAME"
    func resetCategory(){
        var newMap: [TypeName: [HYPlayStation.Game]] = [keyALLGAME: gameList, keyPROGRESSIVE:[]]
        
        for game in gameList{
            guard let category = game.typeFromSuppier else {
                continue
            }
            if nil == newMap[category]{
                newMap[category] = []
            }
            newMap[category]!.append(game)
            if game.isPrograssive{
                newMap[keyPROGRESSIVE]?.append(game)
            }
        }
        for (key, games) in newMap{
            print("分類名: \(key) 數量:\(games.count)")
        }
        
        mapCategoryAndGames = newMap
        categories = genCategory().filter{(newMap[$0.key] ?? []).count > 0}
        if #available(iOS 9.0, *) {
            if let all = categories.first, (viewIfLoaded != nil){
                selectCategory(all)
            }
        } else {
            if let all = categories.first, nil != lbCategory{
                selectCategory(all)
            }
        }
    }
    
    func genCategory() -> [Category]{
        if entryItem == HYPlayStation.Entry.PT_SLOT{
            return [
                Category(withID: 0, key: keyALLGAME, name: Language.Game.Category.All)
                , Category(withID: 1, key: keyPROGRESSIVE, name: Language.Game.Category.Progress)
                , Category(withID: 2, key: "Slot Machines", name: Language.Game.Category.Slot_PT)
                , Category(withID: 3, key: "Table & Card Games", name: Language.Game.Category.TableGame_PT)
                , Category(withID: 4, key: "Arcade", name: Language.Game.Category.Arcades)
                , Category(withID: 5, key: "Scratch Cards", name: Language.Game.Category.Scratch_PT)
            ]
        }
        if entryItem == HYPlayStation.Entry.MG_SLOT{
            return [
                Category(withID: 0, key: keyALLGAME, name: Language.Game.Category.All)
                , Category(withID: 1, key: "PROGRESSIVES", name: Language.Game.Category.Progress)
                , Category(withID: 2, key: "SLOTS", name: Language.Game.Category.Slot)
                , Category(withID: 3, key: "Video Poker", name: Language.Game.Category.Porker)
                , Category(withID: 4, key: "Table Games", name: Language.Game.Category.TableGame)
            ]
        }
        if entryItem == HYPlayStation.Entry.OPE_SLOT{
            return [
                Category(withID: 0, key: keyALLGAME, name: Language.Game.Category.All)
                , Category(withID: 1, key: "Slots", name: Language.Game.Category.Slot)
                , Category(withID: 2, key: "Table Games", name: Language.Game.Category.TableGame)
                , Category(withID: 3, key: "Video Poker", name: Language.Game.Category.Porker)
                , Category(withID: 4, key: "Card Games", name: Language.Game.Category.CardGame)
                , Category(withID: 5, key: "Arcade", name: Language.Game.Category.Arcades)
                , Category(withID: 6, key: "Others", name: Language.Game.Category.Other)
            ]
        }
        if entryItem == HYPlayStation.Entry.BNG_SLOT{
            return [
                Category(withID: 0, key: keyALLGAME, name: Language.Game.Category.All)
            ]
        }
        return [
            Category(withID: 0, key: keyALLGAME, name: Language.Game.Category.All)
        ]
    }
    
    //
    
    var nowCategoryGames: [HYPlayStation.Game] = []{
        didSet{
            reloadFilteredData()
        }
    }
    
    // MARK: - Action
    
    @IBAction func didCtrlCategoryClick(ctrl: UIControl){
        isCategoryMenuShown = !isCategoryMenuShown
    }
    
    func selectCategory(_ category: Category){
        ctrlCategory?.isEnabled = true
        lbCategory?.text = category.name
        nowCategoryGames = mapCategoryAndGames[category.key] ?? []
    }
    
    //MARK: - Category Menu
    
    @IBOutlet weak var spaceForCategoryMenu: UIControl!
    
    var categoryMenu: UIView{
        if _categoryMenu == nil{
            _categoryMenu = genCcategoryMenu()
        }
        return _categoryMenu!
    }
    var _categoryMenu: UIView?
    
    
    func genCcategoryMenu() -> UIView{
        
        let size = ctrlCategory.frame.size // 選項的大小同分類按鈕
        
        var menuFrame: CGRect = spaceForCategoryMenu.frame
        menuFrame.size.height = CGFloat(categories.count) * size.height
        let categoryMenu = UIView(frame:menuFrame)
        categoryMenu.backgroundColor = #colorLiteral(red: 0, green: 0.02099477872, blue: 0.1244999245, alpha: 1)
        let originX: CGFloat = 0
        var originY: CGFloat = 0
        
        for (idx,category) in categories.enumerated(){
            let frame = CGRect(x: originX, y: originY, width: size.width, height: size.height)
            let btn = genOptionView(frame: frame, category: category)
            btn.tag = idx
            btn.addTarget(self, action: #selector(didOptionClick), for: .touchUpInside)
            originY += size.height
            categoryMenu.addSubview(btn)
        }
        return categoryMenu
    }
    
    func genOptionView(frame: CGRect, category: Category) -> UIButton{
        let btn = UIButton(frame: frame)
//        btn.setBackgroundImage(#imageLiteral(resourceName: "Game_CategoryOption_Background"), for: .normal)
//        btn.setBackgroundImage(#imageLiteral(resourceName: "Game_CategoryOption_Background_Highlighted"), for: .highlighted)
        btn.setTitle(category.name, for: .normal)
        return btn
    }
    
    @objc func didOptionClick(btn: UIButton){
        isCategoryMenuShown = false
        selectCategory(categories[btn.tag])
    }
    
    // Category Menu - Show & Dismiss
    
    var isCategoryMenuShown: Bool = false{
        didSet{
            if nil == categoryMenu.superview{
                categoryMenu.frame = hiddenFrame
                spaceForCategoryMenu.addSubview(categoryMenu)
            }
            let isShown = isCategoryMenuShown
            let toFrame = isShown ? shownFrame : hiddenFrame
            spaceForCategoryMenu.isHidden = false
            UIView.animate(withDuration: 0.2, animations: {
                self.categoryMenu.frame = toFrame
                self.categoryMenu.alpha = isShown ? 1 : 0
            }, completion: { _ in
                self.spaceForCategoryMenu.isHidden = !isShown
            })
            
        }
    }
    
    var hiddenFrame: CGRect{
        var frame = categoryMenu.bounds
        frame.origin.y =  -frame.size.height
        return frame
    }
    
    var shownFrame: CGRect{
        var frame = categoryMenu.bounds
        frame.origin.y = 0
        return frame
    }
    
    @IBAction func cancelCategorySelection(ctrl: UIControl){
        isCategoryMenuShown = false
    }
    
    // MARK: - Search
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var isSearchActive: Bool = false{
        didSet{
            reloadFilteredData()
            if !isSearchActive{
                searchBar.text = nil
            }
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        isSearchActive = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        reloadFilteredData()
    }
    
    // MARK: - Keyboard
    
    @IBOutlet weak var scrollView : UIScrollView?
    
    @IBAction open func hideKeyboard(){
        searchBar?.resignFirstResponder()
    }
    
    func registerKeyBoardListener(){
        let nfc = NotificationCenter.default
        nfc.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        nfc.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWasShown(ntf:Notification){
        if let kbRectValue = ntf.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue
        {
            let kbSize = kbRectValue.cgRectValue.size
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
            scrollView?.contentInset = contentInsets
            scrollView?.scrollIndicatorInsets = contentInsets
        }
    }
    
    @objc func keyboardWillBeHidden(ntf:Notification){
        let contentInsets = UIEdgeInsets.zero
        scrollView?.contentInset = contentInsets
        scrollView?.scrollIndicatorInsets = contentInsets
    }
    
    // MARK: View Use Data
    
    var filteredData: [HYPlayStation.Game] {
        set{
            
        }
        get{
            return []
        }
    }
    
    func reloadFilteredData() {
        
        if isSearchActive, let text = searchBar.text{
            filteredData = nowCategoryGames.filter{$0.title.lowercased().contains(text.lowercased())}
        }
        else{
            filteredData = nowCategoryGames
        }
    }
    
}
