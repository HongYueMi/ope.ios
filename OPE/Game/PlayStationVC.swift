//
//  PlayStationVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/18.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import WebKit

extension UIStoryboard{
    static let Game : UIStoryboard = UIStoryboard(name: "Game", bundle: nil)
    static let Platform : UIStoryboard = UIStoryboard(name: "Platform", bundle: nil)
}

class PlayStationVC: UIViewController, NVActivityIndicatorViewable {

    let newLoadingScreenView = LoadingIndicatorVC(); //產生LoadingView畫面物件
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 遊戲
    
    func openGame(_ game: HYPlayable){
        
        // 1. 檢查登入
        guard let _ = HYAuth.auth.currentUser else {
            
            if !game.needSignIn{
                checkCreditThenPlay(game: game)
            }
            else{
                showTwoSelectAlert(.alert_info,lbTextA:"请先登录", btTextOK:"登录"){ okValue, noValue in
                    if okValue == 1 , noValue == 0 {
                        HYAuth.auth.askForUserAuthorization();
                    }
                }
            }
            return
        }
        
        // 2. 檢查 Game 可否提供
        HYPlayStation.checkGameAvalibility(game){
            [weak self] isAvalable, error in
            
            if let err = error{
                self?.showGeneralAlert(.alert_info, lbTextA:err.localizedDescription){ value in }
                return
            }
            guard isAvalable else {
                self?.showGeneralAlert(.alert_info, lbTextA:"系统维护中，当前无法进入游戏，请关闭本窗口。"){ value in }
                return
            }
            
            // 3. 檢查 Game Credit & Member Credit
            self?.checkCreditThenPlay(game: game)
        }
    }
    
    func checkCreditThenPlay(game: HYPlayable){
        if
            let wallet = HYTransfer.transfer.platformWallets[game.platform]
            , let amount = wallet.amount
            , amount <= 0.0
        {
            let dialog = UIStoryboard.Platform.identifier("QuickTransferVC") as! QuickTransferVC
            dialog.wallet = wallet
            dialog.play = { [weak self] in
                self?.prepareURL(withGame: game)
            }
            view.isUserInteractionEnabled = true
            present(dialog, animated: true)
        }
        else{
            prepareURL(withGame: game)
        }
    }
    
    func prepareURL(withGame game: HYPlayable){
        startAnimating()
        HYPlayStation.prepareURLforGame(game){ [weak self] game, error in
            self?.stopAnimating()
            guard nil == error else{
                self?.showGeneralAlert(.alert_error, lbTextA:error?.localizedDescription ?? "Error"){ value in }
                return
            }
            self?.play(withGame: game)
        }
    }
    
    func play(withGame game: HYPlayable){
//        print(game.playURL ?? "Game: \(game.platform) + \(game.code) URL 取得失敗")
        
        var nav: UINavigationController!
        
        if game.platform == HYPlayStation.Platform.PT{
            nav = UIStoryboard.Game.identifier("PlayOnUIWebViewNavVC") as! UINavigationController
            let vc = nav.viewControllers.first! as! PlayOnUIWebViewVC
            vc.game = game
        }
        else if game.platform == HYPlayStation.Platform.OPUS, let url = game.playURL, let scheme = url.scheme, scheme == "itms-appss", UIApplication.shared.canOpenURL(url){
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:])
            } else {
                UIApplication.shared.openURL(url)
            }
            return
        }
        else{
            nav = UIStoryboard.Game.identifier("PlayWebNavVC") as! UINavigationController
            let vc = nav.viewControllers.first! as! PlayWebVC
            
            if game.platform == HYPlayStation.Platform.AG{
                vc.navDelegate = GameWKNavigationDelegate.AG
                vc.navDelegate?.vc = vc
            }else if game.platform == HYPlayStation.Platform.BBIN{
                vc.navDelegate = GameWKNavigationDelegate.BBIN
                vc.navDelegate?.vc = vc
            }else{
                vc.navDelegate = GameWKNavigationDelegate.Develope
            }
            
            vc.game = game
        }
        
        if nil != self.presentedViewController{
            self.dismiss(animated: true){
                self.present(nav, animated: true)
            }
        }else{
            self.present(nav, animated: true)
        }
    }
    
    
}
