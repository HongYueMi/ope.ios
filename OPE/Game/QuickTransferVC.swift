//
//  QuickTransferVC.swift
//  
//
//  Created by Lavend K. Mi on 2017/11/21.
//

import UIKit

class QuickTransferVC: TransferVC {
    
    var wallet: HYWallet!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var viewDialog: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDialog.layer.cornerRadius = 8
        walletTo = wallet
        lbTitle.text = wallet.name + "快速转账"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didControlAroundClick(){
        if tfAmount.isFirstResponder{
            hideKeyboard()
        }else{
            presentingViewController?.dismiss(animated: true)
        }
    }
    
    override func didTransferComplete() {
        play?()
    }
    
    @IBAction func didBtnPlayDirectlyClick(btn: UIButton){
        play?()
    }
    
    var play: (()->Void)?
    
    @IBAction func goDeposit(){
        MainVC.shared.backToMainPage(withIndex: 3, animated: false)
        let vc = UIStoryboard.DepositFund.identifier("DepositPageVC")
        MainVC.shared.navigationController?.pushViewController(vc, animated: false)
    }
    
}
