//
//  GameTabBarVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/10/24.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class GameTabBarVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let w = UIScreen.main.bounds.width
        tabSize = CGSize(width: w / CGFloat(tabItems.count), height: w * 49 / 360)
        collectionView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Tab Bar
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    struct TabIcon {
        let icon: UIImage
        let iconSelected: UIImage
        let title: String
    }
    
    var tabItems: [TabIcon] = []
    
//    var tabItems: [MainVC.TabIcon] = [
//        MainVC.TabIcon(icon: #imageLiteral(resourceName: "Main_TabIcon_OPE"), iconSelected: #imageLiteral(resourceName: "Main_TabIcon_OPE_Selected"), title: "OPE")
//        , MainVC.TabIcon(icon: #imageLiteral(resourceName: "Main_TabIcon_Promotion"), iconSelected: #imageLiteral(resourceName: "Main_TabIcon_Promotion_Selected"), title: "优惠活动")
//        , MainVC.TabIcon(icon: #imageLiteral(resourceName: "Main_TabIcon_LiveStream"), iconSelected: #imageLiteral(resourceName: "Main_TabIcon_LiveStream_Selected"), title: "直播")
//        , MainVC.TabIcon(icon: #imageLiteral(resourceName: "Main_TabIcon_Member"), iconSelected: #imageLiteral(resourceName: "Main_TabIcon_Member_Selected"), title: "个人中心")
//    ]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TabCell", for: indexPath) as! MainTabCell
//        cell.item = tabItems[indexPath.row]
//        cell.isSelected = indexPath.row == 0
        return cell
    }
    
    var tabSize: CGSize = CGSize.zero
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == tabItems.count - 1{
            var s = tabSize
            s.width = UIScreen.main.bounds.width - CGFloat(tabItems.count - 1) * tabSize.width
            return s
        }
        return tabSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        MainVC.shared.backToMainPage(withIndex: indexPath.row)
    }
    
}
