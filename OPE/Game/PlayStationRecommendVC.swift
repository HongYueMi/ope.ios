//
//  PlayStationRecommendVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/12/20.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class PlayStationRecommendCell: UICollectionViewCell{
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var imgvIcon: UIImageView!
    
    var item: PlayStationRecommendVC.Item!{
        didSet{
            lbTitle.text = item.title
            imgvIcon.image = item.image
        }
    }
}

class PlayStationRecommendVC: PlayStationVC, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshRecommend()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let W = collectionView.frame.size.width
        space = CGFloat(Int(W * 7 / 180))
        let w = CGFloat(Int((W - 5 * space) / 4))
        cellSize = CGSize(width: w, height: collectionView.frame.size.height)
        collectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Data
    
    let all: [Item] = [
        //                                                                                                                  Game_Recommend_
        // 體育
        Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "欧洲体育", image: #imageLiteral(resourceName: "Game_Recommend_europe"), imgBackground:#imageLiteral(resourceName: "Game_Recommend_europe_2") )
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "美洲体育", image: #imageLiteral(resourceName: "Game_Recommend_america"), imgBackground:#imageLiteral(resourceName: "Game_Recommend_america_2") )
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "亚洲体育", image: #imageLiteral(resourceName: "Game_Recommend_asia"), imgBackground:#imageLiteral(resourceName: "Game_Recommend_asia_2") )
        
        // 電競
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "星际争霸2", image: #imageLiteral(resourceName: "Game_Recommend_star"), imgBackground:#imageLiteral(resourceName: "Game_Recommend_star_2") )
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "守望先锋", image: #imageLiteral(resourceName: "Game_Recommend_over"), imgBackground:#imageLiteral(resourceName: "Game_Recommend_over_2") )
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "王者荣耀", image: #imageLiteral(resourceName: "Game_Recommend_king"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_king_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "英雄联盟", image: #imageLiteral(resourceName: "Game_Recommend_lol"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_lol_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "DOTA2", image: #imageLiteral(resourceName: "Game_Recommend_dota2"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_dota2_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "炉石传说", image: #imageLiteral(resourceName: "Game_Recommend_heroes"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_heroes_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "CS GO", image: #imageLiteral(resourceName: "Game_Recommend_cs"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_cs_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "风暴英雄", image: #imageLiteral(resourceName: "Game_Recommend_stone"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_stone_2"))
        
        // 現場荷官
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "OPE电竞", image: #imageLiteral(resourceName: "Game_Recommend_ope"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_ope_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "AG真人", image: #imageLiteral(resourceName: "Game_Recommend_ag"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_ag_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "BBIN真人", image: #imageLiteral(resourceName: "Game_Recommend_bbin"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_bbin_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "Opus真人", image: #imageLiteral(resourceName: "Game_Recommend_opus"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_opus_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "eBET真人", image: #imageLiteral(resourceName: "Game_Recommend_ebet"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_ebet_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "OG真人", image: #imageLiteral(resourceName: "Game_Recommend_og"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_og_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "申博真人", image: #imageLiteral(resourceName: "Game_Recommend_sunbet"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_sunbet_2"))
        
        // 遊戲廳
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "PT电游", image: #imageLiteral(resourceName: "Game_Recommend_pt"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_pt_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "MG电游", image: #imageLiteral(resourceName: "Game_Recommend_mg"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_mg_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "OPE电游", image: #imageLiteral(resourceName: "Game_Recommend_ope"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_ope_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "Booongo电游", image: #imageLiteral(resourceName: "Game_Recommend_bng"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_bng_2"))
        
        // 彩票
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "BBIN彩票", image: #imageLiteral(resourceName: "Game_Recommend_bbin"), imgBackground: #imageLiteral(resourceName: "Game_Recommend_bbin_2"))
        , Item(category: HYPlayStation.Category.Sport, platform: HYPlayStation.Platform.BETCONSTRUCT, title: "LiB彩票", image: #imageLiteral(resourceName: "Game_Recommend_lbkeno"), imgBackground:#imageLiteral(resourceName: "Game_Recommend_lbkeno_2") )
    ]
    
    @IBAction func refreshRecommend(){
        let count: UInt32 = UInt32(all.count)
        var idxes: [Int] = []
        while idxes.count < 4 {
            let idx = Int(arc4random_uniform(count))
            if !idxes.contains(idx){
                idxes.append(idx)
            }
        }
        collectionData = idxes.map{all[$0]}
    }
    
    // MARK: - CollectionView
    
    class Item{
        init(category: String, platform: String, title: String, image: UIImage, imgBackground: UIImage) {
            self.category = category
            self.platform = platform
            self.title = title
            self.image = image
            self.imgBackground = imgBackground
        }
        let category: Category
        let platform: Platform
        let title: String
        let image: UIImage
        let imgBackground: UIImage
    }
    
    var collectionData: [Item] = []{
        didSet{
            collectionView.reloadData()
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var cellSize: CGSize = .zero
    private var space: CGFloat = 0
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecommendCell", for: indexPath) as! PlayStationRecommendCell
        cell.item = collectionData[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: space, bottom: 0, right: space)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return space
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = collectionData[indexPath.row]
        let entry = HYPlayStation.entries[item.category]![item.platform]!
        play(withGame: entry)
    }
}
