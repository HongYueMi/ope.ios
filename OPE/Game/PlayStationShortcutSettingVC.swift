

//
//  PlayStationShortcutSettingVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/10/26.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class PlayStationShortcutSettingEntryCell: UICollectionViewCell{
    
    @IBOutlet weak var imgvPicture: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    
    var entry: HYPlayStation.Entry!{
        didSet{
            updateView()
        }
    }
    
    func updateView(){
        lbTitle.text = entry?.title
        imgvPicture.image = entry.shortcutImage[3]
    }
    
    var didBtnCancelClickHandler: ((HYPlayStation.Entry) -> Void)?
    
    @IBAction func didBtnCancelClick(btn: UIButton){
        didBtnCancelClickHandler?(entry)
    }
    
}

class PlayStationShortcutSettingCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var imgvHeaderBackground: UIImageView!
    @IBOutlet weak var imgvCellBackground: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btnExpend: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var item: PlayStationShortcutSettingVC.TableItem!{
        didSet{
            imgvHeaderBackground.image = item.isExpended ? item.headerExpendedBackground : item.headerBackground
            lbTitle.text = item.title
            collectionViewSelected?.reloadData()
            collectionViewCandidate?.reloadData()
        }
    }
    
    // MARK: - Life Cycle
    
    override func layoutSubviews() {
        super.layoutSubviews()
        moveReorderBtnToTop()
        
        // Collection
        if let collectionView = collectionViewSelected{
            var h = CGFloat(Int(collectionView.bounds.size.height))
            h = max(h - 1, 0)
            cellFavorateSize = CGSize(width: h * 7 / 8, height: h)
            collectionView.reloadData()
        }
        if let collectionView = collectionViewCandidate{
            var h = CGFloat(Int(collectionView.bounds.size.height))
            h = max(h - 1, 0)
            cellCandidateSize = CGSize(width: h * 7 / 8, height: h)
            collectionView.reloadData()
        }
    }
    
    // MARK: - Reorder
    
    var ctrlReorder: UIControl?
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        guard editing else {return}
        ctrlReorder = subviews.first(where: {String(describing: type(of: $0)) == "UITableViewCellReorderControl"}) as? UIControl
    }
    
    func moveReorderBtnToTop(){
        if let ctrl = ctrlReorder{
            let rect = btnExpend.frame
            ctrl.frame = CGRect(x: rect.origin.x + rect.size.width + 12, y: rect.origin.y, width: 32, height: 32)
            
            let imgvIcon = ctrl.subviews.first(where: {$0 is UIImageView}) as! UIImageView
            imgvIcon.bounds = CGRect(x: 0, y: 0, width: 28, height: 28)
            imgvIcon.image = #imageLiteral(resourceName: "Game_Shortcut_Icon_Move")
            imgvIcon.contentMode = .scaleAspectFill
        }
    }

    // MARK: - CollectionView
    
    var cellFavorateSize: CGSize = .zero
    var cellCandidateSize: CGSize = .zero
    
    @IBOutlet weak var collectionViewSelected: UICollectionView!
    @IBOutlet weak var collectionViewCandidate: UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if item == nil{
            return 0
        }else if collectionView == collectionViewSelected{
            return item.favorates.count
        }else if collectionView == collectionViewCandidate{
            return item.cadidate.count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewSelected{
            return cellFavorateSize
        }
        return cellCandidateSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShortcutCell", for: indexPath) as! PlayStationShortcutSettingEntryCell
        if collectionView == collectionViewSelected{
            let category = item.category
            let platform = item.favorates[indexPath.row]
            cell.entry = HYPlayStation.entries[category]![platform]!
            cell.didBtnCancelClickHandler = {[weak self] entry in
                let user = HYAuth.auth.currentUser ?? HYUser.anonymous
                user.favorPlatform[category]!.remove(at: indexPath.row)
                self?.item.updateFavorate()
                self?.tableView.reloadData()
            }
        }else{
            let category = item.category
            let platform = item.cadidate[indexPath.row]
            cell.entry = HYPlayStation.entries[category]![platform]!
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard collectionView == collectionViewCandidate else {
            return
        }
        
        let platform = item.cadidate[indexPath.row]
        let user = HYAuth.auth.currentUser ?? HYUser.anonymous
        if nil == user.favorPlatform[item.category]{
            user.favorPlatform[item.category] = [platform]
            item.updateFavorate()
            tableView.reloadData()
        }
        else if user.favorPlatform[item.category]!.count < 3 {
            user.favorPlatform[item.category]!.insert(platform, at: 0)
            item.updateFavorate()
            tableView.reloadData()
        }
        
    }
    
}

class PlayStationShortcutSettingVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.setEditing(true, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for item in tableItems.values{
            item.cellWidth = tableView.bounds.size.width
        }
        updateTableData()
    }
   
    // MARK: - Action

    @IBAction func didBtnDoneClick(item: UIBarButtonItem){
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didBtnDefaultOrderClick(btn: UIButton){
        let user = HYAuth.auth.currentUser ?? HYUser.anonymous
        user.favorCategoryOrder = HYPlayStation.Category.all
        user.favorPlatform = HYPlayStation.Platform.category.mapValues{Array($0.prefix(3))}
        for item in tableItems.values{
            item.updateFavorate()
        }
        updateTableData()
    }
    
    // MARK: - Table
    
    class TableItem {
        
        let category: String
        let title: String
        let headerBackground: UIImage?
        let headerExpendedBackground: UIImage?
        
        init(category: String, title: String, header: UIImage, headerExpended: UIImage) {
            self.category = category
            self.title = title
            self.headerBackground = header
            self.headerExpendedBackground = headerExpended
            self.updateFavorate()
        }
        
        var isExpended: Bool = false{
            didSet{
                updateCellHeight()
            }
        }
        var cellIdentifier: String{
            
            if !isExpended{
                return "Closed"
            }
            if favorates.count == 0{
                if cadidate.count == 0{
                    return "Closed"
                }
                return "AllCandidate"
            }
            else if cadidate.count == 0{
                return "AllFavorate"
            }
            return "Expended"
        }
        
        var cadidate: [String] = []
        var favorates: [String] = []
        
        func updateFavorate(){
            let user = HYAuth.auth.currentUser ?? HYUser.anonymous
            favorates = user.favorPlatform[category] ?? []
            cadidate = user.availableCategoryPlatforms[category]!.filter{!favorates.contains($0)}
            updateCellHeight()
        }
        
        var cellHeight: CGFloat = 0
        var cellWidth: CGFloat = 0{
            didSet{
                updateCellHeight()
            }
        }
        
        func updateCellHeight(){
            if !isExpended{
                cellHeight = CGFloat(Int(cellWidth * 16 / 108) + 1)
                return
            }
            var h: CGFloat = 16
            if favorates.count > 0 {
                h += 37
            }
            if cadidate.count > 0{
                h += 37
            }
            if h > 16{
                h += 5
            }
            cellHeight = CGFloat(Int(cellWidth * h / 108) + 1)
        }
    }
    
    var tableItems: [String: TableItem] = [
        HYPlayStation.Category.Sport: TableItem(category: HYPlayStation.Category.Sport, title: "体育游戏", header: #imageLiteral(resourceName: "Game_Sport_Shortcut_Header"), headerExpended: #imageLiteral(resourceName: "Game_Sport_Shortcut_ExpendedHeader"))
        , HYPlayStation.Category.Electronic: TableItem(category: HYPlayStation.Category.Electronic, title: "电子竞技", header: #imageLiteral(resourceName: "Game_Electronic_Shortcut_Header"), headerExpended: #imageLiteral(resourceName: "Game_Electronic_Shortcut_ExpendedHeader"))
        , HYPlayStation.Category.Real: TableItem(category: HYPlayStation.Category.Real, title: "现场荷官", header: #imageLiteral(resourceName: "Game_Real_Shortcut_Header"), headerExpended: #imageLiteral(resourceName: "Game_Real_Shortcut_ExpendedHeader"))
        , HYPlayStation.Category.Slot: TableItem(category: HYPlayStation.Category.Slot, title: "游戏厅", header: #imageLiteral(resourceName: "Game_Slot_Shortcut_Header"), headerExpended: #imageLiteral(resourceName: "Game_Slot_Shortcut_ExpendedHeader"))
        , HYPlayStation.Category.Lottery: TableItem(category: HYPlayStation.Category.Lottery, title: "彩票游戏", header: #imageLiteral(resourceName: "Game_Lottery_Shortcut_Header"), headerExpended: #imageLiteral(resourceName: "Game_Lottery_Shortcut_ExpendedHeader"))
    ]
    
    var tableData: [TableItem] = []{
        didSet{
            tableView.reloadData()
        }
    }
    
    func updateTableData(){
        let user = HYAuth.auth.currentUser ?? HYUser.anonymous
        tableData = user.favorCategoryOrder.flatMap{tableItems[$0]}
    }
    
    @IBOutlet weak var tableView: UITableView!

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableData[indexPath.row].cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = tableData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cellIdentifier, for: indexPath) as! PlayStationShortcutSettingCell
        cell.item = item
        cell.btnExpend?.tag = indexPath.row
        cell.showsReorderControl = true
        return cell
    }
    
    @IBAction func didCellBtnExpendClick(btn: UIButton){
        for item in tableData{
            if item.isExpended{
                item.isExpended = false
                break
            }
        }
        tableData[btn.tag].isExpended = true
        tableView.reloadData()
    }
    
    @IBAction func didCellBtnCloseClick(btn: UIButton){
        tableData[btn.tag].isExpended = false
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let user = HYAuth.auth.currentUser ?? HYUser.anonymous
        var ary = user.favorCategoryOrder
        let item = ary.remove(at: sourceIndexPath.row)
        ary.insert(item, at: destinationIndexPath.row)
        user.favorCategoryOrder = ary
        updateTableData()
    }

}
