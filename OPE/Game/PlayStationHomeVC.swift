//
//  OPEVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/23.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

typealias Category = String
typealias Platform = String

extension HYPlayStation{
    // MARK: - Short Cut
    
    class Entry: Equatable, HYPlayable {
        let category: String
        let platform: String
        let type: String?
        let code: String = ""// 寫死
        let needSignIn: Bool
        let image: UIImage
        let icon: UIImage
        let shortcutImage: [UIImage]
        let title: String
        let subTitle: String?
        let space: [Space]
        var playURL: URL?
        
        enum Space: String {
            case top = "TitleTopCell"
            case bottom = "TitleBottomCell"
            case right = "TitleRightCell"
            case left = "TitleLeftCell"
        }
        
        static func ==(lhs: Entry, rhs: Entry) -> Bool{
            return lhs.platform == rhs.platform
        }
        init(title: String, subTitle: String, category: String, platform: String, image: UIImage, icon: UIImage, shortcut: [UIImage], type: String? = nil, needSignIn: Bool = true, space: [Space] = [.bottom, .right]) {
            self.title = title
            self.subTitle = subTitle
            self.category = category
            self.platform = platform
            self.image = image
            self.icon = icon
            self.type = type
            self.needSignIn = needSignIn
            self.shortcutImage = shortcut
            self.space = space
        }
        static let BETCONSTRUCT_SPORT = Entry(title: "欧洲体育", subTitle: "Europe Sports", category: Category.Sport, platform: Platform.BETCONSTRUCT, image: #imageLiteral(resourceName: "Game_Sport_Europe_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_BetConstruct"), shortcut: [#imageLiteral(resourceName: "Game_Sport_Europe_Shortcut-1"), #imageLiteral(resourceName: "Game_Sport_Europe_Shortcut-2"), #imageLiteral(resourceName: "Game_Sport_Europe_Shortcut-3"), #imageLiteral(resourceName: "Game_Sport_Europe_Shortcut-4"), #imageLiteral(resourceName: "Game_Sport_Europe_Shortcut-5"), #imageLiteral(resourceName: "Game_Sport_Europe_Shortcut-6")], type: "", space: [.left, .left])// 歐洲
        static let OPUS_SPORT = Entry(title: "美洲体育", subTitle: "America Sports", category: Category.Sport, platform: Platform.OPUS_SPORT, image: #imageLiteral(resourceName: "Game_Sport_America_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OPUS_Sport"), shortcut: [#imageLiteral(resourceName: "Game_Sport_America_Shortcut-1"), #imageLiteral(resourceName: "Game_Sport_America_Shortcut-2"), #imageLiteral(resourceName: "Game_Sport_America_Shortcut-3"), #imageLiteral(resourceName: "Game_Sport_America_Shortcut-4"), #imageLiteral(resourceName: "Game_Sport_America_Shortcut-5"), #imageLiteral(resourceName: "Game_Sport_America_Shortcut-6")],  type: "", space: [.top, .left]) // 美洲
        static let SB = Entry(title: "亚洲体育", subTitle: "Asia Sports", category: Category.Sport, platform: Platform.SB, image: #imageLiteral(resourceName: "Game_Sport_Asia_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_SB"), shortcut: [#imageLiteral(resourceName: "Game_Sport_Asia_Shortcut-1"), #imageLiteral(resourceName: "Game_Sport_Asia_Shortcut-2"), #imageLiteral(resourceName: "Game_Sport_Asia_Shortcut-3"), #imageLiteral(resourceName: "Game_Sport_Asia_Shortcut-4"), #imageLiteral(resourceName: "Game_Sport_Asia_Shortcut-5"), #imageLiteral(resourceName: "Game_Sport_Asia_Shortcut-6")],  type: "", space: [.left, .left]) // 亞洲
        
        static let OPE_E_KING_OF_GLORY = Entry(title: "王者荣耀", subTitle: "King of Glory", category: Category.Electronic, platform: Platform.OPE_ELECTRONIC, image: #imageLiteral(resourceName: "Game_Electronic_OPE_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OPE_Electronic"), shortcut: [#imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-1"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-2"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-3"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-4")],  type: "", space: [.top, .left])
        static let OPE_E_LOL = Entry(title: "英雄联盟", subTitle: "League of Legends", category: Category.Electronic, platform: Platform.OPE_ELECTRONIC, image: #imageLiteral(resourceName: "Game_Electronic_OPE_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OPE_Electronic"), shortcut: [#imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-1"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-2"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-3"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-4")],  type: "", space: [.top, .left])
        static let OPE_E_DOTA2 = Entry(title: "DOTA 2 刀塔", subTitle: "Dota 2", category: Category.Electronic, platform: Platform.OPE_ELECTRONIC, image: #imageLiteral(resourceName: "Game_Electronic_OPE_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OPE_Electronic"), shortcut: [#imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-1"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-2"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-3"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-4")],  type: "", space: [.top, .left])
        static let OPE_E_CS_GO = Entry(title: "CS GO", subTitle: "Counter Strike: Global offensive", category: Category.Electronic, platform: Platform.OPE_ELECTRONIC, image: #imageLiteral(resourceName: "Game_Electronic_OPE_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OPE_Electronic"), shortcut: [#imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-1"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-2"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-3"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-4")],  type: "", space: [.top, .left])
        static let OPE_E_HEARTHSTONE = Entry(title: "炉石传说", subTitle: "Hearthstone: Heroes of Warcraft", category: Category.Electronic, platform: Platform.OPE_ELECTRONIC, image: #imageLiteral(resourceName: "Game_Electronic_OPE_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OPE_Electronic"), shortcut: [#imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-1"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-2"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-3"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-4")],  type: "", space: [.top, .left])
        static let OPE_E_OVERWATCH = Entry(title: "守望先锋", subTitle: "OverWatch", category: Category.Electronic, platform: Platform.OPE_ELECTRONIC, image: #imageLiteral(resourceName: "Game_Electronic_OPE_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OPE_Electronic"), shortcut: [#imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-1"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-2"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-3"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-4")],  type: "", space: [.top, .left])
        static let OPE_E_STAR_CRAFT2 = Entry(title: "星际争霸2", subTitle: "StarCraft 2", category: Category.Electronic, platform: Platform.OPE_ELECTRONIC, image: #imageLiteral(resourceName: "Game_Electronic_OPE_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OPE_Electronic"), shortcut: [#imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-1"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-2"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-3"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-4")],  type: "", space: [.top, .left])
        static let OPE_E_HEROS_OF_THE_STORM = Entry(title: "风暴英雄", subTitle: "Heroes of the Storm", category: Category.Electronic, platform: Platform.OPE_ELECTRONIC, image: #imageLiteral(resourceName: "Game_Electronic_OPE_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OPE_Electronic"), shortcut: [#imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-1"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-2"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-3"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-4")],  type: "", space: [.top, .left])
        
        static let OPE_ELECTRONIC = Entry(title: "OPE电竞", subTitle: "OPE eSports", category: Category.Electronic, platform: Platform.OPE_ELECTRONIC, image: #imageLiteral(resourceName: "Game_Electronic_OPE_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OPE_Electronic"), shortcut: [#imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-1"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-2"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-3"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-4"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-5"), #imageLiteral(resourceName: "Game_Electronic_OPE_Shortcut-6")],  type: "", space: [.right, .right])
        
        
        static let AG_LIVE = Entry(title: "AG真人", subTitle: "AG Live Casino", category: Category.Real, platform: Platform.AG, image: #imageLiteral(resourceName: "Game_Real_AG_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_AG"), shortcut: [#imageLiteral(resourceName: "Game_Real_AG_Shortcut-1"), #imageLiteral(resourceName: "Game_Real_AG_Shortcut-2"), #imageLiteral(resourceName: "Game_Real_AG_Shortcut-3"), #imageLiteral(resourceName: "Game_Real_OG_Shortcut-4"), #imageLiteral(resourceName: "Game_Real_AG_Shortcut-5"), #imageLiteral(resourceName: "Game_Real_AG_Shortcut-6")], type: "", space: [.right, .right])
        static let OG_REAL = Entry(title: "OG真人", subTitle: "OG Live Casino", category: Category.Real, platform: Platform.OG, image: #imageLiteral(resourceName: "Game_Real_OG_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OG"), shortcut: [#imageLiteral(resourceName: "Game_Real_OG_Shortcut-1"), #imageLiteral(resourceName: "Game_Real_OG_Shortcut-2"), #imageLiteral(resourceName: "Game_Real_OG_Shortcut-3"), #imageLiteral(resourceName: "Game_Real_OG_Shortcut-4"), #imageLiteral(resourceName: "Game_Real_OG_Shortcut-5"), #imageLiteral(resourceName: "Game_Real_OG_Shortcut-6")],type: "", space: [.left, .right])
        static let OPUS_CASINO = Entry(title: "Opus真人", subTitle: "Opus Live Casino", category: Category.Real, platform: Platform.OPUS, image: #imageLiteral(resourceName: "Game_Real_OPUS_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OPUS"), shortcut: [#imageLiteral(resourceName: "Game_Real_OPUS_Shortcut-1"), #imageLiteral(resourceName: "Game_Real_OPUS_Shortcut-2"), #imageLiteral(resourceName: "Game_Real_OPUS_Shortcut-3"), #imageLiteral(resourceName: "Game_Real_OPUS_Shortcut-4"), #imageLiteral(resourceName: "Game_Real_OPUS_Shortcut-5"), #imageLiteral(resourceName: "Game_Real_OPUS_Shortcut-6")],type: "", space: [.right, .right])
        
        static let BBIN_LIVE = Entry(title: "BBIN真人", subTitle: "BBIN Live Casino", category: Category.Real, platform: Platform.BBIN, image:#imageLiteral(resourceName: "Game_Real_BBIN_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_BBIN"), shortcut: [#imageLiteral(resourceName: "Game_Real_BBIN_Shortcut-1"), #imageLiteral(resourceName: "Game_Real_BBIN_Shortcut-2"), #imageLiteral(resourceName: "Game_Real_BBIN_Shortcut-3"), #imageLiteral(resourceName: "Game_Real_BBIN_Shortcut-4"), #imageLiteral(resourceName: "Game_Real_BBIN_Shortcut-5"), #imageLiteral(resourceName: "Game_Real_BBIN_Shortcut-6")],type: "live", space: [.left, .left])

        static let EBET_LIVE = Entry(title: "eBET真人", subTitle: "eBET Live Casino", category: Category.Real, platform: Platform.EBET, image: #imageLiteral(resourceName: "Game_Real_eBET_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_eBet"), shortcut: [#imageLiteral(resourceName: "Game_Real_eBET_Shortcut-1"), #imageLiteral(resourceName: "Game_Real_eBET_Shortcut-2"), #imageLiteral(resourceName: "Game_Real_eBET_Shortcut-3"), #imageLiteral(resourceName: "Game_Real_eBET_Shortcut-4"), #imageLiteral(resourceName: "Game_Real_eBET_Shortcut-5"), #imageLiteral(resourceName: "Game_Real_eBET_Shortcut-6")],type: "", space: [.right, .left])
        static let SUNBET_LIVE = Entry(title: "申博真人", subTitle: "SunBet Live Casino", category: Category.Real, platform: Platform.SUMBET, image: #imageLiteral(resourceName: "Game_Real_SunBet_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_SunBet"), shortcut: [#imageLiteral(resourceName: "Game_Real_SunBet_Shortcut-1"), #imageLiteral(resourceName: "Game_Real_SunBet_Shortcut-2"), #imageLiteral(resourceName: "Game_Real_SunBet_Shortcut-3"), #imageLiteral(resourceName: "Game_Real_SunBet_Shortcut-4"), #imageLiteral(resourceName: "Game_Real_SunBet_Shortcut-5"), #imageLiteral(resourceName: "Game_Real_SunBet_Shortcut-6")],type: "", space: [.left, .left])
        
        static let PT_SLOT = Entry(title: "PT电游", subTitle: "Playtech Slots", category: Category.Slot, platform: Platform.PT, image: #imageLiteral(resourceName: "Game_Slot_PT_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_PT"), shortcut: [#imageLiteral(resourceName: "Game_Slot_PT_Shortcut-1"), #imageLiteral(resourceName: "Game_Slot_PT_Shortcut-2"), #imageLiteral(resourceName: "Game_Slot_PT_Shortcut-3"), #imageLiteral(resourceName: "Game_Slot_PT_Shortcut-4"), #imageLiteral(resourceName: "Game_Slot_PT_Shortcut-5"), #imageLiteral(resourceName: "Game_Slot_PT_Shortcut-6")],type: "", space: [.bottom, .right])
        static let MG_SLOT = Entry(title: "MG电游", subTitle: "Microgaming Slots", category: Category.Slot, platform: Platform.MG, image: #imageLiteral(resourceName: "Game_Slot_MG_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_MG"), shortcut: [#imageLiteral(resourceName: "Game_Slot_MG_Shortcut-1"), #imageLiteral(resourceName: "Game_Slot_MG_Shortcut-2"), #imageLiteral(resourceName: "Game_Slot_MG_Shortcut-3"), #imageLiteral(resourceName: "Game_Slot_MG_Shortcut-4"), #imageLiteral(resourceName: "Game_Slot_MG_Shortcut-5"), #imageLiteral(resourceName: "Game_Slot_MG_Shortcut-6")],type: "", space: [.bottom, .right])
        static let OPE_SLOT = Entry(title: "OPE电游", subTitle: "OPE Slots", category: Category.Slot, platform: Platform.OPE, image: #imageLiteral(resourceName: "Game_Slot_OPE_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_OPE"), shortcut: [#imageLiteral(resourceName: "Game_Slot_OPE_Shortcut-1"), #imageLiteral(resourceName: "Game_Slot_OPE_Shortcut-2"), #imageLiteral(resourceName: "Game_Slot_OPE_Shortcut-3"), #imageLiteral(resourceName: "Game_Slot_OPE_Shortcut-4"), #imageLiteral(resourceName: "Game_Slot_OPE_Shortcut-5"), #imageLiteral(resourceName: "Game_Slot_OPE_Shortcut-6")],type: "", space: [.bottom, .right])
        static let BNG_SLOT = Entry(title: "Booongo电游", subTitle: "Booongo Slots", category: Category.Slot, platform: Platform.BNG, image: #imageLiteral(resourceName: "Game_Slot_BNG_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_BNG"), shortcut: [#imageLiteral(resourceName: "Game_Slot_BNG_Shortcut-1"), #imageLiteral(resourceName: "Game_Slot_BNG_Shortcut-2"), #imageLiteral(resourceName: "Game_Slot_BNG_Shortcut-3"), #imageLiteral(resourceName: "Game_Slot_BNG_Shortcut-4"), #imageLiteral(resourceName: "Game_Slot_BNG_Shortcut-5"), #imageLiteral(resourceName: "Game_Slot_BNG_Shortcut-6")],type: "", space: [.bottom, .right])
//        static let BBIN_SLOT = Entry(title: "BBIN电子游戏", subTitle: "BBIN Slots", category: Category.Slot, platform: Platform.BBIN, image: #imageLiteral(resourceName: "Game_Lottery_BBIN_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_BBIN"), shortcut: [#imageLiteral(resourceName: "Game_Lottery_BBIN_Shortcut-1"), #imageLiteral(resourceName: "Game_Lottery_BBIN_Shortcut-2"), #imageLiteral(resourceName: "Game_Lottery_BBIN_Shortcut-3"), #imageLiteral(resourceName: "Game_Lottery_BBIN_Shortcut-4")],type: "Electronic")
        static let AG_SLOT = Entry(title: "AG电子游戏", subTitle: "AG Slots", category: Category.Slot, platform: Platform.AG, image: #imageLiteral(resourceName: "Game_Real_AG_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_AG"), shortcut: [#imageLiteral(resourceName: "Game_Real_AG_Shortcut-1"), #imageLiteral(resourceName: "Game_Real_AG_Shortcut-2"), #imageLiteral(resourceName: "Game_Real_AG_Shortcut-3"), #imageLiteral(resourceName: "Game_Real_OG_Shortcut-4"), #imageLiteral(resourceName: "Game_Real_AG_Shortcut-5"), #imageLiteral(resourceName: "Game_Real_AG_Shortcut-6")], type: "Electronic")
        
        static let BBIN_LOTTERY = Entry(title: "BBIN彩票", subTitle: "BBIN Lottery", category: Category.Lottery, platform: Platform.BBIN, image: #imageLiteral(resourceName: "Game_Lottery_BBIN_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_BBIN"), shortcut: [#imageLiteral(resourceName: "Game_Lottery_BBIN_Shortcut-1"), #imageLiteral(resourceName: "Game_Lottery_BBIN_Shortcut-2"), #imageLiteral(resourceName: "Game_Lottery_BBIN_Shortcut-3"), #imageLiteral(resourceName: "Game_Lottery_BBIN_Shortcut-4"), #imageLiteral(resourceName: "Game_Lottery_BBIN_Shortcut-5"), #imageLiteral(resourceName: "Game_Lottery_BBIN_Shortcut-6")],type: "Lottery", space: [.bottom, .right])
        static let LiB_LOTTERY = Entry(title: "LiB彩票", subTitle: "LiB Lottery", category: Category.Lottery, platform: Platform.LiB, image: #imageLiteral(resourceName: "Game_Lottery_LiB_Background"), icon:#imageLiteral(resourceName: "Fund_Icon_Wallet_Lib"), shortcut: [#imageLiteral(resourceName: "Game_Lottery_LiB_Shortcut-1"), #imageLiteral(resourceName: "Game_Lottery_LiB_Shortcut-2"), #imageLiteral(resourceName: "Game_Lottery_LiB_Shortcut-3"), #imageLiteral(resourceName: "Game_Lottery_LiB_Shortcut-4"), #imageLiteral(resourceName: "Game_Lottery_LiB_Shortcut-5"), #imageLiteral(resourceName: "Game_Lottery_LiB_Shortcut-6")],type: "", space: [.bottom, .right])
    }
    
    static let entries: [String: [String: Entry]] = [
        Category.Sport: [
            Platform.OPUS_SPORT: Entry.OPUS_SPORT
            , Platform.BETCONSTRUCT: Entry.BETCONSTRUCT_SPORT
            , Platform.SB: Entry.SB
        ]
        , Category.Electronic: [
            Platform.OPE_ELECTRONIC: Entry.OPE_ELECTRONIC
        ]
        , Category.Real: [
            Platform.AG: Entry.AG_LIVE
            , Platform.BBIN: Entry.BBIN_LIVE
            , Platform.OPUS: Entry.OPUS_CASINO
            , Platform.OG: Entry.OG_REAL
            , Platform.EBET: Entry.EBET_LIVE
            , Platform.SUMBET: Entry.SUNBET_LIVE
        ]
        , Category.Slot: [
            Platform.PT: Entry.PT_SLOT
            , Platform.MG: Entry.MG_SLOT
            , Platform.OPE: Entry.OPE_SLOT
//            , Platform.BNG: Entry.BNG_SLOT        //暫時不開放
            , Platform.AG: Entry.AG_SLOT
        ]
        , Category.Lottery: [
            Platform.BBIN: Entry.BBIN_LOTTERY
            , Platform.LiB: Entry.LiB_LOTTERY
        ]
    ]
    
}

class PlayStationHomeShortCutCell: UICollectionViewCell{
    
    @IBOutlet weak var imgvPicture: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubTitle: UILabel!

    var entry: HYPlayStation.Entry!{
        didSet{
            updateView()
        }
    }
    
    func updateView(){
        lbTitle.text = entry?.title
        lbSubTitle.text = entry.subTitle
        let rate = bounds.width / bounds.height
        imgvPicture.image = rate < 1.2 ? entry.shortcutImage[0] : rate < 2.2 ? entry.shortcutImage[1] : entry.shortcutImage[2]
    }
}

class PlayStationHomeCategoryCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgvBackground: UIImageView!
    
    var didEntryClickHandler: ((HYPlayStation.Entry) -> Void)!
    
    var category: String!{
        didSet{
            let user = HYAuth.auth.currentUser ?? HYUser.anonymous
            collectionData = user.favorPlatform[category]!.map{HYPlayStation.entries[category]![$0]!}
        }
    }
    
    var item: PlayStationHomeVC.SectionItem!{
        didSet{
            imgvBackground.image = item.cellBackground
        }
    }
    
    var collectionData: [HYPlayStation.Entry] = []{
        didSet{
            collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let count = collectionData.count
        let w = CGFloat(Int(collectionView.bounds.width) / count) - 1
        return CGSize(width: w, height: collectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShortcutCell", for: indexPath) as! PlayStationHomeShortCutCell
        cell.entry = collectionData[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didEntryClickHandler(collectionData[indexPath.item])
    }
    
}

class PlayStationHomeSectionHeader: UITableViewHeaderFooterView{
    
    @IBOutlet weak var ctrlHeader: UIControl!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var imgvBackground: UIImageView!
    
    var item: PlayStationHomeVC.SectionItem!{
        didSet{
            lbTitle.text = item.title
            imgvBackground.image = item.headerBackground
        }
    }
    
}

class PlayStationHomeVC: PlayStationVC, UITableViewDataSource, UITableViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let w = UIScreen.main.bounds.width
        let h = w * 23 / 30 + 12
        let tableHeader = UIView(frame: CGRect(x: 0, y: 0, width: w, height: h))
        
        let adBannerVC = UIStoryboard.Message.identifier("AdBannerVC")
        self.addChildViewController(adBannerVC)
        adBannerVC.view.frame = CGRect(x: 0, y: 0, width: w, height: w * 2 / 3)
        tableHeader.addSubview(adBannerVC.view)
        
        let marqueeVC = UIStoryboard.Message.identifier("MarqueeVC")
        self.addChildViewController(marqueeVC)
        marqueeVC.view.frame = CGRect(x: 0, y: w * 2 / 3
            , width: w, height: w * 1 / 10)
        tableHeader.addSubview(marqueeVC.view)
        
        tableView.tableHeaderView = tableHeader
        
        setUpTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "OPE"
        let user = HYAuth.auth.currentUser ?? HYUser.anonymous
        tableData = user.favorCategoryOrder.filter{
            if let platforms = user.favorPlatform[$0], platforms.count > 0{
                return true
            }
            return false
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        rowHight = tableView.frame.size.width * 87.0 / 216.0
        sectionHight = tableView.frame.size.width * 20.0 / 216.0
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PlayStationCategoryVC{
            vc.category = sender as! String
        }
        if let vc = segue.destination as? GameCategoryTableVC, let entry = sender as? HYPlayStation.Entry {
            vc.entryItem = entry
        }
        else if let vc = segue.destination as? GameCategoryCollectionVC, let entry = sender as? HYPlayStation.Entry {
            vc.entryItem = entry
        }
    }
    
    @IBAction func didCtrlHeaderClick(ctrl: UIControl){
        let category = tableData[ctrl.tag]
        let identifier = category == HYPlayStation.Category.Sport ? "Sport" : category == HYPlayStation.Category.Slot ? "Slot" : "Default"
        self.performSegue(withIdentifier: identifier, sender: category)
    }
    
    // MARK: - Table
    
    struct SectionItem {
        
        init(title: String, header: UIImage, cell: UIImage) {
            self.title = title
            self.headerBackground = header
            self.cellBackground = cell
        }
        
        let title: String
        let headerBackground: UIImage?
        let cellBackground: UIImage
    }
    
    var sectionItems: [String: SectionItem] = [
        HYPlayStation.Category.Sport: SectionItem(title: "体育游戏", header: #imageLiteral(resourceName: "Game_Sport_HeaderBackground"), cell: #imageLiteral(resourceName: "Game_Sport_CellBackground"))
        , HYPlayStation.Category.Electronic: SectionItem(title: "电子竞技", header: #imageLiteral(resourceName: "Game_Electronic_HeaderBackground"), cell: #imageLiteral(resourceName: "Game_Electronic_CellBackground"))
        , HYPlayStation.Category.Real: SectionItem(title: "现场荷官", header: #imageLiteral(resourceName: "Game_Real_HeaderBackground"), cell: #imageLiteral(resourceName: "Game_Real_CellBackground"))
        , HYPlayStation.Category.Slot: SectionItem(title: "游戏厅", header: #imageLiteral(resourceName: "Game_Slot_HeaderBackground"), cell: #imageLiteral(resourceName: "Game_Slot_CellBackground"))
        , HYPlayStation.Category.Lottery: SectionItem(title: "彩票游戏", header: #imageLiteral(resourceName: "Game_Lottery_HeaderBackground"), cell: #imageLiteral(resourceName: "Game_Lottery_CellBackground"))
    ]
    
    var tableData: [String] = []{
        didSet{
            tableView.reloadData()
        }
    }
    var rowHight : CGFloat = 0
    var sectionHight : CGFloat = 0

    @IBOutlet weak var tableView: UITableView!
    
    func setUpTableView(){
        tableView.register(UINib(nibName: "PlayStationHomeSectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderView")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! PlayStationHomeCategoryCell
        cell.category = tableData[indexPath.section]
        cell.item = sectionItems[cell.category]
        cell.didEntryClickHandler = { [weak self] entry in
            if entry.category == HYPlayStation.Category.Slot{
                if entry.platform == HYPlayStation.Platform.OPE{
                    self?.performSegue(withIdentifier: "CollectionStyle", sender: entry)
                    return
                }
                self?.performSegue(withIdentifier: "ListStyle", sender: entry)
                return
            }
            self?.openGame(entry)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return sectionHight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! PlayStationHomeSectionHeader
        header.item = sectionItems[tableData[section]]
        header.ctrlHeader.tag = section
        header.ctrlHeader.addTarget(self, action: #selector(didCtrlHeaderClick), for: .touchUpInside)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
}

