//
//  GameCategoryTableVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/3.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class GameCategoryTableCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btnPlay: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
}

class GameCategoryTableVC: GameCategoryVC, UITableViewDataSource, UITableViewDelegate {

    
    @objc func didBtnPlayClick(btn: UIButton){
        openGame(tableData[btn.tag])
    }
    
    override var filteredData: [HYPlayStation.Game]{
        set{
            tableData = newValue
        }
        get{
            return tableData
        }
    }
    
    //MARK: Table
    
    @IBOutlet weak var tableview : UITableView!
    
    var tableData : [HYPlayStation.Game] = []{
        didSet{
            tableview.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameCategoryTableCell") as! GameCategoryTableCell
        let game = tableData[indexPath.row]
        
        cell.lbTitle.text = game.title
        cell.imgIcon.image = game.imageName == nil ? nil : UIImage(named: game.imageName!)
        if cell.imgIcon.image == nil{
            print("標題:\(game.title) game code:\(game.code)")
        }
        cell.btnPlay.tag = indexPath.row
        cell.btnPlay.addTarget(self, action: #selector(didBtnPlayClick), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }

}
