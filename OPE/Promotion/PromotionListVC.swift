//
//  PromotionListVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/10.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class PromotionCell: UICollectionViewCell{

    static let placeholder: UIImage = #imageLiteral(resourceName: "Promotion_ImagePlaceholder")
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgvPromotion: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    
    var promotion: HYPromotion.Item!{
        didSet{
            updateView()
        }
    }
    
    func updateView(){
        if let path = promotion.imagePath, let url = URL(string: path){
            imgvPromotion.sd_setImage(with: url, placeholderImage: PromotionCell.placeholder)
        }else{
            imgvPromotion.image = PromotionCell.placeholder
        }
        
        lbTitle.text = promotion.title
        lbDate.text = promotion.dateShow
        btnLike.setTitle("\(promotion.numOfLike)", for: .normal)
        btnLike.setImage(promotion.isLike ? #imageLiteral(resourceName: "Promotion_Like_Selected") : #imageLiteral(resourceName: "Promotion_Like"), for: .normal)
    }
    
    @IBAction func didBtnLikeClick(btn: UIButton){
        
        HYPromotion.center.likePromotion(promotion){[weak self] _ in
            guard let promotion = self?.promotion else{
                return
            }
            
            promotion.isLike = !promotion.isLike
            promotion.numOfLike = promotion.numOfLike + (promotion.isLike ? 1 : -1)
            self?.updateView()
        }
    }

}

class PromotionListVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    let originPropotion: CGFloat = 9.0 / 16.0
    let shinkPropotion: CGFloat = 5.0 / 12.0
    
    var category: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HYPromotion.center.updateData(category: category)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        originHeight = collectionView.frame.width * originPropotion
        shinkHeight = collectionView.frame.width * shinkPropotion
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: HYPromotion.NotificationName, object: HYPromotion.center)
        updateData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: HYPromotion.NotificationName, object: HYPromotion.center)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: -  Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PromotionVC, let cell = sender as? PromotionCell{
            vc.promotion = cell.promotion
            vc.category = category
        }
    }
    
    //MARK: -  Data

    @objc func updateData(){
        self.view.isUserInteractionEnabled = true
        collectionData = HYPromotion.center.promotions[category] ?? []
    }
    
    var isLoading: Bool = false{
        didSet{
            view.isUserInteractionEnabled = !isLoading
            navigationItem.rightBarButtonItem?.isEnabled = !isLoading
        }
    }
    
    @IBAction func likePromotion(btn: UIButton){
        let promotion = collectionData[btn.tag]
        isLoading = true
        HYPromotion.center.likePromotion(promotion){[weak self] error in
            if let error = error{
                self?.showGeneralAlert(.alert_error, lbTextA:error.localizedDescription){ value in
                    self?.isLoading = false
                }
            }else{
                self?.isLoading = false
            }
        }
    }
    
    // MARK: - Collection
    
    var collectionData: [HYPromotion.Item] = []{
        didSet{
            collectionView.reloadData()
            viewNoData.isHidden = collectionData.count > 0
        }
    }

    var originHeight: CGFloat = 0.0
    var shinkHeight: CGFloat = 0.0
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewNoData: UIView!

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PromotionCell", for: indexPath) as! PromotionCell
        cell.promotion = collectionData[indexPath.row]

        if indexPath.row == 0{
            var frame = cell.bounds
            frame.size.height = originHeight
            frame.origin.y = cell.bounds.size.height - frame.size.height
            cell.contentView.frame = frame
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: collectionView.bounds.height - originHeight, right: 0   )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0{
            return CGSize(width: collectionView.frame.width, height: originHeight)
        }
        return CGSize(width: collectionView.frame.width, height: shinkHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let topVisiableSpace = cell.frame.origin.y - collectionView.contentOffset.y
        
        if indexPath.row == 0{ // 第一個永遠為 Origin Size
            collectionView.sendSubview(toBack: cell)
            return
        }
        else if topVisiableSpace > originHeight{ // 已縮
            cell.contentView.frame = cell.bounds
        }
        else if topVisiableSpace + cell.bounds.size.height < originHeight{ // 原始尺寸
            var frame = cell.bounds
            frame.size.height = originHeight
            frame.origin.y = shinkHeight - frame.size.height
            cell.contentView.frame = frame
            collectionView.sendSubview(toBack: cell)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        for cell in collectionView.visibleCells{
            let topVisiableSpace = cell.frame.origin.y - scrollView.contentOffset.y
            // 變化中的Cell
            if collectionView.indexPath(for: cell)?.row == 0{
                continue
            }
            
            if topVisiableSpace <= originHeight && topVisiableSpace + shinkHeight >= originHeight{
                //                (cell as! TelescopicCell).indecator.backgroundColor = .green
                collectionView.bringSubview(toFront: cell)
                var frame = cell.bounds
                let deltaRate = (originHeight - topVisiableSpace) / shinkHeight
                frame.size.height =  shinkHeight + (originHeight - shinkHeight) * deltaRate
                //                print("expending Height: \(frame.size.height)")
                frame.origin.y = shinkHeight - frame.size.height
                cell.contentView.frame = frame
                break
            }
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollVisableCellToRightPlace(collectionView: scrollView as! UICollectionView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollVisableCellToRightPlace(collectionView: scrollView as! UICollectionView)
    }
    
    func scrollVisableCellToRightPlace(collectionView: UICollectionView){
        for cell in collectionView.visibleCells{
            let topVisiableSpace = cell.frame.origin.y - collectionView.contentOffset.y
            
            if topVisiableSpace < originHeight && topVisiableSpace + shinkHeight > originHeight{
                
                let deltaRate = (originHeight - topVisiableSpace) / shinkHeight
                let targetY: CGFloat = deltaRate > 0.5 ? cell.frame.origin.y - originHeight + shinkHeight : cell.frame.origin.y - originHeight
                collectionView.setContentOffset(CGPoint(x: 0, y: targetY), animated: true)
                break
            }
        }
    }

}
