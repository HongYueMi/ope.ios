//
//  PromotionPageVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/10.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension UIStoryboard{
    static let Promotion : UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
}

class PromotionPageVC: TabPageVC2 {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var ary = HYPlayStation.Category.all
        ary.insert("All", at: 0)
        pageOrder = ary
        titles = pageOrder.flatMap{categoryTitle[$0]}
    }
    
    let categoryTitle: [String: String] = [
        "All": "全部活动"
        , HYPlayStation.Category.Sport: "体育游戏"
        , HYPlayStation.Category.Electronic: "电子竞技"
        , HYPlayStation.Category.Real: "现场荷官"
        , HYPlayStation.Category.Slot: "游戏厅"
        , HYPlayStation.Category.Lottery: "彩票游戏"
    ]
    
    struct Item {
        var title: String
        var code: Int
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "优惠活动"
    }
    
    //MARK: - Pages
    
    var pageOrder: [String] = []
    
    var pages: [Int: UIViewController] = [:]
    
    override func vc(atIndex idx: Int) -> UIViewController? {
        guard idx >= 0, idx < titles.count else {
            return nil
        }
        if pages[idx] == nil{
            let vc = UIStoryboard.Promotion.identifier("PromotionListVC") as! PromotionListVC
            vc.category = pageOrder[idx]
            pages[idx] = vc
        }
        return pages[idx]
    }
}
