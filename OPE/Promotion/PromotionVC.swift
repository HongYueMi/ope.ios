//
//  PromotionVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/18.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class PromotionVC: UIViewController {
    
    var category: String!
    
    var promotion: HYPromotion.Item!{
        didSet{
            if webview != nil{
                updateView()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let btn = UIButton(type: .custom)
        btn.setTitleColor(#colorLiteral(red: 0, green: 1, blue: 0.9647058824, alpha: 1), for: .normal)
        btn.addTarget(self, action: #selector(likePromotion), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btn)
        btnLike = btn
        updateView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: HYPromotion.NotificationName, object: HYPromotion.center)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: HYPromotion.NotificationName, object: HYPromotion.center)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var isLoading: Bool = false{
        didSet{
            view.isUserInteractionEnabled = !isLoading
            navigationItem.rightBarButtonItem?.isEnabled = !isLoading
        }
    }
    
    @objc func updateData(){
        isLoading = true
        if let id = self.promotion?.id, id != -1{
            for promotion in HYPromotion.center.promotions[category] ?? []{
                if promotion.id == id{
                    self.promotion = promotion
                    break
                }
            }
        }
    }
    
    @IBAction func joinPromotion(btn: UIButton){
        isLoading = true
        HYPromotion.center.joinPromotion(promotion){[weak self] error in
            if let error = error{
                self?.showGeneralAlert(.alert_error, lbTextA:error.localizedDescription){ value in
                    self?.isLoading = false
                }
            }else{
                self?.showGeneralAlert(.alert_ok, lbTextA:"参与成功"){ value in
                    self?.isLoading = false
                }
            }
        }
    }
    
    @IBAction func likePromotion(btn: UIButton){
        isLoading = true
        HYPromotion.center.likePromotion(promotion){[weak self] error in
            if let error = error{
                self?.showGeneralAlert(.alert_error, lbTextA:error.localizedDescription){ value in
                    self?.isLoading = false
                }
            }else{
                self?.isLoading = false
            }
        }
    }

    // MARK: - View
    
//    @IBOutlet weak var imgvPromotion: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet var constraintBtnJoinHeight: NSLayoutConstraint!
    
    func updateView(){
        
//        if let path = promotion.imagePath, let url = URL(string: path){
//            imgvPromotion.sd_setImage(with: url, placeholderImage: PromotionCell.placeholder)
//        }else{
//            imgvPromotion.image = PromotionCell.placeholder
//        }
        
        lbTitle.text = promotion.title
        lbDate.text = promotion.dateShow
        btnLike.setTitle("\(promotion.numOfLike)", for: .normal)
        btnLike.setImage(promotion.isLike ? #imageLiteral(resourceName: "Promotion_Like_Selected") : #imageLiteral(resourceName: "Promotion_Like"), for: .normal)

        let webHTML = "<style> table td, table th{ border: 1px solid #2293fc; text-align: center;} table th{ font-weight: bold; } </style>" + promotion.content;
        webview.loadHTMLString(webHTML, baseURL: nil)

        //參加活動的按鈕
        
        if nil != HYAuth.auth.currentUser{
            btnJoin.isHidden = false
            constraintBtnJoinHeight.isActive = false
            switch promotion.status {
            case 0:
                btnJoin.setTitle("参与活动", for: .normal);
            case 1:
                btnJoin.setTitle("已参与", for: .normal);
            case 2:
                btnJoin.setTitle("任务已达成", for: .normal);
            case 3:
                btnJoin.setTitle("奖金审核中", for: .normal);
            case 4:
                btnJoin.setTitle("已领取", for: .normal);
            default: //沒有值或是不在其中，不顯示
                btnJoin.isHidden = true
            }
            btnJoin.isEnabled = promotion.status == 0
        }else{
            btnJoin.isHidden = true
            constraintBtnJoinHeight.isActive = true
        }
        isLoading = false
    }

}

class PromotionJoinSuccessVC: UIViewController {
    
    @IBAction func didBtnCancelClick(){
        self.presentingViewController?.dismiss(animated: true)
    }
}
