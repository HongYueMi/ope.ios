//
//  FundingSettingsVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/6.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//  設置資金密碼

import UIKit

class FundingSettingsVC: UIViewController {

    @IBOutlet weak var btSetting: UIButton!     //设置/修改
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //设置/修改
    @IBAction func didBtSettingClick(_ sender: UIButton) {
//        print("hear **** \(HYAuth.auth.currentUser!.isBankPasswordSet)")
        if !HYAuth.auth.currentUser!.isBankPasswordSet{ //true有設置
            defer {
                performSegue(withIdentifier: "sendSetFundPW", sender: nil)
            }
        } else {
            defer {
                performSegue(withIdentifier: "sendChangeFundPW", sender: nil)
            }
        }
        
    }
    
    
    
    

}
