//
//  AboutVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/10/17.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {

    @IBOutlet weak var lbVersion: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let build = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
        lbVersion.text = "版本型号：\(version) B\(build)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
