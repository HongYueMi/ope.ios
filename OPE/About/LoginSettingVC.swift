//
//  LoginSettingVC.swift
//  OPE
//
//  Created by rwt113 on 2017/10/19.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//  登錄設置

import UIKit
import LocalAuthentication

class LoginSettingVC: UIViewController {

    @IBOutlet weak var swGesturePassword: UISwitch! //手勢密碼開關
    @IBOutlet weak var swFingerprintID: UISwitch!   //指紋辨識開關
    
    @IBOutlet weak var lbUnlockID: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if getDeviceSystemInfo.SingletonDSI().modelName == "iPhone X" {
            lbUnlockID.text = "脸部解锁"
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let user = HYAuth.auth.currentUser {
            if(user.isFingerprintOn == true){
                swFingerprintID.isOn = true;
            }else{
                swFingerprintID.isOn = false;
            }
            
            if(user.isGestureOn == true){
                swGesturePassword.isOn = true;
            }else{
                swGesturePassword.isOn = false;
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //手勢
    @IBAction func swGesturePassword(_ sender: UISwitch) {
        if(swGesturePassword.isOn == true) {
            self.performSegue(withIdentifier: "AddGesturePassword", sender: nil);
        }else{
            self.performSegue(withIdentifier: "AddGesturePassword", sender: nil);
        }
    }
    
    //指紋
    @IBAction func swFingerprintID(_ sender: UISwitch) {
        if(swFingerprintID.isOn == true) {
            startTouchID();
        }else{
            closeTouchID();
        }
    }
    
    func startTouchID() {
        let context = LAContext()
        if #available(iOS 9.0, *) {
            if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil) {
                if let user = HYAuth.auth.currentUser {
                    user.isFingerprintOn = true;
                    user.oldDomainState = context.evaluatedPolicyDomainState!
                    if getDeviceSystemInfo.SingletonDSI().modelName == "iPhone X" {
                        alertGeneralView("OPE的Face ID", message:"通过系统验证已开启");
                    }else{
                        alertGeneralView("OPE的Touch ID", message:"通过系统验证已开启");
                    }
                }else{
                    swFingerprintID.isOn = false;
                    alertGeneralView(message:"您尚未开启系统ID解锁功能，请至设定开启");
                }
            }else{
                swFingerprintID.isOn = false;
                alertGeneralView(message:"您尚未开启系统ID解锁功能，请至设定开启");
            }
        } else { //iOS 8 以下不支援
            swFingerprintID.isOn = false;
            alertGeneralView(message:"此功能需要升到iOS 9.0以上才能使用");
        }
    }
    
    //關閉指紋開關，要在認證一次
    func closeTouchID() {
        let context = LAContext()
        if #available(iOS 9.0, *) {
            if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil) {
                context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: "请用系统ID解锁", reply: {success, error in
                    if success {
                        if let user = HYAuth.auth.currentUser {
                            user.isFingerprintOn = false;
                        }
                    }else {
                        if let error = error as NSError? {
                            let message = self.errorMessageForLAErrorCode(errorCode: error.code ,error:error as! LAError)
                            let cancelActionB = UIAlertAction(title: "确定",style: .destructive,handler: { action in
                                self.swFingerprintID.isOn = true;
                            });
                            DispatchQueue.main.async {
                                self.alertActionView(message:message, loopAction:[cancelActionB]);
                            }
                        }
                    }
                });
            }else{
                swFingerprintID.isOn = true;
                alertGeneralView(message:"您尚未开启系统ID解锁功能，请至设定开启");
            }
        } else { //iOS 8 以下不支援
            swFingerprintID.isOn = true;
            alertGeneralView(message:"此功能需要升到iOS 9.0以上才能使用");
        }
    }
    
    func errorMessageForLAErrorCode(errorCode: Int, error:LAError) -> String {
        var message = ""
        
        if #available(iOS 9.0, *) {
            switch errorCode {
            case LAError.appCancel.rawValue:
                message = "认证被应用程序取消"
                
            case LAError.authenticationFailed.rawValue:
                message = "指纹认证三次失败，用户无法提供有效指纹"
                
            case LAError.invalidContext.rawValue:
                message = "密码无效"
                
            case LAError.passcodeNotSet.rawValue:
                message = "设备尚未设置密码"
                
            case LAError.systemCancel.rawValue:
                message = "身份验证被系统取消"
                
            case LAError.touchIDLockout.rawValue:
                message = "您太多次失败的尝试。"
                
            case LAError.touchIDNotAvailable.rawValue:
                message = "TouchID在设备上不可用"
                
            case LAError.userCancel.rawValue:
                message = "您按下取消了"
                
            case LAError.userFallback.rawValue:
                message = "您选择使用输入密码"
                
            default:
                message = error.localizedDescription
            }
        } else {
            message = "此功能需要升到iOS 9.0以上才能使用"
        }
        
        return message
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddGesturePassword"{
            let vc = segue.destination as! GesturePasswordVC
            if(swGesturePassword.isOn == true){
                vc.useCase = .add
            }else{
                vc.useCase = .remove
            }
        }
    }
    
}























