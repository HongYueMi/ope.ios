//
//  ScanLoginVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/30.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class ScanLoginVC: UIViewController {

    @IBOutlet weak var viewSignUp: UIView!
    @IBOutlet weak var btnSignUp: UIButton!
    
    var QRString:String = "";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        btnSignUp.layer.borderWidth = 1.0; //邊框大小
        btnSignUp.layer.masksToBounds = true;
        btnSignUp.layer.cornerRadius = 25.0;
        
        viewSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        viewSignUp.layer.borderWidth = 1.0; //邊框大小
        viewSignUp.layer.masksToBounds = true;
        viewSignUp.layer.cornerRadius = 25.0;

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didBtnSignUpClick(_ sender: UIButton) {
        HYSession.member.authRequest(URL_HOST + "/api/member/SendToWebLogin/" + QRString , method: .get).validate().responseHYJson{
            response in
            if response.isSuccess == false {
                self.showGeneralAlert(.alert_error, lbTextA:response.message ?? "登录失败"){ value in }
            }else{
                self.showGeneralAlert(.alert_ok, lbTextA:"登录成功"){ value in
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
            
        }
    }

    //取消登錄
    @IBAction func btBack(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
































