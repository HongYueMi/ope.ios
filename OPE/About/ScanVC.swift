//
//  ScanVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/10/17.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire

extension UIViewController{
    
    func genCustomerScanItem() -> UIBarButtonItem{
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 35))
        btn.setImage(#imageLiteral(resourceName: "Main_Nav_Scan"), for: .normal)
        btn.addTarget(self, action: #selector(showScanVC), for: .touchUpInside)
        
        return UIBarButtonItem(customView: btn)
    }
    
    @IBAction func showScanVC(){
        if HYAuth.auth.currentUser == nil {
            showTwoSelectAlert(.alert_info,lbTextA:"请先登录", btTextOK:"登录"){ okValue, noValue in
                if okValue == 1 , noValue == 0 {
                    HYAuth.auth.askForUserAuthorization();
                }
            }
            return
        }
        let vc = UIStoryboard.UI.identifier("ScanVC")
        navigationController?.pushViewController(vc, animated: true)
    }
}

class ScanVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    @IBOutlet weak var scanView: UIView!
    
    @IBOutlet weak var blackView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            initScanView();
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                if granted == true {
                    self.initScanView();
                } else {
                    self.alertView("您未授权相机使用，请至设定开启。");
                }
            })
        }
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initScanView(){
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        let videoInput: AVCaptureDeviceInput
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            alertView("您的设备不支持从项目中扫描代码。请使用带相机的设备。")
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        } else {
            alertView("您的设备不支持从项目中扫描代码。请使用带相机的设备。")
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        DispatchQueue.main.async {
            self.previewLayer.frame = self.view.layer.bounds
            self.view.layer.addSublayer(self.previewLayer)
            
            self.view.bringSubview(toFront: self.blackView);
        }
        captureSession.startRunning()
        
    }
     
    func alertView(_ msg:String) {
        self.showGeneralAlert(.alert_info, lbTextA: msg){ value in }
        captureSession = nil
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //Swift3
    func metadataOutput(_ captureOutput: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning(); //關閉掃描
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            print(stringValue)
            if (stringValue.components(separatedBy: "weblogin?key"))[0] != "ope://" {   //防呆 ope://weblogin?key=
                self.showGeneralAlert(.alert_info, lbTextA:"QRCode不符合OPE登录规格，请重新确认QRCode", btTextOK:"重新扫描" ){ value in
                    if value == 1 {
                        self.captureSession.startRunning();
                    }
                }
            }else{
                self.performSegue(withIdentifier: "sendScanLoginVC", sender: stringValue);
            }
            
        }
        

    }
    
//Swift4
//    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection){
//        //        captureSession.stopRunning()
//        if let metadataObject = metadataObjects.first {
//            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
//            guard let stringValue = readableObject.stringValue else { return }
//            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
//            print(stringValue)
//        }
//
//        dismiss(animated: true)
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendScanLoginVC" {
            if let vc = segue.destination as? ScanLoginVC {
                vc.QRString = ((sender as! String).components(separatedBy: "?key="))[1]
            }
        }
    }
    
}





































