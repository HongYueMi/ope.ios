//
//  CommentVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/10/17.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class CommentVC: UIEditVC {
    
//    static let placeholderText = "请输入不小于10个字的描述"
//    static let keyComment = "CommentVCKeyComment"
    
    @IBOutlet weak var tvComment: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
//        if let text = UserDefaults.standard.value(forKey: CommentVC.keyComment) as? String{
//            tvComment.text = text
//            tvComment.textColor = .black
//        }else{
//            tvComment.text = CommentVC.placeholderText
//            tvComment.textColor = .lightGray
//        }
        tvComment.becomeFirstResponder()
//        tvComment.selectedTextRange = tvComment.textRange(from: tvComment.beginningOfDocument, to: tvComment.beginningOfDocument)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        UserDefaults.standard.set(tvComment.textColor == .lightGray ? nil : tvComment.text, forKey: CommentVC.keyComment)
//    }
    
    @IBAction func didBtnDoneClick(item: UIBarButtonItem){
        self.view.isUserInteractionEnabled = false
        let comment = tvComment.text.trimmingCharacters(in: .whitespacesAndNewlines)
        guard !comment.isEmpty else {
            showGeneralAlert(.alert_info,lbTextA:"内容不能为空！"){ value in
                self.view.isUserInteractionEnabled = true
            }
            return
        }
        
        HYMessage.shared.leaveAMessage(title: "意见与反馈", message: tvComment.text){
            [weak self] error in
            
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
                    self?.view.isUserInteractionEnabled = true
                }
            }else{
                self?.showGeneralAlert(.alert_info, lbTextA:"信息新增成功"){ value in
                    self?.navigationController?.popViewController(animated: true)
                }
            }
            
        }
    }
    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//
//        let text = (textView.text! as NSString).replacingCharacters(in: range, with: text)
//        if text.isEmpty{
//            textView.text = CommentVC.placeholderText
//            textView.textColor = UIColor.lightGray
//            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
//            return false
//        }else if textView.textColor == UIColor.lightGray && !text.isEmpty {
//            textView.text = nil
//            textView.textColor = UIColor.black
//        }
//        return true
//    }
//
//    func textViewDidChangeSelection(_ textView: UITextView) {
//        if self.view.window != nil {
//            if textView.textColor == UIColor.lightGray {
//                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
//            }
//        }
//    }

}
