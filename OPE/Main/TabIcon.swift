//
//  TabIcon.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/18.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class TabIcon: UIControl {

    @IBInspectable var imageNormal: UIImage?
    @IBInspectable var imageSelected: UIImage?
    
    @IBInspectable var color: UIColor = .clear
    @IBInspectable var colorSelected: UIColor? = .white
    
    @IBInspectable var titleColor: UIColor = .black
    @IBInspectable var titleColorSelected: UIColor?
    
    @IBInspectable var title: String = ""{
        didSet{
            if !isSelected{
                label?.text = title
            }
        }
    }
    @IBInspectable var titleSelected: String?
    
    @IBInspectable var titleFont: CGFloat = 11
    @IBInspectable var useIndecator: Bool = false
    
    @IBInspectable var placeAdjusted: CGFloat = 0
    
    var imageView : UIImageView!
    var label : UILabel!
    var indecator : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpImageView()
        setUpIndecator()
        setUpLabel()
       
        let a = isSelected
        self.isSelected = a
    }
    
    func setUpImageView(){
        
        imageView = UIImageView(frame: CGRect.zero)
        imageView.contentMode = .scaleAspectFit
        self.addSubview(imageView)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: imageView, attribute: .top, multiplier: 1, constant: 0)
            , NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: imageView, attribute: .leading, multiplier: 1, constant: 0)
            , NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: imageView, attribute: .trailing, multiplier: 1, constant: 0)
            ,NSLayoutConstraint(item: self, attribute: .bottom , relatedBy: .equal, toItem: imageView, attribute: .bottom, multiplier: 1, constant: placeAdjusted)
        ]
        
        if #available(iOS 8.0, *){
            for constraint in constraints{
                constraint.isActive = true
            }
        }else {
            imageView.addConstraints(constraints)
        }
        
    }
    
    func setUpLabel(){
        if useIndecator {
            //不變動
        }else{
            let UIScreen_Height = UIScreen.main.bounds.size.height //y螢幕高跟寬
            if(UIScreen_Height >= 800.0){ //iPad尺寸
                print("***************************\(UIScreen_Height) iPad Size");
                titleFont = 20 ;
            }
        }
        
        label = UILabel(frame: CGRect.zero)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: titleFont)
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        var constraints: [NSLayoutConstraint] = []
        if useIndecator{
            constraints = [
                NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: label, attribute: .centerX, multiplier: 1, constant: 0)
                , NSLayoutConstraint(item: label, attribute: .bottom, relatedBy: .equal, toItem: indecator, attribute: .top, multiplier: 1, constant: -8)
            ]
        }
        else{
            constraints = [
                NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: label, attribute: .centerX, multiplier: 1, constant: 0)
                , NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: label, attribute: .bottom, multiplier: 1, constant: 3)
            ]

        }
        
        if #available(iOS 8.0, *){
            for constraint in constraints{
                constraint.isActive = true
            }
        }else {
            imageView.addConstraints(constraints)
        }
    }
    
    func setUpIndecator(){
        guard useIndecator else {
            return
        }
        
        indecator = UIImageView(frame: CGRect.zero)
        indecator.image = #imageLiteral(resourceName: "Fund_White_Arror")
        indecator.contentMode = .scaleAspectFit
        self.addSubview(indecator)
        
        indecator.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            NSLayoutConstraint(item: indecator, attribute: .width, relatedBy: .equal, toItem: indecator, attribute: .height, multiplier: 50 / 29, constant: 0)
            , NSLayoutConstraint(item: indecator, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 12)
            , NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: indecator, attribute: .centerX, multiplier: 1, constant: 0)
            , NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: indecator, attribute: .bottom, multiplier: 1, constant: 0)
        ]
        
        if #available(iOS 8.0, *){
            for constraint in constraints{
                constraint.isActive = true
            }
        }else {
            imageView.addConstraints(constraints)
        }
        
    }
    
    override var isSelected: Bool{
        didSet{
            imageView.image = isSelected ? imageSelected ?? imageNormal : imageNormal
            label.textColor = isSelected ? titleColorSelected ?? titleColor : titleColor
            label.text = isSelected ? titleSelected ?? title : title
            backgroundColor = isSelected ? colorSelected ?? color : color
            indecator?.isHidden = !isSelected
        }
    }
    
//    override var isHighlighted: Bool{
//        didSet{
//            imageView.image = isHighlighted ? imageSelected ?? imageNormal : imageNormal
//            label.textColor = isHighlighted ? titleColorSelected ?? titleColor : titleColor
//            label.text = isHighlighted ? titleSelected ?? title : title
//            backgroundColor = isHighlighted ? colorSelected ?? color : color
//        }
//    }
}

class TabIconForRecord: UIControl {
    
    @IBInspectable var imageNormal: UIImage?
    @IBInspectable var imageSelected: UIImage?
    
    @IBInspectable var color: UIColor = .clear
    @IBInspectable var colorSelected: UIColor? = .white
    
    @IBInspectable var titleColor: UIColor = .black
    @IBInspectable var titleColorSelected: UIColor?
    
    @IBInspectable var title: String = "";
    @IBInspectable var titleSelected: String?
    
    @IBInspectable var titleFont: CGFloat = 11
    var useIndecator: Bool = true
    
    @IBInspectable var placeAdjusted: CGFloat = 0
    
    var imageView : UIImageView!
    var label : UILabel!
    var indecator : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpImageView()
        setUpIndecator()
        setUpLabel()
        
        let a = isSelected
        self.isSelected = a
    }
    
    func setUpImageView(){
        
        imageView = UIImageView(frame: CGRect.zero)
        imageView.contentMode = .scaleAspectFit
        self.addSubview(imageView)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: imageView, attribute: .top, multiplier: 1, constant: 0)
            , NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: imageView, attribute: .leading, multiplier: 1, constant: 0)
            , NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: imageView, attribute: .trailing, multiplier: 1, constant: 0)
            ,NSLayoutConstraint(item: self, attribute: .bottom , relatedBy: .equal, toItem: imageView, attribute: .bottom, multiplier: 1, constant: placeAdjusted)
        ]
        
        if #available(iOS 8.0, *){
            for constraint in constraints{
                constraint.isActive = true
            }
        }else {
            imageView.addConstraints(constraints)
        }
        
    }
    
    func setUpLabel(){
        
        //let width = UIScreen.main.bounds.size.width //x螢幕高跟寬
        let UIScreen_Height = UIScreen.main.bounds.size.height //y螢幕高跟寬
        if(UIScreen_Height >= 800.0){ //iPad尺寸
            print("***************************\(UIScreen_Height) iPad Size");
            useIndecator = false ;
        }
        
        label = UILabel(frame: CGRect.zero)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: titleFont)
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        var constraints: [NSLayoutConstraint] = []
        if useIndecator{
            constraints = [
                NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: label, attribute: .centerX, multiplier: 1, constant: 0)
                , NSLayoutConstraint(item: label, attribute: .bottom, relatedBy: .equal, toItem: indecator, attribute: .top, multiplier: 1, constant: -8)
            ]
        }
        else{
            constraints = [
                NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: label, attribute: .centerX, multiplier: 1, constant: 0)
                , NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: label, attribute: .bottom, multiplier: 1, constant: 3)
            ]
            
        }
        
        if #available(iOS 8.0, *){
            for constraint in constraints{
                constraint.isActive = true
            }
        }else {
            imageView.addConstraints(constraints)
        }
    }
    
    func setUpIndecator(){
        guard useIndecator else {
            return
        }
        
        indecator = UIImageView(frame: CGRect.zero)
        indecator.image = #imageLiteral(resourceName: "Fund_White_Arror")
        indecator.contentMode = .scaleAspectFit
        self.addSubview(indecator)
        
        indecator.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            NSLayoutConstraint(item: indecator, attribute: .width, relatedBy: .equal, toItem: indecator, attribute: .height, multiplier: 50 / 29, constant: 0)
            , NSLayoutConstraint(item: indecator, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 12)
            , NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: indecator, attribute: .centerX, multiplier: 1, constant: 0)
            , NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: indecator, attribute: .bottom, multiplier: 1, constant: 0)
        ]
        
        if #available(iOS 8.0, *){
            for constraint in constraints{
                constraint.isActive = true
            }
        }else {
            imageView.addConstraints(constraints)
        }
        
    }
    
    override var isSelected: Bool{
        didSet{
            imageView.image = isSelected ? imageSelected ?? imageNormal : imageNormal
            label.textColor = isSelected ? titleColorSelected ?? titleColor : titleColor
            label.text = isSelected ? titleSelected ?? title : title
            backgroundColor = isSelected ? colorSelected ?? color : color
            indecator?.isHidden = !isSelected
        }
    }

}
