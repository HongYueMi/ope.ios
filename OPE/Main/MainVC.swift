//
//  MainVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/17.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension HYAuth{
    
    func genSignInBtn() -> UIButton{
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        btn.setTitle(LanMain.genSignInBtn_Login, for: .normal)
        btn.addTarget(self, action: #selector(askForUserAuthorization), for: .touchUpInside)
        return btn
    }
}

extension HYMember{
    func genSignUpBtn() -> UIButton{
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        btn.setTitle(LanMain.genSignUpBtn_Register, for: .normal)
        btn.addTarget(self, action: #selector(askForUserSignUp), for: .touchUpInside)
        return btn
    }
}

extension UIViewController{
    
    func resetNavigationRightBarItems(){
        
//        if let _ = HYAuth.auth.currentUser{
//            navigationItem.rightBarButtonItems = [
//                genCustomerServiceItem(),
//                genMessageBarButtonItem()
//            ]
//        }
//        else{
//            navigationItem.rightBarButtonItems = [
//                UIBarButtonItem(customView: HYMember.shared.genSignUpBtn())
//                , UIBarButtonItem(customView: HYAuth.auth.genSignInBtn())
//            ]
//        }
    }
}

class MainTabCell: UICollectionViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    
    var item: MainVC.TabIcon?{
        didSet{
//            imgIcon.image = item?.iconOpeEyeLtoR
            lbTitle.text = item?.title
        }
    }
    
    override var isSelected: Bool{
        didSet{
//            imgIcon.image = item == nil ? nil : isSelected ? item!.iconSelected : item!.icon
            lbTitle.textColor = isSelected ? ColorHighlighted : ColorDefault
        }
    }
    
}

class MainVC: TabPageVC2{

    static var shared : MainVC!
    
    @IBOutlet weak var btOPE: UIButton!
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return UIInterfaceOrientationMask.portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addPopMenuItems()
        
        let w = UIScreen.main.bounds.width
        tabSize = CGSize(width: w / CGFloat(pages.count), height: w * 49 / 360)
        
        tabChangeToIndex(0)
        pageChangeToIndex(0, animated: false)
        
        MainVC.shared = self
        _ = HYAuth.auth.addAuthStateDidChangeListener{
            [weak self] auth, user in
            self?.backToMainPage(withIndex: 0)
        }
    }
    
    //MARK: - BackToMain
    
    func backToMainPage(withIndex idx: Int, animated: Bool = true){
        
        guard shouldSelectTab(atIndex: idx) else {
            return
        }
        
        if navigationController?.viewControllers.last != self {
            navigationController?.popToViewController(self, animated: animated)
        }
        var presented = presentedViewController
        while let next = presented?.presentedViewController {
            presented = next
        }
        if let top = presented, !(top is GesturePasswordVC){ //手勢 GesturePasswordVC
            dismiss(animated: animated)
        }
        tabChangeToIndex(idx)
        pageChangeToIndex(idx, animated: false)
    }
    
    
    override func shouldSelectTab(atIndex idx: Int) -> Bool {
        
        // 直播暫不開放
        if idx == 2{
            
            KYPopMenuButton.shared.openButton()
            self.view.addSubview(KYPopMenuButton.shared)
            
//            showGeneralAlert(.alert_info, lbTextA: "即将开放"){ value in
//                self.tabChangeToIndex(self.idxTab)
//            }
            return false
        }
            
        //鄉民狀態下，按下個人中心強制開出登錄頁面
        else if idx == 3 {
            if HYAuth.auth.currentUser == nil {
                
                showTwoSelectAlert(.alert_info,lbTextA:"请先登录", btTextOK:"登录"){ okValue, noValue in
                    if okValue == 1 , noValue == 0 {
                        self.tabChangeToIndex(self.idxTab)
                        HYAuth.auth.askForUserAuthorization();
                    }else{
                        self.tabChangeToIndex(self.idxTab)
                    }
                }
                return false
            }
        }
        
        return true
    }
    
    //MARK: - Tab Bar
    
    struct TabIcon {
        let iconHome:[UIImage] = [#imageLiteral(resourceName: "Main_TabIcon_Home_0"), #imageLiteral(resourceName: "Main_TabIcon_Home_on")]     //0未選中初始狀態，1選中開啟狀態
        
        let iconHot:[UIImage] = [#imageLiteral(resourceName: "Main_TabIcon_hot_0"), #imageLiteral(resourceName: "Main_TabIcon_hot_1"), #imageLiteral(resourceName: "Main_TabIcon_hot_on")]  //0左邊，1右邊，2選中
        let iconHotLtoR:[UIImage] = [#imageLiteral(resourceName: "Main_TabIcon_fire_0"), #imageLiteral(resourceName: "Main_TabIcon_fire_1"), #imageLiteral(resourceName: "Main_TabIcon_fire_2"), #imageLiteral(resourceName: "Main_TabIcon_fire_3"), #imageLiteral(resourceName: "Main_TabIcon_fire_4"), #imageLiteral(resourceName: "Main_TabIcon_fire_5"), #imageLiteral(resourceName: "Main_TabIcon_fire_6"), #imageLiteral(resourceName: "Main_TabIcon_fire_7"), #imageLiteral(resourceName: "Main_TabIcon_fire_8")]  //左到右 0 1 2 3 4 5 6 7 8
        let iconHotRtoL:[UIImage] = [#imageLiteral(resourceName: "Main_TabIcon_fire_8"), #imageLiteral(resourceName: "Main_TabIcon_fire_7"), #imageLiteral(resourceName: "Main_TabIcon_fire_6"), #imageLiteral(resourceName: "Main_TabIcon_fire_5"), #imageLiteral(resourceName: "Main_TabIcon_fire_4"), #imageLiteral(resourceName: "Main_TabIcon_fire_3"), #imageLiteral(resourceName: "Main_TabIcon_fire_2"), #imageLiteral(resourceName: "Main_TabIcon_fire_1"), #imageLiteral(resourceName: "Main_TabIcon_fire_0")]  //右到左 8 7 6 5 4 3 2 1 0

        let iconOpeEye = UIImage(named: "Main_TabIcon_OPE_4")  //開跟關 都是使用 Main_TabIcon_OPE_4.png
        
        let iconOpeEyeCtoL:[UIImage] = [#imageLiteral(resourceName: "Main_TabIcon_OPE_4"), #imageLiteral(resourceName: "Main_TabIcon_OPE_3"), #imageLiteral(resourceName: "Main_TabIcon_OPE_2"), #imageLiteral(resourceName: "Main_TabIcon_OPE_1"), #imageLiteral(resourceName: "Main_TabIcon_OPE_0")]  //中往左 4 3 2 1 0
        let iconOpeEyeCtoR:[UIImage] = [#imageLiteral(resourceName: "Main_TabIcon_OPE_4"), #imageLiteral(resourceName: "Main_TabIcon_OPE_5"), #imageLiteral(resourceName: "Main_TabIcon_OPE_6"), #imageLiteral(resourceName: "Main_TabIcon_OPE_7"), #imageLiteral(resourceName: "Main_TabIcon_OPE_8")]  //中往右 4 5 6 7 8

        let iconOpeEyeLtoR = UIImage.animatedImage(with: [#imageLiteral(resourceName: "Main_TabIcon_OPE_0"), #imageLiteral(resourceName: "Main_TabIcon_OPE_1"), #imageLiteral(resourceName: "Main_TabIcon_OPE_2"), #imageLiteral(resourceName: "Main_TabIcon_OPE_3"), #imageLiteral(resourceName: "Main_TabIcon_OPE_4"), #imageLiteral(resourceName: "Main_TabIcon_OPE_5"), #imageLiteral(resourceName: "Main_TabIcon_OPE_6"), #imageLiteral(resourceName: "Main_TabIcon_OPE_7"), #imageLiteral(resourceName: "Main_TabIcon_OPE_8")], duration: 0.5)!   //左到右 0 1 2 3 4 5 6 7 8
        let iconOpeEyeRtoL = UIImage.animatedImage(with: [#imageLiteral(resourceName: "Main_TabIcon_OPE_8"), #imageLiteral(resourceName: "Main_TabIcon_OPE_7"), #imageLiteral(resourceName: "Main_TabIcon_OPE_6"), #imageLiteral(resourceName: "Main_TabIcon_OPE_5"), #imageLiteral(resourceName: "Main_TabIcon_OPE_4"), #imageLiteral(resourceName: "Main_TabIcon_OPE_3"), #imageLiteral(resourceName: "Main_TabIcon_OPE_2"), #imageLiteral(resourceName: "Main_TabIcon_OPE_1"), #imageLiteral(resourceName: "Main_TabIcon_OPE_0")], duration: 0.5)!   //右到左 8 7 6 5 4 3 2 1 0
        
        let iconPromo:[UIImage] = [#imageLiteral(resourceName: "Main_TabIcon_Promo_0"), #imageLiteral(resourceName: "Main_TabIcon_Promo_1"), #imageLiteral(resourceName: "Main_TabIcon_Promo_on")]  //0左邊，1右邊，2選中
        
        let iconMyUser:[UIImage] = [#imageLiteral(resourceName: "Main_TabIcon_User_4"), #imageLiteral(resourceName: "Main_TabIcon_OPE_on")] //0未選中初始狀態，1選中開啟狀態
        
        let iconMyUserCtoL:[UIImage] = [#imageLiteral(resourceName: "Main_TabIcon_User_4"), #imageLiteral(resourceName: "Main_TabIcon_User_3"), #imageLiteral(resourceName: "Main_TabIcon_User_2"), #imageLiteral(resourceName: "Main_TabIcon_User_1"), #imageLiteral(resourceName: "Main_TabIcon_User_0")]     //中往左 4 3 2 1 0
        let iconMyUserLtoC:[UIImage] = [#imageLiteral(resourceName: "Main_TabIcon_User_0"), #imageLiteral(resourceName: "Main_TabIcon_User_1"), #imageLiteral(resourceName: "Main_TabIcon_User_2"), #imageLiteral(resourceName: "Main_TabIcon_User_3"), #imageLiteral(resourceName: "Main_TabIcon_User_4")]    //左往中 0 1 2 3 4
        
        let title: String
        let indexTab: Int
        
    }
    var iconHotControl:Bool = true;     //火焰方向
    var iconOpeEyeControl:Bool = true;  //OPE球的方向
    var iconPromoControl:Bool = true;   //優惠方向
    var iconMyUserControl:Bool = true;  //我的方向
    
    //改變各個按鈕圖示狀態 tabBarView
    func changeIconAnimaiton(_ idx:IndexPath){
        //點擊觸發cell
        func touchAnimation(_ cell:MainTabCell){
            let animation = CABasicAnimation(keyPath: "bounds.size")
            animation.fromValue = NSValue(cgPoint: CGPoint(x:1, y:1))
            animation.toValue = NSValue(cgSize: cell.imgIcon.frame.size)
            
//            let animation2 = CABasicAnimation(keyPath: "transform.scale")
//            animation2.fromValue = 1
//            animation2.toValue = 1.5
            
            let group:CAAnimationGroup = CAAnimationGroup() //連續動畫效果陣列
            group.repeatCount = 1
            group.duration = 0.2
            group.animations = [animation]
            cell.imgIcon.layer.add(group, forKey: "groupS")
        }
        
        //優惠 做旋轉特效
        func changeAnimation(_ cell:MainTabCell){
            let rotationAnim = CABasicAnimation(keyPath: "transform.rotation.y")
            rotationAnim.fromValue = 0
            rotationAnim.toValue = CGFloat(Double.pi) * 2
            rotationAnim.repeatCount = 1
            rotationAnim.duration = 0.3

            rotationAnim.isRemovedOnCompletion = false  //動畫結束後不還原
            rotationAnim.fillMode = kCAFillModeForwards //動畫結束後不還原
            cell.imgIcon.layer.add(rotationAnim, forKey: nil)
        }
        
        //改變動畫
        for i in 0..<self.tabItems.count {
            let indexPath = IndexPath(row: i, section: 0)
            let cell = tabBarView.cellForItem(at: indexPath) as! MainTabCell;
            
            switch i {
            case 0:     //首頁
                if idx.row >= 1 {
                    cell.imgIcon.image = tabItems[0].iconHome[0];
                }else if idx.row == i {
                    cell.imgIcon.image = tabItems[0].iconHome[1];
                    touchAnimation(cell)
                }
            case 1:     //熱點變化
                if idx.row == 0 {   //首頁中獎
                    cell.imgIcon.image = UIImage(named: "Main_TabIcon_fire_0")
                    cell.imgIcon.animationImages = tabItems[i].iconHotRtoL;
                    cell.imgIcon.animationDuration = 0.5
                    cell.imgIcon.animationRepeatCount = 1
                    cell.imgIcon.startAnimating()
                    iconHotControl = true
                }else if idx.row >= 2 {
                    if iconHotControl == true {
                        cell.imgIcon.image = UIImage(named: "Main_TabIcon_fire_8")
                        cell.imgIcon.animationImages = tabItems[i].iconHotLtoR;
                        cell.imgIcon.animationDuration = 0.5
                        cell.imgIcon.animationRepeatCount = 1
                        cell.imgIcon.startAnimating()
                        iconHotControl = false
                    }
                }else if idx.row == i {
                    cell.imgIcon.image = tabItems[0].iconHot[2];
                    touchAnimation(cell)
                }
            case 2:     //OPE 用實體按鈕
                if idx.row <= 1 {   //往左
                    if iconOpeEyeControl == false {
                        btOPE.setImage(UIImage(named: "Main_TabIcon_OPE_0"), for: .normal)
                        btOPE.imageView!.animationImages = tabItems[2].iconOpeEyeCtoL;
                        btOPE.imageView!.animationDuration = 0.5
                        btOPE.imageView!.animationRepeatCount = 1
                        btOPE.imageView!.startAnimating()
                        iconOpeEyeControl = true
                    }
                }else if idx.row >= 3 { //往右
                    if iconOpeEyeControl == true {
                        btOPE.setImage(UIImage(named: "Main_TabIcon_OPE_8"), for: .normal)
                        btOPE.imageView!.animationImages = tabItems[2].iconOpeEyeCtoR;
                        btOPE.imageView!.animationDuration = 0.5
                        btOPE.imageView!.animationRepeatCount = 1
                        btOPE.imageView!.startAnimating()
                        iconOpeEyeControl = false
                    }
                }
            case 3:     //優惠
                if idx.row <= 2 {
                    cell.imgIcon.image = tabItems[0].iconPromo[0];
                    if iconPromoControl == false {
                        changeAnimation(cell)   //旋轉特效
                        iconPromoControl = true
                    }
                }else if idx.row == 4 {
                    cell.imgIcon.image = tabItems[0].iconPromo[1];
                    if iconPromoControl == true {
                        changeAnimation(cell)   //旋轉特效
                        iconPromoControl = false
                    }
                }else if idx.row == i {
                    iconPromoControl = false
                    cell.imgIcon.image = tabItems[0].iconPromo[2];
                    touchAnimation(cell)
                }
            case 4:     //我的
                if idx.row <= 3 {
                    if iconMyUserControl == false {
                        cell.imgIcon.image = UIImage(named: "Main_TabIcon_User_0")
                        cell.imgIcon.animationImages = tabItems[4].iconMyUserCtoL;
                        cell.imgIcon.animationDuration = 0.5
                        cell.imgIcon.animationRepeatCount = 1
                        cell.imgIcon.startAnimating()
                        iconMyUserControl = true
                    }
                }else if idx.row == i {
                    iconMyUserControl = false
                    cell.imgIcon.image = tabItems[0].iconMyUser[1];
                    touchAnimation(cell)
                }
            default:
                break
            }
        }
    }
    
    var tabItems: [TabIcon] = [
        TabIcon(title: "首页", indexTab: 0)
        , TabIcon(title: "热点", indexTab: 1)
        , TabIcon(title: "YA", indexTab: 2)     //假的 眼睛業障重 請搜尋 didBtOPEClick
        , TabIcon(title: "优惠", indexTab: 3)
        , TabIcon(title: "我的", indexTab: 4)
    ]
    //MARK: - popMenuItems
    
    func addPopMenuItems(){
        KYPopMenuButton.shared.add(title:"充值" , image: #imageLiteral(resourceName: "ft_dp"), handle: nil)
        KYPopMenuButton.shared.add(title: "提款", image: #imageLiteral(resourceName: "ft_wd"), handle: nil)
        KYPopMenuButton.shared.add(title: "转帐", image: #imageLiteral(resourceName: "ft_tf"), handle: nil)
        KYPopMenuButton.shared.add(title: "短消息", image: #imageLiteral(resourceName: "ft_ms"), handle: nil)
        KYPopMenuButton.shared.add(title: "交易纪录", image: #imageLiteral(resourceName: "ft_tr"), handle: nil)
        KYPopMenuButton.shared.add(title: "投注记录", image: #imageLiteral(resourceName: "ft_br"), handle: nil)
        KYPopMenuButton.shared.add(title: "银行卡", image: #imageLiteral(resourceName: "ft_bc"), handle: nil)
        KYPopMenuButton.shared.add(title: "修改密码", image: #imageLiteral(resourceName: "ft_cp"), handle: nil)
    }

    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabItems.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TabCell", for: indexPath) as! MainTabCell
        
        cell.item = tabItems[indexPath.row]
        cell.isSelected = indexPath.row == idxTab
        
        switch indexPath.row {
        case 0:
            cell.imgIcon.image = tabItems[0].iconHome[1]
        case 1:
            cell.imgIcon.image = tabItems[0].iconHot[1]
        case 2:
            cell.imgIcon.image = tabItems[0].iconPromo[0]  //假的 眼睛業障重 請搜尋 didBtOPEClick
        case 3:
            cell.imgIcon.image = tabItems[0].iconPromo[0]
        case 4:
            cell.imgIcon.image = tabItems[0].iconMyUser[0]
        default: break
            
        }
        
        return cell
    }
    
    var tabSize: CGSize = CGSize.zero
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == tabItems.count - 1{
            var s = tabSize
            s.width = UIScreen.main.bounds.width - CGFloat(tabItems.count - 1) * tabSize.width
            return s
        }
        return tabSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //MARK: - Pages
    
    let pages: [UIViewController] = [
        UIStoryboard(name: "Platform").instantiateInitialViewController()!
        , UIStoryboard(name: "HotSpot").instantiateInitialViewController()!
        , UIStoryboard(name: "MemberCenter").instantiateInitialViewController()!    //假的 眼睛業障重 請搜尋 didBtOPEClick
        , UIStoryboard(name: "LiveStream").instantiateInitialViewController()!
        , UIStoryboard(name: "MemberCenter").instantiateInitialViewController()!
        
    ]
    
    override func vc(atIndex idx: Int) -> UIViewController? {
        guard idx >= 0, idx < pages.count else {
            return nil
        }
        return pages[idx]
    }
    
    /**
     選中Tab 哇操 找超久原來在這裡，真的眼睛業障重
     */
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let idx = indexPath.row
        guard
            idx != idxTab
            , shouldSelectTab(atIndex: idx)
            else {return}
        tabChangeToIndex(idx)
        changeTargetPage(toIndex: idx, animation: true)
        
        changeIconAnimaiton(indexPath);
        
    }
    
    //點擊中心OPE按鈕
    @IBAction func didBtOPEClick(_ sender: UIButton) {
        KYPopMenuButton.shared.openButton()
    }
    
    
}








































