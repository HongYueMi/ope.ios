//
//  DialogVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/2.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension UIViewController{
    
    func showVC(vc: UIViewController){
        if let nav = self.navigationController{
            if nil == nav.presentedViewController{
                nav.present(vc, animated: true)
            }else{
                nav.dismiss(animated: true){
                    nav.present(vc, animated: true)
                }
            }
        }else if nil == presentedViewController{
            present(vc, animated: true)
        }else{
            dismiss(animated: true){
                self.present(vc, animated: true)
            }
        }
    }
    
    //常用的Alert 純顯示用
    func showGeneralAlert(_ imgType:AlertSpecies = .alert_info , lbTextA:String = "" , lbTextB:String = "" , btTextOK:String = "关闭" , completion : @escaping (Int)-> Void){
        let alertVC = UIStoryboard.Main.instantiateViewController(withIdentifier: "AlertGeneralVC") as! AlertGeneralVC;
        alertVC.imgType = imgType
        alertVC.textA = lbTextA
        alertVC.textB = lbTextB
        alertVC.btTextA = btTextOK
        alertVC.finishHandler = {
            completion(1);
        }
        showVC(vc: alertVC);
    }
    /*
    self.showGeneralAlert(.alert_info, lbTextA:"XXXX"){ value in }
    self?.showGeneralAlert(.alert_info, lbTextA:"请先登录"){ value in
        if value == 1 {
    
        }
    }
     
     self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
        if value == 1 {
            self?.view.isUserInteractionEnabled = true
        }
     }
     
     enum AlertSpecies: String {
     case alert_ok       = "alert_ok"        //確認完成           0
     case alert_info     = "alert_info"      //警告提醒           1
     case alert_error    = "alert_error"     //錯誤              2
     case alert_touchID  = "alert_touchID"   //指紋              3
     case alert_mail     = "alert_mail"      //電子信箱E-mail     4
     case alert_IP       = "alert_IP"        //ＩＰ位置不予許      5
     case alert_maintain = "alert_maintain"  //維護中             6
     }
    */
    
    //附帶有動作 2個按鈕
    func showTwoSelectAlert(_ imgType:AlertSpecies = .alert_info , lbTextA:String = "" , lbTextB:String = "", btTextOK:String = "确认", btTextNO:String = "取消" , completion : @escaping (Int, Int)-> Void){
        let alertVC = UIStoryboard.Main.instantiateViewController(withIdentifier: "AlertSelectVC") as! AlertSelectVC;
        alertVC.imgType = imgType
        alertVC.textA = lbTextA
        alertVC.textB = lbTextB
        
        alertVC.btTextA = btTextOK
        alertVC.btTextB = btTextNO
        
        //1代表用戶按下哪一個按鈕
        alertVC.actionHandler = {   //執行 確認
            completion(1, 0);
        }
        
        alertVC.finishHandler = {   //結束 取消
            completion(0, 1);
        }
        
        showVC(vc: alertVC);
    }
    /*
    showTwoSelectAlert(.alert_info, lbTextA:"请先登录"){ okValue, noValue in
        if okValue == 1 , noValue == 0 {
     
        }
    }
    */
    
    func showTextAlert(completion : @escaping (Int, Int, String)-> Void){
        let alertVC = UIStoryboard.Main.instantiateViewController(withIdentifier: "AlertTextVC") as! AlertTextVC;
        //1代表用戶按下哪一個按鈕
        alertVC.actionHandler = {   //執行 確認
            completion(1, 0, alertVC.tfCashPW.text ?? "");
        }
        
        alertVC.finishHandler = {   //結束 取消
            completion(0, 1, "");
        }
        
        showVC(vc: alertVC);
    }
    
}

//Alert種類圖檔名
enum AlertSpecies: String {
    case alert_ok       = "alert_ok"        //確認完成
    case alert_info     = "alert_info"      //警告提醒
    case alert_error    = "alert_error"     //錯誤
    case alert_touchID  = "alert_touchID"   //指紋
    case alert_mail     = "alert_mail"      //電子信箱E-mail
    case alert_IP       = "alert_IP"        //ＩＰ位置不予許
    case alert_maintain = "alert_maintain"  //維護中
}

//新版本OPE純顯示Alert
class AlertGeneralVC:UIViewController {

    @IBOutlet weak var downView: UIView!        //中央底圖使用圓角
    
    @IBOutlet weak var imgCenter: UIImageView!  //中央警示圖
    
    @IBOutlet weak var lbTextA: UILabel!        //第一行文字
    @IBOutlet weak var lbTextB: UILabel!        //第二行文字
    
    @IBOutlet weak var btNext: UIButton!
    
    var textA:String = "";
    var textB:String = "";

    var imgType:AlertSpecies = AlertSpecies.init(rawValue: "alert_info")!;
    
    var btTextA:String = "";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isLandscape = false;    //禁止轉向
        
        self.lbTextA.text = textA;
        self.lbTextB.text = textB;
        
        self.imgCenter.image = UIImage(named:imgType.rawValue);
        
        btNext.setTitle(btTextA, for: .normal);
        
        downView.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        downView.layer.borderWidth = 1.0; //邊框大小
        downView.layer.masksToBounds = true;
        downView.layer.cornerRadius = 16.0;
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //左右搖擺動畫
        let wobble = CAKeyframeAnimation(keyPath: "transform.rotation") //使用搖擺動畫
        wobble.duration = 0.1   //每次動畫執行秒數
        wobble.repeatCount = 4  //重複動畫次數
        wobble.values = [0.0, -CGFloat(Double.pi)/8, 0.0, CGFloat(Double.pi)/8, 0.0];   //0.0代表原樣，-CGFloat(Double.pi)/8：傾斜角度左
        wobble.keyTimes = [0.0, 0.25, 0.5, 0.75, 1.0]   //在iOS的意思 是幀數，詳細這要再研究
        self.imgCenter.layer.add(wobble, forKey: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func didBtNextClick(_ sender: UIButton) {
        dismissDialog(completion: finishHandler)
    }
    
    func dismissDialog(completion: (() -> Void)?){
        presentingViewController?.dismiss(animated: true, completion: completion)
    }
    
    var finishHandler: (() -> Void)?    //結束 取消
    var actionHandler: (() -> Void)?    //執行 確認

}

//新版本OPE 2個按鈕 *********************************************************************
class AlertSelectVC:UIViewController {
    
    @IBOutlet weak var downView: UIView!        //中央底圖使用圓角
    
    @IBOutlet weak var imgCenter: UIImageView!  //中央警示圖
    
    var imgType:AlertSpecies = AlertSpecies.init(rawValue: "alert_info")!;
    
    @IBOutlet weak var lbTextA: UILabel!        //第一行文字
    @IBOutlet weak var lbTextB: UILabel!        //第二行文字
    
    @IBOutlet weak var btOK: UIButton!          //確定 確認按鈕
    @IBOutlet weak var btNO: UIButton!          //取消 關閉按鈕
    
    var textA:String = "";
    var textB:String = "";
    
    var btTextA:String = "";
    var btTextB:String = "";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isLandscape = false;    //禁止轉向
        
        self.lbTextA.text = textA;
        self.lbTextB.text = textB;
        
        self.imgCenter.image = UIImage(named:imgType.rawValue);
        
        btOK.setTitle(btTextA, for: .normal);
        btNO.setTitle(btTextB, for: .normal);
        
        downView.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        downView.layer.borderWidth = 1.0; //邊框大小
        downView.layer.masksToBounds = true;
        downView.layer.cornerRadius = 16.0;
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //左右搖擺動畫
        let wobble = CAKeyframeAnimation(keyPath: "transform.rotation")
        wobble.duration = 0.1
        wobble.repeatCount = 4 //MAXFLOAT
        wobble.values = [0.0, -CGFloat(Double.pi)/8, 0.0, CGFloat(Double.pi)/8, 0.0]
        wobble.keyTimes = [0.0, 0.25, 0.5, 0.75, 1.0]
        self.imgCenter.layer.add(wobble, forKey: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func didBtOKClick(_ sender: UIButton) {
        dismissDialog(completion: actionHandler)
    }
    
    @IBAction func didBtNOClick(_ sender: UIButton) {
        dismissDialog(completion: finishHandler)
    }
    
    func dismissDialog(completion: (() -> Void)?){
        presentingViewController?.dismiss(animated: true, completion: completion)
    }
    
    var finishHandler: (() -> Void)?    //結束 取消
    var actionHandler: (() -> Void)?    //執行 確認
    
}

//新版本OPE 輸入資金密碼文字框
class AlertTextVC:UIEditVC {
    
    @IBOutlet weak var downView: UIView!        //中央底圖使用圓角
    
    @IBOutlet weak var btOK: UIButton!          //確定 確認按鈕
    @IBOutlet weak var btNO: UIButton!          //取消 關閉按鈕
    
    @IBOutlet weak var tfCashPW: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isLandscape = false;    //禁止轉向
        
        downView.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        downView.layer.borderWidth = 1.0; //邊框大小
        downView.layer.masksToBounds = true;
        downView.layer.cornerRadius = 16.0;
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func didBtOKClick(_ sender: UIButton) {
        dismissDialog(completion: actionHandler)
    }
    
    @IBAction func didBtNOClick(_ sender: UIButton) {
        dismissDialog(completion: finishHandler)
    }
    
    func dismissDialog(completion: (() -> Void)?){
        presentingViewController?.dismiss(animated: true, completion: completion)
    }
    
    var finishHandler: (() -> Void)?    //結束 取消
    var actionHandler: (() -> Void)?    //執行 確認
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            return true;
        }
        
        if let txt = textField.text , txt.count >= 12 {
            return false
        }
        
        return true;
    }
    
}









































