//
//  LoadingVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/12.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension UIStoryboard{
    static let Main : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    static let UI : UIStoryboard = UIStoryboard(name: "UI", bundle: nil)
}

/**
 loading 頁面 出現： launch 時、 被踢出時
 可能再進去
 
 登出分三種：用戶主動登出 & 伺服器 check online async 登出、& token失效
 用戶主動登出-> 忘記設定
 伺服器登出-> 不忘記設定，下次再按，再登入
 token失效-> 自動登入取得新的token
 
 用戶狀態：被登出不代表會忘記使用者設定
 
 auth 登入-> 記住token
 
 */
class LoadingVC: UIViewController {
    
    override var shouldAutorotate: Bool{
        return false
    }

    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        return .portrait
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        //監聽APP是否從背景返回到前景
        NotificationCenter.default.addObserver(self, selector: #selector(LoadingVC.comeBackApp(noti:)), name: Notification.Name.UIApplicationWillEnterForeground, object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didAppStateChange), name: HYApp.NotificationNameStateChange, object: HYApp.app)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        didAppStateChange()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        super.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    //結束畫面時執行
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationWillEnterForeground, object: nil);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func comeBackApp(noti:Notification) {
        print("applicationWillEnterForeground - 當程式由背景狀態重新回到App前景時出現");
    }

    // MARK: - 監聽APP狀況
    
    @objc func didAppStateChange(){
        switch HYApp.app.state {
        case .notConfigure:
            HYApp.app.configure()
        case .configuring:
            return
        case .internetError(let e):
            showError(e: e)
        case .maintenance:
            showMaintenance()
        case .IPLimited:
            showIPLimited()
        case .versionNotAvailable(let isMustUpdate, let urlUpdate):
            if isMustUpdate{
                showNeedUpgradeDialog(url: urlUpdate)
            }else{
                if isChecked == false {
                    showChooseIfUpgradeDialog(url: urlUpdate)
                }else if isChecked == true {
                    if self.isAllServiceRegistered{
                        self.showNextPage()
                    }else{
                        self.registerService()
                    }
                }
            }
        case .prepared:
//            break
            if isAllServiceRegistered{
                showNextPage()
            }else{
                registerService()
            }
        }
    }
    
    /**
     網路錯誤
     */
    func showError(e: Error){
        print("LoadingVC Error")
        self.showGeneralAlert(.alert_error,lbTextA:e.localizedDescription){ value in

        }
    }
    
    /**
     維護中
     */
    func showMaintenance(){
        print("showMaintenance")

        self.showGeneralAlert(.alert_maintain,lbTextA:"维护中"){ value in
            exit(0)
        }
    }
    
    /**
     IP受限
     */
    func showIPLimited(){
        print("showIPLimited")

        self.showGeneralAlert(.alert_IP,lbTextA:"IP位置不允许"){ value in
            exit(0)
        }
    }
    
    /**
     強制升級
     */
    func showNeedUpgradeDialog(url: URL){
        print("showNeedUpgradeDialog")

        showGeneralAlert(.alert_info,lbTextA:"APP版本过旧，请重新下载",btTextOK:"确认"){ value in
            if value == 1 {
                self.upgradeVersion(url: url)
            }
        }
    }
    
    /**
     選擇升級
     */
    func showChooseIfUpgradeDialog(url: URL){
        print("showChooseIfUpgradeDialog")

        showTwoSelectAlert(.alert_info,lbTextA:"有新版本，是否升级？", btTextOK:"升级版本", btTextNO:"继续使用"){ okValue, noValue in
            if okValue == 1 , noValue == 0 {    //升級版本
                self.upgradeVersion(url: url)
            }else { //繼續使用
                self.dismiss(animated: true)
                if self.isAllServiceRegistered{
                    self.showNextPage()
                }else{
                    self.registerService()
                }
            }
        }
    }
    
    //轉跳更新openUrl
    @objc func upgradeVersion(url: URL){
        print("upgradeVersion")
        print(url)
        if UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil);
            } else {
                UIApplication.shared.openURL(url);
            }
        }else{
            showGeneralAlert(.alert_error,lbTextA:"无法开启网址"){ value in
                //
            }
        }
        
    }
    
    // MARK: - 註冊服務
    
    var isAllServiceRegistered = false
    var isMarqueeLoadingComplete: Bool = false
    var isMarketDataPrepared: Bool = false
    var isAuthInitialed: Bool = false
    var authHandler: NSObjectProtocol?
    
    func registerService(){
        print("註冊HY服務")
        isAllServiceRegistered = true
        
        HYPromotion.center.configure()
        HYSportGameRecommend.center.configure()
        HYFund.fund.configure()
        HYWithdrawal.withdrawal.configure()
        HYTransfer.transfer.configure()
        HYRecordCenter.shared.configure()
        HYMessage.shared.configure()
        
        //跑馬燈
        HYMarquee.shared.refreshMessage(){ [weak self] error in
            self?.isMarqueeLoadingComplete = true
            self?.checkIfAllServicePrepared()
        }
        //行銷圖
        HYMarket.center.configure(){ [weak self] in
            self?.isMarketDataPrepared = true
            self?.checkIfAllServicePrepared()
        }
        //用戶資訊服務
        authHandler = HYAuth.auth.addAuthStateDidChangeListener{[weak self]  auth, user in
            guard nil != self else {return}
            self!.isAuthInitialed = true
            HYAuth.auth.removeAuthStateDidChangeListener(self!.authHandler!)
            self!.checkIfAllServicePrepared()
        }
    }
    
    func checkIfAllServicePrepared(){
        if isMarqueeLoadingComplete
            , isMarketDataPrepared
            , isAuthInitialed
        {
            print("資料初始化完成。")
            showNextPage()
        }
    }
    
    
    var isChecked = false
    
    /**
     依據使用者狀態，導入不同頁面
     */
    func showNextPage(){
        if isChecked{
            performSegue(withIdentifier: "goToMainVC", sender: nil)
            isChecked = false
            return
        }

        if let user = HYAuth.auth.currentUser{ // 使用者登入
            if user.isFingerprintOn{ // 開啟指紋鎖、face ID 鎖
                user.showCheckTouchIDVC()
                isChecked = true
            }
            else if user.isGestureOn{ // 開啟手勢鎖
                user.showCheckerVC()
                isChecked = true
            }else{
                performSegue(withIdentifier: "goToMainVC", sender: nil)
            }
        }
        else{
            HYAuth.auth.askForUserAuthorization()
            isChecked = true
        }
    }

}
