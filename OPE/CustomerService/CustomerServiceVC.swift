//
//  CustomerServiceVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/2.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import WebKit

extension UIViewController{
    
    func genCustomerServiceItem() -> UIBarButtonItem{
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 35))
        btn.setImage(#imageLiteral(resourceName: "Main_Nav_CustomService"), for: .normal)
        btn.addTarget(self, action: #selector(showCustomerService), for: .touchUpInside)
        
        return UIBarButtonItem(customView: btn)
    }
    
    @IBAction func showCustomerService(){
        let vc = UIStoryboard.CustomerService.initial()!
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension UIStoryboard{
    static let CustomerService : UIStoryboard = UIStoryboard(name: "CustomerService", bundle: nil)
}

class CustomerServiceVC: UIViewController, WKUIDelegate {
    
    weak var webVC : WebVC!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EmbedWebView"{
            self.webVC = segue.destination as! WebVC
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = (navigationController as! NavigationController).genDismissBarButtonItem()
        webVC.webView.uiDelegate = self
    }
    
    func dismissSelf(){
        presentingViewController?.dismiss(animated: true)
    }
    
    var isActive : Bool = false
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        timer?.invalidate()
        checkActive()
    }
    
    func checkActive(){
        if !isActive{
            isActive = true
            webVC.webView.load(URLRequest(url: HYApp.app.options.customerService))
        }
    }
    
    weak var timer : Timer!
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(stopLoading), userInfo: nil, repeats: false)
    }
    
    @objc func stopLoading(){
        print("stop Loading")
        webVC.webView.stopLoading()
        isActive = false
    }
    
    @IBAction func reloadWeb(){
        webVC.webView.reload()
    }

}
