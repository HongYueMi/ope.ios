//
//  UIEditVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/23.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

//加千位數符號 , 增加小數點2位
func thousandMark(DoubleValue:Double = 0.0, IntValue:Int = 0) -> (doubleValue:String, intValue:String) {
    let fmt = NumberFormatter();
    fmt.numberStyle = .decimal;
    fmt.minimumFractionDigits = 2;  //最小小數位數 0.00
    let douOut = fmt.string(from: NSNumber(value: DoubleValue))!;
    let intOut = fmt.string(from: NSNumber(value: IntValue))!;
    
    return (douOut, intOut);
}

class UIEditVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    let newLoadingScreenView = LoadingIndicatorVC(); //產生LoadingView畫面物件
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerKeyBoardListener()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TextView
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        tvActive = textView
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        tvActive = nil
    }
    
    // MARK: - TextField
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tfActive = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
    }
    
    // MARK: - Keyboard
    
    @IBOutlet weak var scrollView : UIScrollView?
    
    open weak var tfActive : UITextField?
    open weak var tvActive : UITextView?
    
    @IBAction open func hideKeyboard(){
        tfActive?.resignFirstResponder()
        tvActive?.resignFirstResponder()
    }
    
    func registerKeyBoardListener(){
        let nfc = NotificationCenter.default
        nfc.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        nfc.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWasShown(ntf:Notification){
        //(UIKeyboardFrameBeginUserInfoKey <- 舊版)，(UIKeyboardFrameEndUserInfoKey <- 新版)
        if let kbRectValue = ntf.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue
        {
            let kbSize = kbRectValue.cgRectValue.size
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
            scrollView?.contentInset = contentInsets
            scrollView?.scrollIndicatorInsets = contentInsets
        }
    }
    
    @objc func keyboardWillBeHidden(ntf:Notification){
        let contentInsets = UIEdgeInsets.zero
        scrollView?.contentInset = contentInsets
        scrollView?.scrollIndicatorInsets = contentInsets
    }

}


//自定義UIView
class EditUIView:UIView {
    override init(frame: CGRect) {
        super.init(frame: frame);
        
        //監聽UIView 被觸發時關鍵盤
        //        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard));
        //        self.addGestureRecognizer(tap);
        
        loadViewFromNib();
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        
        loadViewFromNib();
    }
    
    //被用來初始化複寫用的
    func loadViewFromNib() {
        
    }
    
    //關閉鍵盤
    func dismissKeyboard(){
        self.endEditing(true);
    }
    
}












































