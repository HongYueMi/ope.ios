//
//  TabPageVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/8/17.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class TabPageTabCell: UICollectionViewCell {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var viewIndecator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override var isSelected: Bool{
        didSet{
            viewIndecator.isHidden = !isSelected
            lbTitle.textColor = isSelected ? ColorHighlighted : ColorDefault
        }
    }
}

class TabPageVC2: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    @IBInspectable var idxStart : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpPageViewController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkPageIndex()
    }
    
    //MARK: - Tab Bar
    
    @IBOutlet weak var tabBarView: UICollectionView!
    
    var titles: [String] = []{
        didSet{
            tabBarView.reloadData()
            let idx: Int? = idxStart < titles.count ? idxStart : 0 < titles.count ? 0 : nil
            if let idx = idx{
                tabChangeToIndex(idx)
                pageChangeToIndex(idx, animated: false)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TabCell", for: indexPath) as! TabPageTabCell
        cell.lbTitle.text = titles[indexPath.row]
        cell.isSelected = indexPath.row == idxTab
        return cell
    }

    //MARK: - PageViewController
    
    weak var pageVC : UIPageViewController!
    
    func setUpPageViewController(){
        pageVC = self.childViewControllers.last! as! UIPageViewController
        pageVC.dataSource = self
        pageVC.delegate = self
        // scroll 不要 bounce
        let scrollView = pageVC.view.subviews.first(where: {String(describing: type(of: $0)) == "_UIQueuingScrollView"}) as? UIScrollView
        scrollView?.bounces = false
    }
    
    func vc(atIndex idx: Int) -> UIViewController?{
        return UIViewController()
    }
    
    var mapVCtoIndex: [UIViewController: Int] = [:]
    
    /**
     右一頁
     */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
//        let idx = mapVCtoIndex[viewController]! + 1
//        if let vc = vc(atIndex: idx){
//            mapVCtoIndex[vc] = idx
//            return vc
//        }
        return nil
    }
    
    /**
     左一頁
     */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
//        let idx = mapVCtoIndex[viewController]! - 1
//        if let vc = vc(atIndex: idx){
//            mapVCtoIndex[vc] = idx
//            return vc
//        }
        return nil
    }
    
    //MARK: - Change Tab
    
    var idxTab: Int = 0

    func shouldSelectTab(atIndex idx: Int) -> Bool{
        return true
    }
    
    /**
     選中Tab
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let idx = indexPath.row
        guard
            idx != idxTab
            , shouldSelectTab(atIndex: idx)
            else {return}
        tabChangeToIndex(idx)
        changeTargetPage(toIndex: idx, animation: true)
    }
    
    /**
     切換到指定Tab
     */
    func tabChangeToIndex(_ idx : Int){
        tabBarView.selectItem(at: IndexPath(item: idx, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        idxTab = idx
    }
    
    //MARK: - Change Page

    var idxActive : Int = 0
    var isAnimating = false
    var timer: Timer?
    
    func changeTargetPage(toIndex idx: Int, animation: Bool){
        if isAnimating{
            timer?.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(checkPageIndex), userInfo: nil, repeats: false)
        }else{
            pageChangeToIndex(idx, animated: animation)
        }
    }
    
    @objc func checkPageIndex(){
        DispatchQueue.main.async { [weak self] in
            if let idx = self?.idxActive, idx != self!.idxTab{
                self!.isAnimating = false
                defer{
                    self!.pageChangeToIndex(self!.idxTab, animated: false)
                }
            }
        }
    }

    /**
     切換到指定VC
     */
    func pageChangeToIndex(_ idx: Int, animated:Bool){
        
        guard let vc = vc(atIndex: idx) else {
            return
        }
        mapVCtoIndex[vc] = idx
        
        let dir: UIPageViewControllerNavigationDirection
            = idx > idxActive ? .forward : .reverse

        isAnimating = animated
        pageVC.setViewControllers([vc], direction: dir, animated: animated){ [weak self] finished in
            if finished{
                DispatchQueue.main.async {
                    self?.isAnimating = false
                    self?.idxActive = idx
                    if let idxTarget = self?.idxTab, idxTarget != idx{
                        self!.pageChangeToIndex(idxTarget, animated: animated)
                    }
                }
            }
        }
    }
    
    /**
     手勢滑動切換到指定VC
     */
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if finished && completed{
            DispatchQueue.main.async { [weak self] in
                let vc = pageViewController.viewControllers!.first!
                guard let idx = self?.mapVCtoIndex[vc] else {return}

                self!.isAnimating = false
                self!.tabChangeToIndex(idx)
                self!.idxActive = idx
                if let idxTarget = self?.idxTab, idxTarget != idx{
                    self!.pageChangeToIndex(idxTarget, animated: true)
                }
            }
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.isAnimating = true
    }
}
