//
//  WebVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/2.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import WebKit

class WebVC: UIViewController {

    var webView : WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 換View
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.preferences = WKPreferences()
        webConfiguration.preferences.javaScriptEnabled = true
        if #available(iOS 9.0, *) {
            webConfiguration.websiteDataStore = WKWebsiteDataStore.default()
        } else {
            // Fallback on earlier versions
        }
//        let contentController = WKUserContentController()
//        let script = WKUserScript(source: "", injectionTime: .atDocumentStart, forMainFrameOnly: false)
        
        
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.backgroundColor = UIColor.clear
        view = webView
    }

}
