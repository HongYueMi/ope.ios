//
//  UIDevice++.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/12/15.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit


//取得手機相關資訊
class getDeviceSystemInfo{
    
    private static var gDSI:getDeviceSystemInfo?;
    static func SingletonDSI() -> getDeviceSystemInfo {
        if gDSI == nil {
            gDSI = getDeviceSystemInfo();
        }
        return gDSI!;
    }
    
    internal let screenSize = UIScreen.main.bounds.size; //取得手機螢幕高跟寬
    private let systemVersion = UIDevice.current.systemVersion; //抓手機iOS版本
    
    public var version:String { //取得APP Version版號
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version;
        }else{
            return "1.0";
        }
    }
    
    public var build:String { //取得APP Build版號
        if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            return build;
        }else{
            return "1";
        }
    }
    
    internal var deviceIdiom:String { //判斷iPad 或是iPhone
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom;
        if(deviceIdiom == .pad){
            return "iPad";
        }else if(deviceIdiom == .phone){
            return "iPhone";
        }else {
            return "NoData";
        }
    }
    
    public var modelName: String { //取得機型，未來會增加
        var systemInfo = utsname();
        uname(&systemInfo);
        let machineMirror = Mirror(reflecting: systemInfo.machine);
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier };
            return identifier + String(UnicodeScalar(UInt8(value)));
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
    //印出手機資訊
    func printInfo(){
        print("This is \(deviceIdiom), \(modelName) - iOS \(systemVersion)");
        print("APP Version: \(version), APP Build: \(build)");
        print("W:\(screenSize.width), H:\(screenSize.height) - UIScreen Size");
    }
    
}
