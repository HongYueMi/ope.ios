//
//  UIViewController++.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/12/15.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension UIViewController {
    
    //常用的Alert 純顯示用
    func alertGeneralView(_ title:String = "注意", message:String, titleButton:String = "关闭", style:UIAlertControllerStyle = UIAlertControllerStyle.alert){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert);
        let cancelActionA = UIAlertAction(title: titleButton,style: .default,handler: { action in });
        alertController.addAction(cancelActionA);
        self.present(alertController, animated: true, completion: nil);
    }
    
    //附帶有動作
    func alertActionView(_ title:String = "注意", message:String, loopAction:[UIAlertAction], style:UIAlertControllerStyle = UIAlertControllerStyle.alert){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style);
        for i in 0..<loopAction.count {
            alertController.addAction(loopAction[i]);
        }
        self.present(alertController, animated: true, completion: nil);
    }
    
    //輸入框
    func alertTextView(_ title:String = "注意", message:String = "", style:UIAlertControllerStyle = .alert, completion : @escaping (String)-> Void){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        
        let saveAction = UIAlertAction(title: "OK", style: .default, handler: {
            alert -> Void in
            
            let firstTF = alertController.textFields![0] as UITextField;
            
            completion(firstTF.text ?? "");
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "";
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
