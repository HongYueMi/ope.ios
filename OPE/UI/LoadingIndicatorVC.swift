//
//  LoadingIndicatorVC.swift
//  OPE
//
//  Created by Wuda on 2017/12/15.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class LoadingIndicatorVC: UIViewController {

    var container: UIView = UIView();
    var loadingView: UIView = UIView();
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView();
    var titleLab: UILabel = UILabel();
    var textLab: UILabel = UILabel();
    
    func showActivityIndicator(uiView: UIView, titleText:String = "", textText:String = "", rootPower:Bool = false) {
        
        container.frame = uiView.frame;
        container.center = uiView.center;
        container.backgroundColor = UIColorFromHex(rgbValue: 0xffffff, alpha: 0.3);
        
        if(titleText == "" || textText == ""){
            loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80);
        }else{
            loadingView.frame = CGRect(x: 0, y: 0, width: 100, height: 100);
        }
        loadingView.center = uiView.center;
        loadingView.backgroundColor = UIColorFromHex(rgbValue: 0x444444, alpha: 0.7);
        loadingView.clipsToBounds = true;
        loadingView.layer.cornerRadius = 10;
        
        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge;
        activityIndicator.center = CGPoint(x: (loadingView.frame.size.width / 2), y:(loadingView.frame.size.height / 2) );
        
        let loadingViewW = loadingView.frame.size.width;
        let loadingViewH = loadingView.frame.size.height;
        
        titleLab.frame = CGRect(x: 5, y: 5, width: loadingViewW-10, height: 20);
        titleLab.text = titleText;
        titleLab.textAlignment = NSTextAlignment.center;
        titleLab.textColor = UIColor.white;
        titleLab.font = UIFont.systemFont(ofSize: 14);
        
        textLab.frame = CGRect(x: 5, y: loadingViewH-25 , width: loadingViewW-10, height: 20);
        textLab.text = textText;
        textLab.textAlignment = NSTextAlignment.center;
        textLab.textColor = UIColor.white;
        textLab.font = UIFont.systemFont(ofSize: 14);
        
        loadingView.addSubview(activityIndicator);
        container.addSubview(loadingView);
        
        //是否使用root權限View - 測試階段
        if rootPower == true {
            let window = UIApplication.shared.keyWindow;
            window?.addSubview(container);
            window?.bringSubview(toFront: container);
        }else{
            uiView.addSubview(container);
        }
        
        activityIndicator.startAnimating();
        
        loadingView.addSubview(titleLab);
        loadingView.addSubview(textLab);
    }
    
    func hideActivityIndicator(uiView: UIView) {
        activityIndicator.stopAnimating();
        container.removeFromSuperview();
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0;
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0;
        let blue = CGFloat(rgbValue & 0xFF)/256.0;
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha));
    }

}
