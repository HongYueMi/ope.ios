//
//  NavigationVC.swift
//  SportsGames
//
//  Created by Lavend K. Mi on 2017/3/7.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

@IBDesignable

class NavigationController: UINavigationController , UINavigationBarDelegate, UIGestureRecognizerDelegate{
    
    // MARK: - Rotate
    
    @IBInspectable var isAutorotate: Bool = false
    
    override var shouldAutorotate: Bool{
        return isAutorotate
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return isAutorotate ? .allButUpsideDown : .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        return .portrait
    }
    
    // MARK: - Back Image
    
    @IBInspectable var imageBack : UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.delegate = self
        interactivePopGestureRecognizer?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard viewControllers.count > 1 else {
            return false
        }
        return true
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func navigationBar(_ navigationBar: UINavigationBar, didPush item: UINavigationItem) {
        interactivePopGestureRecognizer?.isEnabled = true
    }

    func navigationBar(_ navigationBar: UINavigationBar, shouldPush item: UINavigationItem) -> Bool {
        item.leftBarButtonItem = UIBarButtonItem(customView: genBackButton())
        return true
    }
    
    func genBackButton() -> UIButton{
        let btnBack = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btnBack.setImage(imageBack, for: .normal)
        btnBack.addTarget(self, action: #selector(popViewController), for: .touchUpInside)
        return btnBack
    }
    
    func genDismissBarButtonItem() -> UIBarButtonItem {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btn.setImage(imageBack, for: .normal)
        btn.addTarget(self, action: #selector(dismissSelf), for: .touchUpInside)
        return UIBarButtonItem(customView: btn)
    }
    
    @objc func dismissSelf(){
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
}





































