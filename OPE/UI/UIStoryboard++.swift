//
//  UIStoryboard++.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/18.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

let ColorHighlighted: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
let ColorDefault: UIColor = #colorLiteral(red: 0.5176470588, green: 0.5843137255, blue: 0.6549019608, alpha: 1)

extension UIStoryboard {
    
    func identifier(_ id: String) -> UIViewController{
        return self.instantiateViewController(withIdentifier: id)
    }
    
    func initial() -> UIViewController?{
        return self.instantiateInitialViewController()
    }
    
    convenience init(name: String) {
        self.init(name: name, bundle: nil)
    }
}
