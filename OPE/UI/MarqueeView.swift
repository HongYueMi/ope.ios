//
//  MarqueeView.swift
//  UIMarquee
//
//  Created by Lavend K. Mi on 2017/4/10.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

protocol MarqueeViewDataSource {
    func next() -> UIView?
}

class MarqueeView: UIView {
    
    @IBInspectable var verticalDuration: TimeInterval = 1.0
    @IBInspectable var horizonRate: CGFloat = 85.0
    @IBInspectable var beginDelay: TimeInterval = 2.0
    @IBInspectable var afterDelay: TimeInterval = 2.0
    
    var dataSource : MarqueeViewDataSource?
    
    typealias Message = UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    func setUp(){
        self.clipsToBounds = true
    }
    
    //
    
    var shouldPlay: Bool = false
    var isPlaying: Bool = false
    
    func play(){
        timer?.invalidate()
        shouldPlay = true
        if !isPlaying{
            showNextMessage()
        }
    }
    
    func stop(){
        timer?.invalidate()
        shouldPlay = false
    }
    
    // MARK: Animation
    
    var timer: Timer?
    // 一秒滑動幾個Point

    var messageDisplay : Message?{
        didSet{
            // 移除舊的
            oldValue?.removeFromSuperview()
            
            // 展示新的
            if let message = messageDisplay{
                display(message: message){ [weak self] _ in
                    defer{
                        self?.showNextMessage()
                    }
                }
            }
            else{
                showNextMessage()
            }
        }
    }
    
    @objc func showNextMessage(){
        var message = dataSource?.next()
        if shouldPlay{
            if let message = message{
                // 設定Message 起始位置在跑馬燈的上方
                message.sizeToFit()
                var rect = self.bounds
                rect.size.width = message.bounds.width
                rect.origin.y = -rect.size.height
                message.frame = rect
                addSubview(message)
            }
            else if messageDisplay == nil{
                //timer 10秒後再檢查一次
                print("Marquee Timer Start")
                timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(showNextMessage), userInfo: nil, repeats: false)
                isPlaying = false
                return
            }
        }
        else {
            if messageDisplay == nil{
                isPlaying = false
                return
            }
            message = nil
        }
        isPlaying = true

        // Message向下移動
        UIView.animate(withDuration: verticalDuration
            , delay: afterDelay
            , options: [.curveLinear, .beginFromCurrentState]
            , animations: { [weak self] in
                guard nil != self else {return}
                var rect : CGRect!
                for msg in self!.subviews{
                    rect = msg.frame
                    rect.origin.y += rect.size.height
                    msg.frame = rect
                }
            }
            , completion: { [weak self] hasAnimationFinished in
                self?.messageDisplay = message
            }
        )
    }
    
    func display(message : Message, completion: @escaping (Bool)-> Void){
        let duration = message.frame.size.width / horizonRate
        UIView.animate(withDuration: TimeInterval(duration)
            , delay: beginDelay
            , options: [.curveLinear, .beginFromCurrentState]
            , animations: { [weak self] in
                guard nil != self else {return}
                let move = max(0, message.bounds.size.width - self!.bounds.size.width)
                var rect = message.frame
                rect.origin.x = -move
                message.frame = rect
            }
            , completion: completion
        )
    }

}
