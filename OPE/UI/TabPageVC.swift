//
//  TabPageVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/8/17.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class TabPageVC: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    @IBInspectable var idxStart : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTabs()
        idxTab = idxStart
        idxActive = idxStart
        setUpPageViewController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkPageIndex()
    }
    
    //MARK: - Tab Bar
    
    var tabs : [TabIcon] = []{
        didSet{
            for tab in tabs{
                tab.addTarget(self, action: #selector(selectTab), for: .touchUpInside);
                tab.isSelected = false;
            }
            tabs[idxStart].isSelected = true;
        }
    }
    
    func setUpTabs(){
        tabs = []
    }

    //MARK: - PageViewController
    
    weak var pageVC : UIPageViewController!
    var vcs : [UIViewController] = []{
        didSet{
            pageVC.setViewControllers([vcs[idxStart]], direction: .forward, animated: false)
            idxActive = idxStart
        }
    }
    
    func setUpPageViewController(){
        pageVC = self.childViewControllers.last! as! UIPageViewController
        pageVC.dataSource = self
        pageVC.delegate = self
    }
    
    /**
     右一頁
     */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let idx = vcs.index{$0 == viewController}! + 1
        
        if idx < vcs.count, shouldSelectTab(atIndex: idx){
            return vcs[idx]
        }
        return nil //沒有右一頁
    }
    
    /**
     左一頁
     */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let idx = vcs.index{$0 == viewController}! - 1
        if idx >= 0, shouldSelectTab(atIndex: idx){
            return vcs[idx]
        }
        return nil //沒有左一頁
    }
    
    //MARK: - Change Page
    
    var idxActive : Int = 0
    
    var idxTab: Int = 0

    func shouldSelectTab(atIndex idx: Int) -> Bool{
        return true
    }
    
    /**
     選中Tab
     */
    @IBAction func selectTab(_ tab: UIControl){
        guard
            let idx: Int = tabs.index(where: {$0 == tab})
            , idx != idxTab
            , shouldSelectTab(atIndex: idx)
            else {return}
        
        tabChangeToIndex(idx)
        guard idx < vcs.count, idx >= 0 else {return}
        if isAnimating{
            timer?.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(checkPageIndex), userInfo: nil, repeats: false)
            return
        }
        pageChangeToIndex(idx, animated: true)
    }
    
    /**
     切換到指定Tab
     */
    func tabChangeToIndex(_ idx : Int){
        tabs[idxTab].isSelected = false
        tabs[idx].isSelected = true
        idxTab = idx
    }
    
    var isAnimating = false
    
    var timer: Timer?
    
    @objc func checkPageIndex(){
        DispatchQueue.main.async { [weak self] in
            if let idx = self?.idxActive, idx != self!.idxTab{
                self!.isAnimating = false
                defer{
                    self!.pageChangeToIndex(self!.idxTab, animated: false)
                }
            }
        }
    }

    /**
     切換到指定VC
     */
    func pageChangeToIndex(_ idx: Int, animated:Bool){
       
        let vc = vcs[idx]
        let dir: UIPageViewControllerNavigationDirection
            = idx > idxActive ? .forward : .reverse

        isAnimating = animated
        pageVC.setViewControllers([vc], direction: dir, animated: animated){ [weak self] finished in
            if finished{
                DispatchQueue.main.async {
                    self?.isAnimating = false
                    self?.idxActive = idx
                    if let idxTarget = self?.idxTab, idxTarget != idx{
                        self!.pageChangeToIndex(idxTarget, animated: true)
                    }
                }
            }
        }
    }
    
    /**
     手勢滑動切換到指定VC
     */
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if finished && completed{
            DispatchQueue.main.async { [weak self] in
                if let idx = self?.vcs.index(where: {$0 == pageViewController.viewControllers!.last}){
                    self!.isAnimating = false
                    self!.tabChangeToIndex(idx)
                    self!.idxActive = idx
                    if let idxTarget = self?.idxTab, idxTarget != idx{
                        self!.pageChangeToIndex(idxTarget, animated: true)
                    }
                }
            }
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.isAnimating = true
    }
}
