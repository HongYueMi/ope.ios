//
//  UITableViewCell.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/23.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

@IBDesignable

class TableViewCell: UITableViewCell {
    
    @IBInspectable var isShowSelection: Bool = true
    
    // Data
    @IBOutlet weak var lineTop : UIView?
    @IBOutlet weak var lbTitle : UILabel?
    @IBOutlet weak var lbInfo : UILabel?
    @IBOutlet weak var imgvIcon : UIImageView?
    @IBOutlet weak var imgBackground : UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if !isShowSelection{
            self.selectionStyle = .none
        }else{
            self.selectionStyle = .blue
        }
    }
    
}



