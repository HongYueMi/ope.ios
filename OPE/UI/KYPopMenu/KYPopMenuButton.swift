//
//  KYPopMenuButton.swift
//  KYPopMenu
//
//  Created by lawliet on 2017/12/19.
//  Copyright © 2017年 lawliet. All rights reserved.
//

import UIKit

let x = UIScreen.main.bounds.width / 4.5
let y = UIScreen.main.bounds.height / 6
let originY = UIScreen.main.bounds.height / 1.8
let animateY = originY - 40
let originX = UIScreen.main.bounds.width / 16

let sceenMidX = UIScreen.main.bounds.midX
let sceenMidY = UIScreen.main.bounds.midY
let appWindow = UIApplication.shared.keyWindow!


let logoPadding : CGFloat = 10.0
let logoHeightPadding :CGFloat = 50.0

class KYPopMenuButton: UIView {
    
    
    static let shared = KYPopMenuButton()
    
    typealias cellClosure = ((KYPopMenuCells)->Void)
    
    let plusBtn = KYPopMenuPlusButton()
    
    let kyOverlayView =  KYPopMenuOverlayView()
    var items : [KYPopMenuCells] = []
    var isHide : Bool = true
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.closeButton()
    }
    
    
    func initUI(){
        plusBtn.frame = CGRect(origin: CGPoint.init(x: sceenMidX, y: sceenMidY * 1.8), size: CGSize(width: 44.0, height: 44.0))
        plusBtn.center = CGPoint.init(x: sceenMidX, y: sceenMidY * 1.8)
        kyOverlayView.contentView.addSubview(plusBtn)
        kyOverlayView.alpha = 0
        addLogo()
        appWindow.addSubview(kyOverlayView)
    }
    
    
    
    let laligaLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 10)
        label.numberOfLines = 0
        label.text = """
        2017/2018 赛季西甲联赛赞助商
        西甲官方冠名赞助商
        """
        return label
    }()
    
    let plLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 10)
        label.numberOfLines = 0
        label.text = """
        2017/2018 赛季
        英超联赛赞助商
        哈德斯菲尔德
        俱乐部球衣赞助商
        """
        return label
    }()
    
    
    func addLogo(){
        
        let laligaLogo = UIImageView()
        let plLogo = UIImageView()
        
        laligaLogo.frame = CGRect(x: logoPadding, y: logoHeightPadding, width: 130, height: 50)
        plLogo.frame = CGRect(x: sceenMidX + logoPadding, y: logoHeightPadding - 8, width: 50, height: 90)
        
        laligaLogo.image = #imageLiteral(resourceName: "ft_laligo")
        plLogo.image = #imageLiteral(resourceName: "ft_ht")
        
        laligaLabel.frame = CGRect(x: logoPadding, y: logoHeightPadding + laligaLogo.frame.height, width: 180, height: 30)
        plLabel.frame = CGRect(x: sceenMidX + logoPadding + plLogo.frame.width + 8, y: logoHeightPadding - 8, width: 100, height: 100)
        
        
        kyOverlayView.contentView.addSubview(laligaLabel)
        kyOverlayView.contentView.addSubview(plLabel)
        kyOverlayView.contentView.addSubview(laligaLogo)
        kyOverlayView.contentView.addSubview(plLogo)
        
    }
    
    
    func openButton(){
        
        initUI()
        
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: [], animations: {
            self.plusBtn.alpha = 1
            self.plusBtn.transform = CGAffineTransform.init(rotationAngle: .pi / 4)
            self.kyOverlayView.alpha = 1.0
        }, completion: nil)

            for item in items {
                self.kyOverlayView.contentView.addSubview(item)
            }
            popUp(isShow: true)
    }
    
    
    func closeButton(){
        
        UIView.animate(withDuration: 0.3) {
            self.plusBtn.transform = CGAffineTransform.init(rotationAngle: 0).inverted()
            self.plusBtn.alpha = 0
        }
        popUp(isShow: false)
        self.plusBtn.transform = CGAffineTransform.identity
    }
    
    
    fileprivate func popUp(isShow:Bool){
        var delay = 0.0
        if isShow {
            
            
            for (index,item) in items.enumerated() {
                item.alpha = 0.05
                item.frame = getOriginFrame(index: index, paddingY: 0)
                
                UIView.animate(withDuration: 0.3, delay: delay, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: [.curveEaseOut], animations: {
                    item.frame = self.getOriginFrame(index: index, paddingY: 50)
                    item.alpha = 1
                    
                    UIView.animate(withDuration: 0.1, delay: 0.25 + delay, options: [.curveEaseIn], animations: {
                        item.frame = self.getOriginFrame(index: index, paddingY: 40)
                    }, completion: nil)
                    
                }, completion: { (_) in
                    
                })
                
                
                
                delay += 0.04
            }
        }else{
            for item in items.reversed() {
                    UIView.animate(withDuration: 0.3, delay: delay, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [], animations: {
                        
                        var identity = CGAffineTransform.identity
                        identity = identity.translatedBy(x: 0, y: 400)
                        identity = identity.scaledBy(x: 0.8, y: 0.8)
                        item.transform = identity
                        
                    }, completion:{ (_) in
                        item.removeFromSuperview()
                        
                        UIView.animate(withDuration: 0.2, animations: {
                            self.kyOverlayView.alpha = 0
                        })
                        
                        item.transform = CGAffineTransform.identity
                    })
                    delay += 0.04
            }
        }
    }
}



extension KYPopMenuButton {
    
    fileprivate func getOriginFrame(index:Int,paddingY:CGFloat) -> CGRect{
        return CGRect(x: originX + x * CGFloat(index % 4) , y: originY - paddingY + y  * CGFloat(index / 4), width: 50, height: 50)
    }
    
    func add(title:String,image:UIImage,handle:(cellClosure?) = nil){
        let item = KYPopMenuCells()
        item.title = title
        item.icon = image
        item.actionCloure = handle
        item.actionButton = self
        items.append(item)
    }
    
}
