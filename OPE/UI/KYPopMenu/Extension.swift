//
//  Extension.swift
//  KYPopMenu
//
//  Created by lawliet on 2017/12/19.
//  Copyright © 2017年 lawliet. All rights reserved.
//

import UIKit


extension UIBezierPath {
    convenience init (origin:CGPoint,length:CGFloat) {
        self.init()
        
        let topPoint = origin.y + length
        let botPoint = origin.y - length
        let leftPoint = origin.x - length
        let rightPoint = origin.x + length
        
        self.move(to: origin)
        self.addLine(to: CGPoint(x: origin.x, y: topPoint))
        self.move(to: origin)
        self.addLine(to: CGPoint(x: origin.x, y: botPoint))
        self.move(to: origin)
        self.addLine(to: CGPoint(x: leftPoint, y: origin.y))
        self.move(to: origin)
        self.addLine(to: CGPoint(x: rightPoint, y: origin.y))
    }
}
