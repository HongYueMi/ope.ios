//
//  KYPopMenuPlusButton.swift
//  KYPopMenu
//
//  Created by lawliet on 2017/12/19.
//  Copyright © 2017年 lawliet. All rights reserved.
//

import UIKit


class KYPopMenuPlusButton: UIView {
    
    
    let KYLayer = KYPopMenuPluslayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        KYLayer.frame = self.bounds
        self.layer.addSublayer(KYLayer)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        KYPopMenuButton.shared.closeButton()
    }
    
}
