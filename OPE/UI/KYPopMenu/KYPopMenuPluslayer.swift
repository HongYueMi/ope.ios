//
//  KYPopMenuPluslayer.swift
//  KYPopMenu
//
//  Created by lawliet on 2017/12/19.
//  Copyright © 2017年 lawliet. All rights reserved.
//

import UIKit

class KYPopMenuPluslayer: CAShapeLayer {

    
    override var frame: CGRect {
        didSet{
            drawPlus()
            print(frame)
        }
    }
  
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    fileprivate func drawPlus(){
        let plus = UIBezierPath(origin: CGPoint.init(x: frame.midX, y:frame.midY), length: 12)
        self.lineWidth = 3
        self.lineCap = kCALineCapRound
        self.strokeColor = UIColor.lightGray.cgColor
        self.path = plus.cgPath
    }
}
