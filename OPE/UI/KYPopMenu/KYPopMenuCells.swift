//
//  KYPopMenuCells.swift
//  KYPopMenu
//
//  Created by lawliet on 2017/12/19.
//  Copyright © 2017年 lawliet. All rights reserved.
//

import UIKit

class KYPopMenuCells: UIView {
    
    

    weak var actionButton:KYPopMenuButton?
    var actionCloure : ((KYPopMenuCells) -> Void)?
    
    var _titleLabel:UILabel? = nil
    open var titleLabel:UILabel {
        get{
            if _titleLabel == nil {
                _titleLabel = UILabel()
                addSubview(_titleLabel!)
            }
            return _titleLabel!
        }
    }
    
    open var title:String? = nil {
        didSet{
            titleLabel.text = title
            titleLabel.sizeToFit()
            titleLabel.font = UIFont.systemFont(ofSize: 15)
            titleLabel.textAlignment = .center
            titleLabel.frame.origin.y = titleLabel.frame.size.height + 35
        }
    }
    
    var _iconImageView:UIImageView? = nil
    open var iconImageView:UIImageView {
        get{
            if _iconImageView == nil {
                _iconImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                _iconImageView?.contentMode = .scaleAspectFill
                addSubview(_iconImageView!)
            }
            return _iconImageView!
        }
    }

    open var icon : UIImage? = nil {
        didSet{
            iconImageView.image = icon
        }
    }
    
    override var frame: CGRect {
        didSet{
            _iconImageView?.center = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
            _titleLabel?.center.x = self.frame.width/2
            
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        actionCloure?(self)
        actionButton!.closeButton()
    }
    
}
