//
//  KYPopMenuView.swift
//  KYPopMenu
//
//  Created by lawliet on 2017/12/19.
//  Copyright © 2017年 lawliet. All rights reserved.
//

import UIKit

class KYPopMenuOverlayView: UIVisualEffectView {
    
    override init(effect: UIVisualEffect?) {
        super.init(effect: effect)
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
    convenience init(){
        let lightBlur = UIBlurEffect(style: UIBlurEffectStyle.light)
        self.init(effect: lightBlur)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
