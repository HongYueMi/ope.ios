 //
//  AppDelegate.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/12.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import NVActivityIndicatorView
import SwiftTheme

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var isLandscape = true //控制是否可以螢幕轉向
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        HYApp.app.configure()
        ThemeManager.setTheme(plistName: "ThemeDay", path: .mainBundle)
        ThemeManager.setTheme(plistName: "ThemeNight", path: .mainBundle)
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotate
        UIApplication.shared.statusBarStyle = .lightContent //上方資訊狀態 調整成白色
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
//        if let user = HYAuth.auth.currentUser, user.isGestureOn {
//            user.showCheckerVC()
//        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        if let user = HYAuth.auth.currentUser, user.isGestureOn || user.isFingerprintOn {
            if(user.isFingerprintOn == true){
                user.showCheckTouchIDVC()
            }else if(user.isGestureOn == true){
                user.showCheckerVC()
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

 extension AppDelegate {    //控制是否可以螢幕轉向
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if isLandscape {
            return .all
        }
        else {
            return .portrait
        }
    }
 }

