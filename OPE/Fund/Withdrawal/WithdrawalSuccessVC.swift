//
//  WithdrawalSuccessVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/21.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class WithdrawalSuccessVC: UIViewController {
    
    @IBOutlet weak var backBar: UIBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: genBackButton())
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didBackBarClick(_ sender: UIBarButtonItem) {
        _ = navigationController?.popToRootViewController(animated: true);
    }
    
    @IBAction func didBtnEnterClick(btn: UIButton){
        presentingViewController!.dismiss(animated: true)
    }
    
    func genBackButton() -> UIButton{
        let btnBack = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btnBack.setImage(#imageLiteral(resourceName: "Main_Nav_BackArror"), for: .normal)
        btnBack.addTarget(self.navigationController!, action: #selector(self.navigationController!.popViewController), for: .touchUpInside)
        return btnBack
    }
}
