//
//  CashWithdrawalVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/7.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class CashWithdrawalVC: UIEditVC {

    @IBOutlet weak var tfAmount: UITextField!       //提現金額
    
    @IBOutlet weak var imgBankLogo: UIImageView!    //銀行標誌
    @IBOutlet weak var lbBankName: UILabel!         //銀行名稱
    @IBOutlet weak var btSelectBank: UIButton!      //選擇銀行按鈕
    
    @IBOutlet weak var btAllCash: UIButton!         //全部提現按鈕
    
    @IBOutlet weak var lbWarningCash: UILabel!      //不可提现金额：200.00元
    
    @IBOutlet weak var lbUpDownCash: UILabel!       //提款上下限
    
    @IBOutlet weak var btnEnter: UIButton!
    
    var CanWithdrawAmount:Double?; //可提现金额
    
//    var bankAccount:String = "";    //放置前面傳來的資金密碼 **** 不使用 在後續用戶要送出 在要求輸入一次
    
    var tableData: [WithdrawalBankAccount]{
        return HYWithdrawal.withdrawal.withdrawalBankAccounts
    }
    let allBank = HYWithdrawal.withdrawal.banks; //全部銀行
    
    var selectedBankAccount: WithdrawalBankAccount?
    
    var checkMemberWithdrawMoney = CheckMemberWithdrawMoneyAddItem()    //放置提款上下限
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnEnter.layer.borderColor = UIColor(red: 0/255, green: 203/255, blue: 192/255, alpha: 1).cgColor; //邊框顏色1
        btnEnter.layer.borderWidth = 1.0; //邊框大小
        btnEnter.layer.masksToBounds = true;
        btnEnter.layer.cornerRadius = 25.0;

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        view.isUserInteractionEnabled = false
        //抓取可，不可提領金額
        HYWithdrawal.withdrawal.checkAmountMoney(){ CheckAmount , error in
            let NoWithdrawAmount = CheckAmount!.model_NoWithdrawAmount ?? 0.0;
            self.lbWarningCash.text = "不可提现金额：\(NSString(format: "%.2f", NoWithdrawAmount) as String)元";
            
            self.CanWithdrawAmount = CheckAmount!.model_CanWithdrawAmount ?? 0.0;
            self.tfAmount.placeholder = "可提现金额\(NSString(format: "%.2f", self.CanWithdrawAmount!) as String)元";
            
            self.view.isUserInteractionEnabled = true
        }
        
        //抓取 提款上下限
        HYWithdrawal.withdrawal.checkMemberWithdrawMoney(){ CheckAmount , error in
            self.checkMemberWithdrawMoney = CheckAmount!;
            self.lbUpDownCash.text = "单笔提款金额最低\(NSString(format: "%.2f", CheckAmount!.model_LowerLimit) as String)元，上限\(NSString(format: "%.2f", CheckAmount!.model_UpperLimit) as String)元";
        }
        
        //返回直接回到 用戶中心
        if let vcs = navigationController?.viewControllers, vcs.count > 1 {
            navigationController?.viewControllers.removeSubrange(1..<vcs.count-1)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didBtSelectBankClick(_ sender: UIButton) {
        self.performSegue(withIdentifier: "sendSelectWithdrawalUserBankVC", sender: nil);
    }
    
    @IBAction func didBtAllCashClick(_ sender: UIButton) {
        tfAmount.text = "\(NSString(format: "%.2f", self.CanWithdrawAmount!) as String)";
    }
    
    @IBAction func didBtnEnterClick(_ sender: UIButton) {
        hideKeyboard()
        guard let amount = Double(tfAmount.text ?? "") else {
            self.showGeneralAlert(.alert_info, lbTextA:"提现金额不能为空！"){ value in }
            return
        }
        
        if amount <= checkMemberWithdrawMoney.model_LowerLimit {
            self.showGeneralAlert(.alert_info, lbTextA:"单笔提款金额最低\(NSString(format: "%.2f", checkMemberWithdrawMoney.model_LowerLimit) as String)元，上限\(NSString(format: "%.2f", checkMemberWithdrawMoney.model_UpperLimit) as String)元"){ value in }
            return
        }
        
        if selectedBankAccount == nil {
            self.showGeneralAlert(.alert_info, lbTextA:"请选择银行"){ value in }
            return
        }
        
        showTextAlert(){ okValue, noValue, textValue in
            if okValue == 1 , noValue == 0 {
                if textValue != "" {
                    self.withdrawal(textValue);
                }else{
                    self.showGeneralAlert(.alert_info, lbTextA:"资金密码不能是空"){ value in }
                }
            }
        }
        
    }
    
    func withdrawal(_ bankAccount:String){
        
        var item = WithdrawalItem(withAccount: self.selectedBankAccount!)
        item.amount = Double(tfAmount.text!)
        item.password = bankAccount

        HYWithdrawal.withdrawal.withdrawal(item: item) {
            [weak self] error in
            if let err = error{
                let outMsg = err.localizedDescription;
                self?.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
            }
            else{
                self?.showGeneralAlert(.alert_ok,lbTextA:"提款申请已经提交，等待审核中"){ value in
                    _ = self?.navigationController?.popToRootViewController(animated: true);
                }
//                self?.performSegue(withIdentifier: "sendWithdrawalSuccess", sender: nil) //這頁不用
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendSelectWithdrawalUserBankVC" {
            if let vc = segue.destination as? SelectWithdrawalUserBankVC {
                vc.didSelectBankAccount = { [weak self] returnValue in
                    
                    self?.lbBankName.text = self?.translateWord(origin: returnValue.bankName) //把銀行代碼翻譯成中文名稱
                    
                    if (UIImage(named:returnValue.bankName) != nil) {
                        self?.imgBankLogo.image = UIImage(named:returnValue.bankName)
                    }else{
                        self?.imgBankLogo.image = UIImage(named:"OtherBank")
                    }
                    
                    self?.selectedBankAccount = returnValue;
                }
            }
        }
    
    }

    //對照中文銀行
    func translateWord(origin: String) -> String {
        for k in allBank {
            if k.Ename == origin {
                return k.name
            }
        }
        return origin
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if HYRegex.Amount.match(input: text) || text == "" {
            return checkUITextFieldValue(string);
        }else{
            return false
        }
        
    }
    
    func checkUITextFieldValue(_ inputString:String) -> Bool {
        
        let tfCash = Double(tfAmount.text! + inputString)!;
        let maxp = Double(self.checkMemberWithdrawMoney.model_UpperLimit); //單筆上限
        if(tfCash >= maxp){
            if(inputString != "") {
                tfAmount.text = String(maxp);
                return false;
            }
        }
        return true;
    }
    
}


















