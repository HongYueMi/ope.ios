//
//  WithdrawalBankCardAddVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/22.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class WithdrawalBankCardAddVC: UIEditVC {

    @IBOutlet weak var lbTitleBankID: UILabel!

    @IBOutlet weak var tfBankBranch: UITextField!
    @IBOutlet weak var lbTitleBankBranch: UILabel!
    
    @IBOutlet weak var tfAccount: UITextField!
    @IBOutlet weak var lbTitleAccount: UILabel!
    
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbTitleProvince: UILabel!

    @IBOutlet weak var lbTitleAddress: UILabel!
    @IBOutlet weak var tfAddress: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        bankChooseItem = InfoItem(title: LanWithdrawalBankCardAdd.InfoItem_PleaseSelect + "\(lbTitleBankID.text!)", info: nil, type: .optionTable([]))
        bankChooseItem.options = [HYWithdrawal.withdrawal.banks]
        
        provinceChooseItem = InfoItem(title: LanWithdrawalBankCardAdd.InfoItem_PleaseSelect + "\(lbTitleProvince.text!)", info: nil, type: .optionTable([]))
        provinceChooseItem.options = [HYDatas.provinces]
        
        lbName.text = HYAuth.auth.currentUser?.name
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        defer {
            addBankAccount()
        }
        return false
    }
    
    // MARK: - Bank
    
    @IBOutlet weak var btnChooseBank: UIButton!
    @IBOutlet weak var lbBankID: UILabel!
    var bankChooseItem: InfoItem!

    @IBAction func didBtnChooseBankClick(btn: UIButton){
        hideKeyboard()
        view.isUserInteractionEnabled = false
//        let vc = DialogVC.dialog(withInfo: bankChooseItem, actionHandler: { [weak self] in
//            self?.lbBankID.text = self?.bankChooseItem.selectedOption?.name
//            self?.dismiss(animated: true)
//        })
//        self.present(vc, animated: true){ [weak self] in
//            self?.view.isUserInteractionEnabled = true
//        }
    }
    
    // MARK: - Province

    @IBOutlet weak var btnChooseProvince: UIButton!
    @IBOutlet weak var lbProvince: UILabel!
    var provinceChooseItem: InfoItem!

    @IBAction func didBtnChooseProvinceClick(btn: UIButton){
        hideKeyboard()
        view.isUserInteractionEnabled = false
//        let vc = DialogVC.dialog(withInfo: provinceChooseItem, actionHandler: { [weak self] in
//            self?.lbProvince.text = self?.provinceChooseItem.selectedOption?.name
//            self?.dismiss(animated: true)
//        })
//        self.present(vc, animated: true){ [weak self] in
//            self?.view.isUserInteractionEnabled = true
//        }
    }
    
    // MARK: - Action
    
    var item: WithdrawalBankAccountAddItem!
    
    func addBankAccount(){
        guard let bank = bankChooseItem.selectedOption as? HYBankOption else {
            self.showGeneralAlert(.alert_info, lbTextA:"请选择\(lbTitleBankID.text!)"){ value in }
            return
        }
        guard let branch = tfBankBranch.text?.notEmptyValue else {
            self.showGeneralAlert(.alert_info, lbTextA: "\(lbTitleBankBranch.text!)不能为空！"){ value in }
            return
        }
        guard let account = tfAccount.text?.notEmptyValue else {
            self.showGeneralAlert(.alert_info, lbTextA:"\(lbTitleAccount.text!)不能为空！"){ value in }
            return
        }
        guard
            HYRegex.BankAccount.match(input: account)
            else
        {
            self.showGeneralAlert(.alert_info, lbTextA:"\(lbTitleAccount.text!)格式有误！"){ value in }
            return
        }
        guard let province = provinceChooseItem.selectedOption as? HYProvince else {
            self.showGeneralAlert(.alert_info, lbTextA:"请选择\(lbTitleProvince.text!)"){ value in }
            return
        }
        guard let address = tfAddress.text?.notEmptyValue else {
            self.showGeneralAlert(.alert_info, lbTextA:"\(lbTitleAddress.text!)不能为空！"){ value in }
            return
        }
        item.bankID = bank
        item.bankBranch = branch
        item.account = account
        item.name = HYAuth.auth.currentUser!.name
        item.bankProvince = province
        item.bankAddress = address
        
        HYWithdrawal.withdrawal.addWithdrawalBankAccount(withItem: item){
            error in
            if let error = error{
                self.showGeneralAlert(.alert_error, lbTextA: error.localizedDescription){ value in }
            }
            else{
                self.didBankAccountAdded()
            }
        }
    }
    
    func didBankAccountAdded(){
        let presenting = self.presentingViewController as! UINavigationController
        
        if presenting.viewControllers.last is BankCardListVC {
            presenting.dismiss(animated: true)
        }
        else{
            self.performSegue(withIdentifier: "BankToWithdrawal", sender: nil)
        }
    }
    
    // MARK: - TextField
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfBankBranch{
            tfAccount.becomeFirstResponder()
        }
        if textField == tfAccount{
            tfAccount.resignFirstResponder()
        }
        return true
    }

}
