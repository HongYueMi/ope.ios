//
//  BankCardAddSecurityCheckVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/22.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class BankCardAddSecurityCheckVC: PasswordEditVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = genDismissBarButtonItem();
        
    }
    
    //MARK: - Navigation

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        guard checkInput() else {
            return false
        }
        return true
    }
    
    override func didBtnEnterClick(btn: UIButton?) {
        
//        performSegue(withIdentifier: "SetBankPasswordToAddBankCard", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        hideKeyboard()
        let vc = segue.destination as! WithdrawalBankCardAddVC
        var item = WithdrawalBankAccountAddItem()
        item.password = tfPassword1.text!
        item.bankPassword = tfPassword2.text!
        vc.item = item
    }

    func genDismissBarButtonItem() -> UIBarButtonItem {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btn.setImage(#imageLiteral(resourceName: "Main_Nav_BackArror"), for: .normal)
        btn.addTarget(self, action: #selector(dismissSelf), for: .touchUpInside)
        return UIBarButtonItem(customView: btn)
    }
    
    @objc func dismissSelf(){
        presentingViewController?.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnEnter(_ sender: Any) {
        // self.navigationController?.pushViewController(VersionInformationVCA(), animated: true);
        
        if(checkInput() == true){
            var item:PasswordEditItem = PasswordEditItem();
            item.new = tfPassword2.text!;
            item.old = tfPassword1.text!;
            HYWithdrawal.withdrawal.chkPassword(withItem: item){ error in
                if let err = error{
                    print(err);
                    self.showGeneralAlert(.alert_info, lbTextA: err.localizedDescription){ value in }
                }else{
                    // WithdrawalBankCardAddVC
                    defer {
                        self.performSegue(withIdentifier: "sendAddCord", sender: nil);
                    }

                }
            }
        }
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        print("\(range.length)-----------------------Trigger");
        
        if (range.location > 0 && range.length == 1 && string.count == 0){
            let s: String = textField.text!;
            textField.text! = (s as NSString).substring(to: s.count - 1);
            print("\(s)-----------------------false");
            
            return false;
        }
        return true;
        
    }

    
    
}













