//
//  BankCardListVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/27.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class BankCardListVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colors = [color1, color2, color3]

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        
        let item = UIBarButtonItem(title: "添加", style: .plain, target: self, action: #selector(didBtnAddBankCardClick))
//        let item = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didBtnAddBankCardClick))
        navigationItem.rightBarButtonItems = tableData.count < 5 ? [item] : nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Add
    
    @objc func didBtnAddBankCardClick(){
        if !HYAuth.auth.currentUser!.isBankPasswordSet{
            defer {
                performSegue(withIdentifier: "SetBankPassword", sender: nil)
            }
        }
        else if HYWithdrawal.withdrawal.withdrawalBankAccounts.count < 5{
            defer {
                performSegue(withIdentifier: "SetBankCard", sender: nil)
            }
        }
    }
    
    // Bank Select
    
    @IBInspectable var color1: UIColor = .clear
    @IBInspectable var color2: UIColor = .clear
    @IBInspectable var color3: UIColor = .clear
    var colors: [UIColor] = []
    var imageBankCards: [UIImage] = [#imageLiteral(resourceName: "Fund_Icon_BankCard_Blue"), #imageLiteral(resourceName: "Fund_Icon_BankCard_Red"), #imageLiteral(resourceName: "Fund_Icon_BankCard_Gray"), #imageLiteral(resourceName: "Fund_Icon_BankCard_Green"), #imageLiteral(resourceName: "Fund_Icon_BankCard_Gray")]
    
    var tableData: [WithdrawalBankAccount]{
        return HYWithdrawal.withdrawal.withdrawalBankAccounts
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return min(tableData.count, 5)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WithdrawalBankCell") as! WithdrawalBankCell
        cell.viewLabel.backgroundColor = colors[indexPath.row % 3]
        let account = tableData[indexPath.row]
        cell.lbBankName.text = HYDatas.translateWord(origin: account.bankName)
        cell.lbAccount.text = account.account
        cell.imgvBankCard.image = imageBankCards[indexPath.row % 5]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
}
