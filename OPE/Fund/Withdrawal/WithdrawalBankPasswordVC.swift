//
//  WithdrawalBankPasswordVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/22.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class WithdrawalBankPasswordVC: BankPasswordAddVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = genDismissBarButtonItem()
        
        btnEnter.layer.cornerRadius = 25.0;
        btnEnter.layer.masksToBounds = true;
    }
    
    //MARK: - Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        defer {
            didBtnEnterClick(btn: btnEnter)
        }
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! WithdrawalBankCardAddVC
        var item = WithdrawalBankAccountAddItem()
        item.password = tfPassword1.text!
        item.bankPassword = tfPassword2.text!
        vc.item = item
        vc.navigationItem.leftBarButtonItem = genDismissBarButtonItem()
    }
    
    func genDismissBarButtonItem() -> UIBarButtonItem {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btn.setImage(#imageLiteral(resourceName: "Main_Nav_BackArror"), for: .normal)
        btn.addTarget(self, action: #selector(dismissSelf), for: .touchUpInside)
        return UIBarButtonItem(customView: btn)
    }
    
    @objc func dismissSelf(){
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Action
    
    override func didModifySuccess(){
        self.showGeneralAlert(.alert_ok, lbTextA:"修改成功"){ value in
            self.performSegue(withIdentifier: "AddBankCard", sender: nil)
        }
    }
    
    
}
