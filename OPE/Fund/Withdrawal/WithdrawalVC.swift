//
//  WithdrawalVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/20.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class WithdrawalBankCell: UITableViewCell{
    
    @IBInspectable var colorSelected: UIColor = .white
    @IBInspectable var colorDefault: UIColor = .white
    
    @IBOutlet weak var lbBankName: UILabel!
    @IBOutlet weak var lbAccount: UILabel! //銀行卡號
    @IBOutlet weak var viewLabel: UIView!
    @IBOutlet weak var imgvBankCard: UIImageView!
    @IBOutlet weak var viewContent: UIView!

    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: true)
        self.lbBankName.textColor = selected ? colorDefault : colorSelected
        self.lbAccount.textColor = selected ? colorDefault : colorSelected
        self.viewContent.backgroundColor = selected ? colorSelected : colorDefault
    }
}

class WithdrawalVC: UIEditVC, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tfAmount: UITextField! //提現金額
    @IBOutlet weak var tfPassword: UITextField! //資金密碼
    @IBOutlet weak var ctrlAll: UIControl!
    @IBOutlet weak var imgvCheckBox: UIImageView!

    @IBOutlet weak var lbAvailableAmount: UILabel! //不可用金額-變數
    @IBOutlet weak var btnEnter: UIButton!
    @IBOutlet weak var lbPassword: UILabel!

    var CanWithdrawAmount:Double?; //可提现金额
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        colors = [color1, color2, color3]
//        lbAvailableAmount.text = HYFund.fund.strBalance
        
        if tableData.count > 0 {
            tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .top)
            selectedBankAccount = tableData[0]
        }
        
        self.newLoadingScreenView.showActivityIndicator(uiView: self.view);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpNavigationBar()
        
        //抓取可，不可提領金額
        HYWithdrawal.withdrawal.checkAmountMoney(){ CheckAmount , error in
            let NoWithdrawAmount = CheckAmount!.model_NoWithdrawAmount!;
            self.lbAvailableAmount.text = NSString(format: "%.2f", NoWithdrawAmount) as String;
            
            self.CanWithdrawAmount = CheckAmount!.model_CanWithdrawAmount!;
            self.tfAmount.placeholder = "可提现金额\(NSString(format: "%.2f", self.CanWithdrawAmount!) as String)元";
            
            self.newLoadingScreenView.hideActivityIndicator(uiView: (self.view)!);
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Navigation
    
    func setUpNavigationBar(){
        navigationItem.leftBarButtonItem = genDismissBarButtonItem()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func genDismissBarButtonItem() -> UIBarButtonItem {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btn.setImage(#imageLiteral(resourceName: "Main_Nav_BackArror"), for: .normal)
        btn.addTarget(self, action: #selector(dismissSelf), for: .touchUpInside)
        return UIBarButtonItem(customView: btn)
    }
    
    @objc func dismissSelf(){
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    var selectedBankAccount: WithdrawalBankAccount!
    
    func withdrawal(){
        hideKeyboard()
        guard let password = tfPassword.text?.notEmptyValue else {
            self.showGeneralAlert(.alert_info, lbTextA:"资金密码不能为空！（6-12位必须含有字母和数字的组合）"){ value in }
            return
        }
        guard let amount = Double(tfAmount.text ?? "") else {
            self.showGeneralAlert(.alert_info, lbTextA:"提现金额不能为空！"){ value in }
            return
        }
        guard amount >= 100, amount <= 50000 else {
            self.showGeneralAlert(.alert_info, lbTextA:"单笔提现金额最低100元，上限50000元！"){ value in }
            return
        }
        guard
            HYRegex.Password.match(input: tfPassword.text!)
            else
        {
            self.showGeneralAlert(.alert_info, lbTextA:"\(lbPassword.text!)格式有誤！(6-12位必須含有字母和數字的組合)"){ value in }
            return
        }
        
        var item = WithdrawalItem(withAccount: self.selectedBankAccount!)
        item.amount = amount
        item.password = password
        
        HYWithdrawal.withdrawal.withdrawal(item: item){
            [weak self] error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA: err.localizedDescription){ value in }
            }
            else{
                self?.performSegue(withIdentifier: "WithdrawalSuccess", sender: nil)
            }
        }
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "WithdrawalSuccess"{
            defer {
                withdrawal()
            }
            return false
        }
        return true
    }
    
    
    @IBAction func didCtrlMaxAmountClick(ctrl: UIControl){
        ctrl.isSelected = !ctrl.isSelected
        imgvCheckBox.isHighlighted = ctrl.isSelected
        tfAmount.isEnabled = !ctrl.isSelected
        tfAmount.textColor = ctrl.isSelected ? .lightGray : .black
        if ctrl.isSelected {
            tfAmount.text = "\(NSString(format: "%.2f", self.CanWithdrawAmount!) as String)"
        }else{
            tfAmount.text = nil
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == tfAmount{
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            return HYRegex.Amount.match(input: text) || text == ""
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfPassword{
            withdrawal()
        }
        return true
    }
    
    // Bank Select
    
    @IBInspectable var color1: UIColor = .clear
    @IBInspectable var color2: UIColor = .clear
    @IBInspectable var color3: UIColor = .clear
    var colors: [UIColor] = []
    var imageBankCards: [UIImage] = [#imageLiteral(resourceName: "Fund_Icon_BankCard_Blue"), #imageLiteral(resourceName: "Fund_Icon_BankCard_Red"), #imageLiteral(resourceName: "Fund_Icon_BankCard_Gray"), #imageLiteral(resourceName: "Fund_Icon_BankCard_Green"), #imageLiteral(resourceName: "Fund_Icon_BankCard_Gray")]
    
    var tableData: [WithdrawalBankAccount]{
        return HYWithdrawal.withdrawal.withdrawalBankAccounts
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WithdrawalBankCell") as! WithdrawalBankCell
        cell.viewLabel.backgroundColor = colors[indexPath.row % 3]
        let account = tableData[indexPath.row]
        cell.lbBankName.text = HYDatas.translateWord(origin: account.bankName)
        cell.lbAccount.text = account.account
        cell.imgvBankCard.image = imageBankCards[indexPath.row % 5]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedBankAccount = tableData[indexPath.row]
        print("didSelectRowAt: \(indexPath.row)")
    }
    
}
