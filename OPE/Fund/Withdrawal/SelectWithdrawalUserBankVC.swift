//
//  SelectWithdrawalUserBankVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/22.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

class SelectWithdrawalUserBankCell: UITableViewCell{
    
    @IBOutlet weak var lbBankName: UILabel!         //銀行名稱
    @IBOutlet weak var lbBankAccount: UILabel!      //銀行卡號
    @IBOutlet weak var lbUserName: UILabel!         //用戶姓名
    @IBOutlet weak var imgvBankCard: UIImageView!   //銀行Logo
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

import UIKit

class SelectWithdrawalUserBankVC: UIEditVC, UITableViewDataSource, UITableViewDelegate {
    let allBank = HYWithdrawal.withdrawal.banks; //全部銀行
    
    var tableData: [WithdrawalBankAccount]{
        return HYWithdrawal.withdrawal.withdrawalBankAccounts
    }
    
//    var selectedBankAccount: WithdrawalBankAccount!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if tableData.count > 0 {
            tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .top)
//            selectedBankAccount = tableData[0]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var didSelectBankAccount: ((WithdrawalBankAccount) -> Void)!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectWithdrawalUserBankCell") as! SelectWithdrawalUserBankCell
        let account = tableData[indexPath.row]
        cell.lbBankName.text = translateWord(origin: account.bankName)
//        cell.lbBankName.text = HYDatas.translateWord(origin: account.bankName)
        cell.lbBankAccount.text = account.account
        cell.lbUserName.text = HYDatas.translateWord(origin: account.name)
        
        if (UIImage(named:tableData[indexPath.row].bankName) != nil) {
            cell.imgvBankCard.image = UIImage(named:tableData[indexPath.row].bankName)
        }else{
            cell.imgvBankCard.image = UIImage(named:"OtherBank")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.selectedBankAccount = tableData[indexPath.row]
        self.didSelectBankAccount(tableData[indexPath.row]);
        _ = navigationController?.popViewController(animated: true);
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0;
    }
    
    //對照中文銀行
    func translateWord(origin: String) -> String {
        for k in allBank {
            if k.Ename == origin {
                return k.name
            }
        }
        return origin
    }
    
}
































