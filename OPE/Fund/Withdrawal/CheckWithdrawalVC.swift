//
//  CheckWithdrawalVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/7.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class CheckWithdrawalVC: UIEditVC {

    @IBOutlet weak var tfPassword1: UITextField!
    @IBOutlet weak var btnEnter: UIButton!
    
    @IBOutlet weak var btBack: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnEnter.layer.borderColor = UIColor(red: 0/255, green: 203/255, blue: 192/255, alpha: 1).cgColor; //邊框顏色1
        btnEnter.layer.borderWidth = 1.0; //邊框大小
        btnEnter.layer.masksToBounds = true;
        btnEnter.layer.cornerRadius = 25.0;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    @IBAction func didBtnEnterClick(_ sender: UIButton) {
        editPassword();
    }
    
    func editPassword(){
        if let upw = HYAuth.auth.currentUser?.upw , checkInput() == true {
            var item:PasswordEditItem = PasswordEditItem();
            item.new = upw;
            item.old = tfPassword1.text!;
            HYWithdrawal.withdrawal.chkPassword(withItem: item){ error in
                if let err = error {
                    print(err);
                    self.showGeneralAlert(.alert_error, lbTextA: err.localizedDescription){ value in }
                } else {
                    defer {
                        self.performSegue(withIdentifier: "sendCashWithdrawalVC", sender: nil);
                    }
                }
            }
            
        }
        
    }

    func checkInput() -> Bool{
        guard let password1 = tfPassword1.text?.notEmptyValue else {
            showGeneralAlert(.alert_info,lbTextA:"资金密码不能为空！（6-12位必须含有字母和数字的组合）"){ value in }
            return false
        }
        guard
            HYRegex.Password.match(input: password1)
            else
        {
            showGeneralAlert(.alert_info,lbTextA:"资金密码格式有误！（6-12位必须含有字母和数字的组合）"){ value in }
            return false
        }
        
        return true
    }
    
    @IBAction func didBtBackClick(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil);
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true;
        }
        
        if textField == tfPassword1 , let txt = tfPassword1.text , txt.count >= 12 {
            return false
        }
        
        return true;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendCashWithdrawalVC" {
            if let vc = segue.destination as? CashWithdrawalVC {
//                vc.bankAccount = tfPassword1.text!;
            }
        }
        
    }
    
}



















