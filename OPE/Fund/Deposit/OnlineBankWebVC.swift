//
//  OnlineBankWebVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/26.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import WebKit

class OnlineBankWebVC: UIViewController, WKUIDelegate {

    var payment: HYPayment!
    
    weak var webVC : WebVC!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EmbedWebView"{
            self.webVC = segue.destination as! WebVC
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = (navigationController as! NavigationController).genDismissBarButtonItem()
        webVC.webView.uiDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let url = payment.url{
            webVC.webView.load(URLRequest(url: url))
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        webVC.webView.stopLoading()
    }
    
    //MARK: - Action
    
    @IBAction func refresh(){
        webVC.webView.reload()
    }

}
