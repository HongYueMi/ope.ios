//
//  DepositWebBankVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/14.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//  網銀支付

import UIKit

class DepositWebBankVC: OnlineDepositVC {
    
    @IBOutlet weak var btnChooseBank: UIButton! //選取銀行按鈕
    @IBOutlet weak var lbBankName: UILabel!     //銀行名稱
    @IBOutlet weak var imgBank: UIImageView!    //銀行LOGO
    
    var bankData: [HYBankData] = [];
    
    var okBankData: HYBankData?; //選中的銀行
    
    let allBank = HYWithdrawal.withdrawal.banks; //全部銀行
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnEnter.layer.cornerRadius = 25.0;
        btnEnter.layer.masksToBounds = true;
        
        genDepositItem()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //選擇銀行
    @IBAction func didBtSelectBankClick(_ sender: Any) {
        hideKeyboard()
        self.performSegue(withIdentifier: "sendSelectDepositBankListVC", sender: nil);
    }
    
    // MARK: - Deposit
    
    override func didDeposetItemGet(){
        //判斷 /API/MemberDepositGroupPlatform/GetAsync 是否有值，線上版常發生API取不到問題
        if(self.depositItem == nil){
            self.maintenanceView(); //顯示通道維護中
            let lower = self.lowerIsNull; //單筆下限
            let uper = self.uperIsNull; //單筆上限
            self.lbSingle.text = "单笔存款最低\(String(describing: lower)).00元，上限\(String(describing: uper)).00元";
            return
        }else{
            let lower = self.depositItem!.lowerLimit; //單筆下限
            let uper = self.depositItem!.uperLimit; //單筆上限
            self.lbSingle.text = "单笔存款最低\(String(describing: lower)).00元，上限\(String(describing: uper)).00元";
        }
        
        //依該會員的代理商和轉账平台，取得提供的銀行清單
        HYFund.fund.getAvailableBankList(withDepositItem: self.depositItem){
            [weak self] banks, error in
            if let err = error{
                if self?.checkMaintenanceView(self!.view) == false {
                    self?.maintenanceView(); //顯示通道維護中
                }
            }
            else if let banks = banks, banks.count > 0 {
                defer{
                    self?.bankData = banks;
//                    print("hear **** \n \(String(describing: self?.bankData))")
                }
                return
            }else{
                self?.btnEnter.isEnabled = false
            }
        }
    }
    
    //下一步
    @IBAction override func didBtnEnterClick(btn: UIButton){
        hideKeyboard()

        if self.okBankData == nil {
            showGeneralAlert(.alert_info,lbTextA:"请选择银行"){ value in }
            return
        }
        
        if(self.depositItem != nil){
            if(self.depositItem!.lowerLimit > 0 && self.depositItem!.uperLimit > 0){
                if checkParameters(){
                    deposit(withItem: depositItem)
                }
            }else{
                showGeneralAlert(.alert_info,lbTextA:"通道维护中"){ value in }
            }
        }else{
            showGeneralAlert(.alert_info,lbTextA:"通道维护中"){ value in }
        }
        
    }
    
    override func checkParameters() ->Bool{
        guard super.checkParameters() else {
            return false
        }
        
        if self.okBankData?.thirdPartyCode == nil {
            showGeneralAlert(.alert_info,lbTextA:"通道维护中"){ value in }
            return false
        }
        
//        depositItem.bankCode = code
        return true
    }
    
    override func showPaymentWebVC(){
        if let url = payment.url{   //網銀支付 使用的Key 都用url
            print(url)
            if UIApplication.shared.canOpenURL(url){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil);
                } else {
                    UIApplication.shared.openURL(url);
                }
                checkDeposit()
            }
            else{
                showGeneralAlert(.alert_info,lbTextA:"请先安装微信的APP"){ value in }
            }
        }
        else{
            
//            let nav = UIStoryboard.DepositFund.identifier("OnlineBankWebNavVC") as! NavigationController
//            let web = nav.viewControllers[0] as! OnlineBankWebVC
//            web.payment = payment
//            web.navigationItem.title = depositGroup.name
//            self.dismiss(animated: true){
//                self.present(nav, animated: true)
//            }
        }
        
    }

    //對照中文銀行
    func translateWord(origin: String) -> String {
        for k in allBank {
            if k.Ename == origin {
                return k.name
            }
        }
        return origin
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendSelectDepositBankListVC" {
            if let vc = segue.destination as? SelectDepositBankListVC {
                vc.bankData = self.bankData;
                vc.allBank = self.allBank;
                vc.didSelectBankAccount = { [weak self] returnValue in
                    self?.depositItem.bankCode = returnValue[0].code
                    self?.lbBankName.text = returnValue[0].name
                    self?.okBankData = returnValue[0];
                    if (UIImage(named:returnValue[0].code) != nil) {
                        self?.imgBank.image = UIImage(named:returnValue[0].code)
                    }else if returnValue[0].name == "浦发银行" , UIImage(named:"pfyh") != nil {
                        self?.imgBank.image = UIImage(named:"pfyh");
                    }else if returnValue[0].name == "广发银行" , UIImage(named:"gdb") != nil {
                        self?.imgBank.image = UIImage(named:"gdb");
                    }else{
                        for value in self!.allBank {
                            if value.name == returnValue[0].name {
                                if (UIImage(named:value.Ename) != nil) {
                                    self?.imgBank.image = UIImage(named:value.Ename)
                                    break
                                }
                            }else{
                                self?.imgBank.image = UIImage(named:"OtherBank")
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if HYRegex.Amount.match(input: text) || text == "" {
            return checkUITextFieldValue(string);
        }else{
            return false
        }
        
    }
    
    override func checkUITextFieldValue(_ inputString:String) -> Bool {
        
        let tfCash = Double(tfAmount.text! + inputString)!;
        let maxp = Double(self.depositItem!.uperLimit); //單筆上限
        if(tfCash >= maxp){
            if(inputString != "") {
                tfAmount.text = String(maxp);
                return false;
            }
        }
        return true;
    }
    
}







































