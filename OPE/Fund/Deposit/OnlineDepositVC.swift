//
//  OnlineDepositVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/19.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class OnlineDepositVC: UIEditVC {
    
    weak var depositVC: DepositPageVC!

    var depositGroup: DepositGroup!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        genDepositItem()
        
        btnEnter.layer.cornerRadius = 25.0;
        btnEnter.layer.masksToBounds = true;
        
        if depositGroup.id == 3 {
            self.maintenanceView(); //顯示通道維護中
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Deposit
    
    @IBOutlet weak var tfAmount: UITextField!
    
    @IBOutlet weak var btnDollar100: UIButton!
    @IBOutlet weak var btnDollar500: UIButton!
    @IBOutlet weak var btnDollar1000: UIButton!
    
    @IBOutlet weak var btnEnter: UIButton!
    
    @IBOutlet weak var lbSingle: UILabel!
    
    @IBAction func didBtnDollarClick(btn: UIButton){
        let amount = btn == btnDollar100 ? 100 : btn == btnDollar500 ? 500 : btn == btnDollar1000 ? 1000 : 0
        tfAmount.text = "\(amount)"
    }
    
    @IBAction func didBtnEnterClick(btn: UIButton) {
        hideKeyboard()
        
        if(self.depositItem != nil){
            if(self.depositItem!.lowerLimit > 0 && self.depositItem!.uperLimit > 0){
                if checkParameters(){
                    deposit(withItem: depositItem)
                }
            }else{
                showGeneralAlert(.alert_error,lbTextA:"通道维护中"){ value in }
            }
        }else{
            showGeneralAlert(.alert_error,lbTextA:"通道维护中"){ value in }
        }
    }
    
    func checkParameters() ->Bool{
        guard let amount = Double(tfAmount.text ?? "")  else {
            defer {
                showGeneralAlert(.alert_info,lbTextA:"充值金额不可为空"){ value in }
            }
            return false
        }
        guard amount >= Double(depositItem.lowerLimit), amount <= Double(depositItem.uperLimit) else {
            let lower = self.depositItem!.lowerLimit; //單筆下限
            let uper = self.depositItem!.uperLimit; //單筆上限
            showGeneralAlert(.alert_info,lbTextA:"单笔存款最低\(String(describing: lower))元",lbTextB:"上限\(String(describing: uper))元" ){ value in }
            return false
        }
        
        depositItem.amount = Double(amount)
        return true
    }
    
    var depositItem: DepositItem!
    var lowerIsNull:Int = 0; //單筆下限空的時候
    var uperIsNull:Int = 0; //單筆上限空的時候
    func genDepositItem(){
        if(depositGroup != nil){
            HYFund.fund.getHYDepositItem(withDepositGroup: depositGroup){
                [weak self] item, error in
                self?.depositItem = item
                
                if let err = error{
                    self?.showGeneralAlert(.alert_error, lbTextA: err.localizedDescription){ value in }
                }else{
                    self?.didDeposetItemGet()
                }
            }
        }else{
            print("depositGroup is NULL????");
        }
        
    }
    
    func didDeposetItemGet(){
        //判斷 /API/MemberDepositGroupPlatform/GetAsync 是否有值，線上版常發生API取不到問題
        if(self.depositItem == nil){
            self.maintenanceView(); //顯示通道維護中
            let lower = lowerIsNull; //單筆下限
            let uper = uperIsNull; //單筆上限
            self.lbSingle.text = "单笔存款最低\(String(describing: lower)).00元，上限\(String(describing: uper)).00元";
        }else{
            let lower = self.depositItem!.lowerLimit; //單筆下限
            let uper = self.depositItem!.uperLimit; //單筆上限
            self.lbSingle.text = "单笔存款最低\(String(describing: lower)).00元，上限\(String(describing: uper)).00元";
        }
    }
    
    func deposit(withItem item: DepositItem){
        self.view.isUserInteractionEnabled = false
        HYFund.fund.deposit(withDeposit: item){
            [weak self] payment, error in
            
            // 清空金額
            self?.tfAmount.text = nil
            //
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
                    self?.view.isUserInteractionEnabled = true
//                    self?.depositVC.checkDeposit()
                }
            }else if nil != payment{
               self?.showPayment(payment!)
            }else{
                self?.view.isUserInteractionEnabled = true
                self?.checkDeposit()
            }
        }
    }
    
    func showPayment(_ payment: HYPayment){
        self.payment = payment
        self.view.isUserInteractionEnabled = true
        showGeneralAlert(.alert_ok,lbTextA:"订单建立成功，立即前往支付",btTextOK:"前往支付"){ value in
            if value == 1 {
                self.showPaymentWebVC();
            }
        }

    }
    
    func checkDeposit(){
        self.depositVC.checkDeposit()
//        self.dismiss(animated: true){
//            self.depositVC.checkDeposit()
//        }
    }
    
    var payment: HYPayment!
    
    @objc func showPaymentWebVC(){
        if let url = payment.url{
            print(url)
            if UIApplication.shared.canOpenURL(url){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil);
                } else {
                    UIApplication.shared.openURL(url);
                }
                checkDeposit()
            }
            else{
                showGeneralAlert(.alert_info,lbTextA:"请先安装微信的APP"){ value in }
            }
        }
        else{
//            let nav = UIStoryboard.DepositFund.identifier("OnlineBankWebNavVC") as! NavigationController
//            let web = nav.viewControllers[0] as! OnlineBankWebVC
//            web.payment = payment
//            web.navigationItem.title = depositGroup.name
//            self.dismiss(animated: true){
//                self.present(nav, animated: true)
//            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if HYRegex.Amount.match(input: text) || text == "" {
            return checkUITextFieldValue(string);
        }else{
            return false
        }
    }
    
    func checkUITextFieldValue(_ inputString:String) -> Bool {
        
        let tfCash = Double(tfAmount.text! + inputString)!;
        let maxp = Double(self.depositItem!.uperLimit); //單筆上限
        if(tfCash >= maxp){
            if(inputString != "") {
                tfAmount.text = String(maxp);
                return false;
            }
        }
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        didBtnEnterClick(btn: btnEnter)
        return true
    }

    //關閉通道維護中 View
    func removeSubview(_ closeViewNum:Int = 5168){
        while let viewWithTag = self.view.viewWithTag(closeViewNum) {
            viewWithTag.removeFromSuperview()
        }
    }
    
    //通道維護中 View
    func maintenanceView(){
        let container: UIView = UIView();
        container.frame = self.view.frame;
        container.backgroundColor = UIColor.black;
        container.alpha = 0.7;
        container.tag = 5168;
        self.view.addSubview(container);
        
        let container_W = container.frame.size.width;
        let container_H = container.frame.size.height;
        
        let whiteView: UIView = UIView();
        whiteView.backgroundColor = UIColor.white;
        whiteView.frame = CGRect(x:container_W/8,y:(container_H/5),width:container_W-((container_W/8)*2),height:(container_H/4));
        whiteView.layer.cornerRadius = 10;
        whiteView.layer.masksToBounds = true;
        self.view.addSubview(whiteView);
        
        let whiteView_W = whiteView.frame.size.width;
        let whiteView_H = whiteView.frame.size.height;
        
        let imgView = UIImageView();
        let width_imgView = (whiteView_W/3)+16
        imgView.frame = CGRect(x:(whiteView_W/2)-(width_imgView/2),y:(whiteView_H/2)-((whiteView_H/6)*4)/2,width: width_imgView, height: (whiteView_H/6)*4 );
        imgView.image = UIImage(named: "Fund_con001.png");
        whiteView.addSubview(imgView);
    }
    
    func checkMaintenanceView(_ chkView:UIView) -> Bool {
        if chkView.viewWithTag(5168) != nil {
            return true;
        }
        return false;
    }
    
    
}
























