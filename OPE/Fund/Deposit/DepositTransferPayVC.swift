//
//  DepositTransferPayVC.swift
//  OPE
//
//  Created by rwt113 on 2017/7/31.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//  轉账

import UIKit

class DepositTransferPayVC: OnlineDepositVC {

    @IBOutlet weak var tvPrecautions: UITextView!; //注意事項
    @IBOutlet weak var tfAccount: UITextField! //PayCardNumber
    @IBOutlet weak var tfName: UITextField! //PayUserName
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnEnter.layer.cornerRadius = 25.0;
        btnEnter.layer.masksToBounds = true;
        
        genDepositItem()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tfAccount.text = ""
        tfName.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var HYPaymentS:HYPayment?
    @IBAction func didBtSubmitClick(_ sender: Any) {
        
        if(self.depositItem != nil){
            if(self.depositItem!.lowerLimit > 0 && self.depositItem!.uperLimit > 0){
                guard checkInput() else {
                    return
                }

                self.depositItem.PayCardNumber = tfAccount.text!
                self.depositItem.PayUserName = tfName.text!

                hideKeyboard()
                if checkParameters(){
                    
                    self.newLoadingScreenView.showActivityIndicator(uiView: self.view);
                    HYFund.fund.deposit(withDeposit: self.depositItem){
                        [weak self] payment, error in
                        
                        self?.newLoadingScreenView.hideActivityIndicator(uiView: self!.view);
                        
                        self?.tfAmount.text = nil;  // 清空金額
                        
                        if let err = error{
                            self?.showGeneralAlert(.alert_error,lbTextA:err.localizedDescription){ value in }
                        }else{
                            self?.HYPaymentS = payment;
                            self?.performSegue(withIdentifier: "sendDepositManualPayVC", sender: nil)
                        }
                    }
                }
            }else{
                showGeneralAlert(.alert_info,lbTextA:"通道维护中"){ value in }
            }
        }else{
            showGeneralAlert(.alert_info,lbTextA:"通道维护中"){ value in }
        }
        
    }
    
    override func didDeposetItemGet(){
        //判斷 /API/MemberDepositGroupPlatform/GetAsync 是否有值，線上版常發生API取不到問題
        if(self.depositItem == nil){
            self.maintenanceView(); //顯示通道維護中
            let lower = self.lowerIsNull; //單筆下限
            let uper = self.uperIsNull; //單筆上限
            self.lbSingle.text = "单笔存款最低\(String(describing: lower)).00元，上限\(String(describing: uper)).00元";
            return
        }else{
            let lower = self.depositItem!.lowerLimit; //單筆下限
            let uper = self.depositItem!.uperLimit; //單筆上限
            self.lbSingle.text = "单笔存款最低\(String(describing: lower)).00元，上限\(String(describing: uper)).00元";
        }
    }
    
    func checkInput() -> Bool{
        guard let tfAccount = tfAccount.text?.notEmptyValue else {
            showGeneralAlert(.alert_info,lbTextA:"支付账号不可为空"){ value in }
            return false
        }
        guard HYRegex.BankAccount.match(input: tfAccount)
            else
        {
            showGeneralAlert(.alert_info,lbTextA:"支付账号格式错误"){ value in }
            return false
        }
        
        guard (tfName.text?.notEmptyValue) != nil else {
            showGeneralAlert(.alert_info,lbTextA:"支付人姓名不可為空"){ value in }
            return false
        }
        
        return true
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(tfAmount == textField){ //充值金額
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if HYRegex.Amount.match(input: text) || text == "" {
                return checkUITextFieldValue(string);
            }else{
                return false
            }
        }else if(tfName == textField){
            if(string == " "){
                return false
            }else{
                return true
            }
        }else{
            return true
        }
        
    }
    
    override func checkUITextFieldValue(_ inputString:String) -> Bool {
        
        let tfCash = Double(tfAmount.text! + inputString)!;
        let maxp = Double(self.depositItem!.uperLimit); //單筆上限
        if(tfCash >= maxp){
            if(inputString != "") {
                tfAmount.text = String(maxp);
                return false;
            }
        }
        return true;
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(tfAmount == textField){ //充值金額
            didBtnEnterClick(btn: btnEnter)
            return true
        }else if(tfAccount == textField){
            return true
        }else if(tfName == textField){
            return true
        }else{
            return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendDepositManualPayVC" {
            let vc = segue.destination as! DepositManualPayVC;
            vc.bankAccount = self.HYPaymentS;
        }
        
    }
    
    
}


























