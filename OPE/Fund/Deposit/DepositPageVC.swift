//
//  DepositPageVC.swift
//  OPE
//
//  Created by rwt113 on 2017/10/18.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension UIStoryboard{
    static let DepositFund : UIStoryboard = UIStoryboard(name: "DepositFund", bundle: nil)
}

class DepositPageVC: TabPageVC2 {

    weak var depositVC: DepositPageVC!
    
    var feedBack: DepositVerify!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titles = [
            "在线充值"
            , "转账"
            , "微信支付"
            , "支付宝支付"
        ]
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkDeposit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        navigationController?.navigationBar.topItem?.title = "充值"
    }
    
    //MARK: - Pages
    
    var pages: [Int: UIViewController] = [:]
    
    override func vc(atIndex idx: Int) -> UIViewController? {
        
        guard idx >= 0, idx < titles.count else {
            return nil
        }
        if pages[idx] == nil{
            let vc = UIStoryboard.DepositFund.identifier("DepositListVC") as! DepositListVC
            vc.type = -1
            pages[idx] = vc
        }
        
        return pages[idx]
    }

    override func setUpPageViewController(){
        pageVC = self.childViewControllers.last! as! UIPageViewController
        pageVC.dataSource = self
        pageVC.delegate = self
        
        //在線充值
        let webBankVC = UIStoryboard.DepositFund.identifier("DepositWebBankVC") as! DepositWebBankVC
        webBankVC.depositGroup = DepositGroup(name: "在线充值", id: 1, code: "OnlinePayment")
        webBankVC.depositVC = self
        
        //轉账
        let manualVC = UIStoryboard.DepositFund.identifier("DepositTransferPayVC") as! DepositTransferPayVC
        manualVC.depositGroup = DepositGroup(name: "转账", id: 6, code: "BQ")
        manualVC.depositVC = self
        
        //微信支付
        let weChatVC = UIStoryboard.DepositFund.identifier("OnlineDepositVC") as! OnlineDepositVC
        weChatVC.depositGroup = DepositGroup(name: "微信支付", id: 7, code: "WeChatPayment")
        weChatVC.depositVC = self
        
        //支付寶
        let alyPayVC = UIStoryboard.DepositFund.identifier("OnlineDepositVC") as! OnlineDepositVC
        alyPayVC.depositGroup = DepositGroup(name: "支付宝支付", id: 3, code: "AliPayment")
        alyPayVC.depositVC = self

        pages = [
            0:webBankVC
            , 1:manualVC
            , 2:weChatVC
            , 3:alyPayVC
        ]
        
        pageVC.setViewControllers([pages[idxStart]!], direction: .forward, animated: false)
    }
    
    func checkDeposit(){
        self.view.isUserInteractionEnabled = false
        HYFund.fund.depositStatusCheck{
            [weak self] verify, error in
            
            if let err = error {
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
        if value == 1 {
            self?.view.isUserInteractionEnabled = true
        }
     }
            }else if let feedbackS = verify{
                if feedbackS.isVerifying{
                    self?.feedBack = feedbackS
                    self?.showTwoSelectAlert(.alert_info,lbTextA:"您的存款申请已进入审核队列", lbTextB: "预计5分钟内客服会进行处理", btTextOK:"已完成存款", btTextNO:"存款未完成" ){ okValue, noValue in
                        if okValue == 1 , noValue == 0 {
                            self?.feedBackYes()
                        }else{
                            self?.feedBackNO()
                        }
                    }
                    
                }else{
                    self?.view.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    @objc func feedBackYes(){
        feedBack(true)
    }
    
    @objc func feedBackNO(){
        feedBack(false)
    }
    
    func feedBack(_ answer: Bool){
        feedBack.feedBack = answer
        HYFund.fund.DepositFeedback(feedback: feedBack){
            [weak self] error in
            self?.view.isUserInteractionEnabled = true
            if let error = error{
                let outMsg = error.localizedDescription;
                self?.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
            }else{
//                self?.showGeneralAlert(.alert_info,lbTextA:"本次存款申请状态已经提交客服审核，您可以再次存款！"){ value in }
            }
        }
    }
    
}

































