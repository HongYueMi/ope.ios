//
//  SelectDepositBankListVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/24.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

class SelectDepositBankListCell: UITableViewCell{
    
    @IBOutlet weak var lbBankName: UILabel!         //銀行名稱
    @IBOutlet weak var imgvBankCard: UIImageView!   //銀行Logo
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

import UIKit

class SelectDepositBankListVC: UIEditVC, UITableViewDataSource, UITableViewDelegate {

    var bankData: [HYBankData] = [];
    var allBank:[HYBankOption]!;    //從前面取得全部銀行
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "选择银行";
        
        if bankData.count > 0 {
            tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .top)
        }
        
        tableView.reloadData();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var didSelectBankAccount: (([HYBankData]) -> Void)!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bankData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDepositBankListCell") as! SelectDepositBankListCell
        let account = bankData[indexPath.row]
        cell.lbBankName.text = account.name
        if (UIImage(named:account.code) != nil) {
            cell.imgvBankCard.image = UIImage(named:account.code)
        }else if account.name == "浦发银行" , UIImage(named:"pfyh") != nil {
            cell.imgvBankCard.image = UIImage(named:"pfyh");
        }else if account.name == "广发银行" , UIImage(named:"gdb") != nil {
            cell.imgvBankCard.image = UIImage(named:"gdb");
        }else{
            for value in allBank {
                if value.name == account.name {
                    if (UIImage(named:value.Ename) != nil) {
                        cell.imgvBankCard.image = UIImage(named:value.Ename)
                        break
                    }
                }else{
                    cell.imgvBankCard.image = UIImage(named:"OtherBank")
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.selectedBankAccount = tableData[indexPath.row]
        self.didSelectBankAccount([bankData[indexPath.row]]);
        _ = navigationController?.popViewController(animated: true);
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0;
    }

}
