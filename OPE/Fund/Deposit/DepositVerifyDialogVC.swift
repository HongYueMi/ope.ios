//
//  DepositVerifyDialogVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/26.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//  沒在用

import UIKit

class DepositVerifyDialogVC: UIViewController {

    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var ctrlCancel: UIControl!

    var item: DepositVerify!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
