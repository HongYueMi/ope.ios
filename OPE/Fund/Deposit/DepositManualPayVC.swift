//
//  DepositManualPayVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/14.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class DepositManualPayVC: UIViewController {
    
    @IBOutlet weak var btnEnter: UIButton!
    
    var infos: [UILabel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        infos = [lbBankName, lbAccount, lbName, lbNote, lbCash]
        
        update()
        
        btnEnter.layer.cornerRadius = 25.0;
        btnEnter.layer.masksToBounds = true;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "转账";
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - BankAccountData
    
    @IBOutlet weak var lbBankName: UILabel!     //入款銀行：
    @IBOutlet weak var lbAccount: UILabel!      //收款账號：
    @IBOutlet weak var lbName: UILabel!         //收款人姓名：
    @IBOutlet weak var lbNote: UILabel!         //附言：
    @IBOutlet weak var lbCash: UILabel!         //充值金額：
    @IBOutlet weak var lbAddress: UILabel!      //開戶上地址：
    
    var bankAccount: HYPayment!
    
    func update(){
        let bq = bankAccount.BQ!
        lbBankName.text = "\(String(describing: HYDatas.translateWord(origin: bq["BankEName"]! as? String)))"
        lbCash.text = "\(String(describing: bq["Amount"]!))"
        lbAccount.text = "\(String(describing: bq["Account"]!))"
        lbName.text = "\(String(describing: bq["Name"]!))"
        lbNote.text = "\(String(describing: bq["Memo"]!))"
//        lbAddress.text = "\(String(describing: ""))"
    }
    
    func genNoteText() -> String{
        let num = arc4random_uniform(10000)
        return "\(num)"
    }
    
    @IBAction func didBtnCopyClick(btn:UIButton){
        self.showGeneralAlert(.alert_ok, lbTextA:"复制完成"){ value in
            if value == 1 {
                UIPasteboard.general.string = self.infos[btn.tag].text; //複製文字功能
            }
        }
    }
    
    // MARK: - Collection

    @IBAction func didBtNextClick(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: false)
        //轉跳更新openUrl  https://www.ope88.com/Bank/List 線上版
        if let url = URL(string: "http://rd.w2.ssope.com/Bank/List"), UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil);
            } else {
                UIApplication.shared.openURL(url);
            }
        }else{
            self.showGeneralAlert(.alert_info,lbTextA:"无法开启网址"){ value in }
        }
        
    }
 
    
}

















