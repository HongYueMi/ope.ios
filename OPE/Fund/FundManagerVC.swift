//
//  FundManagerVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/18.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class FundManagerVC: UIViewController {

    @IBOutlet weak var upView: UIView!; //充值底
    @IBOutlet weak var downViewL: UIView!; //轉账底
    @IBOutlet weak var downViewR: UIView!; //提款底
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let shadowOffsetView = CGSize(width:2, height:2); //顯示方向
        let shadowOpacityView:Float = 0.5; //透明度
        let shadowRadiusView:CGFloat = 2; //陰影範圍
        let shadowColorView:CGColor = UIColor.darkGray.cgColor; //陰影的顏色
        let borderColorView:CGColor = UIColor.lightGray.cgColor; //邊框顏色
        let borderWidthView:CGFloat = 0.3; //邊框大小
        
        upView.layer.shadowOffset = shadowOffsetView
        upView.layer.shadowOpacity = shadowOpacityView
        upView.layer.shadowRadius = shadowRadiusView
        upView.layer.shadowColor = shadowColorView
        upView.layer.borderColor = borderColorView
        upView.layer.borderWidth = borderWidthView
        
        downViewL.layer.shadowOffset = shadowOffsetView
        downViewL.layer.shadowOpacity = shadowOpacityView
        downViewL.layer.shadowRadius = shadowRadiusView
        downViewL.layer.shadowColor = shadowColorView
        downViewL.layer.borderColor = borderColorView
        downViewL.layer.borderWidth = borderWidthView
        
        downViewR.layer.shadowOffset = shadowOffsetView
        downViewR.layer.shadowOpacity = shadowOpacityView
        downViewR.layer.shadowRadius = shadowRadiusView
        downViewR.layer.shadowColor = shadowColorView
        downViewR.layer.borderColor = borderColorView
        downViewR.layer.borderWidth = borderWidthView
        
//        upView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.upViewtouch(_:))))
//
//        downViewL.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.downViewLtouch(_:))))
//
//        downViewR.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.downViewRtouch(_:))))
        
    }

//    @objc func upViewtouch(_ sender: UITapGestureRecognizer){
//        let nav = UIStoryboard.Fund.identifier("DepositNavVC") as! NavigationController
//        self.present(nav, animated: true)
//    }
//
//    @objc func downViewLtouch(_ sender: UITapGestureRecognizer){
//        let nav = UIStoryboard.Fund.identifier("TransferNavVC") as! NavigationController
//        self.present(nav, animated: true)
//    }
//
//    @objc func downViewRtouch(_ sender: UITapGestureRecognizer){
//        let nav = UIStoryboard.Fund.identifier("WithdrawalNavVC") as! NavigationController
//        self.present(nav, animated: true)
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "Withdrawal" {
            if !HYAuth.auth.currentUser!.isBankPasswordSet{
                
                defer {
                    performSegue(withIdentifier: "SetBankPassword", sender: nil)
                }
            }
            else if HYWithdrawal.withdrawal.withdrawalBankAccounts.count <= 0{
                
                defer {
                    performSegue(withIdentifier: "SetBankCard", sender: nil)
                }
            }
        }
        return true
    }
    
    func genDismissBarButtonItem() -> UIBarButtonItem {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btn.setImage(#imageLiteral(resourceName: "Main_Nav_BackArror"), for: .normal)
        btn.addTarget(self, action: #selector(dismissWithdrawal), for: .touchUpInside)
        return UIBarButtonItem(customView: btn)
    }
    
    @objc func dismissWithdrawal(){
        dismiss(animated: true, completion: nil)
    }
}
