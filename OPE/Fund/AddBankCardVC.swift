//
//  AddBankCardVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/20.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class AddBankCardVC: UIEditVC {

    @IBOutlet weak var lbBankName: UILabel!         //开户行
    @IBOutlet weak var btBankName: UIButton!
    
    @IBOutlet weak var tfBankBranch: UITextField!   //请输入开户分行
    @IBOutlet weak var tfBankCard: UITextField!     //请输入您绑定的银行卡号
    @IBOutlet weak var tfUserName: UITextField!     //请输入您银行卡姓名（设置之后无法更改）
    
    @IBOutlet weak var lbProvince: UILabel!
    @IBOutlet weak var btProvince: UIButton!
    
    @IBOutlet weak var tfAddress: UITextField!      //请输入开户行具体地址
    
    @IBOutlet weak var nextBtn: UIButton!
    
    
    var itemList: WithdrawalBankAccountAddItem!; //前面就會先輸入好密碼跟資金密碼
    
    var routeLink:String = "";  //用來判斷是哪邊過來的路線
    
    var user: HYUser!
    
    var okBankData: [HYBankOption] = []; //選中的銀行
    var okProvinceData: [HYOption] = []; //選中的省份
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if HYAuth.auth.currentUser!.name != "" {
            tfUserName.text = HYAuth.auth.currentUser!.name;
            tfUserName.isEnabled = false;
        }else{
            tfUserName.isEnabled = true;
        }
        
        nextBtn.layer.borderColor = UIColor(red: 0/255, green: 203/255, blue: 192/255, alpha: 1).cgColor; //邊框顏色1
        nextBtn.layer.borderWidth = 1.0; //邊框大小
        nextBtn.layer.masksToBounds = true;
        nextBtn.layer.cornerRadius = 25.0;
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //銀行選擇
    @IBAction func didBtBankNameClick(_ sender: UIButton) {
        hideKeyboard()
        self.performSegue(withIdentifier: "sendSelectAllBankVC", sender: nil);
    }
    //開分行選擇
    @IBAction func didBtProvinceClick(_ sender: UIButton) {
        hideKeyboard()
        self.performSegue(withIdentifier: "sendSelectAllProvinceVC", sender: nil);
    }
    //下一步
    @IBAction func didNextBtnClick(_ sender: UIButton) {
        addBankAccount()
    }
    
    func addBankAccount(){
        if okBankData.count <= 0 || lbBankName.text == "选择开户行" {
            showGeneralAlert(.alert_info,lbTextA:"請选择开户行"){ value in }
            return
        }
        
        if okProvinceData.count <= 0 {
            showGeneralAlert(.alert_info,lbTextA:"請选择开户行省份"){ value in }
            return
        }
        
        guard let branch = tfBankBranch.text?.notEmptyValue else {
            showGeneralAlert(.alert_info,lbTextA:"开户分行不能為空！"){ value in }
            return
        }
        guard let account = tfBankCard.text?.notEmptyValue else {
            showGeneralAlert(.alert_info,lbTextA:"银行卡号不能為空"){ value in }
            return
        }
        guard
            HYRegex.BankAccount.match(input: account)
            else
        {
            showGeneralAlert(.alert_info,lbTextA:"银行卡号格式有误！"){ value in }
            return
        }

        guard let address = tfAddress.text?.notEmptyValue else {
            showGeneralAlert(.alert_info,lbTextA:"请输入开户行具体地址"){ value in }
            return
        }
        
        if(tfUserName.text == ""){
            showGeneralAlert(.alert_info,lbTextA:"请输入您银行卡姓名（设置之后无法更改）"){ value in }
            return
        }

        itemList.bankID = okBankData[0]
        itemList.bankBranch = branch
        itemList.account = account  //銀行卡號
        itemList.name = tfUserName.text
        itemList.bankProvince = okProvinceData[0] as! HYProvince //HYProvince
        itemList.bankAddress = address
        
        if tfUserName.isEnabled == true {
            newUserNameFunc();
        }else{
            addBankAccountFunc();
        }
        
    }
    
    func newUserNameFunc(){
        //新增用戶名
        HYWithdrawal.withdrawal.newNameAsync(tfUserName.text!){ error in
            if let error = error{
                self.showGeneralAlert(.alert_info, lbTextA:error.localizedDescription){ value in }
            }
            else{
                self.updateUserData(["Name": self.tfUserName.text!])    //刷新重抓用戶資料
//                HYAuth.auth.currentUser!.name = self.tfUserName.text!;
                self.addBankAccountFunc();
            }
        }
    }
    //新增信用卡
    func addBankAccountFunc(){
        HYWithdrawal.withdrawal.addWithdrawalBankAccount(withItem: itemList){
            error in
            if let error = error{
                self.showGeneralAlert(.alert_info, lbTextA:error.localizedDescription){ value in }
            }
            else{
                self.didBankAccountAdded()
            }
        }
    }
    
    func didBankAccountAdded(){
        if(routeLink == ""){
            if let vcs = navigationController?.viewControllers, vcs.count > 1 {
                for val in vcs {
                    if val.isKind(of: BankCardSetListVC.self) { //轉跳到銀行卡列表
                        self.navigationController?.popToViewController(val, animated: true);
                    }
                }
            }
//            navigationController?.popToRootViewController(animated: true);
        }else if(routeLink == "Withdrawal"){    //提款路線
            self.performSegue(withIdentifier: "sendCashWithdrawalVC", sender: nil); //槓，終於前往 提款
        }else if(routeLink == "BankCardSet"){   //銀行卡設置路線
            self.performSegue(withIdentifier: "sendBankCardSetListVC", sender: nil); //槓，終於前往 銀行卡設置
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendCashWithdrawalVC" {         //提款路線

        }else if segue.identifier == "sendBankCardSetListVC" {  //銀行卡設置路線
            if let vc = segue.destination as? BankCardSetListVC {
                vc.routeLink = "BankCardSet";
            }
        }else if segue.identifier == "sendSelectAllBankVC" {
            if let vc = segue.destination as? SelectAllBankVC {
                vc.okBank = { value in
                    self.okBankData = value;    //選中回傳的銀行
                    self.lbBankName.text = value[0].name;
                }
            }
        }else if segue.identifier == "sendSelectAllProvinceVC" {
            if let vc = segue.destination as? SelectAllProvinceVC {
                vc.okProvince = { value in
                    self.okProvinceData = value;    //選中回傳的銀行
                    self.lbProvince.text = value[0].name;
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfBankBranch{
            tfUserName.becomeFirstResponder()
        }
        if textField == tfUserName{
            tfUserName.resignFirstResponder()
        }
        return true
    }
    
    func updateUserData(_ info: [String: Any]){
        HYAuth.auth.currentUser!.update { error in
            
        }
    }
    
}
































