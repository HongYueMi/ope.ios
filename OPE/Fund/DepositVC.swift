//
//  DepositVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/14.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension UIStoryboard{
//    static let Fund : UIStoryboard = UIStoryboard(name: "Fund", bundle: nil)
}

class DepositVC: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    @IBInspectable var idxStart: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        setUpTabs()
        setUpPageViewController()
        idxActive = idxStart
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkDeposit()
    }
    
    //MARK: - CheckDeposit
    
    func checkDeposit(){
        self.view.isUserInteractionEnabled = false
        HYFund.fund.depositStatusCheck{
            [weak self] verify, error in
            
            if let err = error {
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
        if value == 1 {
            self?.view.isUserInteractionEnabled = true
        }
     }
            }else if let feedbackS = verify{
                if feedbackS.isVerifying{
                    self?.feedBack = feedbackS
                    
                    self?.showTwoSelectAlert(.alert_info,lbTextA:"您的存款申请已进入审核队列", lbTextB: "预计5分钟内客服会进行处理", btTextOK:"已完成存款", btTextNO:"存款未完成" ){ okValue, noValue in
                        if okValue == 1 , noValue == 0 {
                            self?.feedBack(true)
                        }else{
                            self?.feedBack(false)
                        }
                    }
                    
                }else{
                    self?.view.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    var feedBack: DepositVerify!
    
    @objc func feedBackYes(){
        feedBack(true)
    }
    
    @objc func feedBackNO(){
        feedBack(false)
    }
    
    func feedBack(_ answer: Bool){
        feedBack.feedBack = answer
        HYFund.fund.DepositFeedback(feedback: feedBack){
            [weak self] error in
            self?.view.isUserInteractionEnabled = true
            self?.dismiss(animated: true){
                if let error = error{
                    let outMsg = error.localizedDescription;
                    self?.showGeneralAlert(.alert_error,lbTextA:outMsg){ value in }
                }else{
                    self?.showGeneralAlert(.alert_info,lbTextA:"本次存款申请状态已经提交客服审核，您可以再次存款！"){ value in }
                }
            }
        }
    }
    
    //MARK: - Navigation
    
    func setUpNavigationBar(){
        navigationItem.leftBarButtonItem = (navigationController as! NavigationController).genDismissBarButtonItem()
    }
    
    //MARK: - Change Page
    
    var idxActive: Int = 0
    
    /**
     選中Tab
     */
    @IBAction func selectTab(_ tab: UIControl){
        let idx : Int = tabs.index{$0 == tab}!
        guard idx != idxActive else {return}
        tabChangeToIndex(idx)
        pageChangeToIndex(idx, animated: true)
        idxActive = idx
    }
    
    /**
     手勢滑動切換到指定VC
     */
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if finished && completed{
            let idx = vcs.index{$0 == pageViewController.viewControllers!.last}!
            tabChangeToIndex(idx)
            idxActive = idx
        }
    }
    
    
    //MARK: - Tab Bar
    
    @IBOutlet weak var btnWebBank: TabIcon!
    @IBOutlet weak var btnManual: TabIcon!
    @IBOutlet weak var btnAlyPay: TabIcon!
    @IBOutlet weak var btnWeChatPay: TabIcon!
    
    var tabs : [TabIcon] = []
      var indecators: [UIImageView] = []
    
    func setUpTabs(){
        tabs = [
            btnWebBank
            , btnManual
            , btnAlyPay
            , btnWeChatPay
        ]
        
        btnWebBank.title = LanDeposit.onlinePayment
        btnManual.title = LanDeposit.manual
        btnAlyPay.title = LanDeposit.aliPayment
        btnWeChatPay.title = LanDeposit.weChatPayment
        
        for btn in tabs{
            btn.addTarget(self, action: #selector(selectTab), for: .touchUpInside)
            btn.isSelected = false
        }
        tabs[idxStart].isSelected = true
    }
    
    /**
     切換到指定Tab
     */
    func tabChangeToIndex(_ idx : Int){
        tabs[idxActive].isSelected = false
        tabs[idx].isSelected = true
    }
    
    //MARK: - PageViewController
    
    weak var pageVC : UIPageViewController!
    var vcs : [UIViewController] = []
    
    func setUpPageViewController(){
        pageVC = self.childViewControllers.last! as! UIPageViewController
        pageVC.dataSource = self
        pageVC.delegate = self
        
        //在線充值
//        let webBankVC = UIStoryboard.Fund.identifier("DepositWebBankVC") as! OnlineDepositVC
//        webBankVC.depositGroup = DepositGroup(name: LanDeposit.onlinePayment, id: 1, code: "OnlinePayment")
//        webBankVC.depositVC = self
//
//        //轉账
//        let manualVC = UIStoryboard.Fund.identifier("DepositTransferPayVC") as! DepositTransferPayVC //新DepositTransferPayVC 舊DepositManualPayVC
//        manualVC.depositGroup = DepositGroup(name: LanDeposit.manual, id: 6, code: "BQ")
//        manualVC.depositVC = self
//
//        //支付寶
//        let alyPayVC = UIStoryboard.Fund.identifier("OnlineDepositVC") as! OnlineDepositVC
//        alyPayVC.depositGroup = DepositGroup(name: LanDeposit.aliPayment, id: 3, code: "AliPayment")
//        alyPayVC.depositVC = self
//
//        //微信支付
//        let weChatVC = UIStoryboard.Fund.identifier("OnlineDepositVC") as! OnlineDepositVC
//        weChatVC.depositGroup = DepositGroup(name: LanDeposit.weChatPayment, id: 7, code: "WeChatPayment")
//        weChatVC.depositVC = self
//
//        vcs = [
//            webBankVC
//            , manualVC
//            , alyPayVC
//            , weChatVC
//        ]
        
        pageVC.setViewControllers([vcs[idxStart]], direction: .forward, animated: false)
    }
    
    /**
     切換到指定VC
     */
    func pageChangeToIndex(_ idx: Int, animated:Bool){
        guard idx < vcs.count, idx >= 0 else {return}
        let dir : UIPageViewControllerNavigationDirection
            = idx > idxActive ? .forward : .reverse
        pageVC.setViewControllers([vcs[idx]], direction: dir, animated: animated)
    }
    
    /**
     右一頁
     */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let idx = vcs.index{$0 == viewController}! + 1
        if idx < vcs.count{
            return vcs[idx]
        }
        return nil //沒有右一頁
    }
    
    /**
     左一頁
     */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let idx = vcs.index{$0 == viewController}! - 1
        if idx >= 0{
            return vcs[idx]
        }
        return nil //沒有左一頁
    }

}
