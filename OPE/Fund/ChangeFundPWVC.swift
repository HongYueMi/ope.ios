//
//  ChangeFundPWVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/6.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//  资金密码修改

import UIKit

class ChangeFundPWVC: PasswordEditVC {

    @IBOutlet weak var btNextBar: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didBtNextBarClick(_ sender: UIBarButtonItem) {
        editPassword();
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            return true;
        }
        
        if textField == tfPassword1 , let txt = tfPassword1.text , txt.count >= 12 {
            return false
        }
        
        if textField == tfPassword2 , let txt = tfPassword2.text , txt.count >= 12 {
            return false
        }
        
        if textField == tfCheckPassword , let txt = tfCheckPassword!.text , txt.count >= 12 {
            return false
        }
        
        return true;
    }
    
}
