//
//  WalletBalanceVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/30.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class WalletCell: UITableViewCell{
    
    @IBOutlet weak var lineBottom: UIView?
    @IBOutlet weak var imagvIcon: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbBalance: UILabel? //顯示的金額
    
    var wallet: HYWallet?{
        didSet{
            lbBalance?.text = wallet?.credit == nil ? "维护中" : wallet!.credit!
//            lbBalance.text = wallet?.credit == nil ? "维护中" : nil == Double(wallet!.credit!) ? "\(wallet!.credit!)元" : wallet!.credit!
            lbTitle.text = wallet?.name /*+ "\(LanWalletBalance.lbTitle_Sorting)"*/
            imagvIcon.image = wallet?.image
        }
    }
    
}

class WalletListVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateBalance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableData = HYAuth.auth.currentUser?.availablePlatforms ?? []
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateBalance), name: HYFund.NotificationName, object: HYFund.fund)
        HYTransfer.transfer.updateWallets{
            [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: HYFund.NotificationName, object: HYFund.fund)
    }

    // MARK: - Selection

    var selectedPlatform: String?
    var idxSelected: Int?
    
    // MARK: - Wallet
    
    @objc func updateBalance(){
        guard let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? WalletCell else {return}
        if let amount = HYFund.fund.strBalance{
            cell.lbBalance?.text = "\(amount)元"
        }
        else{
            cell.lbBalance?.text = HYFund.fund.strBalance
        }
    }
    
    // MARK: - TableView

    var tableData: [String] = []{
        didSet{
            tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletCell", for: indexPath) as! WalletCell
        if indexPath.section == 0 {
            cell.wallet = HYTransfer.transfer.mainWallet
            cell.lineBottom?.isHidden = true
        }else{
            cell.wallet = HYTransfer.transfer.platformWallets[tableData[indexPath.row]]
            cell.lineBottom?.isHidden = indexPath.row == tableData.count - 1
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 16 : 0.01
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let v = UIView(frame: .zero)
        v.backgroundColor = .clear
        return v
    }

}

class WalletOptionCell: WalletCell{
    
    @IBOutlet weak var viewContent: UIView!
    
    override func awakeFromNib() {
        selectionStyle = .none
    }
    
    var isChoosen: Bool = false {
        didSet{
            self.lbBalance?.textColor = isChoosen ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0, green: 0.7960784314, blue: 0.7529411765, alpha: 1)
            self.viewContent.backgroundColor = isChoosen ? #colorLiteral(red: 0, green: 0.7960784314, blue: 0.7529411765, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
}

class WalletOptionVC: WalletListVC{
    
    var wallet: HYWallet?
    
    var didSelectWallet: ((HYWallet) -> Void)!
    
    // MARK: - Table
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletCell") as! WalletOptionCell
        if indexPath.section == 0 {
            cell.wallet = HYTransfer.transfer.mainWallet
        }else{
            cell.wallet = HYTransfer.transfer.platformWallets[tableData[indexPath.row]]
        }
        cell.isChoosen = wallet?.platform == nil ? false : cell.wallet!.platform == wallet!.platform
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            self.wallet = HYTransfer.transfer.mainWallet
        }else{
            self.wallet = HYTransfer.transfer.platformWallets[tableData[indexPath.row]]
        }
        self.didSelectWallet(self.wallet!)
    }
    
}

class WalletListOrderAdjustVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isEditing = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableData = HYAuth.auth.currentUser?.availablePlatforms ?? []
    }
    
    @IBAction func didBtnDoneClick(item: UIBarButtonItem){
        presentingViewController?.dismiss(animated: true)
    }
    
    // MARK: - TableView
    
    var tableData: [String] = []{
        didSet{
            tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletCell", for: indexPath) as! WalletCell
        cell.wallet = HYTransfer.transfer.platformWallets[tableData[indexPath.row]]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        guard let user = HYAuth.auth.currentUser else {return}
        var ary = user.availablePlatforms
        let item = ary.remove(at: sourceIndexPath.row)
        ary.insert(item, at: destinationIndexPath.row)
        HYAuth.auth.currentUser?.availablePlatforms = ary
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
}
