//
//  TransferVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/20.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class TransferVC: UIEditVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateBalance()
        
        walletFrom = HYTransfer.transfer.mainWallet
        if let user = HYAuth.auth.currentUser, user.availablePlatforms.count > 0{
            walletTo = HYTransfer.transfer.platformWallets[user.availablePlatforms[0]]
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateBalance), name: HYFund.NotificationName, object: HYFund.fund)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
        NotificationCenter.default.removeObserver(self, name: HYFund.NotificationName, object: HYFund.fund)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any? ) {
        
        if let vc = segue.destination as? WalletOptionVC, let wallet = sender as? HYWallet{            
            vc.wallet = wallet
            vc.didSelectWallet = { [weak self] new in
                if self?.butCheck == true {
                    self?.walletFrom = new
                }else{
                    self?.walletTo = new
                }
//                if self?.walletFrom?.platform == wallet.platform{
//                    self?.walletFrom = new
//                }else if self?.walletTo?.platform == wallet.platform{
//                    self?.walletTo = new
//                }
                self?.navigationController?.popToViewController(self!, animated: true)
            }
        }
    }
    
    // MARK: - Wallet
    
    @objc func updateBalance(){
        let balance = HYFund.fund.strBalance
        if walletFrom?.platform == HYTransfer.codeMainWallet{
            lbWalletFromAmount?.text = balance
        }
        if walletTo?.platform == HYTransfer.codeMainWallet{
            lbWalletToAmount.text = balance
        }
    }
    
    // MARK: - Wallet

    @IBOutlet weak var ctrlWalletFrom: UIControl?
    @IBOutlet weak var lbWalletFromName: UILabel!
    @IBOutlet weak var lbWalletFromAmount: UILabel!
    @IBOutlet weak var imgvWalletFromIcon: UIImageView!
    @IBOutlet weak var lbWalletFromAvailableAmount: UILabel!
    
    @IBOutlet weak var ctrlWalletTo: UIControl?
    @IBOutlet weak var lbWalletToName: UILabel!
    @IBOutlet weak var lbWalletToAmount: UILabel!
    @IBOutlet weak var imgvWalletToIcon: UIImageView!

    @IBOutlet weak var btnChangeSide: UIButton?
    @IBOutlet weak var lbIntOnly: UILabel!
    
    var butCheck = true;    //判斷是按下上面還是下面按鈕
    
    var walletFrom: HYWallet?{
        didSet{
            lbWalletFromName.text = walletFrom?.name
            lbWalletFromAmount?.text = walletFrom?.credit
            imgvWalletFromIcon.image = walletFrom?.image
            
            // 可轉金額
            // 中心錢包沒有可轉金額
            if walletFrom == HYTransfer.transfer.mainWallet{
                lbAvailableAmount?.text = walletFrom?.credit
                lbWalletFromAvailableAmount.text = walletFrom?.credit
            }
            // 檢查可轉金額
            else {
                lbAvailableAmount?.text = nil
                lbWalletFromAvailableAmount.text = nil
                
                isLoading = true
                walletFrom?.checkAvailable(){ [weak self] wallet, error in
                    if let e = error {
                        self?.showGeneralAlert(lbTextA: e.localizedDescription){ value in }
                    }else{
                        var available =  wallet?.availableAmount ?? 0.00
                        if let isAudited = wallet?.isAudited, !isAudited{
                            available = wallet?.amount ?? 0.00
                        }
                        let strAbailable = String(format:"%.2f",available)
                        self?.lbAvailableAmount?.text = strAbailable
                        self?.lbWalletFromAvailableAmount.text = strAbailable
                        if let str = self?.tfAmount.text, let amount = Double(str), amount > available{
                            self?.tfAmount.text = strAbailable
                        }
                        // 是否支援小數
                        self?.checkRegex()
                        self?.isLoading = false
                    }
                }
                
            }
            
            
            
        }
    }
    
    var walletTo: HYWallet?{
        didSet{
            lbWalletToName.text = walletTo?.name
            lbWalletToAmount.text = walletTo?.credit
            imgvWalletToIcon.image = walletTo?.image
            
            // 是否支援小數
            checkRegex()
        }
    }
    
    func checkRegex(){
        for wallet in [walletFrom, walletTo]{
            if let platform = wallet?.platform , platform == HYPlayStation.Platform.BBIN{
                lbIntOnly.text = "BBIN仅支援整数转账"
                tfAmount.keyboardType = .numberPad
                regex = HYRegex.AmountInt
                if let str = tfAmount.text, let amount = Double(str){ //為數字 非「維護中」字樣
                    tfAmount.text = "\(Int(amount))"
                }
                return
            }
        }
        lbIntOnly.text = nil
        regex = HYRegex.Amount
        tfAmount.keyboardType = .decimalPad
    }
    
    @IBAction func didBtnChangeSideClick(ctrl: UIControl){
        let wallet = walletFrom
        walletFrom = walletTo
        walletTo = wallet
    }
    
    @IBAction func didCtrlWalletClick(ctrl: UIControl){
        var sender: HYWallet?
        if ctrl == ctrlWalletFrom{
            butCheck = true;
            sender = walletFrom
        }else if ctrl == ctrlWalletTo{
            butCheck = false;
            sender = walletTo
        }
        
        performSegue(withIdentifier: "SelectWallet", sender: sender)
    }
    
    // MARK: - Amount
    
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var ctrlAll: UIControl!
    @IBOutlet weak var lbAvailableAmount: UILabel?
    
    @IBAction func didCtrlMaxAmountClick(ctrl: UIControl){
        var available = walletFrom?.availableAmount ?? 0.00
        if walletFrom == HYTransfer.transfer.mainWallet{
            available = walletFrom?.amount ?? 0.00
        }
        if regex.regex!.pattern == HYRegex.AmountInt.regex!.pattern{ // 僅支持整數
            tfAmount.text = "\(Int(available))"
        }else{
            tfAmount.text = String(format:"%.2f",available)
        }
    }
    
    var regex = HYRegex.Amount
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfAmount{
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            return regex.match(input: text) || text == ""
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfAmount{
            didBtnEnterClick(btn: nil)
        }
        return true
    }
    
    // MARK: - Transfer
    
    @IBAction func didBtnEnterClick(btn: UIBarButtonItem?){
        transfer()
    }
    
    var isLoading: Bool = false{
        didSet{
            self.view.isUserInteractionEnabled = !isLoading
            self.navigationItem.rightBarButtonItem?.isEnabled = !isLoading
        }
    }
    
    func transfer(){
        hideKeyboard()
        guard let walletFrom = self.walletFrom else {
            self.showGeneralAlert(.alert_info, lbTextA:"请选择转出平台！"){ value in }
            return
        }
        guard let walletTo = self.walletTo else {
            self.showGeneralAlert(.alert_info, lbTextA:"请选择转入平台！"){ value in }
            return
        }
        guard walletFrom.name != walletTo.name  else {
            self.showGeneralAlert(.alert_info, lbTextA:"不能对同一个账户进行操作！"){ value in }
            return
        }
        guard let strAmtFrom = walletFrom.credit, let amountAvailable = walletFrom == HYTransfer.transfer.mainWallet ? Double(strAmtFrom) : walletFrom.availableAmount else {
            self.showGeneralAlert(.alert_info, lbTextA:"无法取得转出平台余额！"){ value in }
            return
        }
        guard let strAmtTo = walletTo.credit,  let _ = Double(strAmtTo) else {
            self.showGeneralAlert(.alert_info, lbTextA:"无法取得转入平台余额！"){ value in }
            return
        }
        guard let strAmountTransfer = tfAmount.text?.notEmptyValue else {
            self.showGeneralAlert(.alert_info, lbTextA:"金额不能为空！"){ value in }
            return
        }
        guard let amountTransfer = Double(strAmountTransfer) else{
            self.showGeneralAlert(.alert_info, lbTextA:"金额格式有误！"){ value in }
            return
        }
        guard amountTransfer > 0.0 else {
            self.showGeneralAlert(.alert_info, lbTextA:"金额不能小于0！"){ value in }
            return
        }
        guard amountTransfer <= amountAvailable else {
            self.showGeneralAlert(.alert_info, lbTextA:"转账金额不能大于可转余额！"){ value in }
            return
        }

        self.isLoading = true
        HYTransfer.transfer.transfer(from: walletFrom, to: walletTo, amount: amountTransfer){[weak self] error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
                    self?.isLoading = false
                }
            }
            else{
                self?.tfAmount.text = nil
                self?.walletFrom = walletFrom
                self?.walletTo = walletTo
                self?.isLoading = false
                self?.didTransferComplete()
            }
        }
    }
    
    func didTransferComplete(){
        
    }
}
