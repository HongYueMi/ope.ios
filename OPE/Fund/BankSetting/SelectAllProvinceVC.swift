//
//  SelectAllProvinceVC.swift
//  OPE
//
//  Created by rwt113 on 2017/12/8.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

class SelectAllProvinceCell: UITableViewCell{
    
    @IBOutlet weak var lbBankName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

import UIKit

class SelectAllProvinceVC: UIEditVC, UITableViewDataSource, UITableViewDelegate {

    var provinceChooseItem:[HYOption] = HYDatas.provinces;
    
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "请选择开户行省份";
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var okProvince: (([HYOption]) -> Void)!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return provinceChooseItem.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectAllProvinceCell") as! SelectAllProvinceCell;
        
        cell.lbBankName.text = provinceChooseItem[indexPath.row].name;
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.okProvince([provinceChooseItem[indexPath.row]]);
        _ = navigationController?.popViewController(animated: true);
    }

}





































