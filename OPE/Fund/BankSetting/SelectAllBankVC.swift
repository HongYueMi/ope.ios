//
//  SelectAllBankVC.swift
//  OPE
//
//  Created by rwt113 on 2017/12/7.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

class SelectAllBankCell: UITableViewCell{
    
    @IBOutlet weak var lbBankName: UILabel!
    @IBOutlet weak var imgBankCard: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

import UIKit

class SelectAllBankVC: UIEditVC, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var myTableView: UITableView!
    
    let allBank = HYWithdrawal.withdrawal.banks; //全部銀行
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "选择开户行";
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var okBank: (([HYBankOption]) -> Void)!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return allBank.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectAllBankCell") as! SelectAllBankCell;
        
        cell.lbBankName.text = allBank[indexPath.row].name;

        if (UIImage(named:allBank[indexPath.row].Ename) != nil) {
            cell.imgBankCard.image = UIImage(named:allBank[indexPath.row].Ename)
        }else{
            cell.imgBankCard.image = UIImage(named:"OtherBank")
        }
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80;
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.okBank([allBank[indexPath.row]]);
        _ = navigationController?.popViewController(animated: true);
    }
    
}

























