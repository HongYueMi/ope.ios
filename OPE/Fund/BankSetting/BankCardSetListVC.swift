//
//  BankCardSetListVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/17.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

class BankCardSetListCell: UITableViewCell{
    
    
    @IBOutlet weak var lbUserName: UILabel!
    @IBOutlet weak var lbBankName: UILabel!
    @IBOutlet weak var lbAccount: UILabel!          //銀行卡號
    @IBOutlet weak var imgvBankCard: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

import UIKit

class BankCardSetListVC: UIEditVC, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var msgView: UIView! //沒銀行卡 就顯示
    
    @IBOutlet weak var myTableView: UITableView!
    
    var routeLink:String = "";
    
    var tableData: [WithdrawalBankAccount]{
        return HYWithdrawal.withdrawal.withdrawalBankAccounts
    }
    let allBank = HYWithdrawal.withdrawal.banks; //全部銀行
    
    override func viewDidLoad() {
        super.viewDidLoad()

        myTableView.delegate = self;
        myTableView.dataSource = self;
        myTableView.separatorStyle = .none;
//        myTableView.rowHeight = UITableViewAutomaticDimension;
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if tableData.count == 0 {
            msgView.isHidden = false;
            self.view.layoutIfNeeded();
        }else{
            msgView.isHidden = true;
            self.view.layoutIfNeeded();
        }
        
        myTableView.reloadData();
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //返回直接回到 用戶中心
        if let vcs = navigationController?.viewControllers, vcs.count > 1 {
            navigationController?.viewControllers.removeSubrange(1..<vcs.count-1)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return min(tableData.count, 5);
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BankCardSetListCell") as! BankCardSetListCell;
        
        cell.lbBankName.text = translateWord(origin: tableData[indexPath.row].bankName);
        cell.lbAccount.text = tableData[indexPath.row].account;
        cell.lbUserName.text = tableData[indexPath.row].name;
        
        if (UIImage(named:tableData[indexPath.row].bankName) != nil) {
            cell.imgvBankCard.image = UIImage(named:tableData[indexPath.row].bankName)
        }else{
            cell.imgvBankCard.image = UIImage(named:"OtherBank")
        }
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100;
    }
    
    //對照中文銀行
    func translateWord(origin: String) -> String {
        for k in allBank {
            if k.Ename == origin {
                return k.name
            }
        }
        return origin
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
