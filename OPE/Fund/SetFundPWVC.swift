//
//  SetFundPWVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/6.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//  资金密码设置

import UIKit

class SetFundPWVC: PasswordEditVC {

    @IBOutlet weak var btNextBar: UIBarButtonItem!
    
    @IBOutlet weak var btBackBar: UIBarButtonItem!
    
    var routeLink:String = "";  //用來判斷是哪邊過來的路線
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didBtNextBarClick(_ sender: UIBarButtonItem) {
        guard checkInput() else {
            return
        }
        addPassword()
    }
    
    //提款會用到
    @IBAction func didBtBackBarClick(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion:nil)
    }
    
    func addPassword() {
        view.isUserInteractionEnabled = false
        var item = PasswordEditItem()
        item.old = tfPassword1.text!
        item.new = tfPassword2.text!
        
        HYFund.fund.addPassword(withItem: item){ [weak self] error in
            if let error = error{
                self?.showGeneralAlert(.alert_error, lbTextA:error.localizedDescription){ value in
                    self?.view.isUserInteractionEnabled = true
                }
            }
            else{
                defer {
                    self?.routeLinkFunc();
                }
            }
        }
    }
    
    override func checkInput() -> Bool{
        guard let password1 = tfPassword1.text?.notEmptyValue else {
            showGeneralAlert(.alert_info, lbTextA:"账号密码 不能为空！（6-12位必须含有字母和数字的组合）"){ value in }
            return false
        }
        guard
            HYRegex.Password.match(input: password1)
            else
        {
            self.showGeneralAlert(.alert_info, lbTextA:"账号密码 格式有误！（6-12位必须含有字母和数字的组合）"){ value in }
            return false
        }
        guard let password2 = tfPassword2.text?.notEmptyValue else {
            self.showGeneralAlert(.alert_info, lbTextA:"资金密码 不能为空！（6-12位必须含有字母和数字的组合）"){ value in }
            return false
        }
        guard
            HYRegex.Password.match(input: password2)
            else
        {
            self.showGeneralAlert(.alert_info, lbTextA:"资金密码 格式有误！（6-12位必须含有字母和数字的组合）"){ value in }
            return false
        }
        
        guard let passwordCheck = tfCheckPassword?.text?.notEmptyValue else {
            showGeneralAlert(.alert_info, lbTextA:"\(lbCheckPassword!.text!) 不能为空！（6-12位必须含有字母和数字的组合）"){ value in }
            return false
        }
        
        guard
            passwordCheck == password2
            else
        {
            showGeneralAlert(.alert_info, lbTextA:"两次输入密码不一致"){ value in }
            return false
        }
        
        return true
    }
    
    //路線Func
    func routeLinkFunc(){
        if(routeLink == ""){
            showGeneralAlert(.alert_ok,lbTextA:"恭喜您",lbTextB: "资金密码设置成功"){ value in
                _ = self.navigationController?.popViewController(animated: true);
            }
        }else if(routeLink == "Withdrawal"){    //提款路線
            self.performSegue(withIdentifier: "sendAddBankCardVC", sender: nil) //新增銀行卡
        }else if(routeLink == "BankCardSet"){   //銀行卡設置路線
            self.performSegue(withIdentifier: "sendAddBankCardVC", sender: nil) //新增銀行卡
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendAddBankCardVC" {   //提款路線,前往新增銀行卡
            var itemList: WithdrawalBankAccountAddItem = WithdrawalBankAccountAddItem();
            itemList.password = tfPassword1.text!
            itemList.bankPassword = tfPassword2.text!
            
            let vc = segue.destination as! AddBankCardVC;
            if(routeLink == "Withdrawal"){
                vc.routeLink = "Withdrawal";
            }else if(routeLink == "BankCardSet"){
                vc.routeLink = "BankCardSet";
            }
            vc.itemList = itemList;
        }
        
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            return true;
        }
        
        if textField == tfPassword1 , let txt = tfPassword1.text , txt.count >= 12 {
            return false
        }
        
        if textField == tfPassword2 , let txt = tfPassword2.text , txt.count >= 12 {
            return false
        }
        
        if textField == tfCheckPassword , let txt = tfCheckPassword!.text , txt.count >= 12 {
            return false
        }
        
        return true;
    }
    
}



































