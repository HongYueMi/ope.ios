//
//  MarqueeVC.swift
//  SportsGames
//
//  Created by Lavend K. Mi on 2017/3/1.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class MarqueeVC: UIViewController, MarqueeViewDataSource {

    @IBInspectable var textColor: UIColor = UIColor.black
    @IBOutlet weak var marqueeView : MarqueeView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        marqueeView.dataSource = self
        marqueeView.play()
    }
    
    override func  viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        marqueeView.play()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        marqueeView.stop()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MarqueeDialogVC{
            vc.idxStart = index - 1 
        }
    }
    
    var messages : [HYBroadcast.Item] {
        return HYMarquee.shared.messages
    }
    
    var index : Int = 0
    
    func next() -> UIView?{
        
        guard messages.count > 0 else {
            return nil
        }
        
        let lb = UILabel(frame: .zero)
        if index >= messages.count{
            index = 0
        }
        lb.text = messages[index].title
        lb.textColor = textColor
        
        index += 1
        return lb
    }

}
