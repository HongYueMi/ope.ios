//
//  MessageVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/2.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension UIStoryboard{
    static let Message : UIStoryboard = UIStoryboard(name: "Message", bundle: nil)
}

class MessageVC: UIViewController{
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var tvInfo: UITextView!
    
    var message: HYMessage.Item!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbTitle.text = message.title
        lbTime.text = message.date?.toString(format: "yyyy-MM-dd HH:mm:ss")
        tvInfo.text = message.info
    }
    
}

class MessageCell: UITableViewCell{
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    
    override func awakeFromNib() {
        self.selectionStyle = .none
    }
    
    var message: HYMessage.Item!{
        didSet{
            lbTitle.text = message.title
            lbDate.text = message.date?.toString(format: "yyyy-MM-dd hh:mm:ss")
            viewContent.backgroundColor = message.isRead ? .white : #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
        }
    }
}

class MessageListVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var isRead: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: HYMessage.NotificationName, object: HYMessage.shared)
        HYMessage.shared.updateData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: HYMessage.NotificationName, object: HYMessage.shared)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func updateData(){
        tableData = HYMessage.shared.messages
    }
    
    //MARK: - Detail
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let cell = sender as! MessageCell
        let vc = segue.destination as! MessageVC
        vc.message = cell.message
        HYMessage.shared.readMessage(sid: cell.message.id)
        cell.viewContent.backgroundColor = .white
    }
    
    //MARK: - Table
    
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var tableData: [HYMessage.Item] = []{
        didSet{
            tableView.reloadData()
            viewNoData.isHidden = tableData.count > 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageCell
        cell.message = tableData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let action = UITableViewRowAction(style: .default, title: "删除"){[weak self] action, index in
            self?.tableView.beginUpdates()
            self?.tableView.deleteRows(at: [index], with: .fade)
            if let message = self?.tableData[index.row]{
                HYMessage.shared.deleteMessage(sid: message.id)
            }
            self?.tableData.remove(at: index.row)
            self?.tableView.endUpdates()
        }
        action.backgroundColor = #colorLiteral(red: 0, green: 0.7960784314, blue: 0.7529411765, alpha: 1)
        return [action]
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
}
