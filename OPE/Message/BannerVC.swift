//
//  BannerVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/12/21.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension UIStoryboard{
    static let Banner : UIStoryboard = UIStoryboard(name: "Banner", bundle: nil)
}

class BannerCell: UICollectionViewCell{
    
    @IBOutlet weak var ctrl: UIControl!
    @IBOutlet weak var imgvBanner: UIImageView!
    
    var item: HYMarket.Item!{
        didSet{
            if let path = item.imagePath, let url = URL(string: path){
                imgvBanner.sd_setImage(with: url)
            }else{
                imgvBanner.image = nil
            }
        }
    }
}

class BannerVC: PlayStationVC, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBInspectable var stopDuration: TimeInterval = 7.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.decelerationRate = UIScrollViewDecelerationRateFast
        NotificationCenter.default.addObserver(self, selector: #selector(updateViewData), name: HYMarket.NotificationName, object: HYMarket.center)
        updateViewData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let W = collectionView.frame.size.width
        space = CGFloat(Int(W * 7 / 180))
        let w = CGFloat(Int(W - 4 * space))
        cellSize = CGSize(width: w, height: collectionView.frame.size.height)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 2 * space, bottom: 0, right: 2 * space)
        collectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action
    
    func open(item: HYMarket.Item){
        view.isUserInteractionEnabled = false
        if let url = item.url, UIApplication.shared.canOpenURL(url){
            UIApplication.shared.openURL(url)
        }else if let title = item.promotionTitle{
            for category in HYPromotion.center.promotions.keys{
                for promotion in HYPromotion.center.promotions[category]!{
                    if promotion.title == title{
                        self.performSegue(withIdentifier: "showPromotion", sender: (category, promotion))
                        break
                    }
                }
            }
        }
    }
    
    // MARK: - Data
    
    @objc func updateViewData(){
        collectionData = HYMarket.center.ads
    }
    
    // MARK: - CollectionView
    
    var collectionData: [HYMarket.Item] = []{
        didSet{
            collectionView.reloadData()
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var cellSize: CGSize = .zero
    private var space: CGFloat = 0
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCell", for: indexPath) as! BannerCell
        cell.item = collectionData[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return space
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        open(item: collectionData[indexPath.row])
    }
    
    // MARK: - 縮放

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        for cell in collectionView.visibleCells{
            let w = cell.frame.size.width
            let leftSide = 2 * space
            let rightSide = leftSide + w
            let leftSpace = cell.frame.origin.x - scrollView.contentOffset.x
            let rightSpace = leftSpace + w
            var delta: CGFloat = 1 // 1 => 變化完 0 => 不變化
            if leftSpace <= leftSide, rightSpace >= leftSide{  // 偏左變化
                delta = (leftSide - leftSpace) / w
            }else if leftSpace <= rightSide, rightSpace >= rightSide{ // 偏右變化
                delta = 1 - (rightSide - leftSpace) / w
            }

            // 變化：水平縮小 2 * space距離，高度等比例縮小
            var frame = cell.contentView.frame
            frame.size.width = cell.bounds.size.width - 2 * space * delta
            frame.size.height = frame.size.width * cell.bounds.size.height / cell.bounds.size.width
            frame.origin.x = space * delta
            frame.origin.y = (cell.bounds.size.height - frame.size.height) / 2
            cell.contentView.frame = frame

        }
    }

    // MARK: - 自動定位

    var begin: (CGFloat, CGFloat) = (0, 0)
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        for cell in collectionView.visibleCells{
            
            let center = collectionView.bounds.size.width / 2 + scrollView.contentOffset.x
            let left = cell.frame.origin.x
            let right = left + cell.bounds.size.width
            
            if
                (left..<right).contains(center)
            {
                begin = (left, cell.bounds.size.width)
                break
            }
        }
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

        var target = CGPoint(x: targetContentOffset.pointee.x, y: targetContentOffset.pointee.y)
        let newCenter = collectionView.bounds.size.width / 2 + target.x

        let (left, w) = begin
        let right = left + w
        let newOffsetX = left - scrollView.contentInset.left

        if (left..<right).contains(newCenter){
            
            let center = collectionView.bounds.size.width / 2 + scrollView.contentOffset.x
            for cell in collectionView.visibleCells{
                if (cell.frame.origin.x..<cell.frame.origin.x + cell.bounds.size.width).contains(center)
                    , let index = collectionView.indexPath(for: cell)
                {
                    // 為放進來高機率不會執行
                    DispatchQueue.main.async { [weak self] in
                        self?.collectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
                    }
                    return
                }
            }
        }
        else{
            if newCenter < left{
                target.x = newOffsetX - w
            }else{
                target.x = newOffsetX + w
            }
            targetContentOffset.pointee = target
        }
    }
}
