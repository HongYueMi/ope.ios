//
//  AdBannerVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/1.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import SDWebImage
import SwiftyJSON

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

class AdBannerContentVC: UIViewController{
    
    var item: HYMarket.Item!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var TimeCountdownView: UIView!
    @IBOutlet weak var centerY: NSLayoutConstraint!
    
    @IBOutlet weak var lbDay: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var lbMinute: UILabel!
    @IBOutlet weak var lbSecond: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.isUserInteractionEnabled = true
        updateView()
    }
    
    func updateView(){
        
        if let path = item.imagePath, let url = URL(string: path){
            imageView.sd_setImage(with: url)
        }
        
        if
            let url = item.url
            , url.scheme == "ope"
        {
            //  ope://{&quot;link&quot;:&quot;&quot;, &quot;method&quot;: &quot;reciprocal&quot;, &quot;data&quot;:{&quot;endDate&quot;: &quot;2017/08/12 22:00:00&quot;}}
            
            //把HTML格式去掉
            var jsonString = url.absoluteString.replacingOccurrences(of: "&quot;", with: "\"")
            
            //把前面六位數的字刪掉
            jsonString = (jsonString as NSString).substring(from: 6)
            
            // "{\"link\":\"http://www.google.com\", \"method\": \"reciprocal\", \"data\":{\"endDate\": \"2017/08/12 22:00:00\"}}";
            
            // 轉換為JSON
            if let data = jsonString.data(using: String.Encoding.utf8), let json = try? JSON(data: data){
                
                if let timeCountdownValue = json["data"]["endDate"].stringValue.notEmptyValue
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss" //2017/08/12 22:00:00
                    var secFromGMT: Int { return TimeZone.current.secondsFromGMT() } //取得手機時區 並轉換成秒數 0800 -> 28800
                    var dates = dateFormatter.date(from: timeCountdownValue)
                    dates = dates?.addingTimeInterval(TimeInterval(secFromGMT)); //將轉換出來的時間加上 手機時區 0800
                    let timeInterval:TimeInterval = dates!.timeIntervalSince1970
                    
                    timestamp = timeInterval;
                    TimeCountdownView.isHidden = false
                    CountdownTimeFunc();
                    return
                }
            }
        }
        TimeCountdownView.isHidden = true
    }
    
    @IBAction func openAd(){
        self.view.isUserInteractionEnabled = false
        if let url = item.url, UIApplication.shared.canOpenURL(url){
            UIApplication.shared.openURL(url)
        }else if let title = item.promotionTitle{
            for category in HYPromotion.center.promotions.keys{
                for promotion in HYPromotion.center.promotions[category]!{
                    if promotion.title == title{
                        self.performSegue(withIdentifier: "showPromotion", sender: (category, promotion))
                        break
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PromotionVC, let (category, promotion) = sender as? (Category, HYPromotion.Item){
            vc.category = category
            vc.promotion = promotion
        }
    }
    
    //變更不需要顯示的View
    func changeLayoutFunc(){
        let newMultiplier:CGFloat = 3.0
        let newConstraint = self.centerY.constraintWithMultiplier(newMultiplier)
        self.view!.removeConstraint(self.centerY)
        self.centerY = newConstraint
        self.view!.addConstraint(self.centerY)
        self.view!.layoutIfNeeded() //layoutIfNeeded立即刷新
    }
    
    //****倒數計時S****
    var timeEnd:NSDate = NSDate() //dateWithTimeIntervalSinceNow(8*60*60)
    
    var timestamp:Double! //放要結束的時間
    weak var timerActivity: Timer?
    
    func CountdownTimeFunc() {
        // Initialize Label
        
        timeEnd = NSDate(timeIntervalSince1970: timestamp)
        
        setTimeLeft()
        
        // Start timer
        timerActivity = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(setTimeLeft), userInfo: nil, repeats: true)
    }
    
    @objc func setTimeLeft() {
        //初始化現在時間 並使用手機時區
        var timeNow:NSDate = NSDate()
        var secFromGMT: Int { return TimeZone.current.secondsFromGMT() } //取得手機時區 並轉換成秒數 0800 -> 28800
        timeNow = timeNow.addingTimeInterval(TimeInterval(secFromGMT));//將轉換出來的時間加上 手機時區 0800
        
        if timeEnd.compare(timeNow as Date) == ComparisonResult.orderedDescending {
            let calendar = NSCalendar.current
            let timeS = calendar.dateComponents([.day, .hour, .minute, .second], from: timeNow as Date, to: timeEnd as Date)
            let day = timeS.day!
            let hour = timeS.hour!
            let minutes = timeS.minute!
            let seconds = timeS.second!
            
            var dayText = String(describing: day)
            var hourText = String(describing: hour)
            var minuteText = String(describing: minutes)
            var secondText = String(describing: seconds)
            
            if day <= 0 {
                dayText = "00"
            }
            
            if hour <= 0 {
                hourText = "00"
            }
            
            if minutes <= 0 {
                minuteText = "00"
            }
            
            if seconds <= 0 {
                secondText = "00"
            }
            
            if day < 10 && day > 0 {
                dayText = "0\(day)"
            }
            if hour < 10 && hour > 0 {
                hourText = "0\(hour)"
            }
            if minutes < 10 && minutes > 0 {
                minuteText = "0\(minutes)"
            }
            if seconds < 10 && seconds > 0 {
                secondText = "0\(seconds)"
            }
            
//            print("\(dayText) + \(hourText) + \(String(describing: minutes))分 + \(String(describing: seconds))秒")
            lbDay.text = "\(dayText)";
            lbTime.text = "\(hourText)";
            lbMinute.text = "\(minuteText)";
            lbSecond.text = "\(secondText)";
            
        } else {
            timerActivity?.invalidate()
            timerActivity = nil
            changeLayoutFunc()
        }
    }
    //****倒數計時E****
    
}

class AdBannerVC: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate{

    weak var pageVC: UIPageViewController!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBInspectable var stopDuration: TimeInterval = 7.0
    
    override func viewDidLoad() {
        super.viewDidLoad();
        pageVC = childViewControllers[0] as! UIPageViewController;
        pageVC.dataSource = self;
        pageVC.delegate = self;
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateViewData), name: HYMarket.NotificationName, object: HYMarket.center)
        updateViewData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        play()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stop()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 輪播
    private var shouldStop: Bool = false
    private var isStopped: Bool = true
    
    func play(){
        shouldStop = false
        // 只有輪播停止的時候才有作用，已經在輪播就不動作
        guard isStopped else {
            return
        }
        isStopped = false
        showNextAd()
    }
    
    func stop(){
        shouldStop = true
    }
    
    func showAd(index: Int){
        guard !shouldStop else {
            shouldStop = false
            isStopped = true
            return
        }
        // 有下一個
        if let vc = vc(atIndex: index){
            pageVC.setViewControllers([vc], direction: .forward, animated: true){ [weak self] _ in
                self?.didPageChanged()
            }
        }
        // 過陣子再檢查
        else{
            startDelay()
        }
    }
    
    @objc func showNextAd(){
        showAd(index: idxActive + 1)
    }
    
    func didPageChanged(){
        idxActive = pageVC.viewControllers![0].view.tag
        pageControl.currentPage = idxActive
        startDelay()
    }
    
    var timer: Timer?
    
    func startDelay(){
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: stopDuration, target: self, selector: #selector(showNextAd), userInfo: nil, repeats: false)
    }
    
    //Guesture Only
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        didPageChanged()
    }
    
    //MARK: - DataSource
    
    var ads: [HYMarket.Item] = []
    @objc func updateViewData(){
        ads = HYMarket.center.ads
        pageControl.numberOfPages = ads.count
    }
    
    var idxActive: Int = -1; //原本是0，但是0會造成跑馬廣告起始廣告會以第二個顯示 故改為-1
    
    var vcs: [Int : AdBannerContentVC] = [:]
    
    func vc(atIndex idx: Int) -> UIViewController?{
        
        guard ads.count > 0 else {
            return nil
        }
        let idxNext = idx >= ads.count ? 0 : idx < 0 ? ads.count - 1 : idx
        
        var vc: AdBannerContentVC! = vcs[idxNext]
        if nil == vc {
            vc = UIStoryboard.Message.identifier("AdBannerContentVC") as! AdBannerContentVC
            vc.loadView()
            vcs[idxNext] = vc
        }
        vc.view.tag = idxNext
        vc.item = ads[idxNext]
        return vc
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return vc(atIndex: idxActive + 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return vc(atIndex: idxActive - 1)
    }
    
}
