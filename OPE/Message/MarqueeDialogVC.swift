//
//  MarqueeDialogVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/3.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class MarqueeDialogVC: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpPageViewController()
        idxActive = idxStart
        updateData()
    }
    
    // MARK: - Data
    
    func updateData(){
        
        var new: [UIViewController] = []
        for message in HYMarquee.shared.messages{
            new.append(genVC(withMessage: message))
        }
        
        self.vcs = new
        if vcs.count > idxActive, idxActive >= 0{
            pageVC.setViewControllers([vcs[idxActive]], direction: .forward, animated: false)
        }
    }
    
    func genVC(withMessage message: HYBroadcast.Item) -> UIViewController{
        let vc = UIViewController()
        let tv = UITextView(frame: .zero)
        tv.backgroundColor = .clear
        tv.textColor = .white
        tv.text = message.title
        tv.isEditable = false
        tv.font = UIFont.systemFont(ofSize: 13)
        vc.view = tv
        return vc
    }

    // MARK: - Action
    
    @IBAction func didBtnCancelClick(btn: UIButton){
        presentingViewController?.dismiss(animated: true)
    }
    
    @IBAction func didBtnNextClick(btn: UIButton){
        let idx = idxActive + 1
        if idx < vcs.count{
            pageVC.setViewControllers([vcs[idx]], direction: .forward, animated: true)
            idxActive = idx
        }
    }
    
    @IBAction func didBtnPreviousClick(btn: UIButton){
        let idx = idxActive - 1
        if idx >= 0{
            pageVC.setViewControllers([vcs[idx]], direction: .reverse, animated: true)
            idxActive = idx
        }
    }
    
    // MARK: - PageControl
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBAction func didPageControlClick(_ sender: UIPageControl) {
//        pageVC.setViewControllers([vcs[sender.currentPage]], direction: .forward, animated: false)
    }
    
    //MARK: - PageViewController
    
    @IBInspectable var idxStart: Int = 0
    var idxActive: Int = 0{
        didSet{
            pageControl.currentPage = idxActive
            btnNext.isHidden = idxActive + 1 >= vcs.count
            btnPrevious.isHidden = idxActive <= 0
        }
    }
    
    weak var pageVC : UIPageViewController!
    var vcs : [UIViewController] = []{
        didSet{
            pageControl.numberOfPages = vcs.count
        }
    }
    
    func setUpPageViewController(){
        pageVC = self.childViewControllers.last! as! UIPageViewController
        pageVC.dataSource = self
        pageVC.delegate = self
    }
    
    /**
     右一頁
     */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let idx = vcs.index{$0 == viewController}! + 1
        if idx < vcs.count{
            return vcs[idx]
        }
        return nil //沒有右一頁
    }
    
    /**
     左一頁
     */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let idx = vcs.index{$0 == viewController}! - 1
        if idx >= 0{
            return vcs[idx]
        }
        return nil //沒有左一頁
    }
    
    /**
     手勢滑動切換到指定VC
     */
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if finished && completed{
            let idx = vcs.index{$0 == pageViewController.viewControllers!.last}!
            idxActive = idx
        }
    }

}
