//
//  LiveSportVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/12/24.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import WebKit

class LiveSportVC: UIViewController, WKUIDelegate, WKNavigationDelegate{
    
    var webView: WKWebView{
        return view as! WKWebView
    }
    
    var url: URL! = {
       return URL(string: "https://www.google.com.tw/")
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 換View
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.preferences = WKPreferences()
        webConfiguration.preferences.javaScriptEnabled = true
        if #available(iOS 9.0, *) {
            webConfiguration.websiteDataStore = WKWebsiteDataStore.default()
        } else {
            // Fallback on earlier versions
        }
        //        let contentController = WKUserContentController()
        //        let script = WKUserScript(source: "", injectionTime: .atDocumentStart, forMainFrameOnly: false)
        
        
        view = WKWebView(frame: .zero, configuration: webConfiguration)
//        webView.isOpaque = false
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.load(URLRequest(url: url))
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        webView.load(URLRequest(url: url))
//    }
//
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        webView.stopLoading()
//    }
    
    //MARK: - Action
    
    @IBAction func refresh(){
        webView.reload()
    }
    
    // MARK: - UI
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let panel = UIStoryboard.Game.identifier("WebViewJavaScriptPanelVCAlert") as! WebViewJavaScriptPanelVC
        panel.message = message
        panel.answerHandler = { _,_  in
            self.dismiss(animated: true){
                completionHandler()
            }
        }
        showVC(vc: panel)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        let panel = UIStoryboard.Game.identifier("WebViewJavaScriptPanelVCConfirm") as! WebViewJavaScriptPanelVC
        panel.message = message
        panel.answerHandler = { answer, _ in
            self.dismiss(animated: true){
                completionHandler(answer)
            }
        }
        showVC(vc: panel)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        let panel = UIStoryboard.Game.identifier("WebViewJavaScriptPanelVCTextInput") as! WebViewJavaScriptPanelVC
        panel.message = prompt
        panel.placeholder = defaultText
        
        panel.answerHandler = { _, panel in
            let answer = panel.tfTextField?.text
            self.dismiss(animated: true){
                completionHandler(answer)
            }
        }
        showVC(vc: panel)
    }
    
}
