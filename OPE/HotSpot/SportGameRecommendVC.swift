//
//  SportGameRecommendVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/12/22.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class SportGameRecommendCell: UICollectionViewCell{
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubTitle: UILabel!
    @IBOutlet weak var tvInfo: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 8
    }
    
    var item: HYSportGameRecommend.Item!{
        didSet{
            lbTitle.text = item.title
            lbSubTitle.text = item.dateShow
            tvInfo.text = item.info
        }
    }
}

class SportGameRecommendVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateViewData), name: HYSportGameRecommend.NotificationName, object: HYSportGameRecommend.center)
        updateViewData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        HYSportGameRecommend.center.syncDatas()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let W = collectionView.frame.size.width
        spaceH = CGFloat(Int(W * 40 / 1080))
        spaceV = CGFloat(Int(W * 30 / 1080))
        let w = CGFloat(Int(W - spaceH * 2))
        let h = CGFloat(Int(W * 496 / 1080))
        cellSize = CGSize(width: w, height: h)
        collectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Data
    
    @objc func updateViewData(){
        collectionData = HYSportGameRecommend.center.recommends
    }
    
    // MARK: - CollectionView
    
    var collectionData: [HYSportGameRecommend.Item] = []{
        didSet{
            collectionView.reloadData()
            viewNoData.isHidden = collectionData.count > 0
        }
    }
    
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    private var cellSize: CGSize = .zero
    private var spaceH: CGFloat = 0
    private var spaceV: CGFloat = 0

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecommendCell", for: indexPath) as! SportGameRecommendCell
        cell.item = collectionData[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: spaceV, left: spaceH, bottom: spaceV, right: spaceH)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spaceV
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = collectionData[indexPath.row]
        print(item)
    }

}
