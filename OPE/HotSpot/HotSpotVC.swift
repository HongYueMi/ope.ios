//
//  HotSpotVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/12/22.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension UIStoryboard{
    static let HotSpot : UIStoryboard = UIStoryboard(name: "HotSpot", bundle: nil)
}

class HotSpotVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        titleView = Bundle.main.loadNibNamed("HotSpotTitleView", owner: self, options: nil)!.first as! UIView
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = nil
        navigationController?.navigationBar.addSubview(titleView)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        titleView.removeFromSuperview()
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var frame = navigationController!.navigationBar.bounds
        let newW = frame.size.width * 0.5
        frame.origin.x = (frame.size.width - newW) / 2
        frame.size.width = newW
        titleView.frame = frame

    }
    
    var titleView: UIView!
    @IBOutlet weak var btnTabSportInfo: UIButton!
    @IBOutlet weak var btnLive: UIButton!
    
    @IBOutlet weak var pageSportInfo: UIView!
    @IBOutlet weak var pageLiveStream: UIView!

    @IBAction func didTabLiveClick(){
        view.bringSubview(toFront: pageLiveStream)
    }
    
    @IBAction func didTabSportInfoClick(){
        view.bringSubview(toFront: pageSportInfo)
    }
    
}
