//
//  LiveStreamVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/18.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class LiveStreamVC: UIViewController {

    @IBOutlet weak var videoView: UIView!
    var player:AVPlayer!
    var Bool:Bool! = false
    @IBOutlet weak var btPlay: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if player == nil {
            btPlay.isEnabled = true
            Bool = false;
            let image = UIImage(named: "play")
            btPlay.setImage(image, for: .normal)
            
            playVideo()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "直播"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if Bool == true{
            Bool = false
            btPlay.isEnabled = true
            let image = UIImage(named: "play")
            btPlay.setImage(image, for: .normal)
            
            player.pause()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func playVideo() {
        player = AVPlayer(url: URL(string: "https://mv.opegame.com/app/videoplayback.mp4")!) //使用遠端
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
        playerLayer.frame = self.videoView.bounds
        self.videoView.layer.addSublayer(playerLayer)
        
    }

    @IBAction func didBtPlayClick(_ sender: UIButton) {
        if Bool == true{
            player.pause()
            Bool = false
        }else{
            btPlay.isEnabled = false
            btPlay.setImage(nil, for: .normal)
            player.play()
            Bool = true
        }
    }
    

}
