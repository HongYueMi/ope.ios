//
//  RecordListVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/29.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class RecordCell: UITableViewCell{
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbAmount: UILabel!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var lbStatus: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var viewLabel: UIView!
    @IBOutlet weak var imgvLabel: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewStatus.layer.borderWidth = 1
    }
    
    var record: RecordCellData!{
        didSet{
            
            lbTitle.text = record.titleShow
            lbAmount.text = record.amountShow
            lbTime.text = record.dateShow?.toString(format: "yyyy-MM-dd HH:mm:ss")
            
            lbStatus.text = record.statusShow
            viewStatus.isHidden = lbStatus.text == nil

            var tint = record.defaultTint
            var image = record.defaultTintImage
            if let status = lbStatus.text{
                if status.contains("成功"){
                    tint = #colorLiteral(red: 0, green: 0.7960784314, blue: 0.7529411765, alpha: 1)
                    image = #imageLiteral(resourceName: "Record_Indicator_Success")
                }else if status.contains("失败"){
                    tint = #colorLiteral(red: 0.9058823529, green: 0.1137254902, blue: 0.6039215686, alpha: 1)
                    image = #imageLiteral(resourceName: "Record_Indicator_Fail")
                }
            }
            lbStatus.textColor = tint
            viewStatus.layer.borderColor = tint.cgColor
//            viewStatus.layer.borderWidth = record is HYOtherTransactionRecord ? 0 : 1
            viewLabel.backgroundColor = tint
            imgvLabel.image = image
            
//            btnCancel.isHidden = !record.canCancel    //取消提款按鈕-暫時不使用
        }
    }
}

protocol RecordCellData {
    var titleShow: String {get}
    var amountShow: String {get}
    var dateShow: Date? {get}
    var statusShow: String? {get}
    var canCancel: Bool{get}
    var defaultTint: UIColor{get}
    var defaultTintImage: UIImage{get}
}

extension HYDepositRecord: RecordCellData{
    
    var canCancel: Bool {
        return false
    }

    var statusShow: String? {
        if let type = self.processType, let status = statusCode{
            return HYDatas.processStatus[type]?[status]
        }
        return nil
    }
    
    var dateShow: Date? {
        return creatDate
    }

    var amountShow: String {
        return NSString(format: "%.2f", amount) as String
    }

    var titleShow: String{
        return depositTypeName + "--" + deviceType
    }
    
    var defaultTint: UIColor{
        return #colorLiteral(red: 0, green: 0.02099477872, blue: 0.1244999245, alpha: 1)
    }
    var defaultTintImage: UIImage{
        return #imageLiteral(resourceName: "Record_Indicator_Processing")
    }
    
}

//提現 提款記錄
extension HYWithdrawalRecord: RecordCellData{
    
    var statusShow: String? {
        if let type = self.processType, let status = statusCode{
            return HYDatas.processStatus[type]?[status]
        }
        return nil
    }
    
    var dateShow: Date? {
        return creatDate
    }
    
    var amountShow: String {
        return NSString(format: "%.2f", amount) as String
    }
    
    var titleShow: String{
//        return "\(LanRecordList.lbTitle_Bank)\n\(bankAccount)"
        return "\(bankAccount)\n--\(deviceType)"
    }
    
    var canCancel: Bool {
        return canBeCancelled
    }
    
    var defaultTint: UIColor{
        return #colorLiteral(red: 0, green: 0.02099477872, blue: 0.1244999245, alpha: 1)
    }
    var defaultTintImage: UIImage{
        return #imageLiteral(resourceName: "Record_Indicator_Processing")
    }
}

//轉账記錄
extension HYTransferRecord: RecordCellData{
    static let map: [Int: String] = [
          0:"转账中"
        , 1: "转账成功"
        , 2: "转账失败"
        , 3: "解锁"
        , 4: "强制转入"
    ]

    var statusShow: String? {
        if let status = statusCode{
            return HYTransferRecord.map[status]
        }
        return nil
    }
    
    var dateShow: Date? {
        return creatDate
    }
    
    var amountShow: String {
        return NSString(format: "%.2f", changePoint) as String
    }
    
    var titleShow: String{
        var code = codeFromPlatform.lowercased()
        let nameWalletFrom = code == "centerwallet" ? "中心钱包" : HYTransfer.transfer.platformWallets[code]?.name ?? code
        code = codeToPlatform.lowercased()
        let nameWalletTo = code == "centerwallet" ? "中心钱包" : HYTransfer.transfer.platformWallets[code]?.name ?? code
        return "从 " + nameWalletFrom + " 转入 " + nameWalletTo
    }
    
    var canCancel: Bool {
        return false
    }
    
    var defaultTint: UIColor{
        return #colorLiteral(red: 0, green: 0.02099477872, blue: 0.1244999245, alpha: 1)
    }
    var defaultTintImage: UIImage{
        return #imageLiteral(resourceName: "Record_Indicator_Processing")
    }
    
}

//優惠紀錄
extension HYPromotionRecord: RecordCellData{
    
    static let map: [String: String] = [
        "Reward": "洗码",
        "Favorable": "优惠",
        "RepairPoint": "补点",
        "ReducePoint": "减值",
        "CommSettle": "佣金结算",
        "UpgradePromotion": "晋升礼金",
        "VIPFreePromotion": "VIP免费筹码",
        "BrithdayPromotion": "生日礼金",
        "RescueGift": "救援金",
        "ExperienceGift": "体验金",
        "FSDepositGift": "首存再存",
        "RecommendGift": "推荐礼金",
        "RandomGift": "随机红包",
        "SpecialDayGift": "节日红包",
        "FavorableGift": "优惠红包",
        "DepositGift": "存送优惠",
        "PreferentialReduce": "优惠扣除",
        "WinloseReduce": "盈利扣除",
        "ManuallyDepositGift": "手动存送优惠",
        "DailyBetGift": "每日彩金",
        "FreeRedEnvelopes": "免费红包",
        "NoviceActivity": "新手活动",
        "DailyExtraRakeBack": "日额外返水",
        "ActivityReward": "活动奖金"
    ]
    
    var statusShow: String? {
        return nil
    }
    
    var dateShow: Date? {
        return closedDate
    }
    
    var amountShow: String {
        return NSString(format: "%.2f", chargePoint) as String
    }
    
    var titleShow: String{
        return HYPromotionRecord.map[name] ?? name
    }
    
    var canCancel: Bool {
        return false
    }
    
    var defaultTint: UIColor{
        return #colorLiteral(red: 0, green: 0.7960784314, blue: 0.7529411765, alpha: 1)
    }
    var defaultTintImage: UIImage{
        return #imageLiteral(resourceName: "Record_Indicator_Success")
    }
}

//其他交易紀錄
extension HYOtherTransactionRecord: RecordCellData{
    
    static let map: [String: String] = [
        "Verifying": "审核中",
        "Added": "优惠已添加",
        "NotPass": "优惠审核未通过",
        "Reduced": "优惠已扣除"
    ]
    
    var statusShow: String? {
        return HYOtherTransactionRecord.map[status]
    }
    
    var dateShow: Date? {
        return takeDate
    }
    
    var amountShow: String {
        return NSString(format: "%.2f", amount) as String
    }
    
    var titleShow: String{
        return name == "AgentAccountTransfer" ? "代理账号转入" : "资金修正类型"
    }
    
    var canCancel: Bool {
        return false
    }
    
    var defaultTint: UIColor{
        return #colorLiteral(red: 0, green: 0.7960784314, blue: 0.7529411765, alpha: 1)
    }
    var defaultTintImage: UIImage{
        return #imageLiteral(resourceName: "Record_Indicator_Success")
    }
}

class RecordListVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var transactionType: SearchItem.TransactionType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func updateData(){
        HYRecordCenter.shared.search(searchItem: SearchItem(transactionType: self.transactionType)){ [weak self] aryTransactions, error in
            if let ary = aryTransactions as? [RecordCellData] {
                self?.tableData = ary
            }else{
                self?.tableData = []
            }
        }
    }
    
    //MARK: - Action
    
    @IBAction func didBtnCancelClick(btn: UIButton){
        let record = tableData[btn.tag] as! HYWithdrawalRecord
        HYRecordCenter.shared.cancelRecord(record){ [weak self] error in
            if let error = error{
                self?.showGeneralAlert(.alert_error, lbTextA:error.localizedDescription){ value in
                    self?.updateData()
                }
            }else{
                self?.updateData()
            }
        }
    }
    
    //MARK: - Table
    
    var tableData: [RecordCellData] = []{
        didSet{
            tableView.reloadData()
            viewNoData.isHidden = tableData.count > 0
        }
    }
    
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecordCell") as! RecordCell
        cell.record = tableData[indexPath.row]
//        cell.btnCancel?.tag = indexPath.row   //取消提款按鈕-暫時不使用
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
}
