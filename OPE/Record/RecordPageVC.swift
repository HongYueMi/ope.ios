//
//  RecordPageVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/29.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

extension UIStoryboard{
    static let Record : UIStoryboard = UIStoryboard(name: "Record", bundle: nil)
}

class RecordPageVC: TabPageVC2 {
    
    override func viewDidLoad() {
        cellWidth = CGFloat(Int(UIScreen.main.bounds.width / 5))
        super.viewDidLoad()
        titles = [
            "充值"
            , "提款"
            , "转账"
            , "优惠"
            , "其他"
        ]
    }

    var cellWidth: CGFloat = 0
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = indexPath.row == 4 ? UIScreen.main.bounds.width - 4 * cellWidth : cellWidth
        return CGSize(width: w, height: collectionView.bounds.size.height)
    }
    
    //MARK: - Pages
    
    var pages: [Int: UIViewController] = [:]
    
    let types: [SearchItem.TransactionType] = [.deposit, .withdrawal, .transfer, .promotion, .other]
    
    override func vc(atIndex idx: Int) -> UIViewController? {
        guard idx >= 0, idx < titles.count else {
            return nil
        }
        if pages[idx] == nil{
            let vc = UIStoryboard.Record.identifier("RecordListVC") as! RecordListVC
            vc.transactionType = types[idx]
            pages[idx] = vc
        }
        return pages[idx]
    }
}

