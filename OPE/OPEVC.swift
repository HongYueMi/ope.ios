//
//  OPEVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/12/19.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

/**
 尺寸設定： 理想
 row: 100:30
 header: 100:10
 space: 100:4
 長圖: 92:26
 短圖: 44:26
 */

/**
 尺寸設定： 出圖
 row: 1080:330
 header: 1080:120
 space: 1080:42
 長圖: 996:288
 短圖: 477:288
 */

class PlayStationPlatformShortcutCell: UICollectionViewCell{
    
    @IBOutlet weak var imgvPicture: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubTitle: UILabel!
    
    var entry: HYPlayStation.Entry!{
        didSet{
            updateView()
        }
    }
    
    func updateView(){
        lbTitle.text = entry?.title
        lbSubTitle.text = entry.subTitle
        let rate = bounds.width / bounds.height
        imgvPicture.image = rate < 2.3 ? entry.shortcutImage[4] : entry.shortcutImage[5]
    }
}

class PlayStationCategoryCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var didEntryClickHandler: ((HYPlayStation.Entry) -> Void)!
    
    var category: String!{
        didSet{
            let user = HYAuth.auth.currentUser ?? HYUser.anonymous
            collectionData = user.favorPlatform[category]!.map{HYPlayStation.entries[category]![$0]!}
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        update(item: self.item, forRow: self.row)
    }
    
    func update(item: OPEVC.TableItem, forRow row: Int){
        self.row = row
        self.item = item
        let aryPlatform = row == 1 ? [item.favorates[2]]
            : item.favorates.count > 1 ? [item.favorates[0], item.favorates[1]]
            : [item.favorates[0]]
        collectionData = aryPlatform.flatMap{HYPlayStation.entries[item.category]?[$0]}
    }
    
    private var item: OPEVC.TableItem!
    private var row: Int = 0
    private var collectionData: [HYPlayStation.Entry] = []{
        didSet{
            let W = collectionView.bounds.size.width
            space = CGFloat(Int(W * 7 / 360)) // (42/2):1080 用一半來設定
            
            let w = collectionData.count > 1 ? CGFloat(Int(W * 476 / 1080)) : CGFloat(Int(W * 994 / 1080))
            cellSize = CGSize(width: w, height: CGFloat(Int(W * 288 / 1080)))
            collectionView.reloadData()
        }
    }
    private var space: CGFloat = 0
    private var cellSize: CGSize = .zero

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: space, left: space * 2, bottom: space, right: space * 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return space * 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let entry = collectionData[indexPath.row]
        let idx = 2 - collectionData.count
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: entry.space[idx].rawValue, for: indexPath) as! PlayStationPlatformShortcutCell
        cell.entry = collectionData[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didEntryClickHandler(collectionData[indexPath.item])
    }
    
}

class PlayStationCategoryHeader: UITableViewHeaderFooterView{
    
    @IBOutlet weak var ctrlHeader: UIControl!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var imgvIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = #colorLiteral(red: 0.1130309179, green: 0.1151234582, blue: 0.1675853133, alpha: 1)
    }
    
    var item: OPEVC.TableItem!{
        didSet{
            lbTitle.text = item.title
            imgvIcon.image = item.icon
        }
    }
    
}

class PlayStationCategoryFooter: UITableViewHeaderFooterView{
    
    @IBOutlet weak var barView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.barView.backgroundColor = #colorLiteral(red: 0.1130309179, green: 0.1151234582, blue: 0.1675853133, alpha: 1)
    }
}

class OPEVC: PlayStationVC, UITableViewDataSource, UITableViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let Ｗ = UIScreen.main.bounds.width
        let h = Ｗ * 31 / 30 + 12
        let tableHeader = UIView(frame: CGRect(x: 0, y: 0, width: Ｗ, height: h))
        
        let bannerVC = UIStoryboard.Banner.initial()!
        self.addChildViewController(bannerVC)
        bannerVC.view.frame = CGRect(x: 0, y: 0, width: Ｗ, height: Ｗ * 16 / 30)
        tableHeader.addSubview(bannerVC.view)
        
//        let adBannerVC = UIStoryboard.Message.identifier("AdBannerVC")
//        self.addChildViewController(adBannerVC)
//        adBannerVC.view.frame = CGRect(x: 0, y: 0, width: Ｗ, height: Ｗ * 2 / 3)
//        tableHeader.addSubview(adBannerVC.view)
        
        let marqueeVC = UIStoryboard.Message.identifier("MarqueeVC")
        self.addChildViewController(marqueeVC)
        marqueeVC.view.frame = CGRect(x: 0, y: Ｗ * 16 / 30
            , width: Ｗ, height: Ｗ * 1 / 10)
        tableHeader.addSubview(marqueeVC.view)
        
        let recommendVC = UIStoryboard.Platform.identifier("RecommendVC")
        self.addChildViewController(recommendVC)
        recommendVC.view.frame = CGRect(x: 0, y: Ｗ * 19 / 30
            , width: Ｗ, height: Ｗ * 2 / 5)
        tableHeader.addSubview(recommendVC.view)
        
        tableView.tableHeaderView = tableHeader
        
        setUpTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "OPE"
        let user = HYAuth.auth.currentUser ?? HYUser.anonymous
        tableData = user.favorCategoryOrder.filter{
            if let platforms = user.favorPlatform[$0], platforms.count > 0{
                return true
            }
            return false
            }
            .flatMap{
                tableItems[$0]
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        rowHight = tableView.frame.size.width * 11 / 36 // 330:1080
        headerHight = tableView.frame.size.width * 1 / 9 // 120:1080
        footerHight = tableView.frame.size.width * 7 / 120 // (42*1.5):1080
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PlayStationCategoryVC{
            vc.category = sender as! String
        }
        if let vc = segue.destination as? GameCategoryTableVC, let entry = sender as? HYPlayStation.Entry {
            vc.entryItem = entry
        }
        else if let vc = segue.destination as? GameCategoryCollectionVC, let entry = sender as? HYPlayStation.Entry {
            vc.entryItem = entry
        }
    }
    
    @IBAction func didCtrlHeaderClick(ctrl: UIControl){
        let category = tableData[ctrl.tag].category
        let identifier = category == HYPlayStation.Category.Sport ? "Sport" : category == HYPlayStation.Category.Slot ? "Slot" : "Default"
        self.performSegue(withIdentifier: identifier, sender: category)
    }
    
    // MARK: - Table
    
    class TableItem {
        
        let category: String
        let title: String
        let icon: UIImage
        
        init(category: String, title: String, icon: UIImage) {
            self.category = category
            self.title = title
            self.icon = icon
            self.updateFavorate()
        }
        
        var favorates: [String] = []
        
        func updateFavorate(){
            let user = HYAuth.auth.currentUser ?? HYUser.anonymous
            favorates = user.favorPlatform[category] ?? []
        }
    }
    
    var tableItems: [String: TableItem] = [
        HYPlayStation.Category.Sport: TableItem(category: HYPlayStation.Category.Sport, title: "体育游戏", icon: #imageLiteral(resourceName: "Game_Sport_Icon"))
        , HYPlayStation.Category.Electronic: TableItem(category: HYPlayStation.Category.Electronic, title: "电子竞技", icon: #imageLiteral(resourceName: "Game_Electronic_Icon"))
        , HYPlayStation.Category.Real: TableItem(category: HYPlayStation.Category.Real, title: "现场荷官", icon: #imageLiteral(resourceName: "Game_Real_Icon"))
        , HYPlayStation.Category.Slot: TableItem(category: HYPlayStation.Category.Slot, title: "游戏厅", icon: #imageLiteral(resourceName: "Game_Slot_Icon"))
        , HYPlayStation.Category.Lottery: TableItem(category: HYPlayStation.Category.Lottery, title: "彩票游戏", icon: #imageLiteral(resourceName: "Game_Lottery_Icon"))
    ]
    
    var tableData: [TableItem] = []{
        didSet{
            for item in tableData{
                item.updateFavorate()
            }
            tableView.reloadData()
        }
    }
    
    var rowHight : CGFloat = 0
    var headerHight : CGFloat = 0
    var footerHight : CGFloat = 0

    @IBOutlet weak var tableView: UITableView!
    
    func setUpTableView(){
        tableView.register(UINib(nibName: "PlayStationCategoryHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderView")
        tableView.register(UINib(nibName: "PlayStationCategoryFooter", bundle: nil), forHeaderFooterViewReuseIdentifier: "FooterView")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (tableData[section].favorates.count + 1) / 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! PlayStationCategoryCell
        cell.update(item: tableData[indexPath.section], forRow: indexPath.row)
        cell.didEntryClickHandler = { [weak self] entry in
            if entry.category == HYPlayStation.Category.Slot{
                if entry.platform == HYPlayStation.Platform.OPE{
                    self?.performSegue(withIdentifier: "CollectionStyle", sender: entry)
                    return
                }
                self?.performSegue(withIdentifier: "ListStyle", sender: entry)
                return
            }
            self?.openGame(entry)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! PlayStationCategoryHeader
        header.item = tableData[section]
        header.ctrlHeader.tag = section
        header.ctrlHeader.addTarget(self, action: #selector(didCtrlHeaderClick), for: .touchUpInside)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return footerHight
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FooterView") as! PlayStationCategoryFooter
        return footer
    }
}
