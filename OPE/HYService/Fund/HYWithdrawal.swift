//
//  HYWithdrawal.swift
//  SportsGames
//
//  Created by Lavend K. Mi on 2017/4/14.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import Alamofire
import SwiftyJSON

class HYWithdrawal: NSObject {

    static let withdrawal = HYWithdrawal()
    
    func configure() {
        _ = HYAuth.auth.addAuthStateDidChangeListener{ auth, user in
            self.updateData(withUser: user){ _ in
                
            }
        }
    }
    
    func updateData(withUser user: HYUser?, completion : @escaping (Error?)-> Void){
        guard let _ = user else {
            withdrawalBankAccounts = []
            banks = []
            return
        }
        
        updateAvailableWithdrawalBank{ [weak self] error in
            if let err = error{
                completion(err)
            }
            else{
                self?.updateWithdrawalBankAccounts{ error in
                    completion(error)
                }
            }
        }
    }
    
    
    // MARK: - Withdrawal Bank Account
    // 提款的账戶
    
    var withdrawalBankAccounts: [WithdrawalBankAccount] = []
    
    func updateWithdrawalBankAccounts(completion : @escaping (Error?)-> Void){
        let parameters: [String: Any] = [:]
        HYSession.member.authRequest(URL_HOST + "/API/MemberWithdrawBank/ListAsync", parameters: parameters).validate().responseHYJson{
            response in
            
            if let ary = response.aryData{
                self.withdrawalBankAccounts = ary.map{return WithdrawalBankAccount(withDictionary: $0)}
            }else{
                self.withdrawalBankAccounts = []
            }
            completion(response.error)
        }
    }
    
    var banks: [HYBankOption] = []
    
    func updateAvailableWithdrawalBank(completion : @escaping (Error?)-> Void){
        HYSession.member.authRequest(URL_HOST + "/API/Bank/ListAsync?source=2", method: .get)
            .validate()
            .responseHYJson{ response in
                if let ary = response.aryData{
                    self.banks = ary.map{HYBankOption(withDictionary: $0)}
                }else{
                    self.banks = []
                }
                completion(response.error)
        }
    }
    
    func addWithdrawalBankAccount(withItem item : WithdrawalBankAccountAddItem, completion : @escaping (Error?)-> Void){
        let parameters: [String: Any] = [
            "Password": item.password
            , "BankPassword": item.bankPassword
            , "AccountName": item.name
            , "BankID": item.bankID.id
            , "BankBranch": item.bankBranch
            , "BankAccount": item.account
            , "BankProvinceID": item.bankProvince.id
            , "BankAddress": item.bankAddress
        ]
        HYSession.member.authRequest(URL_HOST + "/API/MemberWithdrawBank/AddAsync", parameters: parameters).validate().responseHYJson{
            [weak self] response in
            
            if response.isSuccess == false {
                completion(response.error)
            }else{
                self!.updateData(withUser: HYAuth.auth.currentUser, completion: completion)
            }
            
        }
    }
    
    func chkPassword(withItem item : PasswordEditItem, completion : @escaping (Error?)-> Void){
        let parameters: [String: Any] = [
            "Password": item.old!
            , "BankPassword": item.new!
        ]
        
        HYSession.member.authRequest(URL_HOST + "/API/MemberWithdrawBank/AddValidateAsync", parameters: parameters).validate().responseHYJson{ response in
            completion(response.error)
        }
    }
    
    func newNameAsync(_ userName : String, completion : @escaping (Error?)-> Void){
        let parameters: [String: Any] = [
            "Name": userName
        ]
        
        HYSession.member.authRequest(URL_HOST + "/API/Member/EditNameAsync", parameters: parameters).validate().responseHYJson{ response in
            completion(response.error)
        }
    }
    
    // MARK: - Withdrawal
    
    func withdrawal(item : WithdrawalItem, completion : @escaping (Error?)-> Void){
        
        let path = URL_HOST + "/API/MemberWithdraw/AddAsync"
        let parameters : [String : Any] = [
            "Type": 1 // ?? 
            , "SID": item.account.sid
            , "RequestMemberWithdrawAdd": [
                "Amount": item.amount
                ,"BankPassword": item.password
                , "BankID": item.account.bankID
                , "AccountName": item.account.name
                , "BankAccount": item.account.account
                , "BankBranch": item.account.bankBranch
            ]
        ]
        
        HYSession.member.authRequest(path, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseHYJson{ response in
                completion(response.error)
        }
    }
    
    //檢查可提領金額
    func checkAmountMoney(completion : @escaping (CheckAmountMoneyAddItem? ,Error?)-> Void){
        var CheckAmount:CheckAmountMoneyAddItem = CheckAmountMoneyAddItem();
        
        let path = URL_HOST + "/API/MemberWithdrawNormalAudit/GetAsync?PlatformID=0"
        HYSession.member.authRequest(path, method: .get, encoding: JSONEncoding.default).validate().responseHYJson { response in
            if let ary = response.source {
                CheckAmount.model_Guid = ary["Guid"] as? String
                CheckAmount.model_Success = ary["Success"] as? String
                CheckAmount.model_Code = ary["Code"] as? String
                CheckAmount.model_Message = ary["Message"] as? String
                if let aryData = response.dicData {
                    CheckAmount.model_NoWithdrawAmount = aryData["NoTranAmount"] as? Double
                    CheckAmount.model_CanWithdrawAmount = aryData["CanTranAmount"] as? Double
                    CheckAmount.model_MoneyAmount = aryData["PlatformAmount"] as? Double
                }
                
            }
            completion(CheckAmount , response.error)
        }
    }
    
    //檢查提款上下限
    func checkMemberWithdrawMoney(completion : @escaping (CheckMemberWithdrawMoneyAddItem? ,Error?)-> Void){
        var CheckAmount:CheckMemberWithdrawMoneyAddItem = CheckMemberWithdrawMoneyAddItem();
        
        let path = URL_HOST + "/API/MemberWithdraw/LimitCheckAsync"
        HYSession.member.authRequest(path, method: .post, encoding: JSONEncoding.default).validate().responseHYJson { response in
            if let ary = response.source {
                CheckAmount.model_Guid = ary["Guid"] as? String
                CheckAmount.model_Success = ary["Success"] as? String
                CheckAmount.model_Code = ary["Code"] as? String
                CheckAmount.model_Message = ary["Message"] as? String
                if let aryData = response.dicData {
                    CheckAmount.model_UpperLimit = aryData["UpperLimit"] as? Double
                    CheckAmount.model_LowerLimit = aryData["LowerLimit"] as? Double
                    CheckAmount.model_Limit = aryData["Limit"] as? Int
                }
                
            }
            completion(CheckAmount , response.error)
        }
    }
    
}

struct WithdrawalBankAccount {
    
    let dictionary: NSDictionary
    
    init(withDictionary dictionary: NSDictionary) {
        self.dictionary = dictionary
    }
    var account: String{
        return dictionary["BankAccount"] as! String
    }
    
//    var secretAccount: String{
//        
//        var accountOut = "";
//        if(account.count < 9){
//            accountOut = account;
//        }else{
//            //取代中間成星號
//            let start = account.index(account.startIndex, offsetBy: 4);
//            let end = account.index(account.endIndex, offsetBy: -4);
//            let range = start..<end;
//            let rangeString = account.substring(with: range);
//            
//            var replace = "";
//            for _ in 0..<rangeString.count{
//                replace = replace + "*";
//            }
//            
//            accountOut = account.replacingOccurrences(of: rangeString , with: replace);
//        }
//        
//        return accountOut;
//        
//        let numberOfStar = account.count / 2 + 1
//        let start = account.count / 4
//        let startIndex = account.index(account.startIndex , offsetBy: start)
//        let range = startIndex..<account.index(startIndex , offsetBy: numberOfStar)
//        var replace = ""
//        for _ in 1..<numberOfStar{
//            replace = replace + "*"
//        }
//        
//        return account.replacingCharacters(in: range , with: replace)
//    }
    var name: String{
        return dictionary["AccountName"] as! String
    }
    var sid: Int{
        return dictionary["SID"] as! Int
    }
    //
    var bankID: Int{
        return dictionary["BankID"] as! Int
    }
    var bankName: String{
        return dictionary["BankEName"] as? String ?? "该银行卡不支持" //ＡＰＩ抓不到就顯示该银行卡不支持
    }
    var bankBranch: String{
        return dictionary["BankBranch"] as! String
    }
    public var bankProvinceID: Int{
        return dictionary["BankProvinceID"] as! Int
    }
    var bankProvinceIndex: Int?{
        let id = self.bankProvinceID
        for (idx, hyData) in HYDatas.provinces.enumerated(){
            if id == hyData.id{
                return idx
            }
        }
    
        return nil
    }
    public var province : HYProvince?{
        if let idx = self.bankProvinceIndex{
            return HYDatas.provinces[idx]
        }
        return nil
    }
    var bankCity: String{
        return dictionary["BankCity"] as? String ?? ""
    }

}

struct WithdrawalItem {
    
    var amount : Double!
    var password : String!

    var account: WithdrawalBankAccount
    
    init(withAccount account: WithdrawalBankAccount) {
        self.account = account
    }

}

struct WithdrawalBankAccountAddItem {
    
    var password: String!
    var bankPassword: String!
    
    var account: String!
    var name: String!
    
    var bankID: HYBankOption!
    var bankBranch: String!
    var bankProvince: HYProvince!
    var bankAddress: String!
}

//放置 可，不可提領金額
struct CheckAmountMoneyAddItem {
    
    var model_Guid: String!
    
    var model_Success: String!
    
    var model_Code: String!
    
    var model_Message: String!

    var model_NoWithdrawAmount:Double! //不可提款金額
    
    var model_CanWithdrawAmount: Double! //可以提取金額
    
    var model_MoneyAmount: Double! //金額
    
}

//放置提款上下限
struct CheckMemberWithdrawMoneyAddItem {
    
    var model_Guid: String!
    
    var model_Success: String!
    
    var model_Code: String!
    
    var model_Message: String!
    
    var model_UpperLimit:Double! //上限
    
    var model_LowerLimit: Double! //下限
    
    var model_Limit: Int! //提款次數上限
    
}
































