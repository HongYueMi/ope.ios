//
//  HYFund.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/14.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import Alamofire

class HYFund: NSObject {

    static let NotificationName: Notification.Name = Notification.Name(rawValue: "HYFundBalance")
    static let fund = HYFund()
    
    func configure(){
        HYWithdrawal.withdrawal.configure()
        _ = HYAuth.auth.addAuthStateDidChangeListener{ auth, user in
            self.updateData(withUser: user)
        }
    }
    
    func updateData(withUser user: HYUser?){
        guard let _ = user else {
            strBalance = nil
            depositGroup = [:]
            manualDepositBankAccounts = []
            timerRefreshBalance?.invalidate()
            return
        }
        autoUpdateBalance()
        updateDepositGroup()
        updateManualDepositData()
    }
    
    // MARK: - 餘額
    
    var strBalance: String?{
        didSet{
            NotificationCenter.default.post(name: HYFund.NotificationName, object: self, userInfo: nil)
        }
    }
    
    var balance : Double?{
        guard let string = strBalance else {
            return nil
        }
        
        let b = NumberFormatter()
        b.positiveFormat = "#,##0.##"
        return b.number(from: string) as? Double
    }
    
    var timerRefreshBalance : Timer?
    
    @objc func autoUpdateBalance(){
        self.timerRefreshBalance?.invalidate()
        self.updateBalance{ [weak self] error in
            self?.timerRefreshBalance = Timer.scheduledTimer(timeInterval: 30, target: self!, selector: #selector(self!.autoUpdateBalance), userInfo: nil, repeats: false)
        }
    }
    
    func updateBalance(completion: @escaping (Error?) -> Void){
        HYSession.member.authRequest(URL_HOST + "/API/Member/CreditAsync")
            .validate()
            .responseHYJson{[weak self] response in
                self?.strBalance = response.dicData?["Balance"] as? String
                completion(response.error)
        }
    }
    
    // MARK: - 存款
    
    // 本APP可用的存款方式
    var depositGroup: [String: DepositGroup] = [:]
    
    func updateDepositGroup(){
        
        let parameters: [String: Any] = [
            "Showon": 2
        ]
        HYSession.member.authRequest(URL_HOST + "/API/DepositGroup/ListAsync", parameters: parameters).validate().responseHYJson{
            response in
            if let ary = response.aryData{
                for dictionary in ary{
                    let group = DepositGroup(withDictionary: dictionary)
                    self.depositGroup[group.code] = group
                }
            }
        }
    }
    
    // 依不同會員身份，取得對應到該存款方式的平台與代理商。
    func getHYDepositItem(withDepositGroup depositGroup: DepositGroup, completet: @escaping (DepositItem?, Error?)->Void){
        
        let parameters: [String: Any] = [
            "Showon": 2
            , "DepositGroupID": depositGroup.id
        ]
        
        HYSession.member.authRequest(URL_HOST + "/API/MemberDepositGroupPlatform/GetAsync", parameters: parameters).validate().responseHYJson{
            response in
            if let dictionary = response.dicData{
                let depositItem = DepositItem(withDictionary: dictionary, depositGroup: depositGroup)
                completet(depositItem, response.error)
            }
            else{
                completet(nil, response.error)
            }
        }
    }
    
    // 依該會員的代理商和轉账平台，取得提供的銀行清單
    
    func getAvailableBankList(withDepositItem item: DepositItem, completet: @escaping ([HYBankData]?, Error?)->Void){
        
        let parameters: [String: Any] = [
            "ThirdPartyPaymentCode": item.name
            , "APIAccount": item.apiAccount
            , "OnlinePaymentType": item.typeID
        ]
        
        HYSession.member.authRequest(URL_HOST + "/API/ThirdPartyPayment/BankListAsync", parameters: parameters).validate().responseHYJson{
            response in
            var banks: [HYBankData]?
            if let ary = response.aryData{
                banks = ary.map{HYBankData(withDictionary: $0)}
            }
            
            completet(banks, response.error)
        }
    }
    
    // 存款跳轉畫面
    func deposit(withDeposit deposit: DepositItem, complete: @escaping (HYPayment?, Error?)-> Void){
        
//        let parameters: [String: Any] = [
//            "Showon": 2
//            , "ThirdPartyPaymentCode": deposit.name
//            , "DepositGroupID": deposit.id
//            , "OnlinePaymentType": deposit.typeID
//            , "PlatformID": 0
//            , "APIAccount": deposit.apiAccount
//            , "BankCode": deposit.bankCode ?? ""
//            , "Amount": deposit.amount
//        ]
        
        let parameters: [String: Any] = [
            "Showon": "2"
            , "ThirdPartyPaymentCode": deposit.name
            , "DepositGroupID": deposit.id
            , "OnlinePaymentType": "\(deposit.typeID)"
            , "PlatformID": 0
            , "APIAccount": deposit.apiAccount
            , "BankCode": deposit.bankCode ?? ""
            , "Amount": "\(Int(deposit.amount))"
            , "PayCardNumber": deposit.PayCardNumber ?? ""
            , "PayUserName": deposit.PayUserName ?? ""
        ]
        
        HYSession.member.authRequest(URL_HOST + "/API/MemberDeposit/ThirdPartyPaymentAddAsync", parameters: parameters, encoding: JSONEncode.default).validate().responseHYJson{
            response in
            var payment: HYPayment?
            if let dictionary = response.dicData{
                payment = HYPayment(withDictionary: dictionary)
            }
            
            complete(payment, response.error)
        }
    }
    
    // 存款狀態
    func depositStatusCheck(complete: @escaping (DepositVerify?, Error?)-> Void){
        
        HYSession.member.authRequest(URL_HOST + "/API/DepositVerifyingData/CheckAsync").validate().responseHYJson{
            response in
            var status: DepositVerify?
            if let dictionary = response.dicData{
                status = DepositVerify(withDictionary: dictionary)
            }
            complete(status, response.error)
        }
    }
    
    // 回報存款狀態
    func DepositFeedback(feedback: DepositVerify, complete: @escaping (Error?)-> Void){
        
        let parameters: [String: Any] = [
            "DepositGroupID": feedback.groupID
            , "TypeID": feedback.type
            , "SID": feedback.sid
            , "FeedbackType": feedback.feedBack ? 1 : 2
        ]
        
        HYSession.member.authRequest(URL_HOST + "/API/DepositFeedbackType/UpdateAsync", parameters: parameters).validate().responseHYJson{
            response in
            complete(response.error)
        }
    }
    
    // 手動存款
    
    var manualDepositBankAccounts: [DepositAccount] = []
    
    func updateManualDepositData(){
        HYSession.member.authRequest(URL_HOST + "/API/MemberDepositBank/ListAsync?source=1",method: .get).validate().responseHYJson{
            response in
            if let ary = response.aryData{
                self.manualDepositBankAccounts = ary.map{DepositAccount(withDictionary: $0)}
            }
        }
    }
    
    // 資金密碼
    
    // MARK: - ChangePassword
    
    func editPassword(withItem item: PasswordEditItem, complete : @escaping (Error?)-> Void){
        
        var parameters : [String : Any] = [:]
        
        if let value = item.old{
            parameters["Old_BankPassword"] = value
        }
        if let value = item.new{
            parameters["New_BankPassword"] = value
        }
        
//        print(parameters)
        HYSession.member.authRequest(URL_HOST + "/API/MemberSecurity/EditBankPasswordAsync", parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseHYJson{ response in
                complete(response.error)
        }
    }
    
    func addPassword(withItem item: PasswordEditItem, complete : @escaping (Error?)-> Void){
        
        var parameters : [String : Any] = [:]
        
        if let value = item.old{
            parameters["Password"] = value
        }
        if let value = item.new{
            parameters["New_BankPassword"] = value
        }
        
//        print(parameters)
        HYSession.member.authRequest(URL_HOST + "/API/MemberSecurity/AddBankPasswordAsync", parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseHYJson{ response in
                
                if let err = response.error{
                    complete(err)
                }
                else{
                    // 更新資金密碼狀態
                    HYAuth.auth.currentUser?.update(complete: complete)
                }
        }
    }
    
}

