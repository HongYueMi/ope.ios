//
//  HYTransfer.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/20.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class HYWallet: NSObject {
    init(platform: String, name: String, image: UIImage) {
        self.platform = platform
        self.name = name
        self.image = image
    }
    var platform: String
    var name: String
    var credit: String?
    var image: UIImage
    
    var isAudited: Bool?
    var availableAmount: Double?
    var notAvailableAmount: Double?
    var platformAmount: Double?
    
    var amount: Double?{
        if let str = credit, let d = Double(str){
            return d
        }
        return nil
    }
    
    func updateBalance(completion : @escaping (Error?)-> Void){
        let parameters: [String: Any] = ["GameName":platform]
        HYSession.member.authRequest(URL_HOST + "/API/Credit/GameAsync", parameters: parameters, encoding: JSONEncode.default).validate().responseHYJson{[weak self] response in
            self?.credit = response.dicData?["Balance"] as? String ?? CreditError
            completion(response.error)
        }
    }
    
    func checkAvailable(completion : @escaping (HYWallet?, Error?)-> Void){
        HYSession.member.authRequest(URL_HOST + "/API/MemberWithdrawNormalAudit/GetAsync?PlatformID=\(HYWallet.map[platform]!)", method: .get).validate().responseHYJson{[weak self] response in
            
            self?.isAudited = response.dicData?["AuditIsValid"] as? Bool
            self?.availableAmount = response.dicData?["CanTranAmount"] as? Double
            self?.notAvailableAmount = response.dicData?["NoTranAmount"] as? Double
            self?.platformAmount = response.dicData?["PlatformAmount"] as? Double
            completion(self, response.error)
        }
    }
    
    static let map: [String: Int] = [
        HYTransfer.codeMainWallet : 99999
        
        , HYPlayStation.Platform.BETCONSTRUCT: 77
        , HYPlayStation.Platform.OPUS_SPORT: 62
        , HYPlayStation.Platform.SB: 78
        
        , HYPlayStation.Platform.OPE_ELECTRONIC: 76
        
        , HYPlayStation.Platform.AG: 8
        , HYPlayStation.Platform.BBIN: 2
        , HYPlayStation.Platform.OG: 68
        , HYPlayStation.Platform.OPUS: 63
        , HYPlayStation.Platform.EBET: 19
        , HYPlayStation.Platform.SUMBET: 999
        
        , HYPlayStation.Platform.PT: 18
        , HYPlayStation.Platform.MG: 3
        , HYPlayStation.Platform.OPE: 35
        , HYPlayStation.Platform.BNG: 74
        
        , HYPlayStation.Platform.LiB: 9
    ]
    
}
let CreditError = "维护中"; //額度錯誤

class HYTransfer: NSObject {

    static let transfer = HYTransfer()
    
    func configure() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateBalance), name: HYFund.NotificationName, object: HYFund.fund)
        
        let wallets: [HYWallet] = [
            HYWallet(platform: HYPlayStation.Platform.OPUS_SPORT, name: "美洲体育", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_OPUS_Sport"))
            , HYWallet(platform: HYPlayStation.Platform.BETCONSTRUCT, name: "欧洲体育", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_BetConstruct"))
            , HYWallet(platform: HYPlayStation.Platform.SB, name: "亚洲体育", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_SB"))
            
            , HYWallet(platform: HYPlayStation.Platform.OPE_ELECTRONIC, name: "OPE电竞", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_OPE_Electronic"))
            
            , HYWallet(platform: HYPlayStation.Platform.AG, name: "AG真人", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_AG"))
            , HYWallet(platform: HYPlayStation.Platform.SUMBET, name: "申博真人", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_SunBet"))
            , HYWallet(platform: HYPlayStation.Platform.BBIN, name: "BBIN钱包", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_BBIN"))
            , HYWallet(platform: HYPlayStation.Platform.OPUS, name: "OPUS真人", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_OPUS"))
            , HYWallet(platform: HYPlayStation.Platform.EBET, name: "eBET真人", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_eBet"))
            , HYWallet(platform: HYPlayStation.Platform.OG, name: "OG真人", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_OG"))
            
            , HYWallet(platform: HYPlayStation.Platform.PT, name: "PT电游", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_PT"))
            , HYWallet(platform: HYPlayStation.Platform.MG, name: "MG电游", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_MG"))
            , HYWallet(platform: HYPlayStation.Platform.OPE, name: "OPE电游", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_OPE"))
            , HYWallet(platform: HYPlayStation.Platform.BNG, name: "BNG电游", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_BNG"))
            
            , HYWallet(platform: HYPlayStation.Platform.LiB, name: "LiB彩票", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_Lib"))
        ]
        for wallet in wallets{
            platformWallets[wallet.platform] = wallet
        }
        
        _ = HYAuth.auth.addAuthStateDidChangeListener{ auth, user in
            self.updateData(withUser: user){ 
                
            }
        }
    }
    
    static let codeMainWallet = "CenterWallet"
    
    var mainWallet: HYWallet = HYWallet(platform: HYTransfer.codeMainWallet, name: "中心钱包", image: #imageLiteral(resourceName: "Fund_Icon_Wallet_Main"))
    var platformWallets: [String: HYWallet] = [:]
    var errors: [Error?] = []
    
    func updateData(withUser user: HYUser?, completion : @escaping ()-> Void){
        guard let user = user else {
            return
        }
        errors = []
        let platforms = user.availablePlatforms
        let count = platformWallets.count
        for platform in platforms{
            platformWallets[platform]!.updateBalance{ [weak self] error in
                self?.errors.append(error)
                if self?.errors.count == count{
                    HYFund.fund.updateBalance{ [weak self] error in
                        self?.mainWallet.credit = HYFund.fund.strBalance ?? CreditError
                        completion()
                    }
                }
            }
        }
    }
    
    func updateWallets(completion : @escaping ()-> Void){
        updateData(withUser: HYAuth.auth.currentUser){
            completion()
        }
    }
    
    @objc func updateBalance(){
        mainWallet.credit = HYFund.fund.strBalance ?? ""
    }
    
    func transfer(from: HYWallet, to: HYWallet, amount: Double, completion:@escaping (Error?)-> Void){
        if from.platform != HYTransfer.codeMainWallet{
            transter(to: from, add: false, amount: amount){err in
                if let err = err{
                    completion(err)
                }
                else if to.platform != HYTransfer.codeMainWallet{
                    self.transter(to: to, add: true, amount: amount){ error in
                        if let e = error {
                            completion(e)
                        }else{
                            HYFund.fund.updateBalance(completion: completion)
                        }
                    }
                }else{
                    HYFund.fund.updateBalance(completion: completion)
                }
            }
        }
        else if to.platform != HYTransfer.codeMainWallet{
            self.transter(to: to, add: true, amount: amount){ error in
                if let e = error {
                    completion(e)
                }else{
                    HYFund.fund.updateBalance(completion: completion)
                }
            }
        }
    }
    
    func transter(to: HYWallet, add: Bool, amount: Double, completion: @escaping (Error?) -> Void) {
        
        let platform = to.platform
        guard platform != HYTransfer.codeMainWallet else {
            print("程式邏輯錯誤！！！")
            completion(NSError(domain: "OPE.iOS", code: 0111, userInfo: [NSLocalizedDescriptionKey : ""]))
            return
        }
        
        let parameters: [String: Any] = [
            "gameName": platform
            , "type": add ? 1 : 2
            , "credit": "\(amount)"
        ]
        
        HYSession.member.authRequest(URL_HOST + "/API/Member/TransferAsync", parameters: parameters, encoding: JSONEncode.default).responseHYJson{ [weak self] response in
            if let err = response.error{
                completion(err)
            }else{
                self!.platformWallets[platform]!.updateBalance{ _ in
                    completion(nil)
                }
            }
        }
    }
    
}
