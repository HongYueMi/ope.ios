//
//  HYBankDataForm.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/7.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

enum ImageSource{
    case formResource (name : String)
    case exitImage (image : UIImage)
    case formInternet (path : String)
}

class Item : NSObject{
    
    override init() {
        super.init()
    }
    
    init(key : String? = nil, title : String, info : String? = nil, image : ImageSource? = nil) {
        self.title = title
        self.info = info
        self.image = image
        self.key = key
    }
    
    init(title : String? = nil, info : String? = nil) {
        self.title = title
        self.info = info
    }
    
    init(title : String, info : String? = nil, image : UIImage) {
        self.title = title
        self.info = info
        self.image = ImageSource.exitImage(image: image)
    }
    
    var title : String?
    var info : String?
    var image : ImageSource?
    var key : String?
    
    var contentPath : String?
    var action : ((UIViewController) -> ())?
}

class AnswerItem: Item {
    var answer : String?
}

struct DepositGroup {
    var dictionary: NSDictionary
    init(withDictionary dictionary: NSDictionary) {
        self.dictionary = dictionary
    }
    
    var name: String{
        return dictionary["DepositGroupName"] as? String ?? ""
    }
    var id: Int{
//        let oldID = dictionary["DepositGroupID"] as? Int ?? -1
        return dictionary["DepositGroupID"] as? Int ?? -1
    }
    var code: String{
        return dictionary["DepositGroupEName"] as? String ?? ""
    }
    
//    var mapDepositGroupID: [Int: Int] = [
//        1: 1
//        , 3: 3
//        , 4: 7
//    ]
    
    init(name: String, id: Int, code: String) {
        let dic: [String: Any] = [
            "DepositGroupName": name
            , "DepositGroupID": id
            , "DepositGroupEName": code
        ]
        dictionary = NSDictionary(dictionary: dic)
    }
}

struct DepositItem {
    
    var dictionary: NSDictionary
    init(withDictionary dictionary: NSDictionary, depositGroup: DepositGroup) {
        self.dictionary = dictionary
        self.group = depositGroup
    }
    
    var group: DepositGroup
    
    var id: Int{
        return dictionary["GroupID"] as? Int ?? -1
    }
    var name: String{
        return dictionary["GroupName"] as? String ?? ""
    }
    var apiAccount: String{
        return dictionary["APIAccount"] as? String ?? ""
    }
    var apiKey: String{
        return dictionary["APIKey"] as? String ?? ""
    }
    var typeID: Int{
//        return dictionary["TypeID"] as? Int ?? -1 // 先寫死
        return group.id // 目前TypeID 先等同GroupID
    }
    
    var bankCode: String!
    var amount: Double!
    
    var lowerLimit: Int{ //金額下限
        return dictionary["SingleLowerLimit"] as? Int ?? 0
    }
    
    var uperLimit: Int{ //金額上限
        return dictionary["SingleUpperLimit"] as? Int ?? 0
    }
    
    var PayCardNumber: String!
    var PayUserName: String!
    
}

struct DepositAccount{
    
    let dictionary: NSDictionary
    
    init(withDictionary dictionary: NSDictionary) {
        self.dictionary = dictionary
    }
    
    var id: String{
        return dictionary["BankAccountID"] as? String ?? ""
    }
    var account: String{
        return dictionary["BankAccount"] as? String ?? ""
    }
    var name: String{
        return dictionary["BankAccountName"] as? String ?? ""
    }
    var bankID: String{
        return dictionary["BankID"] as? String ?? ""
    }
    var bankCode: String{
        return dictionary["BankCode"] as? String ?? ""
    }
    var bankEName: String{
        return dictionary["BankEName"] as? String ?? ""
    }
    var bankProvince: String{
        return dictionary["BankProvince"] as? String ?? ""
    }
    var bankCity: String{
        return dictionary["BankCity"] as? String ?? ""
    }
    var bankBranch: String{
        return dictionary["BankBranch"] as? String ?? ""
    }
    var bankURL: String{
        return dictionary["BankUrl"] as? String ?? ""
    }

}

struct DepositThirdPartyPaymentAdd{
    
    let dictionary: NSDictionary
    
    init(withDictionary dictionary: NSDictionary) {
        self.dictionary = dictionary
    }
    
    var data: String{
        return dictionary["Data"] as? String ?? ""
    }
    
    var id: String{
        return dictionary["BankAccountID"] as? String ?? ""
    }
    var account: String{
        return dictionary["BankAccount"] as? String ?? ""
    }
    var name: String{
        return dictionary["BankAccountName"] as? String ?? ""
    }
    var bankID: String{
        return dictionary["BankID"] as? String ?? ""
    }
    var bankCode: String{
        return dictionary["BankCode"] as? String ?? ""
    }
    var bankEName: String{
        return dictionary["BankEName"] as? String ?? ""
    }
    var bankProvince: String{
        return dictionary["BankProvince"] as? String ?? ""
    }
    var bankCity: String{
        return dictionary["BankCity"] as? String ?? ""
    }
    var bankBranch: String{
        return dictionary["BankBranch"] as? String ?? ""
    }
    var bankURL: String{
        return dictionary["BankUrl"] as? String ?? ""
    }
    
}

struct HYBankData: HYOption{
    
    let dictionary: NSDictionary
    
    init(withDictionary dictionary: NSDictionary) {
        self.dictionary = dictionary
    }
    
    var id: Int = 0;
    
    var name: String{
        return dictionary["BankName"] as? String ?? ""
    }
    var code: String{
        return dictionary["BankCode"] as? String ?? ""
    }
    var thirdPartyCode: String{
        return dictionary["ThirdPartyPaymentBankCode"] as? String ?? ""
    }
    var path: String{
        return dictionary["BankUrl"] as? String ?? ""
    }
}

struct HYPayment{
    
    let dictionary: NSDictionary
    
    init(withDictionary dictionary: NSDictionary) {
        self.dictionary = dictionary
    }

    var path: String?{
        return dictionary["Url"] as? String
    }
    var url: URL?{
        if
            let path = path
            , let strURL = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        {
            return URL(string: strURL)
        }
        return nil
    }
    
    var appURL: URL?{
        if
            let path = dictionary["qrCodeUrl"] as? String
            , let strURL = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        {
            return URL(string: strURL)
        }
        return nil
    }
    
    var qrCode: String?{
        return dictionary["qrCodeUrl"] as? String
    }
    var qrCodeUrl: URL?{
        if let path = qrCode{
            return URL(string: path)
        }
        return nil
    }
    
    var account_BQ: String{
        return BQ?["Account"] as? String ?? ""
    }
    
    var amount_BQ: String{
        return BQ?["Amount"] as? String ?? ""
    }
    
    var bankEName_BQ: String{
        return BQ?["BankEName"] as? String ?? ""
    }
    
    var memo_BQ: String{
        return BQ?["Memo"] as? String ?? ""
    }
    
    var name_BQ: String{
        return BQ?["Name"] as? String ?? ""
    }
    
    var BQ: NSDictionary?{
        return dictionary["BQ"] as? NSDictionary
    }
    
    var data: NSDictionary?{
        return dictionary["Data"] as? NSDictionary
    }
    
}

struct DepositVerify{
    
    let dictionary: NSDictionary
    
    init(withDictionary dictionary: NSDictionary) {
        self.dictionary = dictionary
    }
    
    var groupID: Int{
        return dictionary["DepositGroupID"] as? Int ?? -1
    }
    
    var isVerifying: Bool{
        return dictionary["IsVerifyingDeposit"] as? Bool ?? false
    }
    
    var type: Int{
        return dictionary["TypeID"] as? Int ?? -1
    }
    
    var sid: Int{
        return dictionary["SID"] as? Int ?? -1
    }
    
    var feedBack: Bool!
    
}
