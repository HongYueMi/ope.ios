//
//  HYPromotion.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/10.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import Alamofire

class HYPromotion: NSObject {
    
    static let center = HYPromotion()
    
    class Item {
        
        var dictionary: NSDictionary
        
        init(dictionary: NSDictionary) {
            self.dictionary = dictionary
        }
        
        var id: Int{
            return dictionary["ActivityID"] as? Int ?? -1
        }
        
        var title: String{
            return dictionary["ActivityName"] as? String ?? ""
        }
        
        var info: String{
            return dictionary["ActivityDescribe"] as? String ?? ""
        }
        
        var content: String{
            return dictionary["Content"] as? String ?? ""
        }
        
        var imagePath: String?{
            return dictionary["CellPhonePath"] as? String
        }
        
        var strStartDate: String?{
            return dictionary["Start_Date"] as? String
        }
        
        var strEndDate: String?{
            return dictionary["End_Date"] as? String
        }
        
        static let formateString = "yyyy年MM月dd日"
        private var _dateShow: String?
        var dateShow: String{
            if nil == _dateShow{
                let start = strStartDate ?? "即日起"
                let end = strEndDate ?? "待定"
                _dateShow = start + " 至 " + end
            }
            return _dateShow!
        }
        
        var type: Int{
            return dictionary["GameType"] as? Int ?? -1
        }
        
        var _numOfLike: Int?
        var numOfLike: Int{
            get{
                if nil == _numOfLike{
                    _numOfLike = dictionary["LikeNumber"] as? Int
                }
                return _numOfLike ?? 0
            }
            set{
                _numOfLike = newValue
            }
        }
        
        var platforms: [String]{
            if let str = dictionary["Sign"] as? String{
                return str.components(separatedBy: CharacterSet(charactersIn: ","))
            }
            return []
        }
        
        var status: Int{
            return dictionary["Status"] as? Int ?? -1
        }
        
        var url: URL?{
            if let path = linkPath{
                return URL(string: path)
            }
            return nil
        }
        
        var linkPath: String?{
            return dictionary["Url"] as? String
        }
        
        // from code
        
        var isLike: Bool = false
        
        var category: Category!
    }
    
    func configure(){
        _ = HYAuth.auth.addAuthStateDidChangeListener{ auth, user in
            self.reset(withUser: user)
        }
    }
    
    // MARK: - Notification
    
    static let NotificationName: Notification.Name = Notification.Name(rawValue: "HYPromotion/ListUpdate")
    
    var name: Notification.Name{
        return HYPromotion.NotificationName
    }
    
    static let keyAllPromotions = "All"
    static let NotificationKeyCategory = "Category"

    private func sendNotification(category: Category){
        NotificationCenter.default.post(name: name, object: self, userInfo: [HYPromotion.NotificationKeyCategory: category])
    }
    
    // MARK: - Data
        
    var promotions: [Category: [Item]] = [:]
    
    func reset(withUser user: HYUser?){
        promotions = [:]
        for category in HYPlayStation.Category.all{
            updateData(category: category)
        }
    }
    
    func updateData(category: Category){
        getList(category: category){ [weak self] promotions, error in
            let user = HYAuth.auth.currentUser ?? HYUser.anonymous
            for promotion in promotions{
                promotion.isLike = user.likePromotion[promotion.id] ?? false
                promotion.category = category
            }
            self?.promotions[category] = promotions
            self?.sendNotification(category: category)
        }
    }
    
    private let mapCategory: [String: Int] = [
        HYPromotion.keyAllPromotions: -1
        , HYPlayStation.Category.Sport: 4
        , HYPlayStation.Category.Electronic: 5
        , HYPlayStation.Category.Real: 2
        , HYPlayStation.Category.Slot: 1
        , HYPlayStation.Category.Lottery: 3
    ]
    
    private func getList(category: Category, completion : @escaping ([Item], Error?)-> Void){
        
        let parameters: [String: Any] = [
            "Data": ["GameType": mapCategory[category]!]
            , "Pagination": Pagination(index: 1, size: 100).toJSON()
        ]
        
        HYSession.member.authRequest(URL_HOST + "/API/Promotions/ListAsync", method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseHYJson{
            response in
            let ary: [Item] = response.aryData?.map{Item(dictionary: $0)} ?? []
            completion(ary, response.error)
        }
    }
    
    func joinPromotion(_ promotion: Item, completion : @escaping (Error?)-> Void){
        HYSession.member.authRequest( URL_HOST + "/API/Promotions/JoinAsync/\(promotion.id)", method: .get).validate().responseHYJson{ [weak self] response in
            if let e = response.error{
                completion(e)
                return
            }
            self?.updateData(category: promotion.category)
        }
    }
    
    func likePromotion(_ promotion: Item, completion : @escaping (Error?)-> Void){
        let likeType =  promotion.isLike ? "unlike" : "like"
        HYSession.member.authRequest(URL_HOST + "/API/Promotions/LikeAsync/\(promotion.id)?likeType=\(likeType)", method: .get).validate().responseHYJson{ [weak self] response in
            if let e = response.error{
                completion(e)
                return
            }
            let user = HYAuth.auth.currentUser ?? HYUser.anonymous
            user.likePromotion[promotion.id] = !promotion.isLike
            self?.updateData(category: promotion.category)
        }
    }
}
