//
//  HYBetRecord.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/11/29.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class HYBetRecord {
    
    static let center = HYBetRecord()
    
    class Item {
        
        var dictionary: NSDictionary
        
        init() {
            self.dictionary = NSDictionary()
        }
        
        init(withDictionary dictionary: NSDictionary) {
            self.dictionary = dictionary
        }
        
        var strCreatDate: String{
            return dictionary["CreateDate"] as? String ?? ""
        }
        
        var statusCode: Int?{
            return dictionary["Status"] as? Int
        }
        
        var processType: Int?{
            return dictionary["ProcessType"] as? Int
        }
        
        var creatDate: Date?{
            return DateFormatter.toHY.date(from: strCreatDate)
        }
        
        var platformName: String{
            return dictionary["PlatformName"] as? String ?? ""
        }
        
        var transSN: String{
            return dictionary["TransSN"] as? String ?? ""
        }
        
        var info: String{
            return dictionary["BetContent"] as? String ?? ""
        }
        
        var status: String{
            return dictionary["Status"] as? String ?? ""
        }
        
        var betAmount: Double{
            return dictionary["BetAmount"] as? Double ?? 0.0
        }
        
        var validBetAmount: Double{
            return dictionary["ValidBetAmount"] as? Double ?? 0.0
        }
        
        var payoutAmount: Double{
            return dictionary["PayoutAmount"] as? Double ?? 0.0
        }
        
        var winLoseAmount: Double{
            return dictionary["WinloseAmount"] as? Double ?? 0.0
        }
    }
    
    class Search {
        
        init(category: Category, platform: Platform) {
            self.category = category
            self.platform = platform
        }
        
        var category: String
        var platform: String
        var dateStart: Date?
        var dateEnd: Date?
        var page: Pagination = Pagination(index: 1, size: 100)
        
    }
    
    func configure(){
        _ = HYAuth.auth.addAuthStateDidChangeListener{ auth, user in
            self.reset(withUser: user)
        }
    }
    
    // MARK: - Notification
    
    static let NotificationName: Notification.Name = Notification.Name(rawValue: "HYBetRecord/ListUpdate")
    
    var name: Notification.Name{
        return HYBetRecord.NotificationName
    }
    
    static let NotificationKeyCategory = "Category"
    
    private func sendNotification(category: Category){
        NotificationCenter.default.post(name: name, object: self, userInfo: [HYPromotion.NotificationKeyCategory: category])
    }
    
    // MARK: - Data

    var records: [Category: [Platform: [Item]]] = [:]
    var searches: [Category: [Platform: Search]] = [:]
    
    private func reset(withUser user: HYUser?){
        records = [:]
        
        for category in HYPlayStation.Category.all{
            for platform in user?.availableCategoryPlatforms[category] ?? []{
                syncRecords(category: category, platform: platform)
            }
        }
    }
    
    func syncRecords(category: Category, platform: Platform){
        syncRecords(search: Search(category: category, platform: platform))
    }
    
    func syncRecords(search: Search){
        if nil == searches[search.category]{
            searches[search.category] = [:]
        }
        if nil == searches[search.category]![search.platform]{
            searches[search.category]![search.platform] = search
        }
        getList(search: search){ [weak self] records, error in
            guard self != nil else {return}
            if nil == self!.records[search.category]{
                self!.records[search.category] = [:]
            }
            self!.records[search.category]![search.platform] = records
            self!.sendNotification(category: search.category)
        }
    }
    
    private func getList(search: Search, completion : @escaping ([Item], Error?)-> Void){
    
        let dateEnd = search.dateEnd ?? Date()
        let dateStart = search.dateStart ?? dateEnd.addingTimeInterval(-14 * 24 * 3600 - 1)
        
        let parameters: [String: Any] = [
            "PlatformCode": search.platform
            , "PlatformName": ""
            , "GameType": search.category
            , "StartDate": dateStart.toString(format: HYDateFormat.to)
            , "EndDate": dateEnd.toString(format: HYDateFormat.to)
            , "Pagination": search.page.toJSON()
        ]
        
        HYSession.member.authRequest(URL_HOST + "/API/BetRecord/ListAsync", parameters: parameters, encoding: JSONEncode.default).validate().responseHYJson
            { response in
                let ary = response.aryData?.map{Item(withDictionary: $0)} ?? []
                completion(ary, response.error)
        }
    }
}
