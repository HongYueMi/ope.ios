//
//  HYRecord.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/29.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class HYRecord: NSObject {
    var dictionary: NSDictionary
    
    override init() {
        self.dictionary = NSDictionary()
    }
    
    init(withDictionary dictionary: NSDictionary) {
        self.dictionary = dictionary
    }
    
    var strCreatDate: String{
        return dictionary["CreateDate"] as? String ?? ""
    }
    
    var creatDate: Date?{
        var strDate = strCreatDate
        if strDate.count > 19{
            let idx = strDate.index(strDate.startIndex, offsetBy: 19)
            strDate = String(strDate[..<idx])
        }
        return DateFormatter.fromHY.date(from: strDate)
    }
    
    var statusCode: Int?{
        return dictionary["Status"] as? Int
    }
    
    var processType: Int?{
        return dictionary["ProcessType"] as? Int
    }

}

class HYTransferRecord: HYRecord {

    var logID: Int{
        return dictionary["LogID"] as? Int ?? -1
    }
    
    var paymentID: String{
        return dictionary["PaymentID"] as? String ?? ""
    }
    
    var codeFromPlatform: String{
        return dictionary["OutWallet"] as? String ?? ""
    }
    
    var codeToPlatform: String{
        return dictionary["InWallet"] as? String ?? ""
    }
    
    var beforePoint: Double{
        return dictionary["BeforePoint"] as? Double ?? 0.00
    }
    
    var changePoint: Double{
        return dictionary["ChangePoint"] as? Double ?? 0.00
    }
    
    var afterPoint: Double{
        return dictionary["AfterPoint"] as? Double ?? 0.00
    }

}

class HYDepositRecord: HYRecord {
    
    var depositTypeEName: String{
        return dictionary["DepositTypeName"] as? String ?? ""
    }
    
    static let mapDepositType = [
        "WeChatPayment": LanHYRecord.LanHYDepositRecord.mapDepositTypeA
        , "OnlinePayment": LanHYRecord.LanHYDepositRecord.mapDepositTypeB
        , "AliPay": LanHYRecord.LanHYDepositRecord.mapDepositTypeC
        , "BQ": LanHYRecord.LanHYDepositRecord.mapDepositTypeD
    ]
    
    var depositTypeName: String{
        return HYDepositRecord.mapDepositType[depositTypeEName] ?? ""
    }
    
    var amount: Double{
        return dictionary["Amount"] as? Double ?? 0.0
    }
    
    var realAmount: Double{
        return dictionary["RealAmount"] as? Double ?? 0.0
    }
    
    var preferentialAmount: Double{
        return dictionary["PreferentialAmount"] as? Double ?? 0.0
    }
    
    var bonusAmount: Double{
        return dictionary["BonusAmount"] as? Double ?? 0.0
    }
    
    var strCloseDate: String{
        return dictionary["CreateDate"] as? String ?? ""
    }
    
    var closeDate: Date?{
        var strDate = strCloseDate
        if strDate.count > 19{
            let idx = strDate.index(strDate.startIndex, offsetBy: 19)
            strDate = String(strDate[..<idx])
        }
        return DateFormatter.fromHY.date(from: strDate)
    }
    
    var deviceTypeName: String{
        return dictionary["DeviceTypeName"] as? String ?? ""
    }
    
    var deviceType: String{ //操作端 iOS Android web
        return dictionary["DeviceType"] as? String ?? ""
    }
}

class HYWithdrawalRecord: HYRecord {

    var withdrawID: Int{
        return dictionary["WithdrawID"] as? Int ?? -1
    }
    
    var amount: Double{
        return dictionary["Amount"] as? Double ?? 0.0
    }
    
    var realAmount: Double{
        return dictionary["RealAmount"] as? Double ?? 0.0
    }
    
    var bankFee: Double{
        return dictionary["BankFee"] as? Double ?? 0.0
    }
    
    var bankAccount: String{
        return dictionary["BankAccount"] as? String ?? ""
    }
    
    var deviceTypeName: String{
        return dictionary["DeviceTypeName"] as? String ?? ""
    }
    
    var canBeCancelled: Bool{
        return dictionary["IsVaildCancel"] as? Bool ?? false
    }
    
    var deviceType: String{ //操作端 iOS Android web
        return dictionary["DeviceType"] as? String ?? ""
    }
}

class HYPromotionRecord: HYRecord {
    
    var account: String{
        return dictionary["Account"] as? String ?? ""
    }
    
    var chargePoint: Double{
        return dictionary["ChangePoint"] as? Double ?? 0.0
    }
    
    var strClosedDate: String{
        return dictionary["ClosedDate"] as? String ?? ""
    }
    
    var closedDate: Date?{
        var strDate = strClosedDate
        if strDate.count > 19{
            let idx = strDate.index(strDate.startIndex, offsetBy: 19)
            strDate = String(strDate[..<idx])
        }
        return DateFormatter.fromHY.date(from: strDate)
    }
    
    var type: Int{
        return dictionary["PreferentialType"] as? Int ?? -1
    }
    
    var name: String{
        return dictionary["PreferentialTypeEName"] as? String ?? ""
    }

}

class HYOtherTransactionRecord: HYRecord {
    
    var amount: Double{
        return dictionary["Amount"] as? Double ?? 0.0
    }
    
    var strTakeDate: String{
        return dictionary["TakeDate"] as? String ?? ""
    }
    
    var takeDate: Date?{
        var strDate = strTakeDate
        if strDate.count > 19{
            let idx = strDate.index(strDate.startIndex, offsetBy: 19)
            strDate = String(strDate[..<idx])
        }
        return DateFormatter.fromHY.date(from: strDate)
    }
    
    var note: String{
        return dictionary["Note"] as? String ?? ""
    }
    
    var name: String{
        return dictionary["TypeName"] as? String ?? ""
    }
    
    var status: String{
        return dictionary["StatusEName"] as? String ?? ""
    }
    
}

class SearchItem {
    
    enum TransactionType: String {
        case deposit = "Deposit"
        case withdrawal = "Withdrawals"
        case transfer = "Transfer"
        case promotion = "Preferential"
        case other = "Others"
    }
    
    enum DurationType: String {
        case yesterday = "yesterday"
        case today = "today"
        case nearly7days = "nearly7days"
        case nearly15days = "nearly15days"
    }
    
    enum DeviceType: Int {
        case all = -1
        case web = 1
        case ios = 2
        case android = 3
    }

    var transactionType: TransactionType = .other
    var durationType: DurationType = .nearly15days
    var deviceType: DeviceType = .all

    var page: Pagination = Pagination(index: 1, size: 100)
    
    init(transactionType type: TransactionType) {
        self.transactionType = type
    }
}

struct Pagination{
    var index: Int = 1
    var size: Int = 10
    
    func toJSON() -> [String: Any]{
        return ["PageIndex": index, "PageSize": size]
    }
}

class HYRecordCenter: NSObject {
    
    static let shared = HYRecordCenter()
    
    func configure() {
        
        _ = HYAuth.auth.addAuthStateDidChangeListener{ auth, user in
            self.updateData(withUser: user){ _ in
                
            }
        }
    }
    
    func updateData(withUser user: HYUser?, completion : @escaping (Error?)-> Void){
        guard let _ = user else {
            return
        }
    }
    
    func search(searchItem search: SearchItem, completion : @escaping ([Any]?, Error?)-> Void){
        
        let parameters: [String: Any] = [
            "TransactionType": search.transactionType.rawValue
            , "DayType": search.durationType.rawValue
            , "DeviceType": search.deviceType.rawValue
            , "Pagination": search.page.toJSON()
        ]
        HYSession.member.authRequest(URL_HOST + "/API/MemberTransaction/RecordListAsync", parameters: parameters).validate().responseHYJson{
            response in
            
            var ary: [Any]? = []
            if search.transactionType == .deposit{
                ary = response.aryData?.map{HYDepositRecord(withDictionary: $0)}
            }
            else if search.transactionType == .withdrawal{
                ary = response.aryData?.map{HYWithdrawalRecord(withDictionary: $0)}
            }
            else if search.transactionType == .transfer{
                ary = response.aryData?.map{HYTransferRecord(withDictionary: $0)}
            }
            else if search.transactionType == .promotion{
                ary = response.aryData?.map{HYPromotionRecord(withDictionary: $0)}
            }
            else if search.transactionType == .other{
                ary = response.aryData?.map{HYOtherTransactionRecord(withDictionary: $0)}
            }
            completion(ary, response.error)
        }
    }
    
    func cancelRecord(_ record: HYRecord, completion : @escaping (Error?)-> Void){
        let parameters: [String: Any] = [
            "WithdrawID": (record as! HYWithdrawalRecord).withdrawID
        ]
        HYSession.member.authRequest(URL_HOST + "/API/MemberTransaction/WithdrawalsCancelAsync", parameters: parameters).validate().responseHYJson
            { response in
                completion(response.error)
        }
    }
    
}
