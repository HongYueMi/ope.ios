//
//  UserForgotPwVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/15.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class UserForgotPwVC: UIEditVC {

    @IBOutlet weak var btnSignUp : UIButton!
    @IBOutlet weak var viewSignUp: UIView!
    
    @IBOutlet weak var tfVerify: UITextField!       //輸入驗證碼
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var ctrlVerify: UIControl!
    
    @IBOutlet weak var tfUserName: UITextField!     //用戶名－账號
    
    @IBOutlet weak var lberrorMsg: UILabel!         //錯誤提醒
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerKeyBoardListener()
        
        initViewController();
        self.didCtrlRefreshVerifyCodeClick(ctrl: self.ctrlVerify);
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initViewController(){
        //navigation 使用透明clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default);
        self.navigationController?.navigationBar.shadowImage = UIImage();
        self.navigationController?.navigationBar.isTranslucent = true;
        self.navigationController?.view.backgroundColor = UIColor.clear;
        
        btnSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        btnSignUp.layer.borderWidth = 1.0; //邊框大小
        btnSignUp.layer.masksToBounds = true;
        btnSignUp.layer.cornerRadius = 25.0;
        
        viewSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        viewSignUp.layer.borderWidth = 1.0; //邊框大小
        viewSignUp.layer.masksToBounds = true;
        viewSignUp.layer.cornerRadius = 25.0;
        
    }

    var verifyItem : VerifyItem!{
        didSet{
            imgVerify.image = verifyItem!.image
        }
    }
    
    @IBAction func didCtrlRefreshVerifyCodeClick(ctrl : UIControl?){
        self.view.isUserInteractionEnabled = false
        genVerifyItem{ [weak self] item in
            self?.tfVerify.text = nil
            self?.verifyItem = item
            self?.view.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func didBtnSignUpClick(_ sender: UIButton) {
        if tfUserName.text! == "" {
            showGeneralAlert(.alert_info,lbTextA:"用户名不能为空"){ value in }
            return
        }
        
        if tfVerify.text! == "" {
            showGeneralAlert(.alert_info,lbTextA:"验证码不能为空"){ value in }
            return
        }
        
        guard let verify = self.verifyItem else {
            return
        }
        
        var item = PasswordResetItem(uid: tfUserName.text!)
        item.code = tfVerify.text!
        
        view.isUserInteractionEnabled = false
        HYMember.shared.getUserAvailableMethodToResetPassword(item: item, verify: verify){[weak self] item, error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
                    if value == 1 {
                        self?.didCtrlRefreshVerifyCodeClick(ctrl: nil)
                    }
                }
            } else{
                self?.performSegue(withIdentifier: "sendUserForgotPwSelectVC", sender: item)
                self?.view.isUserInteractionEnabled = true
            }
        }
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        if textField == tfUserName{
            if tfUserName.text == nil || tfUserName.text == "" {
                lberrorMsg.text = "用户名不能为空";
                return
            }
            
//            guard
//                let uid = tfUserName.text?.notEmptyValue?.trimmingCharacters(in: .whitespacesAndNewlines)
//                , HYRegex.UID.match(input: uid)
//                else
//            {
//                lberrorMsg.text = "用户名格式错误（6-12位英文字母以及数字的组合）";
//                return
//            }
        }
        else if textField == tfVerify{
            guard let _ = tfVerify.text?.notEmptyValue else {
                lberrorMsg.text = "验证码不能为空";
                return
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! UserForgotPwSelectVC
        vc.item = sender as! PasswordResetItem
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        lberrorMsg.text = "";
        
        if string == "" {
            return true;
        }
        
        let inverseSet = CharacterSet(charactersIn:"0123456789ABCDEFGHIJKLMNOPQRSTUVWXUZabcdefghijklmnopqrstuvwxyz").inverted;
        let components = string.components(separatedBy: inverseSet);
        let filtered = components.joined(separator: "");
        if(string != filtered) {
            return false; //如果輸入的是數字英文以外則回傳false
        }

        if textField == tfUserName , let txt = tfUserName.text , txt.count >= 12 {
            return false
        }
        
        if textField == tfVerify , let txt = tfVerify.text , txt.count >= 4 {
            return false
        }
        
        return true;
    }
    
}











































