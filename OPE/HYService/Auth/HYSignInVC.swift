//
//  HYSignInVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/25.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import Alamofire

protocol HYSignInHandler {
    func didSignIn(error: Error?, token: String?)
}

class HYSignInVC: UIViewController {
    
    @IBOutlet weak var tfUID : UITextField!;
    @IBOutlet weak var lbUID : UILabel!
    
    @IBOutlet weak var tfPassword : UITextField!;
    @IBOutlet weak var lbPassword : UILabel!
    @IBOutlet weak var btnEyePassword: UIButton!

    @IBOutlet weak var ctrlAutoLogIn : UIControl!
    @IBOutlet weak var imgCheckAutoLogIn : UIImageView!
    @IBOutlet weak var autoLogIn: UILabel!
    
    @IBOutlet weak var btnSignIn : UIButton!
    
    var labels: [UILabel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.leftBarButtonItem = genDismissBarButtonItem()
        registerKeyBoardListener()
        
        labels = [lbUID, lbPassword]
        for lb in labels{
            lb.layer.masksToBounds = true
            lb.layer.cornerRadius = 7.5
        }
        imgCheckAutoLogIn.image = ctrlAutoLogIn.isSelected ? #imageLiteral(resourceName: "Main_CheckBox_Selected") : #imageLiteral(resourceName: "Main_CheckBox")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
    }
    
    func genDismissBarButtonItem() -> UIBarButtonItem {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btn.setImage(#imageLiteral(resourceName: "Auth_Icon_Back"), for: .normal)
        btn.addTarget(self, action: #selector(didBtnCancelClick), for: .touchUpInside)
        return UIBarButtonItem(customView: btn)
    }
    
    //MARK: - Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if
            identifier == "SignUp"
            , let nav = navigationController?.presentingViewController as? NavigationController
            , nav.viewControllers.first is HYSignUpVC
        {
            nav.dismiss(animated: true)
            return false
        }
        return true
    }
    
    //MARK: - Action
    
    @IBAction func didBtnCancelClick(btn: UIButton){
        if let gestureCheckVC = self.presentingViewController as? GesturePasswordVC{
            gestureCheckVC.dismiss(animated: true)
        }else{
            MainVC.shared.dismiss(animated: true)
        }
    }
    
    @IBAction func didBtnEyeClick(btn: UIButton){
        btnEyePassword.isSelected = !btnEyePassword.isSelected
        tfPassword.isSecureTextEntry = !btnEyePassword.isSelected
    }
    
    //MARK: - 自動登入

    @IBAction func didCtrlAutoLogInClick(ctrl: UIControl){
        ctrl.isSelected = !ctrl.isSelected
        imgCheckAutoLogIn.image = ctrl.isSelected ? #imageLiteral(resourceName: "Main_CheckBox_Selected") : #imageLiteral(resourceName: "Main_CheckBox")
    }
    
    //MARK: - 登入
    
    @IBAction func didBtnSignInClick(btn: UIButton){
        hideKeyboard()

        for tf in [tfUID, tfPassword]{
            textFieldDidEndEditing(tf!)
        }

        for lb in labels{
            if lb.text?.notEmptyValue != nil{
                return
            }
        }
        
        let username : String = tfUID.text!
        let password : String = tfPassword.text!

        HYAuth.auth.signIn(uid: username, password: password){ [weak self]
            user, error in
            
            if nil != error{
                self?.showGeneralAlert(.alert_error, lbTextA:"使用者名称或密码不正确"){ value in }
            }
            else{
                // 關閉手勢密碼
                user?.removeGesture()
            }
        }
        
    }
    
    // MARK: - TextField
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tfActive = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        
        if textField == tfUID{
            guard
                let uid = tfUID.text?.notEmptyValue?.trimmingCharacters(in: .whitespacesAndNewlines)
                , HYRegex.UID.match(input: uid)
                else
            {
                lbUID.text = LanHYSignUp.lbUID_UserFormatError;
                return
            }
            lbUID.text = ""
        }
        else if textField == tfPassword{
            guard
                let password = tfPassword.text?.notEmptyValue
                , HYRegex.Password.match(input: password) else {
                lbPassword.text = LanHYSignIn.lbUID_UserPasswdError;
                return
            }
            lbPassword.text = ""
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfUID{
            tfPassword.becomeFirstResponder()
        }
        if textField == tfPassword{
            textField.resignFirstResponder()
        }
        return true
    }
    
    // MARK: - Keyboard
    
    @IBOutlet open weak var scrollView : UIScrollView?
    open weak var tfActive : UITextField?
    
    @IBAction open func hideKeyboard(){
        tfActive?.resignFirstResponder()
    }
    
    func registerKeyBoardListener(){
        let nfc = NotificationCenter.default
        nfc.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        nfc.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWasShown(ntf:Notification){
        if let kbRectValue = ntf.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue
        {
            let kbSize = kbRectValue.cgRectValue.size
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
            scrollView?.contentInset = contentInsets
            scrollView?.scrollIndicatorInsets = contentInsets
            
        }
    }
    
    @objc func keyboardWillBeHidden(ntf:Notification){
        let contentInsets = UIEdgeInsets.zero
        scrollView?.contentInset = contentInsets
        scrollView?.scrollIndicatorInsets = contentInsets
    }

}
