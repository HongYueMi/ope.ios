//
//  AgreementDialogVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/3.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class AgreementDialogVC: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textView.isScrollEnabled = true
    }
    
    @IBAction func didBtnCancelClick(btn: UIButton){
        dismissDialog(completion: nil)
    }

    func dismissDialog(completion: (() -> Void)?){
        presentingViewController?.dismiss(animated: true, completion: completion)
    }

}
