//
//  UserLoginGestureIDVC.swift
//  OPE
//
//  Created by rwt113 on 2017/10/27.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class UserLoginGestureIDVC: GesturePasswordVC {

    @IBOutlet weak var lbUserName: UILabel!
    @IBOutlet weak var btForgetGestures: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nodeLinkingView.dataSource = self
        nodeLinkingView.delegate = self
        
        _ = HYAuth.auth.addAuthStateDidChangeListener{
            auth, user in
            self.lbUserName.text = user?.uid;
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func updateData(withNode node: [UIControl]){
        let gesture = node.map{$0.tag}
        if HYAuth.auth.currentUser!.check(gesture: gesture){
            self.presentingViewController?.dismiss(animated: true)
//            self.view.window!.rootViewController?.dismiss(animated: true, completion: nil); //關閉所有在window上的view 但這滿危險的
        }
        else{
            fail(message: LanGesturePassword.fail_GesturesEror)
        }

        timerClean = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(resetGesture), userInfo: nil, repeats: false)
    }
    
    override func fail(message: String){
        updateColor(colorFail)
    }
    
    //下狠心 清除手勢密碼
    @IBAction func didBtForgetGesturesClick(_ sender: UIButton) {
        if let user = HYAuth.auth.currentUser {
            user.removeGesture();
            HYAuth.auth.forgotUser();
            performSegue(withIdentifier: "sendUserLoginMainVC", sender: nil);
        }
    }
    
    
}



































