//
//  HYSignUpVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/25.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class HYSignUpVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tfUID : UITextField!
    @IBOutlet weak var lbUID: UILabel!
    
    @IBOutlet weak var tfPassword : UITextField!
    @IBOutlet weak var lbPassword: UILabel!
    @IBOutlet weak var btnEyePassword: UIButton!
    
    @IBOutlet weak var tfPasswordConfirm : UITextField!
    @IBOutlet weak var lbPasswordConfirm: UILabel!
    @IBOutlet weak var btnEyePasswordConfirm: UIButton!
    
    @IBOutlet weak var tfUserName : UITextField!
    @IBOutlet weak var lbUserName : UILabel!
    
    @IBOutlet weak var tfEmail : UITextField!
    @IBOutlet weak var lbEmail : UILabel!
    
    @IBOutlet weak var tfPhone : UITextField!
    @IBOutlet weak var lbPhone: UILabel!
    
    @IBOutlet weak var tfVerify: UITextField!
    @IBOutlet weak var lbVerify: UILabel!
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var ctrlVerify: UIControl!

    @IBOutlet weak var ctrlAgree : UIControl!
    @IBOutlet weak var imgCheckAgree : UIImageView!
    @IBOutlet weak var lbAgree: UILabel!

    @IBOutlet weak var btnSignUp : UIButton!
    
    var labels: [UILabel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.leftBarButtonItem = genDismissBarButtonItem()
        registerKeyBoardListener()
        didCtrlRefreshVerifyCodeClick(ctrl: ctrlVerify)
        
        labels = [lbUID, lbPassword, lbPasswordConfirm, lbUserName, lbEmail, lbPhone, lbVerify, lbAgree]
        for lb in labels{
            lb.layer.masksToBounds = true
            lb.layer.cornerRadius = 7.5
        }
        
        tfUID.delegate = self
        tfPassword.delegate = self
        tfPasswordConfirm.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func genDismissBarButtonItem() -> UIBarButtonItem {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btn.setImage(#imageLiteral(resourceName: "Auth_Icon_Back"), for: .normal)
        btn.addTarget(self, action: #selector(didBtnCancelClick), for: .touchUpInside)
        return UIBarButtonItem(customView: btn)
    }
    
    //MARK: - Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "SignIn"{
            if
                let nav = self.presentingViewController as? NavigationController
                , nav.viewControllers.first is UserLoginMainVC
            {
                nav.dismiss(animated: true)
                return false
            }
        }
        return true
    }
    
    //MARK: - Action
    
    @IBAction func didBtnCancelClick(btn: UIButton){
        MainVC.shared.dismiss(animated: true)
    }
    
    @IBAction func didBtnPasswordEyeClick(btn: UIButton){
        btnEyePassword.isSelected = !btnEyePassword.isSelected
        tfPassword.isSecureTextEntry = !btnEyePassword.isSelected
    }
    
    @IBAction func didBtnPasswordConfirmEyeClick(btn: UIButton){
        btnEyePasswordConfirm.isSelected = !btnEyePasswordConfirm.isSelected
        tfPasswordConfirm.isSecureTextEntry = !btnEyePasswordConfirm.isSelected
    }
    
    @IBAction func didCtrlAgreeClick(ctrl: UIControl){
        ctrl.isSelected = !ctrl.isSelected
        imgCheckAgree.image = ctrl.isSelected ? #imageLiteral(resourceName: "Main_CheckBox_Selected") : #imageLiteral(resourceName: "Main_CheckBox")
    }
    
    @IBAction func didBtnEnterClick(btn : UIButton?){
        hideKeyboard()
        
        for tf in [tfUID, tfPassword, tfPasswordConfirm, tfUserName, tfEmail, tfPhone, tfVerify]{
            textFieldDidEndEditing(tf!)
        }
        lbAgree.text = ctrlAgree.isSelected ? nil : LanHYSignUp.lbAgree_AgreeUserProtocol
        for lb in labels{
            if lb.text?.notEmptyValue != nil{
                // 驗證碼重抓
                didCtrlRefreshVerifyCodeClick(ctrl: nil)
                return
            }
        }

        guard var item = self.signUpItem else {
            return
        }

        item.account = tfUID.text!
        item.password = tfPassword.text!
        item.name = tfUserName.text!
        item.cellPhone = tfPhone.text!
        item.email = tfEmail.text!
        item.code = tfVerify.text!
        
        btn?.isEnabled = false
        
        print("--------------------------開始註冊--------------------------")
        view.isUserInteractionEnabled = false
        
        HYMember.shared.signUp(withItem: item){[weak self] error, item in
            print("--------------------------註冊回覆：--------------------------")
            if nil != error {
                print("--------------------------註冊失敗！--------------------------")
                print(error!)
                btn?.isEnabled = true
                self?.showGeneralAlert(.alert_error, lbTextA:error!.localizedDescription){ value in
                    self?.didCtrlRefreshVerifyCodeClick(ctrl: nil)
                    self?.view.isUserInteractionEnabled = true
                }
            }
            else{
                print("--------------------------註冊成功，自動登入--------------------")
                debugPrint(item!)
                
                HYAuth.auth.signIn(uid: item!.account, password: item!.password){
                    [weak self] user, error in
                    if error != nil{
                        self?.showGeneralAlert(.alert_error, lbTextA: error!.localizedDescription){ value in }
                    }
                    else{
                        self?.showGeneralAlert(.alert_ok, lbTextA:"注册成功，已自动帮您登入"){ value in
                            if value == 1 {
                                self?.presentingViewController?.dismiss(animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Verify
    
    var signUpItem : SignUpItem!{
        didSet{
            guard signUpItem != nil else {
                return
            }
            imgVerify.image = signUpItem.verify.image
        }
    }
    
    @IBAction func didCtrlRefreshVerifyCodeClick(ctrl : UIControl?){
        
        self.signUpItem = nil
        tfVerify.text = nil
        ctrl?.isEnabled = false
        
        HYMember.shared.genSignUpItem{ item in
            self.signUpItem = item
            ctrl?.isEnabled = true
        }
    }

    // MARK: - TextField
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tfActive = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        if textField == tfUID{
            
            guard
                let uid = tfUID.text?.notEmptyValue?.trimmingCharacters(in: .whitespacesAndNewlines)
                , HYRegex.UID.match(input: uid)
                else
            {
                lbUID.text = LanHYSignUp.lbUID_UserFormatError;
                return
            }
            lbUID.text = ""
        }
        else if textField == tfPassword{
            guard
                let password = tfPassword.text?.notEmptyValue
                , HYRegex.Password.match(input: password)
                else
            {
                lbPassword.text = LanHYSignUp.lbPassword_UserPasswdFormatError;
                return
            }
            lbPassword.text = ""
        }
        else if textField == tfPasswordConfirm{
            guard let passwordConfirm = tfPasswordConfirm.text?.notEmptyValue, passwordConfirm == tfPassword.text! else {
                lbPasswordConfirm.text = LanHYSignUp.lbPasswordConfirm_AgainPasswdMustBeConsistent;
                return
            }
            lbPasswordConfirm.text = ""
        }
        else if textField == tfUserName{
            guard let userName = tfUserName.text?.notEmptyValue else {
                lbUserName.text = LanHYSignUp.lbUserName_TrueNameError;
                return
            }
            guard userName.count <= 100 else {
                lbUserName.text = LanHYSignUp.lbUserName_TrueNameCountError;
                return
            }
            lbUserName.text = ""
        }
        else if textField == tfEmail{
            guard
                let value = tfEmail.text?.notEmptyValue
                , HYRegex.Email.match(input: value)
                else
            {
                lbEmail.text = LanHYSignUp.lbEmail_EmailError;
                return
            }
            lbEmail.text = ""
        }
        else if textField == tfPhone{
            guard
                let value = tfPhone.text?.notEmptyValue
                , HYRegex.Phone.match(input: value)
                else
            {
                lbPhone.text = LanHYSignUp.lbPhone_PhoneNimberError;
                return
            }
            lbPhone.text = ""
        }
        else if textField == tfVerify{
            guard let _ = tfVerify.text?.notEmptyValue else {
                lbVerify.text = LanHYSignUp.lbVerify_VerificationCodeError;
                return
            }
            lbVerify.text = ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfUID{ //限制字數
            guard let text = tfUID.text else { return true }

            let newLength = text.count + string.count - range.length
            return newLength <= 12 // Bool
        }
        else if textField == tfPassword{
            guard let text = tfPassword.text else { return true }

            let newLength = text.count + string.count - range.length
            return newLength <= 12 // Bool
        }
        else if textField == tfPasswordConfirm{
            guard let text = tfPasswordConfirm.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 12 // Bool
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfUID{
            tfPassword.becomeFirstResponder()
        }
        else if textField == tfPassword{
            tfPasswordConfirm.becomeFirstResponder()
        }
        else if textField == tfPasswordConfirm{
            tfUserName.becomeFirstResponder()
        }
        else if textField == tfUserName{
            tfEmail.becomeFirstResponder()
        }
        else if textField == tfEmail{
            tfPhone.becomeFirstResponder()
        }
        else if textField == tfPhone{
            tfVerify.becomeFirstResponder()
        }
        return true
    }
    
    // MARK: - Keyboard
    
    @IBOutlet open weak var scrollView : UIScrollView?
    open weak var tfActive : UITextField?
    
    @IBAction open func hideKeyboard(){
        tfActive?.resignFirstResponder()
    }
    
    func registerKeyBoardListener(){
        let nfc = NotificationCenter.default
        nfc.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        nfc.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWasShown(ntf:Notification){
        if let kbRectValue = ntf.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue
        {
            let kbSize = kbRectValue.cgRectValue.size
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
            scrollView?.contentInset = contentInsets
            scrollView?.scrollIndicatorInsets = contentInsets
            
        }
    }
    
    @objc func keyboardWillBeHidden(ntf:Notification){
        let contentInsets = UIEdgeInsets.zero
        scrollView?.contentInset = contentInsets
        scrollView?.scrollIndicatorInsets = contentInsets
    }
    
}
