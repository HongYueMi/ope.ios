//
//  UserTouchIDLoginVC.swift
//  OPE
//
//  Created by rwt113 on 2017/10/26.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit
import LocalAuthentication

class UserTouchIDLoginVC: UIViewController {

    @IBOutlet weak var lbUserName: UILabel!     //用戶名稱
    @IBOutlet weak var btTouchID: UIButton!
    @IBOutlet weak var viewTouchID: UIView!
    
    @IBOutlet weak var imgCenter: UIImageView!  //中間指紋或臉部圖案
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ = HYAuth.auth.addAuthStateDidChangeListener{
            auth, user in
            self.lbUserName.text = user?.uid;
        }
        
        btTouchID.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        btTouchID.layer.borderWidth = 1.0; //邊框大小
        btTouchID.layer.masksToBounds = true;
        btTouchID.layer.cornerRadius = 15.0;
        
        viewTouchID.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        viewTouchID.layer.borderWidth = 1.0; //邊框大小
        viewTouchID.layer.masksToBounds = true;
        viewTouchID.layer.cornerRadius = 15.0;
        
        if getDeviceSystemInfo.SingletonDSI().modelName == "iPhone X" {
            btTouchID.setTitle("点击进行脸部解锁", for: .normal);
            imgCenter.image = UIImage(named:"Login_faceid_login");
        }
        
        self.startTouchID();
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        self.startTouchID();
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didBtTouchIDClick(_ sender: UIButton) {
        startTouchID();
    }
    
    func startTouchID() {
        let context = LAContext()
        if #available(iOS 9.0, *) {
            if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil) {
                if let domainState = context.evaluatedPolicyDomainState, let user = HYAuth.auth.currentUser , domainState == user.oldDomainState  {
                    context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: "请用系统ID解锁", reply: {success, error in
                        if success {
                            self.dismiss(animated: true, completion: nil);
//                            self.view.window!.rootViewController?.dismiss(animated: true, completion: nil); //關閉所有在window上的view 但這滿危險的
                        }else {
                            if let error = error as NSError? {
                                let message = self.errorMessageForLAErrorCode(errorCode: error.code ,error:error as! LAError)
                                DispatchQueue.main.async {
                                    self.alertGeneralView(message:message);
                                }
                            }
                        }
                    })
                } else {
                    print("系統指紋改變")
                    DispatchQueue.main.async {
                        let alertController = UIAlertController(title: "注意", message: "系统指纹有变更，请重新输入账号密码", preferredStyle: UIAlertControllerStyle.alert);
                        let cancelActionA = UIAlertAction(title: "确认",style: .default,handler: { action in
                            self.performSegue(withIdentifier: "sendUserLoginVC", sender: nil);
                        });
                        alertController.addAction(cancelActionA);
                        self.present(alertController, animated: true, completion: nil);
                    }
                }
            }else{
                alertGeneralView(message:"您尚未开启系统ID解锁功能，请至设定开启");
            }
        } else { //iOS 8 以下不支援
            alertGeneralView(message:"此功能需要升到iOS 9.0以上才能使用");
        }
        
    }
    
    func errorMessageForLAErrorCode(errorCode: Int, error:LAError) -> String {
        var message = ""
        
        if #available(iOS 9.0, *) {
            switch errorCode {
            case LAError.appCancel.rawValue:
                message = "认证被应用程序取消"
                
            case LAError.authenticationFailed.rawValue:
                message = "指纹认证三次失败，用户无法提供有效指纹"
                
            case LAError.invalidContext.rawValue:
                message = "密码无效"
                
            case LAError.passcodeNotSet.rawValue:
                message = "设备尚未设置密码"
                
            case LAError.systemCancel.rawValue:
                message = "身份验证被系统取消"
                
            case LAError.touchIDLockout.rawValue:
                message = "您太多次失败的尝试。"
                
            case LAError.touchIDNotAvailable.rawValue:
                message = "Touch ID在设备上不可用"
                
            case LAError.userCancel.rawValue:
                message = "您按下取消了"
                
            case LAError.userFallback.rawValue:
                message = "您选择使用输入密码"
                
            default: //"没有在Error对像上找到错误代码"
                message = error.localizedDescription
            }
        } else {
            message = "此功能需要升到iOS 9.0以上才能使用"
        }
        
        return message
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendUserLoginVC"{
            let navVC = segue.destination as? UINavigationController
            let vc = navVC?.viewControllers.first as! UserLoginVC
            vc.touchIDChange = true;
        }
    }
    
}










