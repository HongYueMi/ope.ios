//
//  HYAuth.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/23.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import Alamofire

extension UIStoryboard{
    static let Auth : UIStoryboard = UIStoryboard(name: "UserLogin", bundle: nil)
}

extension HYAuth{
    // 外部呼叫此法登入。
    @objc public func askForUserAuthorization(){
        
        let nav = UIStoryboard.Auth.instantiateInitialViewController() as! UINavigationController
        var presenting = UIApplication.shared.keyWindow?.rootViewController
        while let next = presenting?.presentedViewController {
            presenting = next
        }
        DispatchQueue.main.async {
            presenting?.present(nav, animated: true)
        }
    }
}

public typealias HYAuthResultCallback = (HYUser?, Error?) -> Void
public typealias HYAuthStateDidChangeListenerHandle = NSObjectProtocol
public typealias HYAuthStateDidChangeListenerBlock = (HYAuth, HYUser?) -> Void


/**
 當App需要User時，需要此模組
 */
public class HYAuth: RequestAdapter, RequestRetrier{
    
    //MARK: - Instance
    
    static var allAuth : [HYApp : HYAuth] = [:]
    
    public static var auth : HYAuth{
        if nil == allAuth[HYApp.app]{
            let auth = HYAuth(app: HYApp.app)
            allAuth[HYApp.app] = auth
        }
        return allAuth[HYApp.app]!
    }
    
    public static func auth(app: HYApp) -> HYAuth?{
        return allAuth[app]
    }
    
    //MARK: - Initial
    // 初始化完沒有user
    
    private let app : HYApp
    
    private init(app: HYApp) {
        self.app = app
        HYSession.member.adapter = self
        HYSession.member.retrier = self
    }
    
    //MARK: - User
    
    public var currentUser : HYUser?{
        return _currentUser
    }
    
    private var _currentUser : HYUser?{
        didSet{
            isUserInitialing = false
            isUserInitialized = true
            if _currentUser != nil{
                saveUser(user: _currentUser)
            }
            isAutoCheckAuthStatus = nil != _currentUser
            for listener in self._authStateDidChangeListeners.values{
                listener(self, _currentUser)
            }
        }
    }
    
    //MARK: User - Save

    private let KEY_USER_OBJECT = "HYAuth.KEY_USER_OBJECT"

    private func saveUser(user: HYUser?){
//        print("-- Save User: \(currentUser?.name ?? "nil") --")
        if let user = user{
            let data = NSKeyedArchiver.archivedData(withRootObject: user)
            UserDefaults.standard.set(data, forKey: KEY_USER_OBJECT)
        }else{
            UserDefaults.standard.set(nil, forKey: KEY_USER_OBJECT)
        }
    }
    
    private func restoreUserFromDisk() -> HYUser?{
//        print("User Restore !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        if let data = UserDefaults.standard.object(forKey: KEY_USER_OBJECT) as? Data, let user = NSKeyedUnarchiver.unarchiveObject(with: data) as? HYUser{
            return user
        }
        return nil
    }
    
    //MARK: User - Listener
    
    private var _authStateDidChangeListeners: [NSObject: HYAuthStateDidChangeListenerBlock] = [:]
    
    public func addAuthStateDidChangeListener(_ listener: @escaping HYAuthStateDidChangeListenerBlock) -> HYAuthStateDidChangeListenerHandle{
        let objIndex = NSObject()
        _authStateDidChangeListeners[objIndex] = listener
        defer {
            if isUserInitialized {
                listener(self, self.currentUser)
            }else if !isUserInitialing{
                initialUser()
            }
        }
        return objIndex
    }
    
    public func removeAuthStateDidChangeListener(_ listenerHandle: HYAuthStateDidChangeListenerHandle){
        if let objIndex = listenerHandle as? NSObject{
            _authStateDidChangeListeners.removeValue(forKey: objIndex)
        }
    }

    //MARK: User - Initial

    private var isUserInitialized = false
    private var isUserInitialing = false
    
    private func initialUser(){

        isUserInitialing = true

        // 取回上次使用user
        guard let user = self.restoreUserFromDisk() else {
            DispatchQueue.main.async {
                self._currentUser = nil
            }
            return
        }
        
        // 自動登入
        signIn(uid: user.uid, password: user.upw){
            user, error in
        }

    }
    
    //MARK: User - Check Alive
    private let checkAuthStatusTimeInterval: TimeInterval = 30.0
    private weak var timer : Timer?
    
    private var isAutoCheckAuthStatus: Bool = false{
        didSet{
            timer?.invalidate()
            if isAutoCheckAuthStatus{
                timer = Timer.scheduledTimer(timeInterval: checkAuthStatusTimeInterval, target: self, selector: #selector(checkCurrentUserAuthStatus), userInfo:nil, repeats: true)
            }
        }
    }
    
    @objc private func checkCurrentUserAuthStatus(){
        guard let user = self._currentUser else {return}
        
        checkAuthStatus(user: user){ [weak self] isAlive in
            if !isAlive{
                // 伺服器已登出該用戶
                DispatchQueue.main.async {
                    self?._currentUser = nil
                }
                self?.timer?.invalidate()
            }
        }
    }
    
    @objc private func checkAuthStatus(user: HYUser, next: ((Bool)-> Void)? = nil){
        HYSession.member.request(URL_HOST + "/API/Member/CheckOnlineAsync", method: .post, headers: [user.token.headerKey: user.token.headerValue]).responseHYJson{[weak self] response in
            
            if let e = response.error{
                self?.app.state = .internetError(e)
                self?.timer?.invalidate()
            }else if let result = response.extData?["CheckResult"] as? String{
                next?(result == "0")
            }else{
                self?.timer?.invalidate()
                self?.app.state = .maintenance
            }
        }
    }
    
    //MARK: - 登入、登出
    
    func signIn(uid: String, password: String, completion: HYAuthResultCallback? = nil){
        
        getUserToken(uid: uid, password: password){[weak self] token, error in
            if let e = error {
                completion?(nil, e)
            }
            else if let token = token{
                let user = HYUser(withUID: uid , withUPW: password)
                user.token = token
                user.update{ error in
                    if let e = error{
                        self?.app.state = .internetError(e)
                    }
                    DispatchQueue.main.async {
                        self?._currentUser = user
                        completion?(user, nil)
                    }
                }
            }else{
                
            }
        }
        
    }
    
    /**
     登出，不會忘記User，下次登入會在自動登入。
     */
    public func signOut(){
        DispatchQueue.main.async {
            self._currentUser = nil
        }
        HYSession.member.authRequest(URL_HOST + "/API/Member/LogoutAsync").responseHYJson{_ in}
    }
    
    /**
     忘記User
     */
    public func forgotUser(){
        saveUser(user: nil)
    }

    //MARK: - Token
    
    /**
     使用Token
     */
    public func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        if let user = currentUser{
            var urlRequest = urlRequest
            urlRequest.setValue("iOS", forHTTPHeaderField: "DeviceType")
            urlRequest.setValue("Bearer " + user.token.access, forHTTPHeaderField: "Authorization")
            return urlRequest
        }
        return urlRequest
    }
    
    /**
     取得Token
     */
    func getUserToken(uid: String, password: String, completion: @escaping (HYUser.Token?, Error?)-> Void){
        let UIScreen_Width = UIScreen.main.bounds.size.width //x螢幕寬
        let UIScreen_Height = UIScreen.main.bounds.size.height //y螢幕高
        
        let systemVersion = UIDevice.current.systemVersion; //抓手機iOS版本
        let modelName = getDeviceSystemInfo.SingletonDSI().modelName; //抓設備機型
        
        let parameters : [String: Any] = [
            "grant_type": "password"
            , "userName": uid.trimmingCharacters(in: .whitespacesAndNewlines) // 不可加空白
            , "password": password
            , "resolution": "\(Int(UIScreen_Width)),\(Int(UIScreen_Height))"
            , "deviceData": "iOS \(systemVersion);\(modelName)"
        ]
        
        HYSession.app.request(URL_HOST + "/API/Member/LoginAsync", method: .post, parameters: parameters, encoding: URLEncoding.default).validate().responseJSON{ [weak self] response in
            
            switch response.result{
            case .success(let json):
                
                guard let dictionary = json as? NSDictionary else{
                    print("Parser 錯誤")
                    completion(nil, nil)
                    self?.app.state = .maintenance
                    break
                }
                completion(HYUser.Token(withDictionary: dictionary), nil)
            case .failure(let error):
                completion(nil, error)
                self?.app.state = .internetError(error)
            }
            
        }
    }
    
    //MARK: Token 更新
    private var requestsToRetry: [RequestRetryCompletion] = []
    private let lock = NSLock()
    private var isRefreshing = false
    
    public func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
        
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401, let user = _currentUser {
            requestsToRetry.append(completion)
            
            if !isRefreshing {
                isRefreshing = true
                getUserToken(uid: user.uid, password: user.upw){[weak self] token, error in
                    var succeeded = false
                    if let token = token{
                        user.token = token
                        succeeded = true
                    }
                    self?.requestsToRetry.forEach { $0(succeeded, 0.0)}
                    self?.requestsToRetry.removeAll()
                    self?.isRefreshing = false
                }
            }
        } else {
            completion(false, 0.0)
        }
    }

}
