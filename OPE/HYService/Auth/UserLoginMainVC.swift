//
//  UserLoginMainVC.swift
//  OPE
//
//  Created by rwt113 on 2017/10/26.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer

class UserLoginMainVC: UIViewController {

    @IBOutlet weak var btUserSignin: UIButton!          //用戶登陸
    @IBOutlet weak var btUserRegistration: UIButton!    //新用戶註冊
    @IBOutlet weak var btVisitorslanding: UIButton!     //鄉民登陸
    
    @IBOutlet weak var viewUserSignin: UIView!
    @IBOutlet weak var viewUserRegistration: UIView!
    
    @IBOutlet weak var lbVersion: UILabel!              //顯示版號
    
    @IBOutlet weak var videoView: UIView!               //播放高清無碼肉片用
    var player:MPMoviePlayerController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView();
        
        lbVersion.text = getDeviceSystemInfo.SingletonDSI().version;

        guard let path = Bundle.main.path(forResource: "Login_Background", ofType:"mp4") else {
            debugPrint("video.m4v not found")
            return
        }
        player = MPMoviePlayerController(contentURL: URL(fileURLWithPath: path));
        if let player = player {
            player.view.frame = UIScreen.main.bounds
            self.videoView.addSubview(player.view)
            player.scalingMode = .aspectFill
            player.shouldAutoplay = true
            player.controlStyle = .none
            player.repeatMode = .one
            player.play()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        player?.play()

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        player?.pause()
//        player = nil;
    }
    
    func initView(){
        //navigation 使用透明clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default);
        self.navigationController?.navigationBar.shadowImage = UIImage();
        self.navigationController?.navigationBar.isTranslucent = true;
        self.navigationController?.view.backgroundColor = UIColor.clear;
        
        viewUserSignin.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        viewUserRegistration.layer.borderColor = UIColor.white.cgColor; //邊框顏色
        
        viewUserSignin.layer.borderWidth = 1.0; //邊框大小
        viewUserSignin.layer.masksToBounds = true;
        viewUserSignin.layer.cornerRadius = 25.0;
        
        viewUserRegistration.layer.borderWidth = 1.0; //邊框大小
        viewUserRegistration.layer.masksToBounds = true;
        viewUserRegistration.layer.cornerRadius = 25.0;
        
        btUserSignin.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        btUserRegistration.layer.borderColor = UIColor.white.cgColor; //邊框顏色
        
        btUserSignin.layer.borderWidth = 1.0; //邊框大小
        btUserSignin.layer.masksToBounds = true;
        btUserSignin.layer.cornerRadius = 25.0;
        
        btUserRegistration.layer.borderWidth = 1.0; //邊框大小
        btUserRegistration.layer.masksToBounds = true;
        btUserRegistration.layer.cornerRadius = 25.0;
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //鄉民登陸
    @IBAction func didBtVisitorslandingClick(_ sender: UIButton) {
        if HYAuth.auth.currentUser != nil {
            HYAuth.auth.signOut();      //只要使用鄉民湊熱鬧 一律登出狀態
            HYAuth.auth.forgotUser();   //將本機記憶體暫存的User清掉
            var rootVC = self.presentingViewController
            while let parent = rootVC?.presentingViewController {
                rootVC = parent
            }
            rootVC?.dismiss(animated: true, completion: nil)
            
//            let vc = UIStoryboard.Main.instantiateViewController(withIdentifier: "MainNavVC")
//            if nil == self.presentedViewController {
//                self.present(vc, animated: true)
//            }
            
        }else{
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil);
            }
            
        }
    }
    

}





































