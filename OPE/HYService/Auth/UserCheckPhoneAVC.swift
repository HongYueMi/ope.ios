//
//  UserCheckPhoneAVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/15.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class UserCheckPhoneAVC: UIEditVC {

    var item: PasswordResetItem!
    
    @IBOutlet weak var btnSignUp : UIButton!
    @IBOutlet weak var viewSignUp: UIView!
    
    @IBOutlet weak var btAgainPush: UIButton!   //再次發送
    @IBOutlet weak var lbCount: UILabel!        //倒數60s
    
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfVerify: UITextField!
    
    weak var timer: Timer?
    var count: Int = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerKeyBoardListener()
        initViewController();
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initViewController(){
        //navigation 使用透明clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default);
        self.navigationController?.navigationBar.shadowImage = UIImage();
        self.navigationController?.navigationBar.isTranslucent = true;
        self.navigationController?.view.backgroundColor = UIColor.clear;
        
        btnSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        btnSignUp.layer.borderWidth = 1.0; //邊框大小
        btnSignUp.layer.masksToBounds = true;
        btnSignUp.layer.cornerRadius = 25.0;
        
        viewSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        viewSignUp.layer.borderWidth = 1.0; //邊框大小
        viewSignUp.layer.masksToBounds = true;
        viewSignUp.layer.cornerRadius = 25.0;
        
    }

    //下一步
    @IBAction func didBtnSignUpClick(_ sender: UIButton) {
        hideKeyboard()
        
        guard checkVerifyInput() else {
            return
        }
        
        item.phone = tfPhone.text!
        item.code = tfVerify.text!

        view.isUserInteractionEnabled = false
        HYMember.shared.resetPasswordByCellPhoneVerify(withItem: item){[weak self] error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
                    if value == 1 {
                        self?.view.isUserInteractionEnabled = true
                    }
                }
            }
            else{
                self?.performSegue(withIdentifier: "sendUserCheckPhoneBVC", sender: self?.item)
            }
        }
        
    }
    
    //再次發送電話認證
    @IBAction func didBtAgainPushClick(_ sender: UIButton) {
        if tfPhone.text != "" {
            btAgainPush.setTitle("再次发送",for: .normal);
            getVerifyCode();
        }else{
            showGeneralAlert(.alert_info,lbTextA:"手机号码不能为空"){ value in }
        }
    }
    
    func startCounting(){
        timer?.invalidate()
        btAgainPush.isEnabled = false
        count = 60
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCount), userInfo: nil, repeats: true)
    }
    
    @objc func updateCount(){
        lbCount.text = "（\(count)s）";
        if 0 == count{
            timer?.invalidate()
            btAgainPush.isEnabled = true
        }else{
            count -= 1
        }
    }
    
    func checkVerifyInput() -> Bool{
        guard let _ = tfVerify.text?.notEmptyValue else {
            showGeneralAlert(.alert_info, lbTextA:"请输入验证码"){ value in }
            return false
        }
        return true
    }
    
    func getVerifyCode(){
        HYSession.member.authRequest(URL_HOST + "/API/MemberSecurity/SendCellPhoneVerifyCodeAsync",  parameters: ["CellPhone": tfPhone.text!], encoding: JSONEncode.default).validate().responseHYJson { response in
            if let _ = response.error{
                self.showGeneralAlert(.alert_error, lbTextA:"发送验证码失败！"){ value in }
            } else{
                self.startCounting()
            }
        }
    }
    
    func setVerifyPhoneCode(phone: String,verifyCode code: String, completion: @escaping (Error?)->Void){
        let parameters: [String: Any] = [
            "IsNew": 0
            , "CellPhone": phone
            , "VerificationCode": code
        ]
        HYSession.app.postJson(url: URL_HOST + "/API/ResetPwd/VerifyCellPhoneVerifyCodeAsync", parameters: parameters).validate().responseHYJson { response in
            if let err = response.error{
                completion(err)
            }else{
                completion(nil);
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! UserCheckPhoneBVC
        vc.item = sender as! PasswordResetItem
    }
    
}


































