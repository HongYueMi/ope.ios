//
//  UserLoginVC.swift
//  OPE
//
//  Created by rwt113 on 2017/10/26.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit
import LocalAuthentication
import AVFoundation

class UserLoginVC: UIEditVC {

    @IBOutlet weak var tfUID : UITextField!;
    @IBOutlet weak var lbUID : UILabel!
    
    @IBOutlet weak var tfPassword : UITextField!;
    @IBOutlet weak var lbPassword : UILabel!
    @IBOutlet weak var btnEyePassword: UIButton!
    
    @IBOutlet weak var ctrlAutoLogIn : UIControl!
    @IBOutlet weak var imgCheckAutoLogIn : UIImageView!
    @IBOutlet weak var autoLogIn: UILabel!
    
    @IBOutlet weak var btnSignIn : UIButton!
    @IBOutlet weak var viewSignIn: UIView!
    
    @IBOutlet weak var lbErrorMsg: UILabel!
    
    var labels: [UILabel] = []
    
    @IBOutlet weak var backItem: UIBarButtonItem!   //只有在指紋跟手勢要用到账密登入才會出現
    
    var touchIDChange:Bool = false;                 //這個值只有在Touch ID發生變動，要用戶重新輸入账密時才會用到，其他流程用不到
    
    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    var paused: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labels = [lbErrorMsg]
        initViewController()
        
    }

    
    @IBAction func backItem(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initViewController(){
        let nfc = NotificationCenter.default
        nfc.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        nfc.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        btnSignIn.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        btnSignIn.layer.borderWidth = 1.0; //邊框大小
        btnSignIn.layer.masksToBounds = true;
        btnSignIn.layer.cornerRadius = 25.0;
    
        viewSignIn.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        viewSignIn.layer.borderWidth = 1.0; //邊框大小
        viewSignIn.layer.masksToBounds = true;
        viewSignIn.layer.cornerRadius = 25.0;
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        
        if textField == tfUID{
            guard
                let uid = tfUID.text?.notEmptyValue?.trimmingCharacters(in: .whitespacesAndNewlines)
                , HYRegex.UID.match(input: uid)
                else
            {
//                lbUID.text = LanHYSignUp.lbUID_UserFormatError;
                lbErrorMsg.text = LanHYSignUp.lbUID_UserFormatError;
                return
            }
//            lbUID.text = ""
            lbErrorMsg.text = ""
        }
        else if textField == tfPassword{
            guard
                let password = tfPassword.text?.notEmptyValue
                , HYRegex.Password.match(input: password) else {
//                    lbPassword.text = LanHYSignIn.lbUID_UserPasswdError;
                    lbErrorMsg.text = LanHYSignIn.lbUID_UserPasswdError;
                    return
            }
//            lbPassword.text = ""
            lbErrorMsg.text = ""
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true;
        }
        
        if textField == tfUID , let txt = tfUID.text , txt.count >= 12 {
            return false
        }
        
        if textField == tfPassword , let txt = tfPassword.text , txt.count >= 12 {
            return false
        }
        
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfUID{
            tfPassword.becomeFirstResponder()
        }
        if textField == tfPassword{
            textField.resignFirstResponder()
        }
        return true
    }
    
    @IBAction func didBtnEyeClick(btn: UIButton){
        btnEyePassword.isSelected = !btnEyePassword.isSelected
        tfPassword.isSecureTextEntry = !btnEyePassword.isSelected
    }
    
    @IBAction func didBtnDeleteClick(_ sender: UIButton) {
        tfUID.text = nil;
    }
    
    @IBAction func didBtnSignInClick(btn: UIButton){
        hideKeyboard()
        
        for tf in [tfUID, tfPassword]{
            textFieldDidEndEditing(tf!)
        }
        
        for lb in labels{
            if lb.text?.notEmptyValue != nil{
                return
            }
        }
        
        let username : String = tfUID.text!
        let password : String = tfPassword.text!
        self.newLoadingScreenView.showActivityIndicator(uiView: self.view , rootPower: false);  //開啟Londing畫面
        HYAuth.auth.signIn(uid: username, password: password){ [weak self]
            user, error in
            self?.newLoadingScreenView.hideActivityIndicator(uiView: (self?.view)!);    //關閉Londing畫面
            if nil != error{
                self?.showGeneralAlert(.alert_error, lbTextA:"使用者名称或密码不正确"){ value in }
            }else{
                if self?.touchIDChange == true , let user = HYAuth.auth.currentUser {
                    let context = LAContext();
                    if #available(iOS 9.0, *) {
                        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil) {
                            user.oldDomainState = context.evaluatedPolicyDomainState!;
                        }
                    }
                }
//                self?.dismiss(animated: true, completion: nil)
                self?.presentingViewController?.dismiss(animated: true)
//                var rootVC = self?.presentingViewController;
//                while let parent = rootVC?.presentingViewController {
//                    rootVC = parent;
//                }
//                rootVC?.dismiss(animated: true, completion: nil);
                
                
            }
        }
        
    }
    
    
}































