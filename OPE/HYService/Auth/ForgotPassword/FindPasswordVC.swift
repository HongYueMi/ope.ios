//
//  FindPasswordVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/6.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class FindPasswordVC: UIViewController {

    @IBOutlet weak var btnFindByEmail: UIButton!
    @IBOutlet weak var btnFindByCellPhone: UIButton!
    @IBOutlet weak var btnFindByCustomerService: UIButton!
    
    @IBOutlet weak var constrainBtnFindByEmailHeight: NSLayoutConstraint!
    @IBOutlet weak var constrainBtnFindByCellPhoneHeight: NSLayoutConstraint!
    @IBOutlet weak var constrainBtnFindByCustomerServiceHeight: NSLayoutConstraint!

    
    var item: PasswordResetItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnFindByEmail.tag = 1
        btnFindByCellPhone.tag = 2
        
        let constrains = [constrainBtnFindByEmailHeight, constrainBtnFindByCellPhoneHeight, constrainBtnFindByCustomerServiceHeight]
        
        for (idx, constrain) in constrains.enumerated(){
            var constant: CGFloat = 0.0
            for type in item.types{
                if let type = type, idx == type - 1{
                    constant = 45.0
                    break
                }
            }
            constrain?.constant = constant
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FindPasswordByCellPhone"{
            let vc = segue.destination as! FindPasswordByCellPhoneVC
            vc.item = sender as! PasswordResetItem
        }
    }
    
    // MARK: - Action
    
    @IBAction func didBtnEnterClick(btn: UIButton){
        item.selectedType = btn.tag
        view.isUserInteractionEnabled = false
        HYMember.shared.resetPassword(withItem: item){[weak self] item, error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
        if value == 1 {
            self?.view.isUserInteractionEnabled = true
        }
     }
            }
            else if item.selectedType == 1{
                self?.showGeneralAlert(.alert_info, lbTextA:"系统已经发送密码到" + item.email + "，请登入信箱查收"){ value in
                    self?.navigationController?.popToRootViewController(animated: true)
                }
            }
            else if item.selectedType == 2{
                self?.performSegue(withIdentifier: "FindPasswordByCellPhone", sender: item)
            }
        }
    }
}
