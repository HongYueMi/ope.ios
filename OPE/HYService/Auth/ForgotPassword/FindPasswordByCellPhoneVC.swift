//
//  FindPasswordByCellPhoneVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/6.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class FindPasswordByCellPhoneVC: UIEditVC {

    var item: PasswordResetItem!
    
    @IBOutlet weak var tfPhone : UITextField!
    @IBOutlet weak var lbPhone: UILabel!
    
    @IBOutlet weak var tfVerify: UITextField!
    @IBOutlet weak var lbVerify: UILabel!
    @IBOutlet weak var btnGetVerifyCode: UIButton!
    
    var labels: [UILabel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        labels = [lbPhone, lbVerify]
        for lb in labels{
            lb.layer.masksToBounds = true
            lb.layer.cornerRadius = 7.5
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! ResetNewPasswordVC
        vc.item = sender as! PasswordResetItem
    }
    
    // MARK: - Count
    
    @IBOutlet weak var lbCount: UILabel!
    weak var timer: Timer?
    var count: Int = 60
    func startCounting(){
        timer?.invalidate()
        btnGetVerifyCode.isEnabled = false
        count = 60
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCount), userInfo: nil, repeats: true)
    }
    
    @objc func updateCount(){
        lbCount.text = "\(count)"
        if 0 == count{
            timer?.invalidate()
            btnGetVerifyCode.isEnabled = true
        }else{
            count -= 1
        }
    }
    
    // MARK: - Action
    
    @IBAction func didBtnGetVerifyCodeClick(btn: UIButton){
        hideKeyboard()
        
        textFieldDidEndEditing(tfPhone)
        guard let phone = tfPhone.text?.notEmptyValue else{return}
        view.isUserInteractionEnabled = false
        
        HYMember.shared.sendVerifyCode(toPhone: phone){ [weak self] error in
            if let _ = error{
                self?.showGeneralAlert(.alert_error, lbTextA:"发送验证码失败！"){ value in
                    self?.view.isUserInteractionEnabled = true
                }
            }
            else{
                self?.view.isUserInteractionEnabled = true
                self?.startCounting()
            }
        }
    }
    
    @IBAction func didBtnEnterClick(btn : UIButton?){
        hideKeyboard()
        
        for tf in [tfPhone, tfVerify]{
            textFieldDidEndEditing(tf!)
        }
        
        for lb in labels{
            if lb.text?.notEmptyValue != nil{
                return
            }
        }

        item.phone = tfPhone.text!
        item.code = tfVerify.text!
        
        view.isUserInteractionEnabled = false
        HYMember.shared.resetPasswordByCellPhoneVerify(withItem: item){[weak self] error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
        if value == 1 {
            self?.view.isUserInteractionEnabled = true
        }
     }
            }
            else{
                self?.performSegue(withIdentifier: "ResetNewPassword", sender: self?.item)
            }
        }
    }
    
    // MARK: - Input
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        if textField == tfPhone{
            guard
                let value = tfPhone.text?.notEmptyValue
                , HYRegex.Phone.match(input: value)
                else
            {
                lbPhone.text = LanFindPasswordByCellPhone.lbPhone_PhoneNimberError;
                return
            }
            lbPhone.text = ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfPhone{
            tfVerify.becomeFirstResponder()
        }
        else if textField == tfVerify{
            didBtnEnterClick(btn: nil)
        }
        return true
    }
}
