//
//  ForgotPasswordVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/6.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIEditVC {
  
    @IBOutlet weak var tfUID : UITextField!
    @IBOutlet weak var lbUID: UILabel!
    
    @IBOutlet weak var tfVerify: UITextField!
    @IBOutlet weak var lbVerify: UILabel!
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var ctrlVerify: UIControl!
    
    var labels: [UILabel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        didCtrlRefreshVerifyCodeClick(ctrl: ctrlVerify)
        
        labels = [lbUID, lbVerify]
        for lb in labels{
            lb.layer.masksToBounds = true
            lb.layer.cornerRadius = 7.5
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! FindPasswordVC
        vc.item = sender as! PasswordResetItem
    }
    
    // MARK: - Action
    
    @IBAction func didBtnEnterClick(btn : UIButton?){
        hideKeyboard()
        
        for tf in [tfUID, tfVerify]{
            textFieldDidEndEditing(tf!)
        }
        
        for lb in labels{
            if lb.text?.notEmptyValue != nil{
                didCtrlRefreshVerifyCodeClick(ctrl: ctrlVerify)
                return
            }
        }
        
        guard let verify = self.verifyItem else {
            return
        }
        
        var item = PasswordResetItem(uid: tfUID.text!)
        item.code = tfVerify.text!
        
        view.isUserInteractionEnabled = false
        HYMember.shared.getUserAvailableMethodToResetPassword(item: item, verify: verify){[weak self] item, error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
                    self?.didCtrlRefreshVerifyCodeClick(ctrl: nil)
                }
            }
            else{
                self?.performSegue(withIdentifier: "FindPasswordChooseMethod", sender: item)
                self?.view.isUserInteractionEnabled = true
            }
        }
    }

    // MARK: - Verify
    
    var verifyItem : VerifyItem!{
        didSet{
            imgVerify.image = verifyItem!.image
        }
    }
    
    @IBAction func didCtrlRefreshVerifyCodeClick(ctrl : UIControl?){
        self.view.isUserInteractionEnabled = false
        genVerifyItem{ [weak self] item in
            self?.tfVerify.text = nil
            self?.verifyItem = item
            self?.view.isUserInteractionEnabled = true
        }
    }
    
    // MARK: - Input
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        if textField == tfUID{
            guard
                let uid = tfUID.text?.notEmptyValue?.trimmingCharacters(in: .whitespacesAndNewlines)
                , HYRegex.UID.match(input: uid)
                else
            {
                lbUID.text = LanForgotPassword.lbUID_UserFormatError;
                return
            }
            lbUID.text = ""
        }
        else if textField == tfVerify{
            guard let _ = tfVerify.text?.notEmptyValue else {
                lbVerify.text = LanForgotPassword.lbVerify_VerificationCodeError;
                return
            }
            lbVerify.text = ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    fileprivate func extractedFunc() {
        didBtnEnterClick(btn: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfUID{
            tfVerify.becomeFirstResponder()
        }
        else if textField == tfVerify{
            extractedFunc()
        }
        return true
    }
    
}
