//
//  ResetNewPasswordVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/6.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class ResetNewPasswordVC: UIEditVC {

    @IBOutlet weak var tfPassword : UITextField!
    @IBOutlet weak var lbPassword: UILabel!
    @IBOutlet weak var btnEyePassword: UIButton!
    
    @IBOutlet weak var tfPasswordConfirm : UITextField!
    @IBOutlet weak var lbPasswordConfirm: UILabel!
    @IBOutlet weak var btnEyePasswordConfirm: UIButton!
    
    var item: PasswordResetItem!

    var labels: [UILabel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labels = [lbPassword, lbPasswordConfirm]
        for lb in labels{
            lb.layer.masksToBounds = true
            lb.layer.cornerRadius = 7.5
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Action
    
    @IBAction func didBtnPasswordEyeClick(btn: UIButton){
        btnEyePassword.isSelected = !btnEyePassword.isSelected
        tfPassword.isSecureTextEntry = !btnEyePassword.isSelected
    }
    
    @IBAction func didBtnPasswordConfirmEyeClick(btn: UIButton){
        btnEyePasswordConfirm.isSelected = !btnEyePasswordConfirm.isSelected
        tfPasswordConfirm.isSecureTextEntry = !btnEyePasswordConfirm.isSelected
    }
    
    @IBAction func didBtnEnterClick(btn : UIButton?){
        hideKeyboard()
        
        for tf in [tfPassword, tfPasswordConfirm]{
            textFieldDidEndEditing(tf!)
        }
        
        for lb in labels{
            if lb.text?.notEmptyValue != nil{
                return
            }
        }
        
        item.password = tfPassword.text!
        
        view.isUserInteractionEnabled = false
        HYMember.shared.resetPasswordNewPassword(withItem: item){[weak self] error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
        if value == 1 {
            self?.view.isUserInteractionEnabled = true
        }
     }
            }
            else{
                self?.showGeneralAlert(.alert_ok, lbTextA:"已设定新密码"){ value in
                    self?.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    // MARK: - Input
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        if textField == tfPassword{
            guard
                let password = tfPassword.text?.notEmptyValue
                , HYRegex.Password.match(input: password)
                else
            {
                lbPassword.text = LanResetNewPassword.lbPassword_UserPasswdFormatError;
                return
            }
            lbPassword.text = ""
        }
        else if textField == tfPasswordConfirm{
            guard let passwordConfirm = tfPasswordConfirm.text?.notEmptyValue, passwordConfirm == tfPassword.text! else {
                lbPasswordConfirm.text = LanResetNewPassword.lbPasswordConfirm_AgainPasswdMustBeConsistent;
                return
            }
            lbPasswordConfirm.text = ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfPassword{
            tfPasswordConfirm.becomeFirstResponder()
        }
        else if textField == tfPasswordConfirm{
            didBtnEnterClick(btn: nil)
        }
        return true
    }

}
