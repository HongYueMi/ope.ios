//
//  UserCheckPhoneBVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/15.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class UserCheckPhoneBVC: UIEditVC {

    var item: PasswordResetItem!
    
    @IBOutlet weak var btnSignUp : UIButton!
    @IBOutlet weak var viewSignUp: UIView!
    
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfPasswordAgain: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerKeyBoardListener()
        initViewController();
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initViewController(){
        //navigation 使用透明clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default);
        self.navigationController?.navigationBar.shadowImage = UIImage();
        self.navigationController?.navigationBar.isTranslucent = true;
        self.navigationController?.view.backgroundColor = UIColor.clear;
        
        btnSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        btnSignUp.layer.borderWidth = 1.0; //邊框大小
        btnSignUp.layer.masksToBounds = true;
        btnSignUp.layer.cornerRadius = 25.0;
        
        viewSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        viewSignUp.layer.borderWidth = 1.0; //邊框大小
        viewSignUp.layer.masksToBounds = true;
        viewSignUp.layer.cornerRadius = 25.0;
        
    }

    //下一步 - 開啟括約肌的奇幻歷程
    @IBAction func didBtnSignUpClick(_ sender: UIButton) {
        if tfPassword.text! != tfPasswordAgain.text! {
            showGeneralAlert(.alert_info,lbTextA:"密码与确认密码不一致"){ value in }
            return
        }
        
        item.password = tfPassword.text!
        
        view.isUserInteractionEnabled = false
        HYMember.shared.resetPasswordNewPassword(withItem: item){[weak self] error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
                    if value == 1 {
                        self?.view.isUserInteractionEnabled = true
                    }
                }
            }
            else{
                self?.showGeneralAlert(.alert_ok, lbTextA:"已设定新密码"){ value in
                    if value == 1 {
                        self?.navigationController?.popToRootViewController(animated: true)
                    }
                }
            }
        }
        
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        if textField == tfPassword{
            guard
                let password = tfPassword.text?.notEmptyValue
                , HYRegex.Password.match(input: password)
                else
            {
                showGeneralAlert(.alert_info,lbTextA:"密码格式错误（6-12位必须含有字母和数字的组合）"){ value in }
                return
            }
        }else if textField == tfPasswordAgain{
            guard
                let value = tfPasswordAgain.text?.notEmptyValue
                , HYRegex.Password.match(input: value)
                else
            {
                showGeneralAlert(.alert_info,lbTextA:"密码格式错误（6-12位必须含有字母和数字的组合）"){ value in }
                return
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
}




























