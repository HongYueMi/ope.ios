//
//  GesturePasswordVC.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/3.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class GesturePasswordBtn: UIButton{
    
    var selectedColor: UIColor = .red{
        didSet{
            setNeedsDisplay()
        }
    }
    var normalColor: UIColor = .white
    
    override func awakeFromNib() {
        layer.cornerRadius = bounds.width / 2
        layer.borderColor = normalColor.cgColor
        layer.borderWidth = 12
    }
    var _isSelected: Bool = false
    override var isSelected: Bool{
        get{
            return _isSelected
        }
        set{
            _isSelected = newValue
            self.setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        let color = isSelected ? selectedColor.cgColor : normalColor.cgColor
        let context:CGContext = UIGraphicsGetCurrentContext()!
        context.setStrokeColor(color)
        context.setFillColor(color)
        if isSelected{
            let r = rect.width / 8
            let center = CGPoint(x: rect.size.width / 2, y: rect.size.height / 2)
            let centerCircle = CGRect(x: center.x - r, y: center.y - r, width: 2 * r, height: 2 * r)
            context.addEllipse(in: centerCircle)
            context.fillPath()
        }
        layer.borderColor = color
    }
}

class GesturePasswordVC: UIViewController, HYNodeLinkingViewDataSource, HYNodeLinkingViewDelegate {

    enum UseCase {
        case add
        case modify
        case check
        case remove
    }
    
    var useCase: UseCase = .check

    @IBInspectable var colorSuccess: UIColor = .blue
    @IBInspectable var colorFail: UIColor = .red
    @IBInspectable var colorNormal: UIColor = UIColor(red: 0/255, green: 203/255, blue: 192/255, alpha: 1)
    
    @IBOutlet weak var lbTitle: UILabel!

    @IBOutlet weak var nodeLinkingView: HYNodeLinkingView!
    
    @IBOutlet var aryNodes: [UIControl] = []

    @IBOutlet weak var btRestGesture: UIBarButtonItem!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nodeLinkingView.dataSource = self
        nodeLinkingView.delegate = self
        
        switch useCase {
        case .add:
            lbTitle.text = LanGesturePassword.lbTitle_DrawGestures
        case .modify:
            lbTitle.text = LanGesturePassword.lbTitle_OldGesture
        case .check: break
//            lbTitle.text = "请输入手势密码"
        case .remove:
            btRestGesture.isEnabled = false;
            lbTitle.text = "请输入手势密码"
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var passwordFirstTime: [Int] = []

    //重新繪製
    @IBAction func didBtRestGestureClick(_ sender: UIBarButtonItem) {
        HYAuth.auth.currentUser!.removeGesture();
        fail(message: "绘制解锁图案")
        passwordFirstTime = []
        lbTitle.text = "绘制解锁图案"
    }
    
    func updateData(withNode node: [UIControl]){
        let gesture = node.map{$0.tag}
        
        switch useCase {
        case .add:
            if gesture.count < 5{
                fail(message: "密码长度不足")
            }
         
            else if passwordFirstTime.count <= 0 { // 第一次繪製
                passwordFirstTime = gesture
                lbTitle.text = LanGesturePassword.lbTitle_AgainGestureCheck
            }
                
            else if gesture == passwordFirstTime{ // 第二次繪製 相同
                HYAuth.auth.currentUser!.addGesture(gesture)
                navigationController?.popViewController(animated: true)
                return
            }
                
            else{ // 第二次繪製 不相同
                fail(message: LanGesturePassword.fail_GesturesEror)
            }
            
        case .modify:
            if HYAuth.auth.currentUser!.check(gesture: gesture){
                lbTitle.text = LanGesturePassword.lbTitle_DrawGestures
                useCase = .add
            }
            else{
                fail(message: LanGesturePassword.fail_GesturesEror)
            }
            
        case .check:
            if HYAuth.auth.currentUser!.check(gesture: gesture){
                presentingViewController?.dismiss(animated: true)
            }
            else{
                fail(message: LanGesturePassword.fail_GesturesEror)
            }
        case .remove:
            if HYAuth.auth.currentUser!.check(gesture: gesture){
                HYAuth.auth.currentUser!.removeGesture()
                navigationController?.popViewController(animated: true)
            }
            else{
                fail(message: LanGesturePassword.fail_GesturesEror)
            }
        }
        
        timerClean = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(resetGesture), userInfo: nil, repeats: false)
    }
    
    var timerClean: Timer?
    
    func updateColor(_ color: UIColor){
        for node in aryNodes{
            (node as! GesturePasswordBtn).selectedColor = color
        }
        nodeLinkingView.lineColor = color
    }
    
    func fail(message: String){
        lbTitle.text = message
        updateColor(colorFail)
    }
    
    @objc func resetGesture(){
        updateColor(colorNormal)
        nodeLinkingView.resetGesture()
    }
    
    // Delegate
    
    func willGestureBegin(){
        timerClean?.invalidate()
        updateColor(colorNormal)
    }
    
    func didGestureComplete(_ nodes: [UIControl]){
        updateData(withNode: nodes)
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        return .portrait
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .portrait
    }

}
