//
//  UserRegisterPhoneVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/13.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class UserRegisterPhoneVC: UIEditVC {

    @IBOutlet weak var btnSignUp : UIButton!
    @IBOutlet weak var viewSignUp: UIView!
    
    @IBOutlet weak var tfPhone: UITextField!
    
    @IBOutlet weak var tfPassword: UITextField!
    
    @IBOutlet weak var btnEyePassword: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initViewController();
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initViewController(){
        //navigation 使用透明clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default);
        self.navigationController?.navigationBar.shadowImage = UIImage();
        self.navigationController?.navigationBar.isTranslucent = true;
        self.navigationController?.view.backgroundColor = UIColor.clear;
        
        let nfc = NotificationCenter.default
        nfc.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        nfc.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        btnSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        btnSignUp.layer.borderWidth = 1.0; //邊框大小
        btnSignUp.layer.masksToBounds = true;
        btnSignUp.layer.cornerRadius = 25.0;
        
        viewSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        viewSignUp.layer.borderWidth = 1.0; //邊框大小
        viewSignUp.layer.masksToBounds = true;
        viewSignUp.layer.cornerRadius = 25.0;
    }

    @IBAction func didBtnPasswordEyeClick(btn: UIButton){
        btnEyePassword.isSelected = !btnEyePassword.isSelected
        tfPassword.isSecureTextEntry = !btnEyePassword.isSelected
    }
    
    @IBAction func didBtnClearUIDClick(_ sender: Any) {
        tfPhone.text = "";
    }
    
    @IBAction func didBtnSignUpClick(_ sender: UIButton) {
        guard checkPhoneInput(), checkPasswordInput() else {
            return
        }
        btnSignUp.isEnabled = false
        checkPhone(toPhone: tfPhone.text!){ [weak self] error in
            self?.btnSignUp.isEnabled = true
            if(error != nil){
                self?.showGeneralAlert(.alert_info,lbTextA:"手机号码格式错误（仅支持中国大陆手机号码）"){ value in }
            }else{
                self?.performSegue(withIdentifier: "sendBindPhoneNumberVC", sender: nil);
            }
        }
        
    }
    
    func checkPhoneInput() -> Bool{
        guard let phone = tfPhone.text?.notEmptyValue else {
            showGeneralAlert(.alert_info,lbTextA:"手机号码不能是空的"){ value in }
            return false
        }
        guard HYRegex.Phone.match(input: phone)
            else
        {
            showGeneralAlert(.alert_info,lbTextA:"手机号码格式错误（仅支持中国大陆手机号码）"){ value in }
            return false
        }
        return true
    }
    
    func checkPasswordInput() -> Bool{
        guard let _ = tfPassword.text?.notEmptyValue else {
            showGeneralAlert(.alert_info,lbTextA:"密码不能是空的"){ value in }
            return false
        }
        return true
    }
    
    public func checkPhone(toPhone: String, completion: @escaping (Error?)->Void){
        let parameters: [String: Any] = ["CellPhone": toPhone]
        HYSession.app.postJson(url: URL_HOST + "/API/Member/CellPhoneDuplicateAsync", parameters: parameters).validate().responseHYJson { response in
            
            if let err = response.error{
                completion(err)
            }else{
                completion(nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendBindPhoneNumberVC"{
            let vc = segue.destination as! BindPhoneNumberVC;
            vc.userPhone = tfPhone.text!
            vc.userPassword = tfPassword.text!
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if string == "" {
            return true;
        }
        
        if textField == tfPhone , let txt = tfPhone.text , txt.count >= 11 {
            return false
        }
        
        if textField == tfPassword , let txt = tfPassword.text , txt.count >= 12 {
            return false
        }
        
        if textField == tfPassword {
            HYRegex.Phone.match(input: string)
        }
        
        return true;
    }
    
}






























