//
//  HYNodeLinkingView.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/4.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

protocol HYNodeLinkingViewDataSource: class {
    var aryNodes: [UIControl]{get}
}

protocol HYNodeLinkingViewDelegate: class {
    func willGestureBegin()
    func didGestureComplete(_ nodes: [UIControl])
}

class HYNodeLinkingView: UIView{
    
    weak var dataSource: HYNodeLinkingViewDataSource!
    weak var delegate: HYNodeLinkingViewDelegate?
    
    var aryNodeInTouchedOrder: [UIControl] = []
    var isGestureCompleted: Bool = false
    var pointEnd: CGPoint!
    
    // MARK: - Touch
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        resetGesture()
        delegate?.willGestureBegin()
        
        guard let pointTouched = touches.first?.location(in: self) else{
            return
        }
        
        // 看touch是否在node
        for node in dataSource.aryNodes{
            guard node.frame.contains(pointTouched) else {
                continue
            }
            // touch在node上，判斷node是否未加入
            if !node.isSelected {
                node.isSelected = true
                aryNodeInTouchedOrder.append(node)
            }
            break
        }
        pointEnd = pointTouched
        setNeedsDisplay()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let pointTouched = touches.first?.location(in: self) else{
            return
        }
        
        // 看touch是否在node
        for node in dataSource.aryNodes{
            guard node.frame.contains(pointTouched) else {
                continue
            }
            // touch在node上，判斷node是否未加入
            if !node.isSelected{
                node.isSelected = true
                aryNodeInTouchedOrder.append(node)
            }
            break
        }
        pointEnd = pointTouched
        setNeedsDisplay()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        isGestureCompleted = true
        delegate?.didGestureComplete(aryNodeInTouchedOrder)
        setNeedsDisplay()
    }
    
    //MARK: - Draw
    
    @IBInspectable var lineColor: UIColor = .white{
        didSet{
            guard lineColor != oldValue else {
                return
            }
            setNeedsDisplay()
        }
    }
    @IBInspectable var lineWidth: CGFloat = 4.0{
        didSet{
            guard lineWidth != oldValue else {
                return
            }
            setNeedsDisplay()
        }
    }
    
    func resetGesture(){
        isGestureCompleted = false
        for node in dataSource.aryNodes{
            node.isSelected = false
        }
        aryNodeInTouchedOrder = []
        setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        guard aryNodeInTouchedOrder.count > 1 else {
            return
        }
        
        for idx in 0..<aryNodeInTouchedOrder.count - 1{
            drawLine(
                from: aryNodeInTouchedOrder[idx].center
                , to: aryNodeInTouchedOrder[idx + 1].center
            )
        }
        guard !isGestureCompleted else {
            return
        }
        drawLine(
            from: aryNodeInTouchedOrder.last!.center
            , to: pointEnd
        )
    }
    
    func drawLine(from: CGPoint, to: CGPoint){
        let context: CGContext = UIGraphicsGetCurrentContext()!
        context.setStrokeColor(lineColor.cgColor)
        context.setLineWidth(lineWidth)
        context.move(to: from)
        context.addLine(to: to)
        context.strokePath()
    }
}
