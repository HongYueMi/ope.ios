//
//  HYUser.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/24.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

public class HYUser: NSObject, NSCoding {
    
    static var anonymous: HYUser = {
        return HYUser(withUID: "anonymous", withUPW: "anonymous")
    }()
    
    //MARK: - Init
    
    public let uid: String
    public let upw: String //密碼
    var dictionary : NSDictionary!
    
    init(withUID uid: String ,withUPW upw: String) {
        self.uid = uid
        self.upw = upw
    }
    
    // 註冊時間
    public var strRegisterDate: String{
        return dictionary["CreateDate"] as? String ?? ""
    }
    
    public var registerDate : Date?{
        var strDate = strRegisterDate
        if strDate.count > 19{
            let idx = strDate.index(strDate.startIndex, offsetBy: 19)
            strDate = String(strDate[..<idx])

        }
        print(strDate)
        return DateFormatter.fromHY.date(from: strDate)
    }
    
    // VIP
    
    public var vipLevel: Int{
        return dictionary["VIPLevel"] as? Int ?? 0
    }
    
    //MARK: - Token
    
    struct Token {
        
        var access: String{
            return dictionary["access_token"] as? String ?? ""
        }
        var type: String{
            return dictionary["token_type"] as? String ?? ""
        }
        var expires: Int{
            return dictionary["expires_in"] as? Int ?? Int.max
        }
        var refresh: String{
            return dictionary["refresh_token"] as? String ?? ""
        }
        var dateIssued: String{
            return dictionary[".issued"] as? String ?? ""
        }
        var dateExpires: String{
            return dictionary[".expires"] as? String ?? ""
        }
        
        var dictionary : NSDictionary
        
        init(withDictionary dictionary: NSDictionary) {
            self.dictionary = dictionary
        }
        
        var headerKey = "Authorization"
        var headerValue: String{
            return "Bearer " + access
        }
    }
    
    var token : Token!
    private let lock = NSLock()

    func updateToken(token: Token){
        lock.lock(); defer {lock.unlock()}
        self.token = token
    }
    
    //MARK: - State
    
    public var isAnonymous: Bool{
        return dictionary[""] as! Bool
    }
    
    //MARK: - Securty Data
    
    //電子郵件
    public var secretEmail: String{
        
        var email = self.email
        var components = email.components(separatedBy: "@")
        let firstPart = components.removeFirst()
        let count = firstPart.count / 2 + 1
        var starts = ""
        for _ in 0..<count{
            starts += "*"
        }
        let range = firstPart.index(firstPart.endIndex, offsetBy: -1 * count)..<firstPart.endIndex
        email = firstPart.replacingCharacters(in: range, with: starts)
        email += "@"
        for part in components{
            email += part
        }
        
        return email
    }
    
    public var email : String{
        return dictionary["Email"] as? String ?? ""
    }
    
    public var isEmailVerified: Bool{
        return dictionary["EmailStatus"] as! Bool
    }
    
    public func verifyEmail(completion: @escaping (Error?)->Void){
        HYSession.member.authRequest(URL_HOST + "/API/MemberSecurity/VerifyEmailAsync", encoding: JSONEncode.default).validate().responseHYJson{[weak self] response in
            if let err = response.error{
                if response.code == "B103"{
                    let e = NSError(domain: "HYErrorDomain", code: 0, userInfo: [NSLocalizedDescriptionKey:LanHYUser.NSError_VerificationCodeSentEmail])
                    completion(e)
                }else{
                    completion(err)
                }
            }else{
                self?.update(complete: completion)
            }
        }
    }
    
    //修改mail
    public func editEmail(email: String, completion: @escaping (Error?)->Void){
        let parameters: [String: Any] = [
            "Email": email
        ]
        HYSession.member.authRequest(URL_HOST + "/api/MemberSecurity/EditEmailAsync", parameters: parameters, encoding: JSONEncode.default).validate().responseHYJson{
            [weak self] response in
            
            if let err = response.error{
                if response.code == "B4"{
                    let e = NSError(domain: "HYErrorDomain", code: 0, userInfo: [NSLocalizedDescriptionKey:"电子邮箱重复"])
                    completion(e)
                }else{
                    completion(err)
                }
            }else{
                self?.update(complete: completion)
            }
        }
    }
    
    // 手機號碼
    public var secretPhone: String{
        let phone = self.phone
        if phone.count > 3{
            let range = phone.index(phone.startIndex , offsetBy: 3)..<phone.index(phone.startIndex , offsetBy: min(phone.count, 7))
            return phone.replacingCharacters(in: range , with: "****")
        }
        return phone
    }
    
    public var phone : String{
        return dictionary["CellPhone"] as? String ?? ""
    }
    
    public var isPhoneVerified: Bool{
        return dictionary["CellPhoneStatus"] as! Bool
    }
    
    public func sendVerifyCode(toPhone: String, completion: @escaping (Error?)->Void){
        HYSession.member.authRequest(URL_HOST + "/API/MemberSecurity/SendCellPhoneVerifyCodeAsync",  parameters: ["CellPhone": toPhone], encoding: JSONEncode.default).validate().responseHYJson{response in
            completion(response.error)
        }
    }
    
    public func verifyPhone(phone: String,verifyCode code: String, completion: @escaping (Error?)->Void){
        let parameters: [String: Any] = [
            "IsNew": isPhoneVerified ? 1 :0
            , "CellPhone": isPhoneVerified ? phone : self.phone
            , "VerificationCode": code
        ]
        HYSession.member.authRequest(URL_HOST + "/API/MemberSecurity/VerifyCellPhoneAsync", parameters: parameters, encoding: JSONEncode.default).validate().responseHYJson{[weak self] response in
            if let err = response.error{
                completion(err)
            }else{
                self?.update(complete: completion)
            }
        }
    }
    
    // 資金密碼
    
    public var isBankPasswordSet: Bool{
        return dictionary["BankPasswordStatus"] as! Bool
    }
    
    //MARK: - Profile Data
    
    public enum Sex : Int {
        
        case female = 0
        case male = 1
        
        func description() -> String{
            switch self {
            case .male:
                return "男"
            case .female:
                return "女"
            }
        }
    }
    
    //姓名
    public var name : String!{
        return (dictionary["Name"] as? String)?.notEmptyValue ?? ""
    }
    
    //性別
    public var sex: Sex?{
        if let flag = dictionary["Sex"] as? Int{
            return Sex(rawValue: flag)
        }
        return nil
    }
    //生日
    public var strBirthdate: String?{
        return dictionary["BirthDay"] as? String
    }
    
    public var birthDate : Date?{
        if let strBirthdate = strBirthdate{
            return DateFormatter.fromHY.date(from: strBirthdate)
        }
        return nil
    }
    //QQ
    public var qq : String?{
        return (dictionary["QQ"] as? String)?.notEmptyValue
    }
    //區域
    public var provinceId: Int?{
        return dictionary["ProvinceID"] as? Int
    }
    var provinceIndex: Int?{
        if let id = self.provinceId{
            for (idx, hyData) in HYDatas.provinces.enumerated(){
                if id == hyData.id{
                    return idx
                }
            }
        }
        return nil
    }
    public var province : HYProvince?{
        if let idx = self.provinceIndex{
            return HYDatas.provinces[idx]
        }
        return nil
    }
    //地址
    public var address : String?{
        return (dictionary["Address"] as? String)?.notEmptyValue
    }
    
    func update(complete: @escaping (Error?)->()){
        HYSession.member.request(URL_HOST + "/API/Member/GetAsync", method: .post, headers: [token.headerKey: token.headerValue]).validate().responseHYJson{
            [weak self] response in
            self?.dictionary = response.dicData
            complete(response.error)
        }
    }
    
    func editInfo(withInfos infos: [String : Any], complete : @escaping (Error?)-> Void){
        
        var parameters : [String : Any] = [
            "Sex": sex?.rawValue ?? -1
            , "BirthDay": birthDate?.toString(format: HYDateFormat.to) ?? ""
            , "QQ": qq ?? ""
            , "ProvinceID": province?.id ?? -1
            , "Address": address ?? ""
        ]
        for (k, v) in infos{
            parameters[k] = v
        }
        
        HYSession.member.authRequest(URL_HOST + "/API/Member/EditAsync", parameters: parameters, encoding: JSONEncode.default)
            .validate()
            .responseHYJson{[weak self] response in
                if let error = response.error{
                    complete(error)
                }else{
                    self?.update(complete: complete)
                }
        }
    }
    
    // 手勢密碼
    private var gesture: [Int]{
        get{
            return UserDefaults.standard.object(forKey: self.uid + KEY_USER_GESTURE) as? [Int] ?? []
        }
        set{
            UserDefaults.standard.set(newValue, forKey: self.uid + KEY_USER_GESTURE)
        }
    }
    
    public var isGestureOn: Bool{
        return gesture.count > 4
    }
    
    func check(gesture: [Int]) -> Bool{
        return gesture == self.gesture
    }
    
    func addGesture(_ gesture: [Int]){
        self.gesture = gesture
    }
    
    func removeGesture(){
        self.gesture = []
    }

    func showCheckerVC(){
        let vc = UIStoryboard.Auth.identifier("UserLoginGestureIDVC") as! UserLoginGestureIDVC
        vc.useCase = .check
        
        var on = UIApplication.shared.delegate!.window!!.rootViewController!
        while let next = on.presentedViewController{
            on = next
        }
        if let _ = on as? UserLoginGestureIDVC {
            return
        }
        
        on.present(vc, animated: true, completion: nil)
    }

    // 指紋密碼開關
    public var isFingerprintOn: Bool{
        get{
            return UserDefaults.standard.object(forKey: self.uid + KEY_USER_FINGERPRINT) as? Bool ?? false
        }
        set{
            UserDefaults.standard.set(newValue, forKey: self.uid + KEY_USER_FINGERPRINT)
        }
    }
    
    //紀錄系統指紋筆數 - 用此紀錄判斷指紋是否有變動
    var oldDomainState:Data {
        get{
            return UserDefaults.standard.object(forKey: "oldDomainState") as? Data ?? "0".data(using: .utf8)!
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "oldDomainState")
        }
    };
    
    func showCheckTouchIDVC(){
        let vc = UIStoryboard.Auth.identifier("UserTouchIDLoginVC") as! UserTouchIDLoginVC
        
        var on = UIApplication.shared.delegate!.window!!.rootViewController!
        while let next = on.presentedViewController{
            on = next
        }
        if let _ = on as? UserTouchIDLoginVC{
            return
        }else{
            if let _ : UIAlertController = on as? UIAlertController {
                //UIAlertController 正在使用中
            } else {
                on.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Promotion Setting
    
    var likePromotion: [Int: Bool]{
        get{
            let data = UserDefaults.standard.object(forKey: self.uid + KEY_USER_LIKE_PROMOTION) as? [String: String] ?? [:]
            var value: [Int: Bool] = [:]
            for (_, v) in data.enumerated(){
                if let key = Int(v.key){
                    value[key] = v.value != "0"
                }
            }
            return value
        }
        set{
            var data: [String: String] = [:]
            for (_, v) in newValue.enumerated(){
                data["\(v.key)"] = v.value ? "1" : "0"
            }
            UserDefaults.standard.set(data, forKey: self.uid + KEY_USER_LIKE_PROMOTION)
        }
    }
    
    // MARK: - PlayStation Setting
    
    var favorCategoryOrder: [String]{
        get{
            return UserDefaults.standard.object(forKey: self.uid + KEY_USER_FAVOR_CATEGORY_ORDER) as? [String] ?? HYPlayStation.Category.all
        }
        set{
            UserDefaults.standard.set(newValue, forKey: self.uid + KEY_USER_FAVOR_CATEGORY_ORDER)
        }
    }
    
    var favorPlatform: [String: [String]]{
        get{
            return UserDefaults.standard.object(forKey: self.uid + KEY_USER_FAVOR_CATEGORY_PLATFORM) as? [String: [String]] ?? HYPlayStation.Platform.category.mapValues{Array($0.prefix(3))}
        }
        set{
            UserDefaults.standard.set(newValue, forKey: self.uid + KEY_USER_FAVOR_CATEGORY_PLATFORM)
        }
    }
    
    var availableCategoryPlatforms: [String: [String]]{
        get{
            return UserDefaults.standard.object(forKey: self.uid + KEY_USER_AVAILABLE_CATEGORY_ORDER) as? [String: [String]] ?? HYPlayStation.Platform.category
        }
        set{
            UserDefaults.standard.set(newValue, forKey: self.uid + KEY_USER_AVAILABLE_CATEGORY_ORDER)
        }
    }
    
    var availablePlatforms: [String]{
        get{
            return UserDefaults.standard.object(forKey: self.uid + KEY_USER_FAVOR_PLATFORM_ORDER) as? [String] ?? HYPlayStation.Platform.all
        }
        set{
            UserDefaults.standard.set(newValue, forKey: self.uid + KEY_USER_FAVOR_PLATFORM_ORDER)
        }
    }
    
    // MARK: - Store
    
    let KEY_USER_UID = "Hong Yue International_user_uid"
    let KEY_USER_UPW = "Hong Yue International_user_upw"
    let KEY_USER_DICTIONARY = "_user_dictionary"
    let KEY_USER_TOKEN = "_user_token"
    let KEY_USER_GESTURE = "_user_gesture"
    let KEY_USER_FINGERPRINT = "_user_fingerprint"
    
    let KEY_USER_FAVOR_CATEGORY_ORDER = "_user_favor_category_order" // 分類的順序
    let KEY_USER_AVAILABLE_CATEGORY_ORDER = "_user_available_category_platform" // 可用的平台，按分類區分
    let KEY_USER_FAVOR_CATEGORY_PLATFORM = "_user_favor_category_platform" //
    let KEY_USER_FAVOR_PLATFORM_ORDER = "_user_favor_platform_order"
    
    let KEY_USER_LIKE_PROMOTION = "_user_like_promotion"
    
    public required init?(coder aDecoder: NSCoder) {
        self.uid = aDecoder.decodeObject(forKey: KEY_USER_UID) as! String
        self.upw = aDecoder.decodeObject(forKey: KEY_USER_UPW) as! String
        self.dictionary = aDecoder.decodeObject(forKey: KEY_USER_DICTIONARY) as! NSDictionary
        let dictionary = aDecoder.decodeObject(forKey: KEY_USER_TOKEN) as! NSDictionary
        self.token = Token(withDictionary: dictionary)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(uid, forKey: KEY_USER_UID)
        aCoder.encode(upw, forKey: KEY_USER_UPW)
        aCoder.encode(dictionary, forKey: KEY_USER_DICTIONARY)
        aCoder.encode(token.dictionary, forKey: KEY_USER_TOKEN)
    }
    
}
