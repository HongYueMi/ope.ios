//
//  UserRegisterAccountVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/13.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class UserRegisterAccountVC: UIEditVC {

    @IBOutlet weak var tfUID : UITextField!         //輸入用戶名 账號
    @IBOutlet weak var tfPassword : UITextField!    //輸入密碼
    @IBOutlet weak var tfVerify: UITextField!       //輸入驗證碼
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var ctrlVerify: UIControl!
    @IBOutlet weak var tfEmail: UITextField!
    
    @IBOutlet weak var btnSignUp : UIButton!
    @IBOutlet weak var viewSignUp: UIView!
    
    @IBOutlet weak var btnEyePassword: UIButton!
    
    @IBOutlet weak var btnClearUID: UIButton!
    
    @IBOutlet weak var btBackPhone: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initViewController();
        
        didCtrlRefreshVerifyCodeClick(ctrl: ctrlVerify)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initViewController(){
        //navigation 使用透明clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default);
        self.navigationController?.navigationBar.shadowImage = UIImage();
        self.navigationController?.navigationBar.isTranslucent = true;
        self.navigationController?.view.backgroundColor = UIColor.clear;
        
        let nfc = NotificationCenter.default
        nfc.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        nfc.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        btnSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        btnSignUp.layer.borderWidth = 1.0; //邊框大小
        btnSignUp.layer.masksToBounds = true;
        btnSignUp.layer.cornerRadius = 25.0;
        
        viewSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        viewSignUp.layer.borderWidth = 1.0; //邊框大小
        viewSignUp.layer.masksToBounds = true;
        viewSignUp.layer.cornerRadius = 25.0;
        
    }
    
    @IBAction func didBtnPasswordEyeClick(btn: UIButton){
        btnEyePassword.isSelected = !btnEyePassword.isSelected
        tfPassword.isSecureTextEntry = !btnEyePassword.isSelected
    }
    
    @IBAction func didBtnClearUIDClick(_ sender: Any) {
        tfUID.text = "";
    }
    
    var signUpItem : SignUpItem!{
        didSet{
            guard signUpItem != nil else {
                return
            }
            imgVerify.image = signUpItem.verify.image
        }
    }
    
    @IBAction func didCtrlRefreshVerifyCodeClick(ctrl : UIControl?){
        
        self.signUpItem = nil
        tfVerify.text = nil
        ctrl?.isEnabled = false
        
        HYMember.shared.genSignUpItem{ item in
            self.signUpItem = item
            ctrl?.isEnabled = true
        }
    }
    
    @IBAction func didBtBackPhoneClick(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true);
    }
    
    //開啟括約肌的奇幻歷程
    @IBAction func didBtnSignUpClick(_ sender: UIButton) {
        hideKeyboard()
        if tfVerify.text! == "" {
            showGeneralAlert(.alert_info,lbTextA:"验证码不能为空"){ value in }
            // 驗證碼重抓
            didCtrlRefreshVerifyCodeClick(ctrl: nil)
            return
        }
        
        guard
            let uid = tfUID.text?.notEmptyValue?.trimmingCharacters(in: .whitespacesAndNewlines)
            , HYRegex.UID.match(input: uid)
            else
        {
            showGeneralAlert(.alert_info,lbTextA:"用户名格式错误（6-12位小写英文字母以及数字的组合）"){ value in }
            return
        }
        
        guard
            let password = tfPassword.text?.notEmptyValue
            , HYRegex.Password.match(input: password)
            else
        {
            showGeneralAlert(.alert_info,lbTextA:"密码格式错误（6-12位必须含有字母和数字的组合）"){ value in }
            return
        }
        
        guard
            let value = tfEmail.text?.notEmptyValue
            , HYRegex.Email.match(input: value)
            else
        {
            showGeneralAlert(.alert_info,lbTextA:"电子邮件格式有误"){ value in }
            return
        }
        
        view.isUserInteractionEnabled = false
        
        guard var item = self.signUpItem else {
            return
        }
        
        item.account = tfUID.text!
        item.password = tfPassword.text!
        item.name = ""
        item.cellPhone = ""
        item.email = tfEmail.text!
        item.code = tfVerify.text!
        
        HYMember.shared.signUp(withItem: item){[weak self] error, item in
            print("--------------------------註冊回覆：--------------------------")
            if nil != error {
                print("--------------------------註冊失敗！--------------------------")
                print(error!)
                self?.showGeneralAlert(.alert_error, lbTextA:error!.localizedDescription){ value in
                    self?.didCtrlRefreshVerifyCodeClick(ctrl: nil)
                    self?.view.isUserInteractionEnabled = true
                }
            }
            else{
                print("--------------------------註冊成功，自動登入--------------------")
                
                self?.newLoadingScreenView.showActivityIndicator(uiView: (self?.view)!);  //開啟Londing畫面
                HYAuth.auth.signIn(uid: (self?.tfUID.text!)!, password: (self?.tfPassword.text!)!){ [weak self]
                    user, error in
                    self?.newLoadingScreenView.hideActivityIndicator(uiView: (self?.view)!);    //關閉Londing畫面
                    if nil != error{
                        self?.showGeneralAlert(.alert_error, lbTextA:"使用者名称或密码不正确"){ value in }
                    }else{                        
                        var rootVC = self?.presentingViewController;
                        while let parent = rootVC?.presentingViewController {
                            rootVC = parent;
                        }
                        rootVC?.dismiss(animated: true, completion: nil);
                    }
                }
                
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let inverseSet = CharacterSet(charactersIn:".0123456789").inverted;
//        let components = string.components(separatedBy: inverseSet);
//        let filtered = components.joined(separator: "");
//        if(string != filtered) {
//            return false; //如果輸入的是數字以外則回傳false
//        }
        
        if string == "" {
            return true;
        }
        
        if textField == tfUID , let txt = tfUID.text , txt.count >= 12 {
            return false
        }
        
        if textField == tfPassword , let txt = tfPassword.text , txt.count >= 12 {
            return false
        }
        
        if textField == tfEmail , let txt = tfEmail.text , txt.count >= 45 {
            return false
        }
        
        if textField == tfVerify , let txt = tfVerify.text , txt.count >= 4 {
            return false
        }
        
//        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//        if(tfBetCash == textField){
//            if(HYRegex.Amount.match(input: text) || text == ""){
//                return checkUITextFieldValue(string);
//            }else{
//                return false
//            }
//        }
        
        return true;
    }
    
    
    
}
















































