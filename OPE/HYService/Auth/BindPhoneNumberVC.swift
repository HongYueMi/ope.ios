//
//  BindPhoneNumberVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/13.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class BindPhoneNumberVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var btnSignUp : UIButton!
    @IBOutlet weak var viewSignUp: UIView!
    
    @IBOutlet weak var btAgainPush: UIButton!
    @IBOutlet weak var lbCount: UILabel!
    
    @IBOutlet weak var tfVerify: UITextField!
    
    var userPhone:String = "";
    var userPassword:String = "";
    
    weak var timer: Timer?
    var count: Int = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViewController();
        
        getVerifyCode();
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initViewController(){
        //navigation 使用透明clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default);
        self.navigationController?.navigationBar.shadowImage = UIImage();
        self.navigationController?.navigationBar.isTranslucent = true;
        self.navigationController?.view.backgroundColor = UIColor.clear;
        
        let nfc = NotificationCenter.default
        nfc.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        nfc.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        btnSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        btnSignUp.layer.borderWidth = 1.0; //邊框大小
        btnSignUp.layer.masksToBounds = true;
        btnSignUp.layer.cornerRadius = 25.0;
        
        viewSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        viewSignUp.layer.borderWidth = 1.0; //邊框大小
        viewSignUp.layer.masksToBounds = true;
        viewSignUp.layer.cornerRadius = 25.0;
        
    }
    
    @IBOutlet open weak var scrollView : UIScrollView?
    open weak var tfActive : UITextField?
    
    @IBAction open func hideKeyboard(){
        tfActive?.resignFirstResponder()
    }
    
    @objc func keyboardWasShown(ntf:Notification){
        if let kbRectValue = ntf.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let kbSize = kbRectValue.cgRectValue.size
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
            scrollView?.contentInset = contentInsets
            scrollView?.scrollIndicatorInsets = contentInsets
        }
    }
    
    @objc func keyboardWillBeHidden(ntf:Notification){
        let contentInsets = UIEdgeInsets.zero
        scrollView?.contentInset = contentInsets
        scrollView?.scrollIndicatorInsets = contentInsets
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tfActive = textField
    }

    //下一步
    @IBAction func didBtnSignUpClick(_ sender: UIButton) {
        hideKeyboard()
        guard checkVerifyInput() else {
            return
        }
        view.isUserInteractionEnabled = false
        setVerifyPhoneCode(phone: userPhone, verifyCode: tfVerify.text!){ [weak self] error in
            if let err = error{
                self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in
                    if value == 1 {
                        self?.view.isUserInteractionEnabled = true
                    }
                 }
            }else{
                self?.view.isUserInteractionEnabled = true
                self?.performSegue(withIdentifier: "sendBindUserAccountVC", sender: nil);
            }
        }
    }
    
    //再次發送電話認證
    @IBAction func didBtAgainPushClick(_ sender: UIButton) {
        getVerifyCode();
    }
    
    func startCounting(){
        timer?.invalidate()
        btAgainPush.isEnabled = false
        count = 60
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCount), userInfo: nil, repeats: true)
    }
    
    @objc func updateCount(){
        lbCount.text = "（\(count)s）";
        if 0 == count{
            timer?.invalidate()
            btAgainPush.isEnabled = true
        }else{
            count -= 1
        }
    }
    
    func getVerifyCode(){
        HYSession.member.authRequest(URL_HOST + "/API/MemberSecurity/SendCellPhoneVerifyCodeAsync",  parameters: ["CellPhone": userPhone], encoding: JSONEncode.default).validate().responseHYJson { response in
            if let _ = response.error{
                self.showGeneralAlert(.alert_error, lbTextA:"发送验证码失败！"){ value in }
            } else{
                self.startCounting()
            }
        }
    }
    
    func checkVerifyInput() -> Bool{
        guard let _ = tfVerify.text?.notEmptyValue else {
            showGeneralAlert(.alert_info, lbTextA:"请输入验证码"){ value in }
            return false
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendBindUserAccountVC"{
            let vc = segue.destination as! BindUserAccountVC;
            vc.userPhone = userPhone
            vc.userPassword = userPassword
        }
    }
    
    func setVerifyPhoneCode(phone: String,verifyCode code: String, completion: @escaping (Error?)->Void){
        let parameters: [String: Any] = [
            "IsNew": 0
            , "CellPhone": phone
            , "VerificationCode": code
        ]
        HYSession.app.postJson(url: URL_HOST + "/API/ResetPwd/VerifyCellPhoneVerifyCodeAsync", parameters: parameters).validate().responseHYJson { response in
            if let err = response.error{
                completion(err)
            }else{
                completion(nil);
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            return true;
        }
        
        if textField == tfVerify , let txt = tfVerify.text , txt.count >= 6 {
            return false
        }
        
        return true;
    }
    
}



































