//
//  BindUserAccountVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/14.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit
import Alamofire

class BindUserAccountVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var btnSignUp : UIButton!
    @IBOutlet weak var viewSignUp: UIView!
    
    @IBOutlet weak var tfVerify: UITextField!       //輸入驗證碼
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var ctrlVerify: UIControl!
    
    @IBOutlet weak var tfUserName: UITextField!     //用戶名－账號
    
    var userPhone:String = "";
    var userPassword:String = "";
    
    var verifyKey = "";
    
    let newLoadingScreenView = LoadingIndicatorVC(); //產生LoadingView畫面物件
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initViewController();
        self.didCtrlRefreshVerifyCodeClick(ctrl: self.ctrlVerify);
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initViewController(){
        //navigation 使用透明clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default);
        self.navigationController?.navigationBar.shadowImage = UIImage();
        self.navigationController?.navigationBar.isTranslucent = true;
        self.navigationController?.view.backgroundColor = UIColor.clear;
        
        let nfc = NotificationCenter.default
        nfc.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        nfc.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        btnSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        btnSignUp.layer.borderWidth = 1.0; //邊框大小
        btnSignUp.layer.masksToBounds = true;
        btnSignUp.layer.cornerRadius = 25.0;
        
        viewSignUp.layer.borderColor = UIColor.white.cgColor; //邊框顏色1
        viewSignUp.layer.borderWidth = 1.0; //邊框大小
        viewSignUp.layer.masksToBounds = true;
        viewSignUp.layer.cornerRadius = 25.0;
        
    }
    
    @IBOutlet open weak var scrollView : UIScrollView?
    open weak var tfActive : UITextField?
    
    @IBAction open func hideKeyboard(){
        tfActive?.resignFirstResponder()
    }
    
    @objc func keyboardWasShown(ntf:Notification){
        if let kbRectValue = ntf.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let kbSize = kbRectValue.cgRectValue.size
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
            scrollView?.contentInset = contentInsets
            scrollView?.scrollIndicatorInsets = contentInsets
        }
    }
    
    @objc func keyboardWillBeHidden(ntf:Notification){
        let contentInsets = UIEdgeInsets.zero
        scrollView?.contentInset = contentInsets
        scrollView?.scrollIndicatorInsets = contentInsets
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tfActive = textField
    }
    
    var signUpItem : SignUpItem!{
        didSet{
            guard signUpItem != nil else {
                return
            }
            imgVerify.image = signUpItem.verify.image
        }
    }
    
    @IBAction func didCtrlRefreshVerifyCodeClick(ctrl : UIControl?){
        self.signUpItem = nil
        tfVerify.text = nil
        ctrl?.isEnabled = false
        
        HYMember.shared.genSignUpItem{ item in
            self.signUpItem = item
            ctrl?.isEnabled = true
        }
    }
    
    //開啟括約肌的奇幻歷程
    @IBAction func didBtnSignUpClick(_ sender: UIButton) {
        hideKeyboard()
        if tfVerify.text! == "" {
            showGeneralAlert(.alert_info,lbTextA:"验证码不能为空"){ value in }
            // 驗證碼重抓
            didCtrlRefreshVerifyCodeClick(ctrl: nil)
            return
        }
        
        guard
            let uid = tfUserName.text?.notEmptyValue?.trimmingCharacters(in: .whitespacesAndNewlines)
            , HYRegex.UID.match(input: uid)
            else
        {
            hideKeyboard()
            showGeneralAlert(.alert_info,lbTextA:"用户名格式错误（6-12位小写英文字母以及数字的组合）"){ value in }
            return
        }
        
        view.isUserInteractionEnabled = false
        
        guard var item = self.signUpItem else {
            return
        }
        
        item.account = tfUserName.text!
        item.password = userPassword
        item.name = ""
        item.cellPhone = userPhone
        item.email = ""
        item.code = tfVerify.text!
        
        HYMember.shared.signUp(withItem: item){[weak self] error, item in
            print("--------------------------註冊回覆：--------------------------")
            if nil != error {
                print("--------------------------註冊失敗！--------------------------")
                print(error!)
                self?.showGeneralAlert(.alert_error, lbTextA:error!.localizedDescription){ value in
                    self?.didCtrlRefreshVerifyCodeClick(ctrl: nil)
                    self?.view.isUserInteractionEnabled = true
                }
            }
            else{
                print("--------------------------註冊成功，自動登入--------------------")
                
                self?.newLoadingScreenView.showActivityIndicator(uiView: (self?.view)!);  //開啟Londing畫面
                HYAuth.auth.signIn(uid: (self?.tfUserName.text!)!, password: (self?.userPassword)!){ [weak self]
                    user, error in
                    self?.newLoadingScreenView.hideActivityIndicator(uiView: (self?.view)!);    //關閉Londing畫面
                    if nil != error{
                        self?.showGeneralAlert(.alert_error, lbTextA:"使用者名称或密码不正确"){ value in }
                    }else{                        
                        var rootVC = self?.presentingViewController;
                        while let parent = rootVC?.presentingViewController {
                            rootVC = parent;
                        }
                        rootVC?.dismiss(animated: true, completion: nil);
                    }
                }
                
            }
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            return true;
        }
        
        if textField == tfUserName , let txt = tfUserName.text , txt.count >= 12 {
            return false
        }
        
        if textField == tfVerify , let txt = tfVerify.text , txt.count >= 4 {
            return false
        }
        
        return true;
    }
    
    
    

}
































