//
//  UserForgotPwSelectVC.swift
//  OPE
//
//  Created by rwt113 on 2017/11/15.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

class UserForgotPwSelectCell: UITableViewCell {
    
    @IBOutlet weak var menuName: UILabel!
    @IBOutlet weak var menuImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

import UIKit

class UserForgotPwSelectVC: UIEditVC, UITableViewDelegate, UITableViewDataSource {

    var item: PasswordResetItem!
    
    @IBOutlet weak var viewFindByEmail: UIView!
    @IBOutlet weak var viewFindByCellPhone: UIView!
    @IBOutlet weak var viewFindByCustomerService: UIView!
    
    @IBOutlet weak var myTableView: UITableView!
    
    let imgItem = ["Forgot_mail","Forgot_phone","Forgot_service"];
    let lbName = ["电子邮箱方式找回","手机号码方式找回","客服方式找回"];
    
    var numberOfRows:[Int] = [];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for type in item.types{
            if let numt = type {
                numberOfRows.append(numt)
            }
        }
        
        myTableView.dataSource = self
        myTableView.delegate = self
        myTableView.separatorColor = UIColor.clear; //線條顏色
        myTableView.reloadData();
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendUserCheckPhoneAVC"{
            let vc = segue.destination as! UserCheckPhoneAVC
            vc.item = sender as! PasswordResetItem
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier:String = "UserForgotPwSelectCell"
        var cell:UserForgotPwSelectCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UserForgotPwSelectCell
        if(cell == nil){
            cell = UserForgotPwSelectCell(style: UITableViewCellStyle.default, reuseIdentifier: cellIdentifier)
        }
        
        cell.selectionStyle = .none
        
        cell.menuName.text = "\(lbName[numberOfRows[indexPath.row]-1])";
        let image = UIImage(named:imgItem[numberOfRows[indexPath.row]-1]);
        cell.menuImg.image = image;
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if numberOfRows[indexPath.row] == 1 {       //Mail
            showGeneralAlert(.alert_touchID,lbTextA:"系统已经发送密码到 \(item.email)",lbTextB:"请登入信箱查收"){ value in }
        }else if numberOfRows[indexPath.row] == 2 { //手機號碼
            item.selectedType = 2
            HYMember.shared.resetPassword(withItem: item){[weak self] item, error in
                if let err = error{
                    self?.showGeneralAlert(.alert_error, lbTextA:err.localizedDescription){ value in }
                }else {
                    self?.performSegue(withIdentifier: "sendUserCheckPhoneAVC", sender: item)
                }
            }
        }else if numberOfRows[indexPath.row] == 3 { //客服
            self.performSegue(withIdentifier: "sendCustomerServiceNavVC", sender: nil)
        }
    }
    
}

































