//
//  HYMember.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/12.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import Alamofire

struct SignUpItem {
    
    init(verify : VerifyItem){
        self.verify = verify
    }
    let verify : VerifyItem
    var code : String!
    
    //MARK: - 會員資料
    var account : String!
    var password : String!
    var name : String!
    var cellPhone : String!
    var email : String!
    var qq : String?
    var weChat : String?
}

struct MemberEditItem {
    
    //MARK: - 會員資料
    var sex : HYUser.Sex?
    var birthday : Date!
    var secureQuestion : HYOption!
    var secureAnswer : String!
    var qq : String?
    var weChat : String?
    
    var isDataComplete : Bool{
        return sex != nil && birthday != nil && secureQuestion != nil && secureAnswer != nil
    }
    
}

struct PasswordEditItem {
    var old : String?
    var new : String?
}

struct PasswordResetItem {
    
    init(uid: String) {
        self.uid = uid
    }
    //MARK: - 會員資料
    var uid : String
    var types: [Int?] = [nil, nil, 3]
    var selectedType: Int!
    var sid: Int!
    var email: String!
    var phone: String!
    var code: String!
    var password: String!
}

struct SecureQuestion : HYOption {
    
    init(raw : NSDictionary) {
        _raw = raw
    }
    
    var _raw : NSDictionary
    var raw : NSDictionary{
        return _raw
    }
    
    var id : Int{
        return raw["SQID"] as? Int ?? -1
    }
    var name : String{
        return raw["Name"] as? String ?? ""
    }
}

class HYMember: NSObject {
    
    static let shared = HYMember()

    // MARK: - Registration
    
    // 外部呼叫此法註冊。
    @objc func askForUserSignUp(){
        
        let nav = UIStoryboard.Auth.instantiateViewController(withIdentifier: "SignUpNavVC") as! UINavigationController
        var presenting = UIApplication.shared.keyWindow?.rootViewController
        while let next = presenting?.presentedViewController {
            presenting = next
        }
        DispatchQueue.main.async {
            presenting?.present(nav, animated: true)
        }
    }
    
    func genSignUpItem(newItem : @escaping (SignUpItem)-> Void){
        genVerifyItem{ verify in
            newItem(SignUpItem(verify: verify))
        }
    }
    
    func signUp(withItem item: SignUpItem, complete : @escaping (Error?, SignUpItem?)-> Void){
        
        let parameters : [String : Any] = [
            "Account" : item.account
            , "Password" : item.password
            , "Name" : item.name
            , "CellPhone" : item.cellPhone
            , "Email" : item.email
            , "QQ" : item.qq ?? ""
            , "WeChat" : item.weChat ?? ""
            , "Host" : "" // 帶空，不可不帶
            , "SessionID" : "" // 帶空，不可不帶
            , "VerifyKey" : item.verify.key
            , "VerifyCode" : item.code ?? ""
        ]
        let header:[String:String] = ["DeviceType":"iOS"];
        request(URL_HOST + "/API/Member/RegisterAsync?code=258O5F", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header)
            .validate()
            .responseHYJson{ response in
                debugPrint(item)
                complete(response.error, item)
        }
    }
    
    
    // MARK: - Secure Question
    
    var sequreQuestionList : [HYOption] = []
    
    func getSecureQuestionList(complete : @escaping ([HYOption])->()){
        let header:[String:String] = ["DeviceType":"iOS"];
        request(URL_HOST + "/API/Member/QuestionListAsync", method: .post, headers: header).validate().responseHYJson{
            response in
            let data = response.aryData ?? []
            var list : [HYOption] = []
            for dic in data{
                list.append(SecureQuestion(raw: dic))
            }
            self.sequreQuestionList = list
            complete(list)
        }
    }
    
    // MARK: - Member Edit
    
    func genMemberEditItem(resultItme : @escaping (MemberEditItem)-> Void){
        getSecureQuestionList{ questions in
            
            resultItme(MemberEditItem())
        }
    }
    
    func editMember(withInfos infos: [String : Any], complete : @escaping (HYUser?, Error?)-> Void){
        
        var parameters : [String : Any] = [
            "Sex" : ""
            , "BirthDay" : ""
            , "QQ" : ""
            , "ProvinceID" : ""
            , "Address" : ""
            , "Name"    : ""
        ]
        for (k, v) in infos{
            parameters[k] = v
        }
        
        HYSession.member.authRequest(URL_HOST + "/API/Member/EditAsync", parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseHYJson{ response in
                if let error = response.error{
                    complete(nil, error)
                }else{
                    HYAuth.auth.currentUser!.update{error in
                        complete(HYAuth.auth.currentUser, error)
                    }
                }
        }
    }
    
    // MARK: - ChangePassword
    
    func genPasswordEditItem(resultItme : @escaping (PasswordEditItem)-> Void){
        let item = PasswordEditItem()
        resultItme(item)
    }
    
    func editPassword(withItem item: PasswordEditItem, complete : @escaping (Error?)-> Void){
        
        var parameters : [String : Any] = [:]
        
        if let value = item.old{
            parameters["OldPassword"] = value
        }
        if let value = item.new{
            parameters["NewPassword"] = value
        }
        
        print(parameters)
        HYSession.member.authRequest(URL_HOST + "/API/Member/ChangePasswordAsync", parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseHYJson{ response in
                complete(response.error)
        }
    }
    
    // MARK: - ResetPassword  重置密碼
    
    func getUserAvailableMethodToResetPassword(item: PasswordResetItem, verify: VerifyItem, completion: @escaping (PasswordResetItem, Error?)-> Void){
        
        let parameters : [String : Any] = [
            "RequestConfirmAccountSource": [
                "Account": item.uid
            ]
            , "VerifyKey" : verify.key
            , "VerifyCode" : item.code
        ]
        
        var newItem = item
        HYSession.member.authRequest(URL_HOST + "/API/ResetPwd/ConfirmAccountAsync", parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseHYJson{ response in
                newItem.types = response.aryData?.map{$0["Type"] as? Int} ?? [nil, nil, 3]
                completion(newItem, response.error)
        }
    }
    
    func resetPassword(withItem item: PasswordResetItem, complete :  @escaping (PasswordResetItem, Error?)-> Void){
        
        let parameters : [String : Any] = [
            "Account": item.uid
            , "Type" : item.selectedType
        ]
        var newItem = item

        HYSession.member.authRequest(URL_HOST + "/API/ResetPwd/ResetPwdMethodAsync", parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseHYJson{ response in
                newItem.sid = response.dicData?["SID"] as? Int ?? -1
                newItem.email = response.dicData?["Email"] as? String
                complete(newItem, response.error)
        }
    }
    
    public func sendVerifyCode(toPhone: String, completion: @escaping (Error?)->Void){
        HYSession.member.authRequest(URL_HOST + "/API/MemberSecurity/SendCellPhoneVerifyCodeAsync",  parameters: ["CellPhone": toPhone], encoding: JSONEncode.default).validate().responseHYJson{response in
            completion(response.error)
        }
    }
    
    func resetPasswordByCellPhoneVerify(withItem item: PasswordResetItem, complete :  @escaping (Error?)-> Void){
        let parameters : [String : Any] = [
            "CellPhone": item.phone
            , "ID" : item.sid
            , "VerificationCode": item.code
        ]
        HYSession.member.authRequest(URL_HOST + "/API/ResetPwd/VerifyCellPhoneVerifyCodeAsync", parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseHYJson{ response in
                complete(response.error)
        }
    }

    func resetPasswordNewPassword(withItem item: PasswordResetItem, complete :  @escaping (Error?)-> Void){
        let parameters : [String : Any] = [
            "SID" : item.sid
            , "Password": item.password
        ]
        HYSession.member.authRequest(URL_HOST + "/API/ResetPwd/CellPhoneChangePwdAsync", parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseHYJson{ response in
                complete(response.error)
        }
    }
}
