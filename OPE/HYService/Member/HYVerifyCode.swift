//
//  HYVerifyCode.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/12.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import Alamofire

// MARK: - Verify Code

var verifyKey : String!

struct VerifyItem {
    
    init (key: String, image: UIImage){
        self.key = key
        self.image = image
    }
    
    let key : String
    let image : UIImage
}

// MARK: - 產生Image

func refreshVerifyCode(toImageView : UIImageView){
    let key = genVerifyKey()
    let path = URL_HOST + "/API/VerifyCode/Get?key=\(key)"
    let url = URL(string: path)!
    
    toImageView.sd_setImage(with: url){
        image, err, type, url in
        if err != nil {
            print(err!)
        }
    }
}

func genVerifyItem(newItem : @escaping (VerifyItem) ->Void){
    let key = genVerifyKey()
    
    request(URL_HOST + "/API/VerifyCode/Get", parameters: ["key" : key], encoding: URLEncoding.default)
        .validate()
        .responseData{ respondData in
            if let data = respondData.data, let image = UIImage(data: data){
                newItem(VerifyItem(key: key, image: image))
            }
    }
}

// MARK: - 產生Key

let randonSource = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqustuvwxyz0123456789"

func genVerifyKey()-> String{
    let count = randonSource.count
    var key = ""
    
    var times = 0
    while times < 10  {
        let index = randonSource.index(randonSource.startIndex, offsetBy: Int(arc4random()) % count)
        let letter = randonSource[index]
        key.append(letter)
        times += 1
    }
    print(key)
    return key
}

