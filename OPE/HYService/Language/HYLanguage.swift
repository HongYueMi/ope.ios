//
//  HYLanguage.swift
//  OPE
//
//  Created by rwt113 on 2017/7/13.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import Foundation

// HYUser .swift
public class LanHYUser {
    
    static let NSError_VerificationCodeSentEmail = NSLocalizedString("NSError_VerificationCodeSentEmail", tableName: "LanHYUser", value: "驗證碼已發送至電子郵件，請勿重複發送", comment: "驗證碼已發送至電子郵件，請勿重複發送");
    static let NSError_EmailRepeat = NSLocalizedString("NSError_EmailRepeat", tableName: "LanHYUser", value: "電子郵箱重複", comment: "電子郵箱重複");
    
}

// GesturePasswordVC .swift
public class LanGesturePassword {
    
    static let lbTitle_DrawGestures = NSLocalizedString("LanGesturePassword_lbTitle_DrawGestures", value: "绘制解锁图案", comment: "绘制解锁图案") ;
    static let lbTitle_OldGesture = NSLocalizedString("LanGesturePassword_lbTitle_OldGesture", value: "請輸入舊手勢密碼", comment: "請輸入舊手勢密碼");
    static let lbTitle_InputGesture = NSLocalizedString("LanGesturePassword_lbTitle_InputGesture", value: "請輸入手勢密碼", comment: "請輸入手勢密碼");
    static let lbTitle_AgainGestureCheck = NSLocalizedString("LanGesturePassword_lbTitle_AgainGestureCheck", value: "再次繪製密碼進行確認", comment: "再次繪製密碼進行確認");
    
    static let fail_PasswordLengthInsufficient = NSLocalizedString("LanGesturePassword_fail_PasswordLengthInsufficient", value: "密碼長度不足", comment: "密碼長度不足");
    static let fail_GesturesEror = NSLocalizedString("LanGesturePassword_fail_GesturesEror", value: "手勢密碼錯誤", comment: "手勢密碼錯誤");
    
}

// HYSignInVC .swift
public class LanHYSignIn {
    
    static let showDialog_UserIdOrPasswdError = NSLocalizedString("LanHYSignIn_showDialog_UserIdOrPasswdError", value: "使用者名稱或密碼不正確", comment: "使用者名稱或密碼不正確");
    static let lbUID_UserIdError = NSLocalizedString("LanHYSignIn_lbUID_UserIdError", value: "  用戶名格式錯誤(6-12位小寫英文字母以及數字的組合)  ", comment: "  用戶名格式錯誤(6-12位小寫英文字母以及數字的組合)  ");
    static let lbUID_UserPasswdError = NSLocalizedString("LanHYSignIn_lbUID_UserPasswdError", value: "  密碼格式錯誤(6-12位必須含有字母和數字的組合)  ", comment: "  密碼格式錯誤(6-12位必須含有字母和數字的組合)  ");
}

// HYSignUpVC .swift
public class LanHYSignUp {
    
    static let lbAgree_AgreeUserProtocol = NSLocalizedString("LanHYSignUp_lbAgree_AgreeUserProtocol", value: "  請同意用戶協議  ", comment: "  請同意用戶協議  ");
    static let showDialog_RegisterOk = NSLocalizedString("LanHYSignUp_showDialog_RegisterOk", value: "註冊成功，已自動幫您登入", comment: "註冊成功，已自動幫您登入");
    static let lbUID_UserFormatError = NSLocalizedString("LanHYSignUp_lbUID_UserFormatError", value: "  用戶名格式錯誤(6-12位小寫英文字母以及數字的組合)  ", comment: "  用戶名格式錯誤(6-12位小寫英文字母以及數字的組合)  ");
    
    static let lbPassword_UserPasswdFormatError = NSLocalizedString("LanHYSignUp_lbPassword_UserPasswdFormatError", value: "  密碼格式錯誤(6-12位必須含有字母和數字的組合)  ", comment: "  密碼格式錯誤(6-12位必須含有字母和數字的組合)  ");
    static let lbPasswordConfirm_AgainPasswdMustBeConsistent = NSLocalizedString("LanHYSignUp_lbPasswordConfirm_AgainPasswdMustBeConsistent", value: "  兩次輸入的密碼必須一致  ", comment: "  兩次輸入的密碼必須一致  ");
    static let lbUserName_TrueNameError = NSLocalizedString("LanHYSignUp_lbUserName_TrueNameError", value: "  真實姓名格式有誤  ", comment: "  真實姓名格式有誤  ");
    static let lbUserName_TrueNameCountError = NSLocalizedString("LanHYSignUp_lbUserName_TrueNameError", value: "  真實姓名長度不可超過100字元  ", comment: "  真實姓名長度不可超過100字元  ");
    static let lbEmail_EmailError = NSLocalizedString("LanHYSignUp_lbEmail_EmailError", value: "  電子郵件格式有誤  ", comment: "  電子郵件格式有誤  ");
    static let lbPhone_PhoneNimberError = NSLocalizedString("LanHYSignUp_lbPhone_PhoneNimberError", value: "  手機號碼格式錯誤(僅支持中國大陸手機號碼)  ", comment: "  手機號碼格式錯誤(僅支持中國大陸手機號碼)  ");
    static let lbVerify_VerificationCodeError = NSLocalizedString("LanHYSignUp_lbVerify_VerificationCodeError", value: "  驗證碼不能為空  ", comment: "  驗證碼不能為空  ");
}

// ForgotPasswordVC .swift
public class LanForgotPassword {
    
    static let lbUID_UserFormatError = NSLocalizedString("LanForgotPassword_lbUID_UserFormatError", value: "  用戶名格式錯誤(4-12位小寫英文字母以及數字的組合)  ", comment: "  用戶名格式錯誤(4-12位小寫英文字母以及數字的組合)  ");
    static let lbVerify_VerificationCodeError = NSLocalizedString("LanForgotPassword_lbVerify_VerificationCodeError", value: "  驗證碼不能為空  ", comment: "  驗證碼不能為空  ");
}

// FindPasswordVC .swift
public class LanFindPassword {
    
    static let showDialog_infoA = NSLocalizedString("LanFindPassword_showDialog_infoA", value: "系統已經發送密碼到", comment: "系統已經發送密碼到");
    static let showDialog_infoB = NSLocalizedString("LanFindPassword_showDialog_infoB", value: "，請登入信箱查收", comment: "，請登入信箱查收");
    
}

// FindPasswordByCellPhoneVC .swift
public class LanFindPasswordByCellPhone {
    
    static let showDialog_SendVerificationError = NSLocalizedString("LanFindPasswordByCellPhone_showDialog_SendVerificationError", value: "發送驗證碼失敗！", comment: "發送驗證碼失敗！");
    static let lbPhone_PhoneNimberError = NSLocalizedString("LanFindPasswordByCellPhone_lbPhone_PhoneNimberError", value: "  手機號碼格式錯誤(僅支持中國大陸手機號碼)  ", comment: "  手機號碼格式錯誤(僅支持中國大陸手機號碼)  ");
    
}

// ResetNewPasswordVC .swift
public class LanResetNewPassword {
    
    static let showDialog_NewPasswordSet = NSLocalizedString("LanResetNewPassword_showDialog_NewPasswordSet", value: "已設定新密碼", comment: "已設定新密碼");
    static let lbPassword_UserPasswdFormatError = NSLocalizedString("LanResetNewPassword_lbPassword_UserPasswdFormatError", value: "  密碼格式錯誤(6-12位必須含有字母和數字的組合)  ", comment: "  密碼格式錯誤(6-12位必須含有字母和數字的組合)  ");
    static let lbPasswordConfirm_AgainPasswdMustBeConsistent = NSLocalizedString("LanResetNewPassword_lbPasswordConfirm_AgainPasswdMustBeConsistent", value: "  兩次輸入的密碼必須一致  ", comment: "  兩次輸入的密碼必須一致  ");
}

// HYTransfer .swift
public class LanHYTransfer {

    static let HYWallet_NameMain = NSLocalizedString("LanHYTransfer_HYWallet_NameMain", value: "中心錢包", comment: "中心錢包");
    static let HYWallet_NameA = NSLocalizedString("LanHYTransfer_HYWallet_NameA", value: "美洲體育", comment: "美洲體育");
    static let HYWallet_NameB = NSLocalizedString("LanHYTransfer_HYWallet_NameB", value: "歐洲體育", comment: "歐洲體育");
    static let HYWallet_NameC = NSLocalizedString("LanHYTransfer_HYWallet_NameC", value: "亞洲體育", comment: "亞洲體育");
    static let HYWallet_NameD = NSLocalizedString("LanHYTransfer_HYWallet_NameD", value: "AG真人", comment: "AG真人");
    static let HYWallet_NameE = NSLocalizedString("LanHYTransfer_HYWallet_NameE", value: "申博真人", comment: "申博真人");
    static let HYWallet_NameF = NSLocalizedString("LanHYTransfer_HYWallet_NameF", value: "BBIN", comment: "BBIN");
    static let HYWallet_NameG = NSLocalizedString("LanHYTransfer_HYWallet_NameG", value: "OPUS真人", comment: "OPUS真人");
    static let HYWallet_NameH = NSLocalizedString("LanHYTransfer_HYWallet_NameH", value: "eBet真人", comment: "eBet真人");
    static let HYWallet_NameI = NSLocalizedString("LanHYTransfer_HYWallet_NameI", value: "OG真人", comment: "OG真人");
    static let HYWallet_NameJ = NSLocalizedString("LanHYTransfer_HYWallet_NameJ", value: "PT電遊", comment: "PT電遊");
    static let HYWallet_NameK = NSLocalizedString("LanHYTransfer_HYWallet_NameK", value: "MG電遊", comment: "MG電遊");
    static let HYWallet_NameL = NSLocalizedString("LanHYTransfer_HYWallet_NameL", value: "OPE電遊", comment: "OPE電遊");
    static let HYWallet_NameM = NSLocalizedString("LanHYTransfer_HYWallet_NameM", value: "BNG電遊", comment: "BNG電遊");
    static let HYWallet_NameN = NSLocalizedString("LanHYTransfer_HYWallet_NameN", value: "LiB彩票", comment: "LiB彩票");

}

// HYRecord .swift
public class LanHYRecord {

    public class LanHYDepositRecord {
        
        static let mapDepositTypeA = NSLocalizedString("LanHYDepositRecord_mapDepositTypeA", value: "微信支付", comment: "微信支付");
        static let mapDepositTypeB = NSLocalizedString("LanHYDepositRecord_mapDepositTypeB", value: "在線充值", comment: "在線充值");
        static let mapDepositTypeC = NSLocalizedString("LanHYDepositRecord_mapDepositTypeC", value: "支付寶", comment: "支付寶");
        static let mapDepositTypeD = NSLocalizedString("LanHYDepositRecord_mapDepositTypeD", value: "轉账", comment: "轉账");
    }
    
    public class LanHYWithdrawalRecord {
    
        static let statusTitleA = NSLocalizedString("LanHYWithdrawalRecord_statusTitleA", value: "審核中", comment: "審核中");
        static let statusTitleB = NSLocalizedString("LanHYWithdrawalRecord_statusTitleB", value: "出款中", comment: "出款中");
        static let statusTitleC = NSLocalizedString("LanHYWithdrawalRecord_statusTitleC", value: "已結案", comment: "已結案");
        static let statusTitleD = NSLocalizedString("LanHYWithdrawalRecord_statusTitleD", value: "未能提款", comment: "未能提款");
        static let statusTitleE = NSLocalizedString("LanHYWithdrawalRecord_statusTitleE", value: "投注額不足", comment: "投注額不足");
        static let statusTitleF = NSLocalizedString("LanHYWithdrawalRecord_statusTitleF", value: "取消提款", comment: "取消提款");
        static let statusTitleG = NSLocalizedString("LanHYWithdrawalRecord_statusTitleG", value: "查詢中", comment: "查詢中");
        static let statusTitleH = NSLocalizedString("LanHYWithdrawalRecord_statusTitleH", value: "自動出款中", comment: "自動出款中");
        static let statusTitleI = NSLocalizedString("LanHYWithdrawalRecord_statusTitleI", value: "第三方出款中", comment: "第三方出款中");
        
    }
    
    public class LanBetSearchItem {
        
        static let PlatformA = NSLocalizedString("LanBetSearchItem_PlatformA", value: "188體育", comment: "188體育");
        static let PlatformB = NSLocalizedString("LanBetSearchItem_PlatformB", value: "OG真人", comment: "OG真人");
        static let PlatformC = NSLocalizedString("LanBetSearchItem_PlatformC", value: "OPUS真人", comment: "OPUS真人");
        static let PlatformD = NSLocalizedString("LanBetSearchItem_PlatformD", value: "BBIN視訊", comment: "BBIN視訊");
        static let PlatformE = NSLocalizedString("LanBetSearchItem_PlatformE", value: "AG真人", comment: "AG真人");
        static let PlatformF = NSLocalizedString("LanBetSearchItem_PlatformF", value: "歐博真人", comment: "歐博真人");
        static let PlatformG = NSLocalizedString("LanBetSearchItem_PlatformG", value: "eBet真人", comment: "eBet真人");
        static let PlatformH = NSLocalizedString("LanBetSearchItem_PlatformH", value: "MG電遊", comment: "MG電遊");
        static let PlatformI = NSLocalizedString("LanBetSearchItem_PlatformI", value: "PT電遊", comment: "PT電遊");
        static let PlatformJ = NSLocalizedString("LanBetSearchItem_PlatformJ", value: "OPE電遊", comment: "OPE電遊");
        static let PlatformK = NSLocalizedString("LanBetSearchItem_PlatformK", value: "BBIN彩票", comment: "BBIN彩票");
        static let PlatformL = NSLocalizedString("LanBetSearchItem_PlatformL", value: "LIB彩票", comment: "LiB彩票");
        
    }
    
}

// HYGames .swift
public class LanHYGames {
    
    static let Category_SPORT = NSLocalizedString("LanHYGames_Category_SPORT", value: "SPORT", comment: "SPORT");
    static let Category_Real = NSLocalizedString("LanHYGames_Category_Real", value: "真人遊戲", comment: "真人遊戲");
    static let Category_Slot = NSLocalizedString("LanHYGames_Category_Slot", value: "電子遊戲", comment: "電子遊戲");
    static let Category_Lottery = NSLocalizedString("LanHYGames_Category_Lottery", value: "彩票遊戲", comment: "彩票遊戲");
    
    static let NSError_NoOpenUrl = NSLocalizedString("LanHYGames_NSError_NoOpenUrl", value: "無法開啟URL", comment: "無法開啟URL");
    
}


































