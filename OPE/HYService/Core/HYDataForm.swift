//
//  HYDataForm.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/7.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

struct HYRegex {
    let regex: NSRegularExpression?
    
    init(pattern: String) {
        regex = try? NSRegularExpression(pattern: pattern,
                                         options: .caseInsensitive)
    }
    
    func match(input: String) -> Bool {
        if let matches = regex?.matches(in: input, options: [], range: NSRange(location: 0, length: input.count)){
            return matches.count > 0
        } else {
            return false
        }
    }
    
    static let AmountInt = HYRegex(pattern: "^[0-9]*$")
    static let Amount = HYRegex(pattern: "^[0-9]+(.[0-9]{0,2})?$")
    static let UID = HYRegex(pattern: "^(?=.*\\d)(?=.*[a-zA-Z]).{6,12}$")
    static let Password = HYRegex(pattern: "^(?=.*\\d)(?=.*[a-zA-Z]).{6,12}$")
    static let Email = HYRegex(pattern: "^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$")
    static let Phone = HYRegex(pattern: "^$|^0?(13[0-9]|15[0123456789]|18[0123456789]|14[57]|17[0123456789])[0-9]{8}$")
    static let QQ = HYRegex(pattern: "^0?[0-9]{5,13}$")
    static let BankAccount = HYRegex(pattern: "^(?=.*\\d).{5,20}$")
    static let UserName = HYRegex(pattern: "^[\\u4e00-\\u9fa50-9a-zA-Z-·.]{2,30}$")
}

public protocol HYOption {
    var id : Int {get}
    var name : String {get}
}

public class HYDatas: NSObject {
    static var provinces: [HYProvince] = []
    static var translate: [String: String] = [:]
//    static let translateWalletName: [
//    
//    
//    ]
    static func translateWord(origin: String?) -> String{
        if let key = origin, let new = HYDatas.translate[key]{
            return new
        }else{
            return origin ?? ""
        }
    }
    
    static var processStatus: [Int: [Int: String]] = [:]
    
    static var mailList: [String: String] = [:] //放置轉跳對應信箱的網址
}

class InfoItem{
    
    enum `Type` {
        case optionTable([HYOption])
    }
    
    var key: String?
    var title : String
    var info : String?
    var type: Type
    var isEditable: Bool
    var options : [[HYOption]] = []
    var index: Int?
    var date : Date?
    var dateMode : UIDatePickerMode = .date
    var dateFormate : String = DateFormat.yearDate
    
    init(title: String, info: String?, type: Type, isEditable: Bool = true, key: String? = nil) {
        self.title = title
        self.info = info
        self.type = type
        self.isEditable = isEditable
        self.key = key
    }
    
    var selectedOption : HYOption? {
        if let idx = index, idx < options[0].count{
            return options[0][idx]
        }
        return nil
    }
}

public struct HYProvince: HYOption {
    
    init(dictionary : NSDictionary) {
        self.dictionary = dictionary
    }
    
    var dictionary: NSDictionary
    
    public var id: Int{
        return dictionary["ProvinceID"] as? Int ?? -1
    }
    public var name: String{
        return dictionary["ProvinceName"] as? String ?? ""
    }
}

struct HYBankOption: HYOption {
    
    let dictionary: NSDictionary
    
    init(withDictionary dictionary: NSDictionary) {
        self.dictionary = dictionary
    }
    
    var id: Int{
        return dictionary["BankID"] as! Int
    }
    
    var name: String{
        return dictionary["BankName"] as? String ?? "NoData"
    }
    
    var Ename: String{
        return dictionary["BankEName"] as? String ?? "NoData"
    }
}

class HYProcessStatus {
    
    let dictionary: NSDictionary
    
    init(withDictionary dictionary: NSDictionary) {
        self.dictionary = dictionary
    }
    
    var status: Int{
        return dictionary["Status"] as! Int
    }
    
    var processStatus: Int{
        return dictionary["ProcessStatus"] as! Int
    }

    var name: String{
        return dictionary["StatusName"] as! String
    }
    
    var next: Int{
        return dictionary["NextStatus"] as! Int
    }

}

//OPE API大部份基本回傳判斷格式用
struct HYGeneralItem: Codable {
    
    var Guid:String?
    
    var Success:Bool?
    
    var Code:String?
    
    var Message:String?
    
    var Data: [dataItem];
    
    struct dataItem: Codable {
        var Result: Int?
    }
    
}









