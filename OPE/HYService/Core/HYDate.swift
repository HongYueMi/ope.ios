//
//  HYDate.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/6/7.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import Foundation

class DateFormat {
    static var yearDate : String { return "yyyy / MM / dd"}
    static var date : String { return "MM / dd"}
    static var time : String { return "HH : mm : ss"}
    static var dateTime : String { return "yyyy / MM / dd HH : mm : ss"}
}

extension DateFormatter{
    convenience init(dateFormat : String) {
        self.init()
        self.dateFormat = dateFormat
    }
    
}

extension Date{
    func toString(format : String = DateFormat.yearDate) -> String{
        return DateFormatter(dateFormat: format).string(from: self)
    }
}

//MARK : HY

extension DateFormatter{
    
    static let fromHY : DateFormatter = {
        let f = DateFormatter()
        f.dateFormat = HYDateFormat.from
        return f
    }()
    
    static let toHY : DateFormatter = {
        let f = DateFormatter()
        f.dateFormat = HYDateFormat.to
        return f
    }()
}

class HYDateFormat {
    static var to : String { return "yyyy-MM-dd HH:mm:ss"}
    static var from : String { return "yyyy-MM-dd'T'HH:mm:ss"}
}









