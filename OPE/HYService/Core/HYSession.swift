//
//  HYSession.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/5/23.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import Alamofire

extension String{
    var notEmptyValue : String?{
        if self == ""{
            return nil
        }
        return self
    }
}

typealias JSONEncode = JSONEncoding

class HYSession: Alamofire.SessionManager {
    
    static var member: HYSession = {
        let config = URLSessionConfiguration.default;
        config.timeoutIntervalForRequest = 30; //秒 讓Alamofire抓資料的時可以接受延遲的時間
        config.urlCache = nil; //get 不使用暫存
        
        return HYSession(configuration: config);
    }();
    
    static var app : HYSession = {
        let config = URLSessionConfiguration.default;
        config.timeoutIntervalForRequest = 30; //秒 讓Alamofire抓資料的時可以接受延遲的時間
        config.urlCache = nil; //get 不使用暫存
        
        return HYSession(configuration: config);
    }();
    
    public func authRequest(
        _ url: URLConvertible,
        method: HTTPMethod = .post,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil)
        -> DataRequest
    {
        var newHeaders = headers ?? [:]
        newHeaders["DeviceType"] = "iOS"
        newHeaders["Authorization"] = "Bearer " + (HYAuth.auth.currentUser?.token?.access ?? "No token")
        return super.request(url, method: method, parameters: parameters, encoding: encoding, headers: newHeaders)
    }
    
    override open func request(
        _ url: URLConvertible,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil)
        -> DataRequest
    {
        var newHeaders = headers ?? [:]
        newHeaders["DeviceType"] = "iOS"
        return super.request(url, method: method, parameters: parameters, encoding: encoding, headers: newHeaders)
    }
    
    public func postJson(url: URLConvertible, parameters: Parameters) -> DataRequest{
        return request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
    }
    
    public func postURLEncode(url: URLConvertible, parameters: Parameters) -> DataRequest{
        return request(url, method: .post, parameters: parameters)
    }
    
}

extension DataRequest{
    
    public func responseHYJson(
        queue: DispatchQueue? = nil,
        options: JSONSerialization.ReadingOptions = .allowFragments,
        completionHandler: @escaping (HYResponse) -> Void)
    {
        responseJSON(queue: queue, options: options){response in
            
            print("\nAlamofire to path: \n\(response.request?.url?.absoluteString ?? "no Path")")
            let res = HYResponse(afResponse: response)
            if response.result.isSuccess{
                debugPrint(res.source)
                print("Message = \(res.source["Message"] as? String ?? "")")
            }
            else{
                debugPrint(res.error ?? "No Error Object")
            }
            completionHandler(res)
        }
    }
    
}


let HongYueErrorDomain = "com.HongYue.HYService.ErrorDomain"

public struct HYResponse {
    
    init(afResponse : DataResponse<Any>) {
        self.afResponse = afResponse
    }
    
    var afResponse : DataResponse<Any>
    
    var source : NSDictionary!{
        return afResponse.result.value as? NSDictionary
    }
    
    var guid : String?{
        return source?["Guid"] as? String
    }
    
    var isSuccess : Bool{
        return source?["Success"] as? Bool ?? false
    }
    
    var error : Error?{
        if let err = afResponse.result.error{
            return err
        }
        else if !isSuccess{
            return NSError(domain: HongYueErrorDomain, code: Int(code ?? "0") ?? 0, userInfo: [NSLocalizedDescriptionKey : message ?? ""])
        }
        return nil
    }
    
    var code : String?{
        return source?["Code"] as? String
    }
    var message : String?{
        return source?["Message"] as? String
    }
    var dicData : NSDictionary?{
        return source?["Data"] as? NSDictionary
    }
    var aryData : [NSDictionary]?{
        return source?["Data"] as? [NSDictionary]
    }
    var extData : NSDictionary?{
        return source?["ExtensionData"] as? NSDictionary
    }
    var pagination : HYPageFlag{
        return HYPageFlag(json:source?["Pagination"] as? NSDictionary)
    }
}

struct HYPageFlag {
    
    var pageIndex, pageSize, totalCount, pageCount : Int
    
    init(json: NSDictionary?) {
        pageIndex = json?["PageIndex"] as? Int ?? 0
        pageSize = json?["PageSize"] as? Int ?? 0
        totalCount = json?["TotalCount"] as? Int ?? 0
        pageCount = json?["pageCount"] as? Int ?? 0
    }
}

