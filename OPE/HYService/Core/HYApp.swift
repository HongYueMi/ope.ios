//
//  HYApp.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/12/15.
//  Copyright © 2017年 Lavend K. Mi All rights reserved.
//

import UIKit

public struct HYAppOptions{
    static let RD = HYAppOptions(api: "http://api.ope.rd.biz", resource: "http://cdn.ope.rd.biz")
    static let Demostration = HYAppOptions(api: "http://newmgt.api.ssope.com", resource: "http://rd.cdn.ssope.com")
    static let Online = HYAppOptions(api: "https://api.ope88.com", resource: "https://cncdn.rayvis.net")
    
    init(api: String, resource: String) {
        self.api = URL(string: api)!
        self.resource = URL(string: resource)!
    }
    
    let api: URL
    let resource: URL
    
    let customerService: URL = URL(string: "https://v88.live800.com/live800/chatClient/chatbox.jsp?companyID=588813&configID=3456&jid=4899767313&s=1")!
    let promotionKey: String = ""
}

let URL_HOST = HYApp.app.options.api.absoluteString

/*
 HYApp表示HongYue服務，App端的設定集合
 */
public class HYApp: NSObject {
    
    // MARK: - Singleton
    
    // 可以有多重服務。
    public static var allApps : [String : HYApp] = [
//                HYApp.defaultName: HYApp(name: HYApp.defaultName, options: HYAppOptions.RD)
        HYApp.defaultName: HYApp(name: HYApp.defaultName, options: HYAppOptions.Demostration)
        //        HYApp.defaultName: HYApp(name: HYApp.defaultName, options: HYAppOptions.Online)
    ]
    
    // 預設
    private static var defaultName: String = "__HYAPP_DEFAULT"
    
    public static var app : HYApp{
        return allApps[HYApp.defaultName]!
    }
    
    // 特定App
    public static func app(name: String) -> HYApp?{
        return allApps[name]
    }
    
    public static func addApp(name: String, options: HYAppOptions) -> Bool{
        if nil != HYApp.allApps[name]{
            return false
        }
        HYApp.allApps[name] = HYApp(name: name, options: options)
        return true
    }
    
    
    // MARK: - Init
    
    public let name: String
    public var options : HYAppOptions
    
    private init(name: String, options: HYAppOptions) {
        self.name = name
        self.options = options
    }
    
    
    //MARK: - State
    
    static let NotificationNameStateChange: Notification.Name = Notification.Name(rawValue: "HYApp.State")
    
    public enum State {
        case notConfigure // 剛初始化，還未與服務取得連線確認
        case configuring // 組態中
        case internetError(Error) // 網路問題
        case maintenance // 維護中
        case IPLimited // 該IP受限制
        case versionNotAvailable(Bool, URL) // 版本過低
        case prepared // 組態完成，可供使用
    }
    
    var state: State = .notConfigure{
        didSet{
            print("To Send Notification")
            NotificationCenter.default.post(name: HYApp.NotificationNameStateChange, object: self, userInfo: nil)
        }
    }
    
    //MARK: - Configure
    
    public func configure(){
        state = .configuring
        checkHost()
        self.loadAppSharedData()

//        checkVersionAvailable{ [weak self] in
//            self?.checkAppStatus{
//                self?.startcCheckAppStatus()
//                self?.loadAppSharedData()
//            }
//        }
    }
    
    // TODO: 動態更換host
    private func checkHost(){
        
    }
    
    // MARK: - 檢查版本
    
    private func checkVersionAvailable(next: @escaping ()-> Void){
        HYSession.app.request(options.api.appendingPathComponent("/API/AppVersion/GetAsync"), method: .post).responseHYJson{ [weak self] response in
            
            if let e = response.error{
                self?.state = State.internetError(e)
                return
            }
            
            let a = response.aryData?.first{
                if let os = $0["OS"] as? String, os == "iOS"{
                    return true
                }else{
                    return false
                }
            }
            if
                let limit = a as? [String: Any]
                , let latestVersion = limit["VersionNumber"] as? String
            {
                if getDeviceSystemInfo.SingletonDSI().version >= latestVersion {
                    print("----版本正常----");
                    next()
                    return
                }
                else if let str = limit["Url"] as? String, let url = URL(string: str){
                    if let needUpgrade = limit["IsUpgrade"] as? Bool, !needUpgrade{
                        print("----舊版本(可選更新)----");
                        self?.state = .versionNotAvailable(false, url)
                        return
                    }else{
                        print("----舊版本(強制更新)----");
                        self?.state = .versionNotAvailable(true, url)
                        return
                    }
                }
            }
            self?.state = .maintenance
        }
    }
    
    // MARK: - 檢查維護和IP Maintenance And IP Check
    
    private weak var timer: Timer?
    
    @objc private func startcCheckAppStatus(){
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timerCallCheckAppStatus), userInfo: nil, repeats: true)
    }
    
    @objc private func timerCallCheckAppStatus(){
        checkAppStatus {
            //
        }
    }
    
    @objc private func checkAppStatus(next: @escaping ()-> Void){
        
        HYSession.app.request(URL_HOST + "/API/CountryLimit/CheckAsync", method: .post)
            .validate()
            .responseHYJson{[weak self] response in
                
                if let e = response.error{
                    self?.state = .internetError(e)
                    self?.timer?.invalidate()
                }else if let isAllow = response.dicData?["IsAllow"] as? Bool, isAllow{
                    next()
                }else{
                    self?.timer?.invalidate()
                    self?.state = .maintenance
                }
        }
    }
    
    // MARK: - 公用資訊
    
    private var isTranslationMapPrepared = false
    private var isProvinceListLoaded = false
    
    private func checkIfAllSharedDataLoaded(){
        if isTranslationMapPrepared, isProvinceListLoaded{
            state = .prepared
            print("HYApp 確認完成，可供使用！")
        }
    }
    
    private func loadAppSharedData(){
        getProvinceList()
        getTranslation()
    }
    
    // MARK: - 省份列表
    
    private func getProvinceList(){
        HYSession.app.request(URL_HOST + "/API/Province/GetAsync?countryID=-1").validate().responseHYJson{[weak self] response in
            if let e = response.error{
                self?.state = .internetError(e)
            }
            else if let ary = response.aryData {
                HYDatas.provinces = ary.map{HYProvince(dictionary: $0)}
                self?.isProvinceListLoaded = true
                self?.checkIfAllSharedDataLoaded()
            }
            else{
                self?.state = .maintenance
            }
        }
    }
    
    // MARK: - 資源包
    
    private func getTranslation(){
        let url = options.resource.appendingPathComponent("/Mobile/Resource/Translation.json")
        HYSession.app.request(url).validate().responseJSON{[weak self] response in
            print("資源包: \(url)")
            
            guard self != nil else {return}
            
            if let e = response.error{
                self!.state = .internetError(e)
                return
            }
            if let json = response.value as? NSDictionary{
                
                self!.transJSON(json: json)
                
                if let banks = json["Bank"] as? NSDictionary{
                    self!.transJSON(json: banks)
                }
                
                if let statuses = json["ProcessTypeStatus"] as? [[String: Any]]{
                    
                    var processStatus: [Int: [Int: String]] = [:]
                    for status in statuses{
                        if
                            let type = status["ProcessType"] as? Int
                            , let statusCode = status["Status"] as? Int
                            , let name = status["StatusName"] as? String
                        {
                            
                            if nil == processStatus[type]{
                                processStatus[type] = [:]
                            }
                            processStatus[type]![statusCode] = name
                        }
                    }
                    HYDatas.processStatus = processStatus
                }
                
                if let banks = json["ProcessStatus"] as? NSDictionary{
                    self!.transJSON(json: banks)
                }
                if let promotions = json["Promotion"] as? NSDictionary{
                    self!.transJSON(json: promotions)
                }
                if let preferentialType = json["PreferentialType"] as? NSDictionary{
                    self!.transJSON(json: preferentialType)
                }
                if let other = json["Other"] as? NSDictionary{
                    self!.transJSON(json: other)
                }
                
                if let EmailList = json["EmailLinkList"] as? NSArray{
                    for value in EmailList {
                        if let mailKey = value as? NSDictionary{
                            self!.transMailJSON(json: mailKey)
                        }
                    }
                    
                }
                
            }
            self?.isTranslationMapPrepared = true
            self?.checkIfAllSharedDataLoaded()
        }
    }
    
    //用來處理responseString
    func dataToJSON(_ data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    
    var transKey = "zhCN"
    
    func transJSON(json: NSDictionary){
        for (k, v) in json{
            if
                let key = k as? String
                , let trans = v as? NSDictionary
                , let word = trans[transKey] as? String
            {
                HYDatas.translate[key] = word
            }
        }
    }
    
    func transMailJSON(json: NSDictionary){
        if let mailK:String = json["Key"] as? String , let mailV:String = json["Value"] as? String {
            HYDatas.mailList[mailK] = mailV;
        }
    }
    
}
