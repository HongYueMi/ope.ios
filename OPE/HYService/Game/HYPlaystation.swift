//
//  HYPlaystation.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/7/26.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

protocol HYPlayable {
    var category: String{get}
    var platform: String{get}
    var type: String?{get}
    var title: String{get}
    var code: String{get}
    var needSignIn: Bool{get}
    var playURL: URL?{set get}
}

class HYPlayStation: NSObject{
    
    static let shared = HYPlayStation()
    
    struct Category{
        static let Sport = "Sport"
        static let Electronic = "E"
        static let Real = "Live"
        static let Slot = "Electronic"
        static let Lottery = "Lottery"
        static let all: [String] = [
            Sport, Electronic, Real, Slot, Lottery
//            Sport, Real, Slot, Lottery
        ]
    }
    
    struct Platform{ // 對應到錢包

        static let OPUS_SPORT = "opussb"
        static let BETCONSTRUCT = "agp"
        static let SB = "saba"
        
        static let OPE_ELECTRONIC = "gresport"
        
        static let AG = "ag"
        static let SUMBET = "imsunbet" // 申博
        static let BBIN = "bbin"
        static let OPUS = "opuscasino"
        static let EBET = "ebet"
        static let OG = "og"
        
        static let PT = "pt2"
        static let MG = "mg"
        static let OPE = "imttg"
        static let BNG = "bng"          //暫時不開放

        static let LiB = "lb"
        
        static let category: [String: [String]] = [
            Category.Sport: [Platform.OPUS_SPORT, Platform.BETCONSTRUCT, Platform.SB]
            , Category.Electronic: [Platform.OPE_ELECTRONIC]
            , Category.Real: [Platform.AG, Platform.SUMBET, Platform.BBIN, Platform.OPUS, Platform.EBET, Platform.OG]
            , Category.Slot: [Platform.PT, Platform.MG, Platform.OPE]   //暫時不開放
//            , Category.Slot: [Platform.PT, Platform.MG, Platform.OPE, Platform.BNG]
            , Category.Lottery: [Platform.BBIN, Platform.LiB]
        ]
        
        static let all: [String] = [
            Platform.OPUS_SPORT, Platform.BETCONSTRUCT, Platform.SB
            , Platform.OPE_ELECTRONIC
            , Platform.AG, Platform.SUMBET, Platform.BBIN, Platform.OPUS, Platform.EBET, Platform.OG
            , Platform.PT, Platform.MG, Platform.OPE                    //暫時不開放
//            , Platform.PT, Platform.MG, Platform.OPE, Platform.BNG
            , Platform.LiB
        ]
        
    }
    
    // MARK: -
    
    func getGameList(playform: String, complete:@escaping ([Game]?, Error?)->Void){
        
        let parameters: [String: Any] = [
            "Platform": playform
            , "IsMobile": true
        ]
        
        HYSession.member.authRequest(URL_HOST + "/API/Game/GameListAsync", method: .get, parameters: parameters).validate().responseHYJson{ response in
            
            // 改掉
            func genGame(dictionary: NSDictionary) -> Game{
                if playform == Platform.PT{
                    return PTGame(dictionary: dictionary, platform: playform, category: Category.Slot)
                }
                if playform == Platform.MG{
                    return MGGame(dictionary: dictionary, platform: playform, category: Category.Slot)
                }
                if playform == Platform.OPE{
                    return OPEGame(dictionary: dictionary, platform: playform, category: Category.Slot)
                }
                if playform == Platform.BNG{
                    return BNGGame(dictionary: dictionary, platform: playform, category: Category.Slot)
                }
                return Game(dictionary: dictionary, platform: playform, category: Category.Slot)
            }
            
            var ary = response.dicData?["GameList"] as? [NSDictionary]
            if ary == nil, let jsonString = response.source?["Data"] as? String{
                let data = jsonString.data(using: String.Encoding.utf8)!
                ary = try! JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary]
                debugPrint(ary ?? [])
            }
            
            let games = ary?
                .map{genGame(dictionary: $0)}
                .filter{$0.isAvalable}
            print("原本Game數量:\(ary?.count ?? 0) 刪減後:\(games?.count ?? 0)")
            complete(games, response.error)
            
        }
    }
    
    class Game: HYPlayable {
       
        let category: String
        let platform: String
        var type: String?
        let dictionary: NSDictionary
        
        let needSignIn: Bool = true
        var playURL: URL?
        var title: String{
            return dictionary["GameNameCN"] as? String ?? dictionary["GameEnName"] as? String ?? ""
        }
        var code: String {
            return ""
        }
        var typeFromSuppier: String?{
            return nil
        }
        var isPrograssive: Bool{
            return false
        }
        var imageName: String?{
            return nil
        }
        var image: UIImage?{
            return nil
        }
        var isAvalable: Bool{
            return nil != imageName
        }

        required init(dictionary: NSDictionary, platform: String, category: String) {
            self.category = category
            self.platform = platform
            self.type = platform == Platform.MG ? "Electronic" : nil
            self.dictionary = dictionary
        }
    }
    
    final class PTGame: Game {
        
        override var code: String {
            return dictionary["MobileGameCode"] as? String ?? ""
        }
        override var typeFromSuppier: String?{
            return dictionary["GameType"] as? String
        }
        override var imageName: String?{
            if let code = code.notEmptyValue{
                return "\(code).png"
            }
            return nil
        }
        override var isPrograssive: Bool{
            return dictionary["HasProgressive"] as? Bool ?? false
        }
    }
    
    final class MGGame: Game {
        
        override var code: String {
            return dictionary["GameName"] as? String ?? ""
        }
        override var typeFromSuppier: String?{
            return dictionary["Category"] as? String
        }
        override var imageName: String?{
            return dictionary["ImageFileName"] as? String
        }
    }
    
    final class OPEGame: Game {
        
        override var code: String {
            return dictionary["MobileGameCode"] as? String ?? ""
        }
        override var typeFromSuppier: String?{
            return dictionary["Filter"] as? String
        }
        override var imageName: String?{
            if(code == " imgame12133m"){ //處理猴子先生的圖檔問題
                return "12133.jpg"
            }else{
                if code.count >= 11{
                    let startIndex = code.index(code.startIndex, offsetBy: 6)
                    let endIndex = code.index(startIndex, offsetBy: 5)
                    return "\(code[startIndex..<endIndex]).jpg"
                }
            }
            return nil
        }
        
        override var isAvalable: Bool{
            if
                let _ = imageName
                , let isSupport = dictionary["SupportMobile"] as? String
                , isSupport == "Yes"
            {
                return true
            }
            return false
        }
        
    
    }
    
    final class BNGGame: Game {
        
        override var code: String {
            return dictionary["GameCode"] as? String ?? ""
        }
        override var typeFromSuppier: String?{
            return dictionary["GameType"] as? String
        }
        override var imageName: String?{
            if let code = code.notEmptyValue{
                return "\(code).png"
            }
            return nil
        }
        override var title: String{
            return dictionary["GameName"] as? String ?? ""
        }
    }
    
    static func timesPlayed(completion:@escaping ([String: Int], Error?)-> Void){
            
        let parameters: [String: Any] = [
            "Platform": Platform.MG
            , "IsMobile": true
        ]
        
        HYSession.member.authRequest(URL_HOST + "/API/Game/GameListAsync", method: .get, parameters: parameters).validate().responseHYJson{ response in
            
            var times: [String: Int] = [:]
            if let aryTimes = response.extData?["ExtensionData"] as? [NSDictionary]{
                for time in aryTimes{
                    if let key = time["PlatformName"] as? String
                        , let value = time["Times"] as? Int{
                        times[key] = value
                    }
                }
            }
            completion(times, response.error)
        }
    }
    
    static func jackpodinfo(completion:@escaping (Double)-> Void){
        
        HYSession.member.authRequest(URL_HOST + "/API/Game/JackpotAsync?Platform=pt2", method: .get).validate().responseHYJson{ response in
            
            if let dicJackpot = response.dicData?["JackpotList"] as? NSDictionary{
                if let amount = dicJackpot["Amount"] as? Double{
                    completion(amount)
                    return
                }
            }
            completion(0)
        }
    }
    
    struct SportMessage{
        
        var date : Date?
        var teamHomeTitle: String?
        var urlImageTeamHome: URL?
        var teamAwayTitle: String?
        var urlImageTeamAway: URL?
        
        init(dictionary: NSDictionary) {
            if var strDate = dictionary["GameDate"] as? String{
                if strDate.count > 19{
                    let idx = strDate.index(strDate.startIndex, offsetBy: 19)
                    strDate = String(strDate[..<idx])
                }
                self.date = DateFormatter.fromHY.date(from: strDate)
            }
            if let strTitle = dictionary["Title"] as? String{
                let aryWord = strTitle.components(separatedBy: CharacterSet(charactersIn: "VSvs"))
                if aryWord.count >= 3{
                    teamHomeTitle = aryWord[0]
                    teamAwayTitle = aryWord[2]
                }
            }
            if let strURL = dictionary["TeamOnePath"] as? String, let url = URL(string: strURL){
                urlImageTeamHome = url
            }
            if let strURL = dictionary["TeamTwoPath"] as? String, let url = URL(string: strURL){
                urlImageTeamAway = url
            }
        }

    }
    
    static func fetchSportInfo(completion:@escaping ([SportMessage])-> Void){
        
        let parameters: [String: Any] = [
            "IsHomePage": 1
            , "Pagination": Pagination(index: 1, size: 100).toJSON()
        ]
        
        HYSession.member.authRequest(URL_HOST + "/Api/SportMessage/ListAsync", method: .post, parameters: parameters, encoding: JSONEncode.default).validate().responseHYJson{ response in
            
            let ary = response.aryData?.map{SportMessage(dictionary: $0)} ?? []
            completion(ary)
        }
    }
    
    static func checkGameAvalibility(_ game: HYPlayable, answer: @escaping (Bool, Error?) -> ()){
        
        
        HYSession.member.authRequest(URL_HOST + "/API/Game/ListAsync").validate().responseHYJson{ response in
            
            var isAvalable = false
            
            if
                response.isSuccess
                , let playforms = response.aryData
            {
                for playform in playforms{
                    if
                        let code = playform["PlatformEName"] as? String
                        , code.lowercased() == game.platform
                        ,let isEnable = playform["IsEnabled"] as? Bool
                        , isEnable
                    {
                        isAvalable = true
                        break
                    }
                }
            }
            
            defer{
                answer(isAvalable, response.error)
            }
        }
        
    }
    
    static func prepareURLforGame(_ game: HYPlayable, complete: @escaping (HYPlayable, Error?)->Void){
        
        if HYAuth.auth.currentUser == nil{
            prepareGameURLforGuest(game, complete: complete)
            return
        }
        
        let parameters: [String: Any] = [
            "GameName": game.platform
            , "OurGameType": game.category
            , "GameType": game.type ?? ""
            , "GameCode": game.code
            , "IsMobile": 1
        ]
        
        HYSession.member.authRequest(URL_HOST + "/API/Game/LoginAsync", parameters: parameters, encoding: JSONEncode.default).validate().responseHYJson{ response in
            var g = game
            if let err = response.error{
                complete(g, err)
            }
            else if game.platform == Platform.OPE_ELECTRONIC
                , let path = response.dicData?["Url"] as? String
                , let url = URL(string: path)
            {
                g.playURL = url
                complete(g, response.error)
            }
            else if let path = response.dicData?["Url"] as? String
                , let strURL = path.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                , let url = URL(string: strURL)
            {
                g.playURL = url
                complete(g, response.error)
            }
            else{
                let e = NSError(domain: HongYueErrorDomain, code: -1, userInfo: [NSLocalizedDescriptionKey : LanHYGames.NSError_NoOpenUrl])
                complete(g, e)
            }
        }
    }
    
    static func prepareGameURLforGuest(_ game: HYPlayable, complete: @escaping (HYPlayable, Error?)->Void){
        
        HYSession.member.authRequest(URL_HOST + "/API/Game/\(game.platform.uppercased())/GuestLoginAsync")
            .validate()
            .responseHYJson{ response in
                var g = game
                if let err = response.error{
                    complete(g, err)
                }
                else if
                    let path = response.dicData?["Url"] as? String
                    , let strURL = path.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    , let url = URL(string: strURL)
                {
                    g.playURL = url
                    complete(g, response.error)
                }
                else{
                    let e = NSError(domain: HongYueErrorDomain, code: -1, userInfo: [NSLocalizedDescriptionKey : LanHYGames.NSError_NoOpenUrl])
                    complete(g, e)
                }
        }
    }

}
