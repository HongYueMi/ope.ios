//
//  HYSportGameRecommend.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/12/22.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class HYSportGameRecommend {
    
    static let center = HYSportGameRecommend()
    
    class Item {
        
        var dictionary: NSDictionary
        
        init(dictionary: NSDictionary) {
            self.dictionary = dictionary
        }
        
        var id: Int{
            return dictionary["SID"] as? Int ?? -1
        }
        
        var title: String{
            return dictionary["Title"] as? String ?? ""
        }
        
        var titleType: Int{
            return dictionary["TitleType"] as? Int ?? 0
        }
        
        var info: String{
            return dictionary["Describe"] as? String ?? ""
        }
        
        var content: String{
            return dictionary["Content"] as? String ?? ""
        }

        var strGameDate: String?{
            return dictionary["GameDate"] as? String
        }
        
        var gameDate: Date?{
            guard var strDate = strGameDate else {
                return nil
            }
            if strDate.count > 19{
                let idx = strDate.index(strDate.startIndex, offsetBy: 19)
                strDate = String(strDate[..<idx])
            }
            return DateFormatter.fromHY.date(from: strDate)
        }
        
        static let formatString = "yyyy/MM/dd HH:mm"
        private var _dateShow: String?
        var dateShow: String{
            if nil == _dateShow{
                _dateShow = gameDate?.toString(format: Item.formatString) ?? ""
            }
            return _dateShow!
        }
        
        var linkPath: String?{
            return dictionary["Url"] as? String
        }
        
        var url: URL?{
            if let path = linkPath, !path.isEmpty{
                return URL(string: "http://" + path)
            }
            return nil
        }
        
    }
    
    func configure(completion: (()->Void)? = nil){
        self.reset(){
            completion?()
        }
    }
    
    // MARK: - Notification
    
    static let NotificationName: Notification.Name = Notification.Name(rawValue: "HYSportGameRecommend/ListUpdate")
    
    var name: Notification.Name{
        return HYSportGameRecommend.NotificationName
    }
    
    private func sendNotification(){
        NotificationCenter.default.post(name: name, object: self, userInfo: nil)
    }
    
    // MARK: - Data
    
    var recommends: [Item] = []
    
    private func reset(completion: @escaping ()->Void){
        recommends = []
        getList{ [weak self] records, error in
            self?.recommends = records
            self?.sendNotification()
            completion()
        }
    }
    
    func syncDatas(){
        getList{ [weak self] records, error in
            self?.recommends = records
            self?.sendNotification()
        }
    }
    
    private func getList(completion : @escaping ([Item], Error?)-> Void){
        let parameters:[String:Any] = ["IsHomePage": 0, "Pagination": Pagination(index: 1, size: 50).toJSON()]
        HYSession.member.postJson(url: URL_HOST + "/Api/SportMessage/ListAsync", parameters: parameters).validate().responseHYJson{ response in
            completion(response.aryData?.map{Item(dictionary: $0)} ?? [], response.error)
        }
    }
}
