//
//  HYMarket.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/12/6.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class HYMarket {
    
    static let center = HYMarket()
    
    class Item {
        
        var dictionary: NSDictionary
        
        init(dictionary: NSDictionary) {
            self.dictionary = dictionary
        }
        
        var id: Int{
            return dictionary["SID"] as? Int ?? -1
        }
        
        var title: String{
            return dictionary["Title"] as? String ?? ""
        }
        
        var imagePath: String?{
            return dictionary["CellPhonePath"] as? String
        }
        
        var strStartDate: String?{
            return dictionary["StartDate"] as? String
        }
        
        var strEndDate: String?{
            return dictionary["EndDate"] as? String
        }
        
        static let formateString = "yyyy年MM月dd日"
        private var _dateShow: String?
        var dateShow: String{
            if nil == _dateShow{
                let start = strStartDate ?? "即日起"
                let end = strEndDate ?? "待定"
                _dateShow = start + " 至 " + end
            }
            return _dateShow!
        }
        
        var deviceType: Int{
            return dictionary["DeviceType"] as? Int ?? -1
        }
        
        var url: URL?{
            if let path = linkPath, !path.isEmpty, actionType == 2{
                return URL(string: "http://" + path)
            }
            return nil
        }
        
        var linkPath: String?{
            return dictionary["Url"] as? String
        }
        
        var actionType: Int?{
            return dictionary["PictureType"] as? Int
        }
        
        var promotionTitle: String?{
            if actionType == 1{
                return linkPath
            }
            return nil
        }
        
    }
    
    func configure(completion: (()->Void)? = nil){
        self.reset(){
            completion?()
        }
    }
    
    // MARK: - Notification
    
    static let NotificationName: Notification.Name = Notification.Name(rawValue: "HYMarkey/ListUpdate")
    
    var name: Notification.Name{
        return HYMarket.NotificationName
    }
    
    private func sendNotification(){
        NotificationCenter.default.post(name: name, object: self, userInfo: nil)
    }
    
    // MARK: - Data
    
    var ads: [Item] = []
    
    private func reset(completion: @escaping ()->Void){
        ads = []
        getList{ [weak self] records, error in
            self?.ads = records
            self?.sendNotification()
            completion()
        }
    }
    
    func syncAds(){
        getList{ [weak self] records, error in
            self?.ads = records
            self?.sendNotification()
        }
    }
    
    private func getList(completion : @escaping ([Item], Error?)-> Void){
        let parameters:[String:Int] = ["UseType": 1, "DeviceType": 1];
        HYSession.member.postJson(url: URL_HOST + "/API/Market/ListAsync", parameters: parameters).validate().responseHYJson{ response in
            completion(response.aryData?.map{Item(dictionary: $0)} ?? [], response.error)
        }
    }
}
