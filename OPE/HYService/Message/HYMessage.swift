//
//  HYMessage.swift
//  OPE
//
//  Created by Lavend K. Mi on 2017/11/22.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import UIKit

class HYMessage: NSObject {
    
    struct Item {
        var dictionary: NSDictionary
        init(dictionary: NSDictionary) {
            self.dictionary = dictionary
        }
        
        var title: String?{
            return dictionary["Title"] as? String
        }
        
        var info: String?{
            return dictionary["Content"] as? String
        }
        
        var strDate: String?{
            if var strDate = dictionary["CreateDate"] as? String{
                if strDate.count > 19{
                    let idx = strDate.index(strDate.startIndex, offsetBy: 19)
                    strDate = String(strDate[..<idx])
                }
                return strDate
            }
            return nil
        }
        
        var date: Date?{
            if let strDate = strDate{
                return DateFormatter.fromHY.date(from: strDate)
            }
            return nil
        }
        
        var id: Int{
            return dictionary["InternalMailID"] as? Int ?? -1
        }
        
        var isRead: Bool{
            return dictionary["IsRead"] as? Bool ?? false
        }
    }
    
    static let shared: HYMessage = HYMessage()
    
    func configure(){
        _ = HYAuth.auth.addAuthStateDidChangeListener{ auth, user in
            print("Message HYAuth.auth.addAuthStateDidChangeListener")
            self.reset(withUser: user)
        }
    }
    
    func reset(withUser user: HYUser?){
        guard let _ = user else {
            unreadCount = nil
            messages = []
            return
        }
        updateData()
    }
    
    func updateData(){
        timer?.invalidate()
        getUnreadCount{ [weak self] count in
            guard self != nil else {return}
            self?.unreadCount = count
        }
        timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(autoUpdate), userInfo: nil, repeats: true)
    }
    
    // MARK: - UnreadCount
    
    var unreadCount: String?{
        didSet{
            updateMessage()
        }
    }
    
    static let NotificationName: Notification.Name = Notification.Name(rawValue: "HYMessage/UnreadCount")
    
    var name: Notification.Name{
        return HYMessage.NotificationName
    }
    
    private weak var timer: Timer?
    
    @objc private func autoUpdate(){
        getUnreadCount{ [weak self] count in
            guard self != nil else {return}
            if self!.unreadCount != count{
                self!.unreadCount = count
            }
        }
    }
    
    @objc private func getUnreadCount(completion: @escaping (String?) -> Void){
        HYSession.member.authRequest(URL_HOST + "/API/Message/UnreadCountAsync")
            .responseString{[weak self] response in
                guard self != nil else {return}
                completion(response.value?.trimmingCharacters(in: CharacterSet(charactersIn: "\"")))
        }
    }
    
    // Messages
    
    var messages: [Item] = []{
        didSet{
            NotificationCenter.default.post(name: name, object: self, userInfo: nil)
        }
    }
    
    func updateMessage(){
        print("Update Message ! 更新訊息")
        if HYAuth.auth.currentUser != nil{
            getMessage{message, error in
                HYMessage.shared.messages = message ?? []
            }
        }else{
            messages = []
        }
        
    }
    
    func getMessage(completion : @escaping ([Item]?, Error?)-> Void){
        let parameters: [String: Any] = [
            "Pagination": Pagination(index: 1, size: 100).toJSON()
        ]
        HYSession.member.authRequest(URL_HOST + "/API/Message/UnreadListAsync", parameters: parameters, encoding: JSONEncode.default).validate().responseHYJson{
            response in
            let ary = response.aryData?.map{Item(dictionary: $0)}
            completion(ary, response.error)
        }
    }
    
    func readMessage(sid: Int){
        let parameters: [String: Any] = ["InternalMailID": sid]
        HYSession.member.authRequest(URL_HOST + "/API/Message/ReadAsync", parameters: parameters, encoding: JSONEncode.default).validate().responseHYJson{
            [weak self] response in
            self?.updateData()
        }
    }
    
    func deleteMessage(sid: Int){
        let parameters: [String: Any] = ["InternalMailID": sid]
        HYSession.member.authRequest(URL_HOST + "/API/Message/DeleteAsync", parameters: parameters, encoding: JSONEncode.default).validate().responseHYJson{
            [weak self] response in
            self?.updateData()
        }
    }
    
    // 留言反饋
    func leaveAMessage(title: String, message: String, completion : @escaping (Error?)-> Void){
        let parameters: [String: Any] = [
            "Title": title
            , "Content": message
        ]
        HYSession.member.authRequest(URL_HOST + "/API/Message/AddAsync", parameters: parameters, encoding: JSONEncode.default).validate().responseHYJson{
            response in
            completion(response.error)
        }
    }
}

