//
//  HYBroadcast.swift
//  SportsGames
//
//  Created by Lavend K. Mi on 2017/4/11.
//  Copyright © 2017年 Hong Yue International, Inc. All rights reserved.
//

import Alamofire

class HYBroadcast: NSObject {
    
    struct Item {
        var id: Int!
        var title: String?
        var subtitle: String?
        var info: String?
        var imagePath: String?
        var link: String?
        var date: Date?
        var isRead: Bool!
        var sequence: Int?
        
        init(title: String? = nil, subtitle: String? = nil, info: String? = nil, imagePath: String? = nil, link: String? = nil, sequence: Int? = nil) {
            self.title = title
            self.subtitle = subtitle
            self.info = info
            self.imagePath = imagePath
            self.link = link
            self.sequence = sequence
        }
    }
    
    static let UserInfoKey = "HYBroadcast"
    
    var name: Notification.Name{
        return Notification.Name(rawValue: "")
    }
    var source: String{
        return ""
    }
    
    public var messages : [Item] = []{
        didSet{
            if messages.count != oldValue.count{
                self.sendNotification()
            }
            else{
                for (idx,message) in oldValue.enumerated(){
                    if message.title != messages[idx].title{
                        self.sendNotification()
                        break
                    }
                }
            }
        }
    }
    
    weak var timer : Timer!
    func startFetchBroadcast(){
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(_refreshMessage), userInfo: nil, repeats: true)
    }
    
    @objc func _refreshMessage(){
        refreshMessage{_ in }
    }
    
    func refreshMessage(complete: @escaping (Error?) -> ()){ //下載任務
        let parameters:[String:Int] = ["UseType": 1, "DeviceType": 2];
        request(source, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseHYJson{[weak self] response in
                guard self != nil else {return}
                var messages : [Item] = []
                if let ary = response.aryData, ary.count > 0{
                    messages = ary.map{
                        return self!.message(fromJson: $0)
                    }
                }
                self!.messages = messages
                complete(response.error)
        }
    }
    
    func message(fromJson json: NSDictionary) -> Item{
        return Item()
    }
    
    // Notification
   
    func sendNotification(){
        NotificationCenter.default.post(name: name, object: self, userInfo: [ HYBroadcast.UserInfoKey : self.messages])
    }
    
}

class HYMarquee: HYBroadcast {
    
    static let shared: HYBroadcast = HYMarquee()
    static let NotificationName: Notification.Name = Notification.Name(rawValue: "HYMarquee")
    
    override var name: Notification.Name{
        return HYMarquee.NotificationName
    }
    
    override var source: String{
        return URL_HOST + "/api/Marquee/ValidAsync"
    }
    
    override func message(fromJson json: NSDictionary) -> Item{
        return Item(title: json["Content"] as? String)
    }
}

class HYAdBanner: HYBroadcast { //廣告圖資
    
    static let shared: HYAdBanner = HYAdBanner()
    static let NotificationName: Notification.Name = Notification.Name(rawValue: "HYAdBanner")
    
    override var name: Notification.Name{
        return HYAdBanner.NotificationName
    }
    
    override var source: String{
        return URL_HOST + "/API/Market/ListAsync"
    }
    
    override func message(fromJson json: NSDictionary) -> Item{
        return Item(imagePath: json["CellPhonePath"] as? String, link: json["Url"] as? String, sequence: json["SID"] as? Int)
    }
}


